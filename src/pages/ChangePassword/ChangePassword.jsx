import Title from "../../components/Title";
import { useState } from "react";
import RedirectToHome from "../../components/RedirectPage/RedirectToHome";
import FormChangePassword from "../../components/ChangePassword/FormChangePassword";
const ChangePasswordComponent = () => {
  return (
    <div className="pageWithForm">
      <div className="form md:p-6 md:pb-12">
        <Title title="Đổi mật khẩu"></Title>

        <div className="mt-5 sm:mx-auto sm:w-full sm:max-w-sm">
          <FormChangePassword />
          <div className="flex flex-col justify-center items-center mt-5">
            <RedirectToHome />
          </div>
        </div>
      </div>
    </div>
  );
};
export default ChangePasswordComponent;
