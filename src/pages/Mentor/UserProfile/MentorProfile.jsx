import React, { useEffect, useState } from "react";
import mentorStore from "../../../store/mentorStore";
import BasicInformation from "../../../components/UserProfile/Mentor/BasicInformation";
import { toast } from "react-toastify";
import bgImg from "../../../assets/bg-img.png";
import LoadingState from "../../../components/LoadingState";

function MentorProfile() {
  const [mentor, setMentor] = useState();
  const { mentorProfileApi } = mentorStore();
  useEffect(() => {
    const fetchProfile = async () => {
      try {
        const userData = await mentorProfileApi();
        console.log("Mentor: ", userData);
        setMentor(userData);
      } catch (error) {
        toast.error("Không tìm thấy gia sư");
      }
    };
    fetchProfile();
  }, [mentorProfileApi]);

  return (
    <>
      {!mentor ? (
        <LoadingState />
      ) : (
        <div
          className="bg-gray-100 bg-cover bg-center h-80"
          style={{ backgroundImage: `url(${bgImg})` }}
        >
          <div className="container mx-auto py-8">
            <BasicInformation mentor={mentor} />
          </div>
        </div>
      )}
    </>
  );
}

export default MentorProfile;
