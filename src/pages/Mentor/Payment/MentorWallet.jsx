import React from "react";
import TitleHeader from "../../../components/TitleHeader";
import MentorPayment from "../../../components/Payment/MentorPayment";

function MentorWallet() {
  return (
    <div className="mx-20">
      <TitleHeader title="Ví của tôi" />
      <MentorPayment />
    </div>
  );
}

export default MentorWallet;
