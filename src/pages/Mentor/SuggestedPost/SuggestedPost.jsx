import React, { useEffect, useState } from "react";
import Pagination from "@mui/material/Pagination";
import mentorStore from "../../../store/mentorStore";
import MentorPostCard from "../../../components/Card/MentorPostCard";
import LoadingState from "../../../components/LoadingState";

const SuggestedPost = () => {
  const { fetchSuggestPost, listSuggestPost, isLoading } = mentorStore();
  const [currentPage, setCurrentPage] = useState(1);
  const pageSize = 4;

  useEffect(() => {
    fetchSuggestPost();
  }, []);

  const handlePageChange = (event, page) => {
    setCurrentPage(page);
  };

  const startIndex = (currentPage - 1) * pageSize;
  const paginatedPosts = listSuggestPost
    ? listSuggestPost.slice(startIndex, startIndex + pageSize)
    : [];

  return (
    <main className="mx-20">
      <h1 className="m-5 text-3xl flex justify-center font-bold text-textHeader pb-5 my-10">
        Gợi ý bài đăng cho bạn
      </h1>
      {isLoading ? (
        <LoadingState />
      ) : paginatedPosts.length > 0 ? (
        <div className="mx-5">
          <ul role="list" className="divide-y divide-gray-500">
            {paginatedPosts.map((post) => (
              <li key={post.postId}>
                <MentorPostCard post={post} />
              </li>
            ))}
          </ul>

          <Pagination
            count={Math.ceil(listSuggestPost.length / pageSize)}
            page={currentPage}
            onChange={handlePageChange}
            color="primary"
            className="flex flex-col items-center"
          />
        </div>
      ) : (
        <div className="text-3xl text-center mt-3">
          Hiện chúng tôi không có bài đăng nào cho bạn :(
        </div>
      )}
    </main>
  );
};

export default SuggestedPost;
