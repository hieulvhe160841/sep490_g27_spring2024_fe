import React from "react";
import FormRegisterCourse from "../../../components/RegisterCourse/FormRegisterCourse";
import TitleHeader from "./../../../components/TitleHeader";

function RegisterCourse() {
  return (
    <>
      <div className="flex justify-center items-center">
        <div className="form">
          <TitleHeader title="Thêm khóa học" />
          <FormRegisterCourse />
        </div>
      </div>
    </>
  );
}

export default RegisterCourse;
