import React, { useEffect, useState } from "react";
import Header from "../../../components/Header/Header";
import BasicInformation from "../../../components/MentorDetails/BasicInformation";
import DetailsInformation from "../../../components/MentorDetails/DetailsInformation";
import { useParams } from "react-router-dom";
import mentorStore from "../../../store/mentorStore";
import bgImg from "../../../assets/bg-img.png";
import LoadingState from "../../../components/LoadingState";
import Sidebar from "../../../components/Sidebar/AdminSidebar";
import MenteeSidebar from "../../../components/Sidebar/MenteeSidebar";
import MentorSidebar from "../../../components/Sidebar/MentorSidebar";
import EducationStaffSidebar from "../../../components/Sidebar/EducationStaffSidebar";
import TransactionStaffSidebar from "../../../components/Sidebar/TransactionStaffSidebar";
import userStore from "../../../store/userStore";
import FeedBackComponent from "./../../../components/MentorDetails/FeedBackComponent";

function MentorDetails() {
  const { username } = useParams();
  const [mentor, setMentor] = useState();
  const { fetchDetailMentor } = mentorStore();
  const role = localStorage.getItem("role");
  const menteeRole = "ROLE_MENTEE";
  const mentorRole = "ROLE_MENTOR";
  const eduStaffRole = "ROLE_EDUCATION_STAFF";
  const transStaffRole = "ROLE_TRANSACTION_STAFF";
  const adminRole = "ROLE_ADMIN";
  const { isOpen, setIsOpen } = userStore();

  useEffect(() => {
    const fetchMentor = async () => {
      try {
        const userData = await fetchDetailMentor(username);
        console.log("Mentor: ", userData);
        setMentor(userData);
      } catch (error) {
        console.error("Error fetching user:", error);
      }
    };
    fetchMentor();
  }, [fetchDetailMentor, username]);

  let SidebarComponent;
  let showSidebar = true;
  switch (role) {
    case menteeRole:
      SidebarComponent = MenteeSidebar;
      break;
    case mentorRole:
      SidebarComponent = MentorSidebar;
      break;
    case eduStaffRole:
      SidebarComponent = EducationStaffSidebar;
      break;
    case transStaffRole:
      SidebarComponent = TransactionStaffSidebar;
      break;
    case adminRole:
      SidebarComponent = Sidebar;
      break;
    default:
      SidebarComponent = () => <></>;
      showSidebar = false;
      break;
  }

  return (
    <>
      <Header />
      <div className={showSidebar ? "h-screen flex flex-row " : ""}>
        {console.log(mentor)}
        <SidebarComponent isOpen={isOpen} toggleSidebar={setIsOpen} />
        <div
          className={
            showSidebar
              ? `flex flex-col flex-1 ${isOpen ? "md:ml-52" : ""} ml-5`
              : ""
          }
        >
          {!mentor ? (
            <LoadingState />
          ) : (
            <div
              className={`bg-gray-100 bg-cover bg-center h-80 ${
                showSidebar ? "lg:ml-4" : ""
              } `}
              style={{ backgroundImage: `url(${bgImg})` }}
            >
              <div className="mx-auto py-8 ml-10 xl:mx-28">
                <div className="grid grid-cols-5 lg:grid-cols-12 gap-6 px-4 mt-20">
                  <div className="col-span-4 lg:col-span-4 relative ">
                    <BasicInformation username={username} mentor={mentor} />
                  </div>
                  <div className="col-span-4 lg:col-span-7">
                    <DetailsInformation mentor={mentor} />
                  </div>
                  <div className="col-span-4 lg:col-span-11 my-5">
                    <FeedBackComponent mentor={mentor} />
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </>
  );
}

export default MentorDetails;
