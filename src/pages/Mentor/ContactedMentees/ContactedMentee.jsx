import React from "react";
import ContactedMenteeData from "../../../components/ListData/ContactedMenteeData";
import TitleHeader from "../../../components/TitleHeader";

function ContactedMentee() {
  return (
    <>
      <div className="mx-20 md:mt-5">
        <TitleHeader title="Học viên đã hỗ trợ" />

        <ContactedMenteeData />
      </div>
    </>
  );
}

export default ContactedMentee;
