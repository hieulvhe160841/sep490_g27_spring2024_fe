import React from "react";
import TitleHeader from "../../../components/TitleHeader";
import ListRequest from "../../../components/ListCourseRequest/ListRequest";

function ListCourseRequest() {
  return (
    <div className="mx-20 md:mt-5">
      <TitleHeader title="Danh sách khóa học đã yêu cầu phê duyệt" />
      <ListRequest />
    </div>
  );
}

export default ListCourseRequest;
