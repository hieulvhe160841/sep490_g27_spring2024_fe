import React from "react";

import TitleHeader from "../../../components/TitleHeader";
import CourseListOfMentor from "../../../components/ListData/CourseListOfMentor";

function MyCourse() {
  return (
    <>
      <div className="mx-20 md:mt-5">
        <TitleHeader title="Các khóa học của tôi" />
        <CourseListOfMentor />
      </div>
    </>
  );
}

export default MyCourse;
