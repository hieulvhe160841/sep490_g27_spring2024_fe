import React, { useEffect } from "react";
import CourseDetailMentor from "../../../components/Course/CourseDetailMentor";
import { useParams } from "react-router-dom";
import mentorStore from "../../../store/mentorStore";
import TitleHeader from "../../../components/TitleHeader";
import bgImg from "../../../assets/bg-img.png";

function CourseDetail() {
  const { id } = useParams();
  const { viewCourseDetail, course, isLoading } = mentorStore();
  useEffect(() => {
    viewCourseDetail(id);
  }, []);
  return (
    <div className="relative">
      <div
        className="bg-gray-100 bg-cover bg-center h-80 "
        style={{ backgroundImage: `url(${bgImg})` }}
      ></div>
      <div className="lg:mx-20 md:mt-5 absolute -top-3 left-20">
        <div className="md:mx-auto md:w-full mt-3">
          <h2 className="text-center text-3xl font-bold leading-9 tracking-tight text-white">
            Chi tiết khóa học
          </h2>
        </div>
        <CourseDetailMentor course={course} />
      </div>
    </div>
  );
}

export default CourseDetail;
