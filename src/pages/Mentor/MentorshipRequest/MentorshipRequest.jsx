import React from "react";
import TitleHeader from "../../../components/TitleHeader";
import MentorshipRequestOfMentorData from "../../../components/ListData/MentorshipRequestOfMentorData";

function MentorshipRequest() {
  return (
    <>
      <div className="mx-20 my-5">
        <TitleHeader title="Yêu cầu gia sư" />

        <MentorshipRequestOfMentorData />
      </div>
    </>
  );
}

export default MentorshipRequest;
