import React from "react";
import { useParams } from "react-router-dom";
import Header from "../../../components/Header/Header";
import EditProfileForm from "../../../components/EditProfileMentor/EditProfileForm";

function EditProfile({ mentors }) {
  const { id } = useParams();
  const mentor = mentors.find((mentor) => mentor.id === parseInt(id));

  return (
    <div>
      <main>
        <Header />
        <h1 className="mb-5 text-xl flex justify-center font-bold">
          Chỉnh sửa hồ sơ
        </h1>
        <div className="w-full flex justify-center items-center">
          <EditProfileForm mentor={mentor} />
        </div>
      </main>
    </div>
  );
}

export default EditProfile;
