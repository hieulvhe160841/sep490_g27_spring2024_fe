import FormResetPassword from "../../components/ForgotPassword/Reset/FormResetPassword";
import Title from "../../components/Title";
const ResetPasswordComponent = () => {
  return (
    <div className="pageWithForm">
      <div className="form md:p-6 md:pb-12">
        <Title title="Đặt lại mật khẩu"></Title>

        <div className="mt-5 sm:mx-auto sm:w-full sm:max-w-sm">
          <FormResetPassword />
        </div>
      </div>
    </div>
  );
};
export default ResetPasswordComponent;
