import Title from "../../components/Title";

import FormForgotPassword from "../../components/ForgotPassword/Forgot/FormForgotPassword";
const ForgotPasswordComponent = () => {
  return (
    <>
      <div className="pageWithForm">
        <div className="form md:p-6 md:pb-12">
          <Title title="Quên mật khẩu"></Title>

          <div className="mt-5 sm:mx-auto sm:w-full sm:max-w-sm">
            <FormForgotPassword />
          </div>
        </div>
      </div>
    </>
  );
};
export default ForgotPasswordComponent;
