import logo from "../../assets/Logo.png";
import Title from "../../components/Title";
import userStore from "../../store/userStore";
import FormLogin from "../../components/Login/FormLogin";

const LoginComponent = () => {
  return (
    <>
      <div className="pageWithForm">
        <div className="form md:p-6 md:pb-12">
          <img
            className="mx-auto md:w-[100px] md:h-[100px] w-[50px] h-[50px] mb-2"
            src={logo}
          />
          <Title title="Đăng nhập"></Title>

          <div className="mt-5 sm:mx-auto sm:w-full sm:max-w-sm">
            <FormLogin />
          </div>
        </div>
      </div>
    </>
  );
};

export default LoginComponent;
