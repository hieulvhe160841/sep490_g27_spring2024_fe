import Title from "../../components/Title";
import FormVerify from "../../components/EmailVerify/FormVerify";

const EmailVerifyComponent = () => {
  return (
    <>
      <div className="pageWithForm">
        <div className="form md:p-6 md:pb-12">
          <Title title="Xác minh email"></Title>

          <div className="mt-5 sm:mx-auto sm:w-full sm:max-w-sm">
            <FormVerify />
          </div>
        </div>
      </div>
    </>
  );
};
export default EmailVerifyComponent;
