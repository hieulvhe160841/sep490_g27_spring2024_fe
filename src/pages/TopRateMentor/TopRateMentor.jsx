import React from "react";
import TopRatedMentor from "../../components/ListData/TopRatedMentor";
import TitleHeader from "../../components/TitleHeader";
import Header from "../../components/Header/Header";
import Sidebar from "../../components/Sidebar/AdminSidebar";
import MenteeSidebar from "../../components/Sidebar/MenteeSidebar";
import MentorSidebar from "../../components/Sidebar/MentorSidebar";
import EducationStaffSidebar from "../../components/Sidebar/EducationStaffSidebar";
import TransactionStaffSidebar from "../../components/Sidebar/TransactionStaffSidebar";
import userStore from "../../store/userStore";

function TopRateMentor() {
  const role = localStorage.getItem("role");
  const menteeRole = "ROLE_MENTEE";
  const mentorRole = "ROLE_MENTOR";
  const eduStaffRole = "ROLE_EDUCATION_STAFF";
  const transStaffRole = "ROLE_TRANSACTION_STAFF";
  const adminRole = "ROLE_ADMIN";
  const { isOpen, setIsOpen } = userStore();

  let SidebarComponent;
  let showSidebar = true;
  switch (role) {
    case menteeRole:
      SidebarComponent = MenteeSidebar;
      break;
    case mentorRole:
      SidebarComponent = MentorSidebar;
      break;
    case eduStaffRole:
      SidebarComponent = EducationStaffSidebar;
      break;
    case transStaffRole:
      SidebarComponent = TransactionStaffSidebar;
      break;
    case adminRole:
      SidebarComponent = Sidebar;
      break;
    default:
      SidebarComponent = () => <></>;
      showSidebar = false;
      break;
  }

  return (
    <>
      <Header />
      <div className={showSidebar ? "h-screen flex flex-row" : ""}>
        <SidebarComponent isOpen={isOpen} toggleSidebar={setIsOpen} />
        <div
          className={
            showSidebar
              ? `flex flex-col flex-1 ${isOpen ? "md:ml-56" : "md:ml-14"} ml-14`
              : ""
          }
        >
          <div className=" md:mx-20 my-10 pb-10">
            <TitleHeader title="Gia sư có đánh giá cao" />
            <TopRatedMentor />
          </div>
        </div>
      </div>
    </>
  );
}

export default TopRateMentor;
