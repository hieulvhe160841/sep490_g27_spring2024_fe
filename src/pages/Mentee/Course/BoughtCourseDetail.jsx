import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import BoughtCourseDetailComponent from "./../../../components/Course/BoughtCourseDetailComponent";
import mentorStore from "../../../store/mentorStore";
import TitleHeader from "../../../components/TitleHeader";

function BoughtCourseDetail() {
  const { id } = useParams();
  const { viewCourseDetail, course, isLoading } = mentorStore();
  useEffect(() => {
    viewCourseDetail(id);
  }, []);
  return (
    <div className="mx-20 md:mt-5">
      <TitleHeader title="Chi tiết khóa học" />
      <BoughtCourseDetailComponent course={course} />
    </div>
  );
}

export default BoughtCourseDetail;
