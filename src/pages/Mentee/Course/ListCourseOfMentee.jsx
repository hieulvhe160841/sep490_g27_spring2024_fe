import React from "react";
import CourseListOfMentee from "../../../components/ListData/CourseListOfMentee";
import TitleHeader from "../../../components/TitleHeader";

function ListCourseOfMentee() {
  return (
    <div className="mx-20 md:mt-5">
      <TitleHeader title="Các khóa học của tôi" />
      <CourseListOfMentee />
    </div>
  );
}

export default ListCourseOfMentee;
