import React, { useEffect, useState } from "react";
import Payment from "../../../components/Payment/Payment";
import TitleHeader from "../../../components/TitleHeader";

function ChoosePrice() {
  return (
    <>
      <div className="mx-20">
        <TitleHeader title="Ví của tôi" />
        <Payment />
      </div>
    </>
  );
}

export default ChoosePrice;
