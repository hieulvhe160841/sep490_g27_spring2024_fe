import { useEffect } from "react";
import FormRegisterMentor from "../../../components/RegisterAsMentor/FormRegisterMentor";
import TitleHeader from "../../../components/TitleHeader";

const RegisterAsMentor = () => {
  return (
    <>
      <div className="flex justify-center items-center">
        <div className="form md:p-0 md:pb-12">
          <TitleHeader title="Đăng ký làm gia sư" />
          <FormRegisterMentor />
        </div>
      </div>
    </>
  );
};
export default RegisterAsMentor;
