import React, { useEffect } from "react";
import TitleHeader from "../../../components/TitleHeader";
import ListRegistRequest from "../../../components/RegisterAsMentor/ListRegistRequest";
import menteeStore from "../../../store/menteeStore";
import LoadingState from "./../../../components/LoadingState";

function ListRegisterRequest() {
  const { listMentorRequest, fetchListMentorRequest, isLoading } =
    menteeStore();
  useEffect(() => {
    fetchListMentorRequest();
  }, []);

  return (
    <>
      {isLoading ? (
        <LoadingState />
      ) : (
        <div className="mt-10 ml-14">
          <TitleHeader title="Danh sách đăng ký làm gia sư" />
          {listMentorRequest ? (
            <ListRegistRequest request={listMentorRequest} />
          ) : (
            <div className="flex justify-center text-lg font-semibold">
              Bạn không có yêu cầu đăng ký làm gia sư nào
            </div>
          )}
        </div>
      )}
    </>
  );
}

export default ListRegisterRequest;
