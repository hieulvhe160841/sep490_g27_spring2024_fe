import React from "react";
import Header from "../../../components/Header/Header";
import TitleHeader from "../../../components/TitleHeader";
import SubmitCertificateForm from "../../../components/RegisterAsMentor/SubmitCertificateForm";

function SubmitCertificate() {
  return (
    <>
      <TitleHeader title="Thêm chứng chỉ" />
      <SubmitCertificateForm />
    </>
  );
}

export default SubmitCertificate;
