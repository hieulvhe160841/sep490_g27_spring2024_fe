import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import menteeStore from "../../../store/menteeStore";
import ModalViewImg from "../../../components/Modal/ModalViewImg";
import TitleHeader from "../../../components/TitleHeader";
import LoadingState from "../../../components/LoadingState";

function RegisterRequestDetail() {
  const { requestId } = useParams();
  const { viewDetailMentorRequest, isLoading } = menteeStore();
  const [request, setRequest] = useState();
  const [error, setError] = useState(null);
  useEffect(() => {
    const viewDetail = async () => {
      try {
        const mentorRegisData = await viewDetailMentorRequest(requestId);
        setRequest(mentorRegisData.data);
      } catch (error) {
        console.error("Error fetching MentorRegis:", error);
        setError(error);
      }
    };
    viewDetail();
  }, [viewDetailMentorRequest, requestId]);
  console.log(request);
  if (isLoading) {
    return <LoadingState />;
  }

  if (error) {
    return <div>Có lỗi vui lòng thử lại sau</div>;
  }

  if (!request) {
    return <div>Không có thông tin yêu cầu nào</div>;
  }
  const statusToVietnamese = (status) => {
    switch (status) {
      case "ACCEPTED":
        return "Đã duyệt";
      case "REJECTED":
        return "Đã từ chối";
      case "WAITING":
        return "Chờ phê duyệt";
      default:
        return status;
    }
  };
  return (
    <>
      <TitleHeader title="Chi tiết yêu cầu làm gia sư"></TitleHeader>

      <div className="container mx-auto py-7 px-20">
        <div className="bg-gray-100 shadow-xl rounded-lg p-6 border-2">
          <h2 className="text-xl font-bold mt-6 mb-4 text-textHeader">
            Chứng chỉ:
          </h2>
          {request.userCertificateDetailDTOList &&
            request.userCertificateDetailDTOList.map((cert) => {
              return (
                <div className="mb-6" key={cert.certificateName}>
                  <div className="flex justify-between flex-wrap gap-2 w-full">
                    <div className="text-gray-700 font-bold items-start">
                      {cert.certificateName}
                    </div>
                  </div>
                  <div className="mt-2 text-blue-600 justify-start">
                    <ModalViewImg cert={cert} />
                  </div>
                </div>
              );
            })}

          <h2 className="text-xl font-bold mt-6 mb-4 text-textHeader">
            Trình độ học vấn:
            <span className="text-lg font-semibold text-gray-800">
              {" "}
              {request.educationLevel}
            </span>
          </h2>

          <h2 className="text-xl font-bold mt-6 mb-4 text-textHeader">
            Kinh nghiệm:
            <span className="text-lg font-semibold text-gray-800">
              {request.experience}
            </span>
          </h2>
          <h2 className="text-xl font-bold mt-6 mb-4 text-textHeader">
            Trạng thái:
            <span className="text-lg font-semibold text-gray-800">
              {statusToVietnamese(request.mentorRequestStatus)}
            </span>
          </h2>
          <h2 className="text-xl font-bold mt-6 mb-4 text-textHeader">
            Nhận xét:
            {request.mentorRequestFeedback ? (
              <span className="text-lg font-semibold text-gray-800">
                {request.mentorRequestFeedback}
              </span>
            ) : (
              <span className="text-lg font-semibold text-gray-800">
                Không có nhận xét
              </span>
            )}
          </h2>
        </div>
      </div>
    </>
  );
}

export default RegisterRequestDetail;
