import React from "react";
import BookedMentorData from "../../../components/ListData/BookedMentorData";
import TitleHeader from "../../../components/TitleHeader";

function BookedMentor() {
  return (
    <>
      <div className=" md:mx-28 mx-2">
        <TitleHeader title="Gia sư đã thuê" />
        <BookedMentorData />
      </div>
    </>
  );
}

export default BookedMentor;
