import React from "react";
import MentorshipRequestOfMentee from "../../../components/ListData/MentorshipRequestOfMentee";
import TitleHeader from "../../../components/TitleHeader";

function MentorshipRequest() {
  return (
    <>
      <div className="mx-20 ">
        <TitleHeader title="Yêu cầu gia sư" />
        <MentorshipRequestOfMentee />
      </div>
    </>
  );
}

export default MentorshipRequest;
