import React from "react";
import MenteeTransactionDataTable from "../../../components/Table/MenteeTransactionDataTable";

const MenteeTransaction = () => {
  return (
    <div>
      <main>
        <h1 className="my-10 text-3xl flex justify-center font-bold text-textHeader">
          Giao dịch của bạn
        </h1>
        <div className="w-full flex justify-center items-center">
          <MenteeTransactionDataTable />
        </div>
      </main>
    </div>
  );
};

export default MenteeTransaction;
