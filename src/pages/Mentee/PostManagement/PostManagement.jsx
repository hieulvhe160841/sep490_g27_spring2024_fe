import React, { useEffect, useState } from "react";
import { Pagination, Button } from "@mui/material";
import MenteePostCard from "../../../components/Card/MenteePostCard";
import menteeStore from "../../../store/menteeStore";
import ModalAddPost from "../../../components/Modal/ModalAddPost";
import LoadingState from "../../../components/LoadingState";

function PostManagement() {
  const { fetchAllMenteePost, listMenteePost, isLoading } = menteeStore();
  useEffect(() => {
    fetchAllMenteePost();
  }, []);

  const [currentPage, setCurrentPage] = useState(1);
  const [sortBy, setSortBy] = useState("postId");
  const [sortOrder, setSortOrder] = useState("desc");
  const pageSize = 3;

  const handlePageChange = (event, page) => {
    setCurrentPage(page);
  };

  const toggleSortOrder = () => {
    setSortOrder(sortOrder === "asc" ? "desc" : "asc");
  };

  const sortedPosts = listMenteePost
    ? [...listMenteePost].sort((a, b) => {
        if (sortBy === "postId") {
          return sortOrder === "asc"
            ? a.postId - b.postId
            : b.postId - a.postId;
        } else if (sortBy === "postStatus") {
          const postStatusA = String(a.postStatus);
          const postStatusB = String(b.postStatus);
          return sortOrder === "asc"
            ? postStatusA.localeCompare(postStatusB)
            : postStatusB.localeCompare(postStatusA);
        }
        return 0;
      })
    : [];

  const startIndex = (currentPage - 1) * pageSize;
  const paginatedPosts = sortedPosts.slice(startIndex, startIndex + pageSize);

  const handleSortBy = (criteria) => {
    if (sortBy === criteria) {
      toggleSortOrder();
    } else {
      setSortBy(criteria);
      setSortOrder("asc");
    }
  };
  return (
    <main className="md:mx-28">
      <h1 className=" text-3xl flex justify-center font-bold text-textHeader my-10">
        Quản lý bài đăng
      </h1>
      <div className="flex md:justify-center my-5 justify-center">
        <ModalAddPost />
      </div>

      {isLoading ? (
        <LoadingState />
      ) : paginatedPosts.length > 0 ? (
        <div className="mx-5">
          <div className="flex items-center justify-start mb-2 gap-x-1">
            <span className="text-lg font-bold">Sắp xếp theo:</span>
            <Button variant="contained" onClick={() => handleSortBy("postId")}>
              Bài đăng
            </Button>
            <Button
              variant="contained"
              onClick={() => handleSortBy("postStatus")}
            >
              Trạng thái
            </Button>
          </div>
          <ul role="list" className="divide-y divide-gray-500">
            {paginatedPosts.map((post) => (
              <li key={post.postId}>
                <MenteePostCard post={post} />
              </li>
            ))}
          </ul>

          <Pagination
            count={Math.ceil(listMenteePost.length / pageSize)}
            page={currentPage}
            onChange={handlePageChange}
            color="primary"
            className="flex flex-col items-center mb-5"
          />
        </div>
      ) : (
        <div className="text-3xl text-center mt-3">
          Bạn không có bài đăng nào
        </div>
      )}
    </main>
  );
}

export default PostManagement;
