import React from "react";
import SuggestedMentorData from "../../../components/ListData/SuggestedMentorData";
import TitleHeader from "../../../components/TitleHeader";

function SuggestMentor() {
  return (
    <>
      <div className="mx-auto max-w-2xl lg:max-w-7xl lg:px-28">
        <TitleHeader title="Gợi ý gia sư cho bạn" />
        <SuggestedMentorData />
      </div>
    </>
  );
}

export default SuggestMentor;
