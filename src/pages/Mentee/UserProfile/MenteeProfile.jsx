import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import BasicInformation from "../../../components/UserProfile/Mentee/BasicInformation";
import MoreInformation from "../../../components/MenteeDetail/MoreInformation";
import { toast } from "react-toastify";
import menteeStore from "../../../store/menteeStore";
import bgImg from "../../../assets/bg-img.png";
import LoadingState from "../../../components/LoadingState";
function menteeProfile() {
  const [mentee, setMentee] = useState();
  const { menteeProfileApi } = menteeStore();
  useEffect(() => {
    const fetchProfile = async () => {
      try {
        const userData = await menteeProfileApi();
        console.log("Mentee: ", userData);
        setMentee(userData);
      } catch (error) {
        toast.error("Không tìm thấy học viên");
      }
    };
    fetchProfile();
  }, [menteeProfileApi]);

  return (
    <>
      {!mentee ? (
        <LoadingState />
      ) : (
        <div
          className="bg-gray-100 bg-cover bg-center h-80"
          style={{ backgroundImage: `url(${bgImg})` }}
        >
          <div className="container mx-auto py-8">
            <BasicInformation mentee={mentee} />
          </div>
        </div>
      )}
    </>
  );
}

export default menteeProfile;
