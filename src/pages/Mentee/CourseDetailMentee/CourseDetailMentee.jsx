import React from "react";
import { useParams } from "react-router-dom";
import CourseMentee from "../../../components/Course/CourseMentee";

const CourseDetailMentee = ({ courseRows }) => {
  const { id } = useParams();
  const course = courseRows.find((row) => row.id === parseInt(id));

  return (
    <div>
      <main>
        <div className="m-2">
          <CourseMentee course={course} />
        </div>
      </main>
    </div>
  );
};

export default CourseDetailMentee;
