import React from "react";
import DashboardStatsGrid from "../../components/AdminDashboard/DashboardStatsGrid";
import TransactionChart from "../../components/AdminDashboard/TransactionChart";
import UserProfilePieChart from "../../components/AdminDashboard/UserProfilePieChart";
import TopRatedMentor from "../../components/AdminDashboard/TopRatedMentor";
import RecentCourses from "../../components/AdminDashboard/RecentCourses";

const Dashboard = () => {
  return (
    <div>
      <main className="flex flex-col gap-4 mx-4">
        <DashboardStatsGrid />
        <div className="md:flex md:flex-row md:gap-4 md:w-full">
          <TransactionChart />
          <UserProfilePieChart />
        </div>
        <div className="md:flex md:flex-row md:gap-4 md:w-full">
          <RecentCourses />
          <TopRatedMentor />
        </div>
      </main>
    </div>
  );
};

export default Dashboard;
