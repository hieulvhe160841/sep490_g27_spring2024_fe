import React, { useState, useEffect } from "react";
import UserDetailInputForm from "../../components/InputForm/UserDetailInputForm";
import { useParams } from "react-router-dom";
import userStore from "../../store/userStore";

const UserDetail = () => {
  const { username } = useParams();
  const [user, setUser] = useState({});
  const { fetchUserByUsername } = userStore();

  useEffect(() => {
    const fetchUser = async () => {
      try {
        const userData = await fetchUserByUsername(username);
        console.log("User Data: ", userData);
        setUser(userData.data);
      } catch (error) {
        console.error("Error fetching user:", error);
      }
    };
    fetchUser();
  }, [fetchUserByUsername, username]);

  return (
    <div>
      <main>
        <h1 className="my-5 text-3xl flex justify-center font-bold text-textHeader">
          Thông tin người dùng
        </h1>
        <div className="w-full flex 2xl:justify-center justify-start items-center">
          <UserDetailInputForm user={user} />
        </div>
      </main>
    </div>
  );
};

export default UserDetail;
