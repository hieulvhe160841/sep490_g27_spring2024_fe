import React from "react";
import SkillDataTable from "../../components/Table/SkillDataTable";
import ModalAddSkill from "../../components/Modal/ModalAddSkill";

const SkillManagement = () => {
  return (
    <div>
      <main>
        <h1 className="mb-8 text-3xl flex justify-center font-bold text-textHeader">
          Quản lý kỹ năng
        </h1>
        <div className="w-full flex justify-center items-center">
          <SkillDataTable />
        </div>
        <div className="flex md:justify-end md:mr-6 md:mt-3 justify-center">
          <ModalAddSkill />
        </div>
      </main>
    </div>
  );
};

export default SkillManagement;
