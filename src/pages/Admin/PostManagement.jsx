import React, { useState, useEffect } from "react";
import PostCard from "../../components/Card/PostCard";
import Pagination from "@mui/material/Pagination";
import userStore from "../../store/userStore";
import LoadingState from "../../components/LoadingState";

const PostManagement = () => {
  const { isLoading, listPost, fetchPosts } = userStore();

  useEffect(() => {
    fetchPosts();
  }, []);

  const [currentPage, setCurrentPage] = useState(1);
  const pageSize = 3;

  const handlePageChange = (event, page) => {
    setCurrentPage(page);
  };

  const startIndex = (currentPage - 1) * pageSize;
  const paginatedPosts = listPost
    ? listPost.slice(startIndex, startIndex + pageSize)
    : [];

  return (
    <main>
      <h1 className="m-5 pb-8 text-3xl flex justify-start font-bold text-textHeader">
        Quản lý bài đăng
      </h1>
      {isLoading ? (
        <LoadingState />
      ) : paginatedPosts.length > 0 ? (
        <div className="mx-5">
          <ul role="list" className="divide-y divide-gray-500">
            {paginatedPosts.map((post) => (
              <li key={post.postId}>
                <PostCard post={post} />
              </li>
            ))}
          </ul>

          <Pagination
            count={Math.ceil(listPost.length / pageSize)}
            page={currentPage}
            onChange={handlePageChange}
            color="primary"
            className="flex flex-col items-center"
          />
        </div>
      ) : (
        <div className="text-3xl text-center mt-3">
          Hiện không có bài đăng nào :(
        </div>
      )}
    </main>
  );
};

export default PostManagement;
