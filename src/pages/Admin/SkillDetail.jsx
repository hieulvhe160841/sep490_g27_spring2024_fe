import React, { useState, useEffect } from "react";
import SkillDetailInputForm from "../../components/InputForm/SkillDetailInputForm";
import { useParams } from "react-router-dom";
import userStore from "../../store/userStore";

const SkillDetail = () => {
  const { id } = useParams();
  const [skill, setSkill] = useState({});
  const { fetchSkillById } = userStore();

  useEffect(() => {
    const fetchSkillDetail = async () => {
      try {
        const skillDetail = await fetchSkillById(id);
        console.log("Skill Detail: ", skillDetail);
        setSkill(skillDetail.data);
      } catch (error) {
        console.error("Error fetching skillDetail:", error);
      }
    };
    fetchSkillDetail();
  }, [fetchSkillById, id]);

  return (
    <div>
      <main>
        <h1 className="my-5 text-2xl flex justify-center font-bold text-textHeader">
          Thông tin Kỹ năng
        </h1>
        <div className="w-full flex 2xl:justify-center justify-start items-center">
          <SkillDetailInputForm skill={skill} />
        </div>
      </main>
    </div>
  );
};

export default SkillDetail;
