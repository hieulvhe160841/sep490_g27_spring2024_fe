import React from "react";
import TransactionDataTable from "../../components/Table/TransactionDataTable";

const TransactionManagement = () => {
  return (
    <div>
      <main>
        <h1 className="mb-8 text-3xl flex justify-center font-bold text-textHeader">
          Quản lý giao dịch
        </h1>
        <div className="w-full flex justify-center items-center">
          <TransactionDataTable />
        </div>
      </main>
    </div>
  );
};

export default TransactionManagement;
