import React, { useEffect } from "react";
import ApprovedCourseAdmin from "./../../components/Course/ApprovedCourseAdmin";
import mentorStore from "../../store/mentorStore";
import { useParams } from "react-router-dom";
import bgImg from "../../assets/bg-img.png";

const ApprovedCourseDetail = () => {
  const { id } = useParams();
  const { viewCourseDetail, course, isLoading } = mentorStore();
  useEffect(() => {
    viewCourseDetail(id);
  }, []);

  return (
    <div className="relative">
      <div
        className="bg-gray-100 bg-cover bg-center h-80 opacity-80"
        style={{ backgroundImage: `url(${bgImg})` }}
      ></div>
      <div className="container mx-10 p-6 absolute top-5 left-20">
        <ApprovedCourseAdmin course={course} />
      </div>
    </div>
  );
};

export default ApprovedCourseDetail;
