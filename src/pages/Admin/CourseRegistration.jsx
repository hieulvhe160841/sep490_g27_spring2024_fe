import React, { useState, useEffect } from "react";
import CourseRegistrationCard from "../../components/Card/CourseRegistrationCard";
import Pagination from "@mui/material/Pagination";
import userStore from "../../store/userStore";
import LoadingState from "../../components/LoadingState";

const CourseRegistration = () => {
  const { isLoading, listCourseRegis, fetchCourseRegis } = userStore();

  useEffect(() => {
    fetchCourseRegis();
  }, []);

  const [currentPage, setCurrentPage] = useState(1);
  const pageSize = 3;

  const handlePageChange = (event, page) => {
    setCurrentPage(page);
  };

  const startIndex = (currentPage - 1) * pageSize;
  const paginatedCourses = listCourseRegis
    ? listCourseRegis.slice(startIndex, startIndex + pageSize)
    : [];

  console.log(paginatedCourses);

  return (
    <main>
      <h1 className="m-5 pb-8 text-3xl flex justify-start font-bold text-textHeader">
        Duyệt khóa học
      </h1>
      {isLoading ? (
        <LoadingState />
      ) : paginatedCourses.length > 0 ? (
        <div>
          <div className="mx-5">
            <ul role="list" className="divide-y divide-gray-500">
              {paginatedCourses.map((courseRegis) => (
                <li key={courseRegis.courseRequestId}>
                  <CourseRegistrationCard courseRegis={courseRegis} />
                </li>
              ))}
            </ul>

            <Pagination
              count={Math.ceil(listCourseRegis.length / pageSize)}
              page={currentPage}
              onChange={handlePageChange}
              color="primary"
              className="flex flex-col items-center"
            />
          </div>
        </div>
      ) : (
        <div className="text-3xl text-center mt-3">
          Hiện không có gia sư nào tạo khóa học :(
        </div>
      )}
    </main>
  );
};

export default CourseRegistration;
