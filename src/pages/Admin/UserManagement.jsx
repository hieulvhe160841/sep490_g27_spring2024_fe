import React from "react";
import UserDataTable from "../../components/Table/UserDataTable";
import ModalAddStaff from "../../components/Modal/ModalAddStaff";

const UserManagement = () => {
  return (
    <div>
      <main>
        <h1 className="mb-8 text-3xl flex justify-center font-bold text-textHeader">
          Quản lý người dùng
        </h1>
        <div className="w-full flex justify-center items-center">
          <UserDataTable />
        </div>
        <div className="flex md:justify-end md:mr-6 md:mt-3 justify-center">
          <ModalAddStaff />
        </div>
      </main>
    </div>
  );
};

export default UserManagement;
