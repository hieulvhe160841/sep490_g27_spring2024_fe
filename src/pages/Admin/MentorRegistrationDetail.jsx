import React, { useState, useEffect } from "react";
import MentorRegisDetail from "../../components/MentorRegisDetail";
import { useParams } from "react-router-dom";
import userStore from "../../store/userStore";

const ViewProfile = () => {
  const { userId, requestId } = useParams();
  const [mentorRegis, setMentorRegis] = useState();
  const [username, setUsername] = useState("");
  const { fetchMentorRegisByUserIdAndRequestId } = userStore();

  useEffect(() => {
    const fetchMentorRegis = async () => {
      try {
        const mentorRegisData = await fetchMentorRegisByUserIdAndRequestId(
          userId,
          requestId
        );
        console.log("MentorRegis Data: ", mentorRegisData);
        setMentorRegis(mentorRegisData.data);
        setUsername(
          mentorRegisData.data.mentorRequestDetail.userCreatedMentorRequestDTO
            .userName
        );
      } catch (error) {
        console.error("Error fetching MentorRegis:", error);
      }
    };
    fetchMentorRegis();
  }, [fetchMentorRegisByUserIdAndRequestId, userId, requestId]);

  return (
    <div>
      <main>
        <h1 className="m-5 text-3xl flex justify-center font-bold text-textHeader">
          Thông tin chi tiết đăng ký làm gia sư
        </h1>
        <div className="w-full flex justify-center items-center">
          <MentorRegisDetail username={username} mentorRegis={mentorRegis} />
        </div>
      </main>
    </div>
  );
};

export default ViewProfile;
