import React, { useEffect, useState } from "react";
import MentorshipRequestCard from "../../components/Card/MentorshipRequestCard";
import {
  Pagination,
  Button,
  Select,
  MenuItem,
  TextField,
  FormControl,
  InputLabel,
  InputAdornment,
} from "@mui/material";
import userStore from "../../store/userStore";
import LoadingState from "../../components/LoadingState";

const MentorshipRequest = () => {
  const { isLoading, listMentorshipRequest, fetchMentorshipRequest } =
    userStore();
  const [status, setStatus] = useState([]);
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");

  useEffect(() => {
    const statusString = status.join(",");
    fetchMentorshipRequest(statusString, startDate, endDate);
  }, [status, startDate, endDate]);

  const [currentPage, setCurrentPage] = useState(1);
  const [sortBy, setSortBy] = useState("supportId");
  const [sortOrder, setSortOrder] = useState("desc");
  const pageSize = 3;

  const handlePageChange = (event, page) => {
    setCurrentPage(page);
  };

  const handleSortBy = (criteria) => {
    if (sortBy === criteria) {
      setSortOrder(sortOrder === "asc" ? "desc" : "asc");
    } else {
      setSortBy(criteria);
      setSortOrder("asc");
    }
  };

  const sortedRequests = listMentorshipRequest
    ? [...listMentorshipRequest].sort((a, b) => {
        if (sortBy === "supportId") {
          return sortOrder === "asc"
            ? a.supportId - b.supportId
            : b.supportId - a.supportId;
        }
        return 0;
      })
    : [];

  const startIndex = (currentPage - 1) * pageSize;
  const paginatedRequests = sortedRequests.slice(
    startIndex,
    startIndex + pageSize
  );

  return (
    <div>
      <main>
        <h1 className="m-5 text-3xl flex justify-start font-bold text-textHeader">
          Quản lý Yêu cầu gia sư
        </h1>
        <div className="flex gap-x-3 justify-center my-10">
          <FormControl sx={{ minWidth: 150 }}>
            <InputLabel id="status-label">Trạng thái</InputLabel>
            <Select
              labelId="status-label"
              label="Trạng thái"
              multiple
              value={status}
              onChange={(event) => setStatus(event.target.value)}
            >
              <MenuItem value="WAITING">Đang chờ</MenuItem>
              <MenuItem value="ACCEPTED">Đã nhận</MenuItem>
              <MenuItem value="DONE">Hoàn thành</MenuItem>
              <MenuItem value="REJECTED">Từ chối</MenuItem>
              <MenuItem value="IN_PROGRESS">Đang tiến hành</MenuItem>
            </Select>
          </FormControl>
          <TextField
            type="date"
            value={startDate}
            onChange={(event) => setStartDate(event.target.value)}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">Từ</InputAdornment>
              ),
            }}
          />
          <TextField
            type="date"
            value={endDate}
            onChange={(event) => setEndDate(event.target.value)}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">Đến</InputAdornment>
              ),
            }}
          />
        </div>
        {isLoading ? (
          <LoadingState />
        ) : paginatedRequests.length > 0 ? (
          <div className="mx-5">
            <div className="flex items-center justify-start my-3 gap-x-1">
              <span className="text-lg font-bold">Sắp xếp theo thời gian:</span>
              <Button
                variant="contained"
                onClick={() => handleSortBy("supportId")}
              >
                Sắp xếp
              </Button>
            </div>
            <ul role="list" className="divide-y divide-gray-500">
              {paginatedRequests.map((request) => (
                <li key={request.supportId}>
                  <MentorshipRequestCard request={request} />
                </li>
              ))}
            </ul>

            <Pagination
              count={Math.ceil(listMentorshipRequest.length / pageSize)}
              page={currentPage}
              onChange={handlePageChange}
              color="primary"
              className="flex flex-col items-center"
            />
          </div>
        ) : (
          <div className="text-3xl text-center mt-3">
            Không có yêu cầu gia sư nào :(
          </div>
        )}
      </main>
    </div>
  );
};

export default MentorshipRequest;
