import React, { useState, useEffect } from "react";
import MentorRegistrationCard from "../../components/Card/MentorRegistrationCard";
import Pagination from "@mui/material/Pagination";
import userStore from "../../store/userStore";
import LoadingState from "../../components/LoadingState";

const MentorRegistration = () => {
  const { isLoading, listMentorRegis, fetchMentorRegis } = userStore();

  useEffect(() => {
    fetchMentorRegis();
  }, []);

  const [currentPage, setCurrentPage] = useState(1);
  const pageSize = 3;

  const handlePageChange = (event, page) => {
    setCurrentPage(page);
  };

  const startIndex = (currentPage - 1) * pageSize;
  const paginatedMentors = listMentorRegis
    ? listMentorRegis.slice(startIndex, startIndex + pageSize)
    : [];

  console.log(paginatedMentors);

  return (
    <main>
      <h1 className="m-5 pb-8 text-3xl flex justify-start font-bold text-textHeader">
        Đăng ký làm gia sư
      </h1>
      {isLoading ? (
        <LoadingState />
      ) : paginatedMentors.length > 0 ? (
        <div>
          <div className="mx-5">
            <ul role="list" className="divide-y divide-gray-500">
              {paginatedMentors.map((mentorRegis) => (
                <li key={mentorRegis.mentorRequestId}>
                  <MentorRegistrationCard mentorRegis={mentorRegis} />
                </li>
              ))}
            </ul>

            <Pagination
              count={Math.ceil(listMentorRegis.length / pageSize)}
              page={currentPage}
              onChange={handlePageChange}
              color="primary"
              className="flex flex-col items-center"
            />
          </div>
        </div>
      ) : (
        <div className="text-3xl text-center mt-3">
          Hiện không có học viên nào đăng ký làm gia sư :(
        </div>
      )}
    </main>
  );
};

export default MentorRegistration;
