import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import RequestDetail from "../../components/RequestDetail";
import userStore from "../../store/userStore";

const MentorshipRequestDetail = () => {
  const { id } = useParams();
  const [mentorshipRequest, setMentorshipRequest] = useState();
  const { fetchMentorshipRequestById } = userStore();

  useEffect(() => {
    const fetchMentorshipRequest = async () => {
      try {
        const mentorshipRequestData = await fetchMentorshipRequestById(id);
        console.log("Mentorship Request Data: ", mentorshipRequestData);
        setMentorshipRequest(mentorshipRequestData.data);
      } catch (error) {
        console.error("Error fetching Mentorship Request:", error);
      }
    };
    fetchMentorshipRequest();
  }, [fetchMentorshipRequestById, id]);

  return (
    <div>
      <main>
        <h1 className="mb-5 text-xl flex justify-center font-bold text-textHeader">
          Thông tin chi tiết Yêu cầu gia sư
        </h1>
        <div className="w-full flex justify-center items-center">
          <RequestDetail mentorshipRequest={mentorshipRequest} />
        </div>
      </main>
    </div>
  );
};

export default MentorshipRequestDetail;
