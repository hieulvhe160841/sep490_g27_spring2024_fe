import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import CourseAdmin from "../../components/Course/CourseAdmin";
import userStore from "../../store/userStore";

const CourseDetail = () => {
  const { id } = useParams();
  const [courseRegis, setCourseRegis] = useState({});
  const { fetchCourseRegisById } = userStore();

  useEffect(() => {
    const fetchCourseRegis = async () => {
      try {
        const courseRegisData = await fetchCourseRegisById(id);
        console.log("CourseRegis Data: ", courseRegisData);
        setCourseRegis(courseRegisData.data);
      } catch (error) {
        console.error("Error fetching CourseRegis:", error);
      }
    };
    fetchCourseRegis();
  }, [fetchCourseRegisById, id]);

  return (
    <div>
      <main>
        <div className="m-2">
          <CourseAdmin courseRegis={courseRegis} />
        </div>
      </main>
    </div>
  );
};

export default CourseDetail;
