import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import menteeStore from "../../store/menteeStore";
import BasicInformation from "../../components/MenteeDetail/BasicInformation";
import MoreInformation from "../../components/MenteeDetail/MoreInformation";
import { toast } from "react-toastify";
import bgImg from "../../assets/bg-img.png";
import LoadingState from "../../components/LoadingState";

function MenteeDetails() {
  const { username } = useParams();
  const [mentee, setMentee] = useState();
  const { fetchMenteeDetail } = menteeStore();
  useEffect(() => {
    const fetchMentee = async () => {
      try {
        const userData = await fetchMenteeDetail(username);
        console.log("Mentee: ", userData);
        setMentee(userData);
      } catch (error) {
        toast.error("Không tìm thấy học viên");
      }
    };
    fetchMentee();
  }, [fetchMenteeDetail, username]);

  return (
    <>
      {!mentee ? (
        <LoadingState />
      ) : (
        <div
          className="bg-gray-100 bg-cover bg-center h-80"
          style={{ backgroundImage: `url(${bgImg})` }}
        >
          <div className="container mx-auto py-8">
            <div className="grid grid-cols-4 lg:grid-cols-12 gap-6 px-4 mt-20 ml-14">
              <div className="col-span-4 relative">
                <BasicInformation username={username} mentee={mentee} />
              </div>
              <div className="col-span-4 lg:col-span-8">
                <MoreInformation mentee={mentee} />
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export default MenteeDetails;
