import React, { useEffect } from "react";
import CourseMentee from "./../../components/Course/CourseMentee";
import mentorStore from "../../store/mentorStore";
import { useParams } from "react-router-dom";
import Header from "../../components/Header/Header";
import bgImg from "../../assets/bg-img.png";
import Sidebar from "../../components/Sidebar/AdminSidebar";
import MenteeSidebar from "../../components/Sidebar/MenteeSidebar";
import MentorSidebar from "../../components/Sidebar/MentorSidebar";
import EducationStaffSidebar from "../../components/Sidebar/EducationStaffSidebar";
import TransactionStaffSidebar from "../../components/Sidebar/TransactionStaffSidebar";
import userStore from "../../store/userStore";

function CourseDetail() {
  const { id } = useParams();
  const { viewCourseDetail, course, isLoading } = mentorStore();
  const role = localStorage.getItem("role");
  const menteeRole = "ROLE_MENTEE";
  const mentorRole = "ROLE_MENTOR";
  const eduStaffRole = "ROLE_EDUCATION_STAFF";
  const transStaffRole = "ROLE_TRANSACTION_STAFF";
  const adminRole = "ROLE_ADMIN";
  const { isOpen, setIsOpen } = userStore();

  let SidebarComponent;
  let showSidebar = true;
  switch (role) {
    case menteeRole:
      SidebarComponent = MenteeSidebar;
      break;
    case mentorRole:
      SidebarComponent = MentorSidebar;
      break;
    case eduStaffRole:
      SidebarComponent = EducationStaffSidebar;
      break;
    case transStaffRole:
      SidebarComponent = TransactionStaffSidebar;
      break;
    case adminRole:
      SidebarComponent = Sidebar;
      break;
    default:
      SidebarComponent = () => <></>;
      showSidebar = false;
      break;
  }

  useEffect(() => {
    viewCourseDetail(id);
  }, []);

  return (
    <>
      <Header />
      <div className={showSidebar ? "h-screen flex flex-row" : ""}>
        <SidebarComponent isOpen={isOpen} toggleSidebar={setIsOpen} />
        <div
          className={
            showSidebar
              ? `flex flex-col flex-1 ${isOpen ? "md:ml-56" : ""}`
              : ""
          }
        >
          <div className="relative">
            <div
              className="bg-gray-100 bg-cover bg-center h-80"
              style={{ backgroundImage: `url(${bgImg})` }}
            ></div>
            <div className="container mx-10 p-6 ">
              <CourseMentee course={course} />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default CourseDetail;
