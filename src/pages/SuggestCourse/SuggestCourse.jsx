import React from "react";
import Header from "../../components/Header/Header";
import ListData from "../../components/SuggestCourse/ListData";
import TitleHeader from "../../components/TitleHeader";
import Sidebar from "../../components/Sidebar/AdminSidebar";
import MenteeSidebar from "../../components/Sidebar/MenteeSidebar";
import MentorSidebar from "../../components/Sidebar/MentorSidebar";
import EducationStaffSidebar from "../../components/Sidebar/EducationStaffSidebar";
import TransactionStaffSidebar from "../../components/Sidebar/TransactionStaffSidebar";
import userStore from "../../store/userStore";

function SuggestCourse() {
  const role = localStorage.getItem("role");
  const menteeRole = "ROLE_MENTEE";
  const mentorRole = "ROLE_MENTOR";
  const eduStaffRole = "ROLE_EDUCATION_STAFF";
  const transStaffRole = "ROLE_TRANSACTION_STAFF";
  const adminRole = "ROLE_ADMIN";
  const { isOpen, setIsOpen } = userStore();

  let SidebarComponent;
  let showSidebar = true;
  switch (role) {
    case menteeRole:
      SidebarComponent = MenteeSidebar;
      break;
    case mentorRole:
      SidebarComponent = MentorSidebar;
      break;
    case eduStaffRole:
      SidebarComponent = EducationStaffSidebar;
      break;
    case transStaffRole:
      SidebarComponent = TransactionStaffSidebar;
      break;
    case adminRole:
      SidebarComponent = Sidebar;
      break;
    default:
      SidebarComponent = () => <></>;
      showSidebar = false;
      break;
  }

  return (
    <>
      <Header />
      <div className={showSidebar ? "h-screen flex flex-row" : ""}>
        <SidebarComponent isOpen={isOpen} toggleSidebar={setIsOpen} />
        <div
          className={
            showSidebar
              ? `flex flex-col flex-1 ${isOpen ? "md:ml-56" : ""} ml-14`
              : ""
          }
        >
          <div className=" md:mx-20 my-10 pb-10">
            <TitleHeader title="Danh sách khóa học" />
            <ListData />
          </div>
        </div>
      </div>
    </>
  );
}

export default SuggestCourse;
