import Title from "../../components/Title";
import FormRegister from "../../components/Register/FormRegister";
const RegisterComponent = () => {
  return (
    <>
      <div className="pageWithForm">
        <div className="form md:p-6 md:pb-8">
          <Title title="Đăng ký"></Title>

          <div className="md:mx-auto md:w-full md:max-w-sm mt-3">
            <FormRegister />
          </div>
        </div>
      </div>
    </>
  );
};
export default RegisterComponent;
