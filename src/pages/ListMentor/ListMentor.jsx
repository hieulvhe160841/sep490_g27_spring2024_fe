import React from "react";
import Header from "../../components/Header/Header";
import Title from "../../components/Title";
import FilterSideBar from "../../components/ListMentor/FilterSideBar";
import ListData from "../../components/ListMentor/ListData";
import TitleHeader from "../../components/TitleHeader";

function ListMentor() {
  return (
    <>
      <Header />
      <div className="p-10 mt-3">
        <TitleHeader title="Language Experts for you" />
        <div className="mt-5 grid lg:gap-5 lg:grid-cols-8 grid-cols-1 divide-x divide-gray-400">
          <FilterSideBar />
          <ListData />
        </div>
      </div>
    </>
  );
}

export default ListMentor;
