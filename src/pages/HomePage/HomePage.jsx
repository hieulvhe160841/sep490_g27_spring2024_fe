import Header from "../../components/Header/Header";
import { Link } from "react-router-dom";
import HomepageIntroduction from "../../components/HomepageIntroduction";
import Footer from "../../components/Footer/Footer";
import bgImg from "../../assets/bg-img.png";

const HomeComponent = () => {
  return (
    <>
      <Header />
      <div className="w-full">
        <div
          className="text-5xl mx-auto text-left p-12 md:p-20 font-bold text-white text-shadow-lg 
        bg-cover bg-center"
          style={{ backgroundImage: `url(${bgImg})` }}
        >
          <div className="text-3xl md:text-5xl leading-tight mx-36">
            <p className="mb-4">Online</p>
            <p className="mb-4">Mentor</p>
            <p className="mb-4">System</p>
          </div>
        </div>
        <div className="bg-white p-6 md:p-10 mx-auto text-center">
          <div className="text-5xl font-bold text-sidebarTop text-shadow-lg uppercase py-5">
            Về chúng tôi
          </div>
          <p className="mt-4 text-lg text-gray-700 md:text-left md:px-56">
            Chào mừng bạn đến với nền tảng cho thuê gia sư của chúng tôi! Khám
            phá và kết nối với các gia sư giàu kinh nghiệm, được điều chỉnh theo
            nhu cầu và sở thích học tập của bạn. Dễ dàng lên lịch các phiên trực
            tuyến và tương tác trực tiếp thông qua các công cụ tiện lợi như cuộc
            gọi video và chat.
          </p>
          <br />
          <p className="mt-2 text-lg text-gray-700 md:text-left md:px-56 mb-3">
            Bắt đầu hành trình học tập cá nhân hóa của bạn ngay hôm nay và đạt
            được mục tiêu một cách hiệu quả. Hãy cùng nhau khởi hành trên hành
            trình kiến thức và phát triển ngoại ngữ!
          </p>
          <Link to="/topRateMentor">
            <button className="mt-5 px-4 py-2 bg-sidebarTop text-white rounded-full hover:bg-active focus:outline-none focus:ring focus:border-blue-300">
              Tìm gia sư
            </button>
          </Link>
        </div>

        <div className="bg-[#f6f6f6] p-6 md:p-10 mx-auto text-center">
          <div className="text-5xl font-bold text-sidebarTop text-shadow-lg uppercase py-5">
            Dịch vụ gia sư của chúng tôi
          </div>
          <p className="mt-4 text-lg text-gray-700 md:px-64 pb-7">
            Dịch vụ gia sư của chúng tôi được thiết kế để cung cấp cho bạn sự hỗ
            trợ bạn cần để điều hướng hành trình học tập của mình. Với các gia
            sư của chúng tôi, bạn sẽ nhận được sự chú ý cá nhân và hướng dẫn
            được điều chỉnh theo nhu cầu và mục tiêu cụ thể của bạn. Các gia sư
            của chúng tôi là những chuyên gia dày dạn kinh nghiệm trong lĩnh vực
            của họ và cam kết giúp bạn đạt được thành công. Họ sẽ cung cấp cho
            bạn các chiến lược, công cụ và động lực bạn cần để phát huy hết tiềm
            năng của mình. Với dịch vụ gia sư của chúng tôi, bạn không chỉ có
            một giáo viên; bạn có một đối tác tận tâm trong hành trình học tập
            của mình.
          </p>
          <div className="mx-[70px] my-10 text-left">
            <HomepageIntroduction />
          </div>
        </div>

        <div className="bg-sidebarTop p-6 md:p-10 mx-auto text-center">
          <div className="text-3xl font-bold text-white text-shadow-lg uppercase py-5">
            Liên hệ chúng tôi
          </div>
          <p className="mt-4 text-lg text-white md:px-56">
            Nếu bạn có bất kỳ câu hỏi nào hoặc cần thêm thông tin, đừng ngần
            ngại liên hệ với chúng tôi. Đội ngũ của chúng tôi luôn sẵn lòng hỗ
            trợ bạn. Bạn có thể liên hệ với chúng tôi qua email, điện thoại,
            hoặc các kênh truyền thông xã hội của chúng tôi. Chúng tôi mong được
            nghe từ bạn!
          </p>
          <button className="mt-5 px-4 py-2 bg-white text-sidebarTop rounded-full hover:bg-active focus:outline-none focus:ring focus:border-blue-300">
            Liên hệ
          </button>
        </div>
      </div>
      <Footer />
    </>
  );
};
export default HomeComponent;
