import React, { useState, useEffect } from "react";
import { ChatEngine } from "react-chat-engine";
import CollapseCardSession from "../../components/Card/CollapseCardSession";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import SockJS, { log } from "sockjs-client/dist/sockjs";
import Stomp from "stompjs";
import menteeStore from "../../store/menteeStore";
import mentorStore from "../../store/mentorStore";
import { Button, Modal, Box, Typography } from "@mui/material";
import { toast } from "react-toastify";
import { FaStar } from "react-icons/fa";

const RequestSession = () => {
  const vietnamTimeZoneOffset = +7;
  const location = useLocation();
  const [mentorUsername, setMentorUsername] = useState(null);
  const [menteeUsername, setMenteeUsername] = useState(null);
  const currentUsername = localStorage.getItem("user");
  const currentSecret = localStorage.getItem("secret");
  const { id } = useParams();
  const [time, setTime] = useState(0);
  const [sessionStatus, setSessionStatus] = useState("Bắt đầu");
  const [isSessionActive, setIsSessionActive] = useState(false);
  const {
    confirmSupportStartMentee,
    confirmSupportEnd,
    fetchSupportByIdMentee,
    menteeRatingSupportOfMentor,
  } = menteeStore();
  const { confirmSupportStartMentor, fetchSupportByIdMentor } = mentorStore();
  const userRole = localStorage.getItem("role");
  const [isSessionStarted, setIsSessionStarted] = useState(false);
  const [showEndSessionModal, setShowEndSessionModal] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [rating, setRating] = useState(0);
  const [feedback, setFeedBack] = useState();
  const navigate = useNavigate();

  const handleRating = (rate) => {
    setRating(rate);
  };

  const onHandleRatingForSupport = async () => {
    const res = await menteeRatingSupportOfMentor(id, rating, feedback);
    if (res.statusCode === 201) {
      toast.success("Đánh giá của bạn đã được gửi.");
    } else {
      toast.error(res.messages);
    }
    setShowModal(false);
    navigate("/mentee");
    localStorage.removeItem("requestTimer");
  };

  const onCloseRating = () => {
    setShowModal(false);
    navigate("/mentee");
    localStorage.removeItem("requestTimer");
  };

  const handleEndSessionClick = () => {
    setShowEndSessionModal(true);
  };

  const handleCancelEndSession = () => {
    setShowEndSessionModal(false);
  };

  const saveTimerState = (currentTime) => {
    localStorage.setItem("requestTimer", JSON.stringify(currentTime));
  };

  useEffect(() => {
    const socket = new SockJS("https://54.169.171.52/oms");
    const stompClient = Stomp.over(socket);

    stompClient.connect({}, () => {
      console.log("Connected to WebSocket server");
      stompClient.subscribe("/topic/sessionStart", (message) => {
        console.log("Received session start message:", message);
        setSessionStatus("Đang chạy");
        setIsSessionStarted(true);
        setIsSessionActive(true);
      });
      stompClient.subscribe("/topic/sessionEnd", (message) => {
        console.log("Received session end message:", message);
        setIsSessionActive(false);
        setSessionStatus("Hoàn thành");
        localStorage.removeItem("requestTimer");
        if (userRole === "ROLE_MENTOR") {
          toast.success("Phiên hỗ trợ đã kết thúc.");
          navigate("/mentor/chat");
        }
      });
    });

    return () => {
      stompClient.disconnect();
    };
  }, [id, userRole]);

  useEffect(() => {
    const savedTimer = JSON.parse(localStorage.getItem("requestTimer"));
    if (savedTimer !== null) {
      if (savedTimer === 0 || !isSessionStarted) {
        setSessionStatus("Bắt đầu");
        setIsSessionActive(false);
        setTime(savedTimer);
      } else if (isSessionStarted) {
        setTime(savedTimer);
        setSessionStatus("Đang chạy");
        setIsSessionActive(true);
      }
    } else {
      const fetchSupportDuration = async () => {
        try {
          let duration;
          let status;
          if (userRole === "ROLE_MENTEE") {
            const response = await fetchSupportByIdMentee(id);
            duration = response.data.duration;
            status = response.data.supportStatus;
            if (
              response.data.menteeConfirmStart &&
              !response.data.mentorConfirmStart
            ) {
              setSessionStatus("Đang chờ");
            }
          } else if (userRole === "ROLE_MENTOR") {
            const response = await fetchSupportByIdMentor(id);
            duration = response.data.duration;
            status = response.data.supportStatus;
            if (
              response.data.mentorConfirmStart &&
              !response.data.menteeConfirmStart
            ) {
              setSessionStatus("Đang chờ");
            }
          }

          if (status !== "DONE") {
            localStorage.removeItem("requestTimer");
            setTime(duration * 3600);
          } else {
            setSessionStatus("Hoàn thành");
            localStorage.removeItem("requestTimer");
          }
        } catch (error) {
          console.error("Error fetching support details:", error);
        }
      };
      fetchSupportDuration();
    }
  }, [id, userRole]);

  useEffect(() => {
    const fetchUsernames = async () => {
      try {
        let response;
        if (userRole === "ROLE_MENTEE") {
          response = await fetchSupportByIdMentee(id);
          if (response && response.data) {
            setMentorUsername(response.data.mentorUserName);
          }
        } else if (userRole === "ROLE_MENTOR") {
          response = await fetchSupportByIdMentor(id);
          if (response && response.data) {
            setMenteeUsername(response.data.menteeUserName);
          }
        }
      } catch (error) {
        console.error("Error fetching usernames:", error);
      }
    };
    fetchUsernames();
  }, []);

  useEffect(() => {
    let timer;
    if (isSessionActive && time > 0 && isSessionStarted) {
      timer = setInterval(() => {
        setTime((prevTime) => {
          const newTime = prevTime - 1;
          if (newTime <= 0) {
            clearInterval(timer);
            return 0;
          }
          saveTimerState(newTime);
          return newTime;
        });
      }, 1000);
    } else {
      clearInterval(timer);
    }

    return () => clearInterval(timer);
  }, [isSessionActive, time]);

  useEffect(() => {
    saveTimerState(time);
  }, [time]);

  const handleStart = async () => {
    try {
      let res;
      let response;
      if (userRole === "ROLE_MENTEE") {
        response = await fetchSupportByIdMentee(id);
        if (
          response.data.mentorConfirmStart &&
          response.data.menteeConfirmStart
        ) {
          setSessionStatus("Đang chạy");
          setIsSessionActive(true);
          setIsSessionStarted(true);
        } else if (!isSessionActive && sessionStatus === "Bắt đầu") {
          res = await confirmSupportStartMentee(id);
          if (res.statusCode === 400) {
            toast.error(res.messages);
          } else if (
            res.data.mentorConfirmStart &&
            res.data.menteeConfirmStart
          ) {
            setSessionStatus("Đang chạy");
            setIsSessionActive(true);
            setIsSessionStarted(true);
          } else {
            setSessionStatus("Đang chờ");
          }
        }
      } else if (userRole === "ROLE_MENTOR") {
        if (!isSessionActive && sessionStatus === "Bắt đầu") {
          res = await confirmSupportStartMentor(id);
          if (res.data.mentorConfirmStart && res.data.menteeConfirmStart) {
            setSessionStatus("Đang chạy");
            setIsSessionActive(true);
            setIsSessionStarted(true);
          } else {
            setSessionStatus("Đang chờ");
          }
        }
      }
    } catch (error) {
      console.error("Error confirming support start:", error);
    }
  };

  const handleEndSession = async () => {
    try {
      await confirmSupportEnd(id);
      setIsSessionActive(false);
      setSessionStatus("Hoàn thành");
      localStorage.removeItem("requestTimer");
      setShowEndSessionModal(false);
      setShowModal(true);
    } catch (error) {
      console.error("Error confirming support end:", error);
    }
  };

  const formatTime = (timeInSeconds) => {
    const roundedTime = Math.round(timeInSeconds);
    const minutes = Math.floor(roundedTime / 60);
    const seconds = roundedTime % 60;
    return `${minutes.toString().padStart(2, "0")}:${seconds
      .toString()
      .padStart(2, "0")}`;
  };
  useEffect(() => {
    if (time === 0 && isSessionActive) {
      const endSession = async () => {
        try {
          const res = await confirmSupportEnd(id);
          if (res.statusCode === 200) {
            setIsSessionActive(false);
            setSessionStatus("Hoàn thành");
            localStorage.removeItem("requestTimer");
            if (userRole === "ROLE_MENTEE") {
              setShowModal(true);
            } else {
              toast.success("Phiên hỗ trợ đã kết thúc.");
              navigate("/mentor/chat");
            }
          }
        } catch (error) {
          console.error("Error confirming support end:", error);
        }
      };
      endSession();
    }
  }, [time, id, isSessionActive]);

  return (
    <>
      <div>
        <div className={`flex flex-col items-center`}>
          <div className={`border rounded p-5 text-center`}>
            <div className="text-5xl mb-4">{formatTime(time)}</div>
            <div className="flex">
              <Button
                variant="contained"
                color="primary"
                onClick={handleStart}
                disabled={
                  sessionStatus === "Đang chờ" ||
                  sessionStatus === "Đang chạy" ||
                  sessionStatus === "Hoàn thành"
                }
                sx={{ mr: 2 }}
              >
                {sessionStatus}
              </Button>
              {userRole === "ROLE_MENTEE" && (
                <Button
                  variant="outlined"
                  color="error"
                  disabled={!isSessionActive}
                  onClick={handleEndSessionClick}
                >
                  Kết thúc
                </Button>
              )}
            </div>
          </div>
        </div>
      </div>
      <div className="flex ml-14 mr-3 mt-4 border rounded">
        <div className="w-[70%] p-1">
          <ChatEngine
            height="83vh"
            projectID={"3115972e-266e-4915-b179-1d21a682a59d"}
            userName={currentUsername}
            userSecret={currentSecret}
            offset={vietnamTimeZoneOffset}
          />
        </div>
        <div className="w-[30%] ml-1 pl-2 border-l-2 border-gray-300 pt-2">
          {mentorUsername !== null ? (
            <CollapseCardSession username={mentorUsername} />
          ) : (
            <CollapseCardSession username={menteeUsername} />
          )}
        </div>
      </div>
      <Modal
        open={showEndSessionModal}
        onClose={handleCancelEndSession}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            width: 400,
            bgcolor: "background.paper",
            border: "2px solid #000",
            boxShadow: 24,
            p: 4,
          }}
        >
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Bạn có chắc muốn kết thúc không?
          </Typography>
          <Typography id="modal-modal-description" sx={{ mt: 2 }}>
            <div className="text-center">
              <Button onClick={handleEndSession}>Có</Button>
              <Button onClick={handleCancelEndSession}>Không</Button>
            </div>
          </Typography>
        </Box>
      </Modal>
      {showModal ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-[30%] my-6 mx-auto max-w-4xl">
              <div className="border-0 rounded-lg shadow-lg pt-5 relative w-full bg-white outline-none focus:outline-none">
                <div className="mb-5">
                  <div className="text-xl flex justify-center font-semibold text-sidebarTop p-5">
                    Đánh giá gia sư
                  </div>
                  <div className="flex justify-center">
                    {[1, 2, 3, 4, 5].map((rate) => (
                      <button
                        key={rate}
                        onClick={() => handleRating(rate)}
                        style={{
                          color: rate <= rating ? "#FACA15" : "#BFBFBF",
                        }}
                      >
                        <FaStar size={"55px"} />
                      </button>
                    ))}
                  </div>
                  <div className="mt-7 justify-center flex w-full">
                    <textarea
                      rows="3"
                      className="border-2 border-black w-full mx-5 rounded-lg shadow-md p-3"
                      placeholder="Hãy cho chúng tôi biết về cảm nhận của bạn..."
                      onChange={(e) => setFeedBack(e.target.value)}
                      maxLength={255}
                    ></textarea>
                  </div>
                </div>

                <div className="btnInModal">
                  <Button
                    type="submit"
                    variant="outlined"
                    sx={{
                      border: "2px solid #2B90D9",
                      textTransform: "none",
                      borderRadius: "12px",
                      height: "35px",
                      width: "115px",
                      color: "#000000",
                    }}
                    onClick={onHandleRatingForSupport}
                  >
                    Đánh giá
                  </Button>
                  <Button
                    variant="contained"
                    color="error"
                    sx={{
                      backgroundColor: "#FF0A0A",
                      textTransform: "none",
                      borderRadius: "12px",
                      height: "35px",
                      width: "115px",
                      color: "#ffffff",
                    }}
                    onClick={onCloseRating}
                  >
                    Đóng
                  </Button>
                </div>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </>
  );
};

export default RequestSession;
