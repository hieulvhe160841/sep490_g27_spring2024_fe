import React from "react";
import { ChatEngine } from "react-chat-engine";
import CollapseCard from "../../components/Card/CollapseCard";
import { useLocation } from "react-router-dom";
import { toast } from "react-toastify";

const Chat = () => {
  const vietnamTimeZoneOffset = +7;
  const location = useLocation();
  const { username } = location.state || { username: null };
  const currentUsername = localStorage.getItem("user");
  const currentSecret = localStorage.getItem("secret");

  return (
    <div className="flex ml-14 mr-3">
      <div className="w-[70%]">
        <ChatEngine
          height="83vh"
          projectID={import.meta.env.VITE_CHAT_ENGINE_PROJECT_ID}
          userName={currentUsername}
          userSecret={currentSecret}
          offset={vietnamTimeZoneOffset}
          onNewChat={(chat) => toast.success("Created New Chat Successfully")}
          onDeleteChat={(chat) => console.log(chat)}
        />
      </div>
      <div className="w-[30%] ml-1 pl-2 border-l-2 border-gray-300">
        <CollapseCard username={username} />
      </div>
    </div>
  );
};

export default Chat;
