import React from "react";
import { render, screen } from "@testing-library/react";
import TransactionDataTable from "../../components/Table/TransactionDataTable";
import userStore from "../../store/userStore";
import "@testing-library/jest-dom";

jest.mock("../../store/userStore");

describe("TransactionDataTable", () => {
  // Normal case: The component renders correctly when there are transactions
  it("renders correctly when there are transactions", () => {
    userStore.mockReturnValue({
      isLoading: false,
      listTransaction: [
        {
          transactionId: 1,
          amount: 100,
          transactionType: "Test Type",
          timeStamp: "2022-01-01",
        },
      ],
      fetchTransactions: jest.fn(),
    });

    render(<TransactionDataTable />);

    expect(screen.getByText("1")).toBeInTheDocument();
    expect(screen.getByText("100")).toBeInTheDocument();
    expect(screen.getByText("Test Type")).toBeInTheDocument();
    // Check if the formatted date is rendered
    expect(screen.getByText("2022-01-01")).toBeInTheDocument();
  });

  // Abnormal case: The component renders correctly when there are no transactions
  it("renders correctly when there are no transactions", () => {
    userStore.mockReturnValue({
      isLoading: false,
      listTransaction: [],
      fetchTransactions: jest.fn(),
    });

    render(<TransactionDataTable />);

    expect(
      screen.getByText("Trang web chưa có giao dịch nào :(")
    ).toBeInTheDocument();
  });

  // Boundary case: The component renders correctly when it is loading
  it("renders correctly when it is loading", () => {
    userStore.mockReturnValue({
      isLoading: true,
      listTransaction: [],
      fetchTransactions: jest.fn(),
    });

    render(<TransactionDataTable />);

    expect(screen.getByRole("img")).toBeInTheDocument();
  });
});
