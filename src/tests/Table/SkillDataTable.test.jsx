import React from "react";
import { render, screen } from "@testing-library/react";
import SkillDataTable from "../../components/Table/SkillDataTable";
import userStore from "../../store/userStore";
import "@testing-library/jest-dom";

jest.mock("../../store/userStore");

describe("SkillDataTable", () => {
  // Normal case: The component renders correctly when there are skills
  it("renders correctly when there are skills", () => {
    userStore.mockReturnValue({
      isLoading: false,
      listSkills: [
        {
          skillId: 1,
          skillName: "Test Skill",
          skillDescription: "Test Description",
          skillStatus: true,
        },
      ],
      fetchSkills: jest.fn(),
    });

    render(<SkillDataTable />);

    expect(screen.getByText("Test Skill")).toBeInTheDocument();
    expect(screen.getByText("Test Description")).toBeInTheDocument();
  });

  it("renders correctly when there are no skills", () => {
    userStore.mockReturnValue({
      isLoading: false,
      listSkills: [],
      fetchSkills: jest.fn(),
    });

    render(<SkillDataTable />);

    expect(
      screen.getByText("Trang web chưa có Skill nào :(")
    ).toBeInTheDocument();
  });

  it("renders correctly when it is loading", () => {
    userStore.mockReturnValue({
      isLoading: true,
      listSkills: [],
      fetchSkills: jest.fn(),
    });

    render(<SkillDataTable />);

    expect(screen.getByRole("img")).toBeInTheDocument();
  });
});
