import React from "react";
import { render, screen } from "@testing-library/react";
import MenteeTransactionDataTable from "../../components/Table/MenteeTransactionDataTable";
import menteeStore from "../../store/menteeStore";
import "@testing-library/jest-dom";

jest.mock("../../store/menteeStore");

describe("MenteeTransactionDataTable", () => {
  // Normal case: The component renders correctly when there are transactions
  it("renders correctly when there are transactions", () => {
    menteeStore.mockReturnValue({
      isLoading: false,
      listMenteeTransaction: [
        {
          denominationTransactionId: 1,
          orderId: "Test Order",
          value: 100,
          createdDate: "2024-03-29",
        },
      ],
      fetchMenteeTransaction: jest.fn(),
    });

    render(<MenteeTransactionDataTable />);

    expect(screen.getByText("Test Order")).toBeInTheDocument();
    expect(screen.getByText("100")).toBeInTheDocument();
    expect(screen.getByText("2024-03-29")).toBeInTheDocument();
  });

  // Abnormal case: The component renders correctly when there are no transactions
  it("renders correctly when there are no transactions", () => {
    menteeStore.mockReturnValue({
      isLoading: false,
      listMenteeTransaction: [],
      fetchMenteeTransaction: jest.fn(),
    });

    render(<MenteeTransactionDataTable />);

    expect(
      screen.getByText("Bạn chưa có giao dịch nào :(")
    ).toBeInTheDocument();
  });

  // Boundary case: The component renders correctly when it is loading
  it("renders correctly when it is loading", () => {
    menteeStore.mockReturnValue({
      isLoading: true,
      listMenteeTransaction: [],
      fetchMenteeTransaction: jest.fn(),
    });

    render(<MenteeTransactionDataTable />);

    expect(screen.getByRole("img")).toBeInTheDocument();
  });
});
