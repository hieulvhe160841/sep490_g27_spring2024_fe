import React from "react";
import { render, screen } from "@testing-library/react";
import UserDataTable from "../../components/Table/UserDataTable";
import userStore from "../../store/userStore";
import "@testing-library/jest-dom";

jest.mock("../../store/userStore");

describe("UserDataTable", () => {
  // Normal case: The component renders correctly when there are users
  it("renders correctly when there are users", () => {
    userStore.mockReturnValue({
      isLoading: false,
      listUsers: [
        {
          userId: 1,
          userName: "Test User",
          roleDTO: { roleName: "Test Role" },
          status: true,
        },
      ],
      fetchUsers: jest.fn(),
    });

    render(<UserDataTable />);

    expect(screen.getByText("Test User")).toBeInTheDocument();
    expect(screen.getByText("Test Role")).toBeInTheDocument();
    expect(screen.getByText("Hoạt động")).toBeInTheDocument();
  });

  // Abnormal case: The component renders correctly when there are no users
  it("renders correctly when there are no users", () => {
    userStore.mockReturnValue({
      isLoading: false,
      listUsers: [],
      fetchUsers: jest.fn(),
    });

    render(<UserDataTable />);

    // Check if the DataGrid is empty
    expect(screen.queryByRole("gridcell")).toBeNull();
  });

  // Boundary case: The component renders correctly when it is loading
  it("renders correctly when it is loading", () => {
    userStore.mockReturnValue({
      isLoading: true,
      listUsers: [],
      fetchUsers: jest.fn(),
    });

    render(<UserDataTable />);

    expect(screen.getByRole("img")).toBeInTheDocument();
  });
});
