import React from "react";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import { BrowserRouter as Router } from "react-router-dom";
import CourseRegistrationCard from "../../components/Card/CourseRegistrationCard";

describe("CourseRegistrationCard", () => {
  it("renders correctly", () => {
    const courseRegis = {
      courseName: "Test Course",
      mentorUserName: "Test Mentor",
      description: "Test Description",
      courseRequestId: 1,
    };

    const { getByText } = render(
      <Router>
        <CourseRegistrationCard courseRegis={courseRegis} />
      </Router>
    );

    expect(getByText(courseRegis.courseName)).toBeInTheDocument();
    expect(getByText(courseRegis.mentorUserName)).toBeInTheDocument();
    expect(getByText(courseRegis.description)).toBeInTheDocument();
  });

  it("navigates to course info page on button click", () => {
    const courseRegis = {
      courseName: "Test Course",
      mentorUserName: "Test Mentor",
      description: "Test Description",
      courseRequestId: 1,
    };

    const { getByText } = render(
      <Router>
        <CourseRegistrationCard courseRegis={courseRegis} />
      </Router>
    );

    fireEvent.click(getByText("Chi tiết"));
    expect(window.location.pathname).toBe(
      `/courseInfo/${courseRegis.courseRequestId}`
    );
  });

  it("renders correctly when courseRegis is an empty object", () => {
    const { queryByText } = render(
      <Router>
        <CourseRegistrationCard courseRegis={{}} />
      </Router>
    );

    expect(queryByText(/Test Course/i)).not.toBeInTheDocument();
    expect(queryByText(/Test Mentor/i)).not.toBeInTheDocument();
    expect(queryByText(/Test Description/i)).not.toBeInTheDocument();
  });

  it("renders correctly when courseRegis has maximum length properties", () => {
    const longCourseName = "a".repeat(255);
    const longMentorUserName = "b".repeat(255);
    const longDescription = "c".repeat(255);
    const courseRegis = {
      courseName: longCourseName,
      mentorUserName: longMentorUserName,
      description: longDescription,
      courseRequestId: 1,
    };

    const { getByText } = render(
      <Router>
        <CourseRegistrationCard courseRegis={courseRegis} />
      </Router>
    );

    expect(getByText(courseRegis.courseName)).toBeInTheDocument();
    expect(getByText(courseRegis.mentorUserName)).toBeInTheDocument();
    expect(getByText(courseRegis.description)).toBeInTheDocument();
  });
});
