import React from "react";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import CollapseCard from "../../components/Card/CollapseCard";

describe("CollapseCard", () => {
  it("renders correctly", () => {
    const { getByText } = render(<CollapseCard username="testUser" />);

    expect(getByText("Hướng dẫn Chat")).toBeInTheDocument();
    expect(getByText("Liên hệ với chúng tôi")).toBeInTheDocument();
  });

  it('toggles chat instructions when "Hướng dẫn Chat" button is clicked', async () => {
    const { getByText, findByText, queryByText } = render(
      <CollapseCard username="testUser" />
    );

    fireEvent.click(getByText("Hướng dẫn Chat"));
    expect(
      await findByText(
        /Chọn nút "\+" bên cạnh My Chats, nhập tiêu đề đoạn chat rồi nhấn Enter/i
      )
    ).toBeInTheDocument();

    fireEvent.click(getByText("Hướng dẫn Chat"));
    expect(
      queryByText(
        /Chọn nút "\+" bên cạnh My Chats, nhập tiêu đề đoạn chat rồi nhấn Enter/i
      )
    ).not.toBeInTheDocument();
  });

  it('toggles contact instructions when "Liên hệ với chúng tôi" button is clicked', async () => {
    const { getByText, findByText, queryByText } = render(
      <CollapseCard username="testUser" />
    );

    fireEvent.click(getByText("Liên hệ với chúng tôi"));
    expect(
      await findByText(
        /Nếu bạn có bất kỳ vấn đề gì liên quan đến các giao dịch, hãy chat với nhân viên của chúng tôi thông qua Tài khoản: "transactionStaff"./i
      )
    ).toBeInTheDocument();

    fireEvent.click(getByText("Liên hệ với chúng tôi"));
    expect(
      queryByText(
        /Nếu bạn có bất kỳ vấn đề gì liên quan đến các giao dịch, hãy chat với nhân viên của chúng tôi thông qua Tài khoản: "transactionStaff"./i
      )
    ).not.toBeInTheDocument();
  });

  it("renders correctly when username is null", () => {
    const { getByText } = render(<CollapseCard username={null} />);
  });
});
