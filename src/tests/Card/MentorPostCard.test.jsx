import React from "react";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import { useNavigate } from "react-router-dom";
import MentorPostCard from "../../components/Card/MentorPostCard";

const mockNavigate = jest.fn();

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useNavigate: () => mockNavigate,
}));

describe("MentorPostCard", () => {
  it("renders correctly", () => {
    const post = {
      postId: 1,
      postTitle: "Test Title",
      postCreatedUser: "Test User",
      price: "1000",
    };

    const { getByText } = render(<MentorPostCard post={post} />);

    expect(getByText(`ID Bài đăng: ${post.postId}`)).toBeInTheDocument();
    expect(getByText(`Tiêu đề: ${post.postTitle}`)).toBeInTheDocument();
    expect(getByText(`Học viên: ${post.postCreatedUser}`)).toBeInTheDocument();
    expect(
      getByText((content, node) => node.textContent.startsWith("Giá tiền:"))
    ).toBeInTheDocument();
  });

  it("calls useNavigate with correct path on button click", () => {
    const post = {
      postId: 1,
      postTitle: "Test Title",
      postCreatedUser: "Test User",
      price: "1000",
    };

    const { getByText } = render(<MentorPostCard post={post} />);

    fireEvent.click(getByText("Học viên"));
    expect(mockNavigate).toHaveBeenCalledWith(
      `/mentor/menteeDetails/${post.postCreatedUser}`
    );
  });

  it("renders correctly when post has maximum length properties", () => {
    const longString = "a".repeat(255); // adjust this to the actual maximum length
    const post = {
      postId: 1,
      postTitle: longString,
      postCreatedUser: longString,
      price: longString,
    };

    const { getByText } = render(<MentorPostCard post={post} />);

    expect(getByText(`Tiêu đề: ${post.postTitle}`)).toBeInTheDocument();
    expect(getByText(`Học viên: ${post.postCreatedUser}`)).toBeInTheDocument();
  });
});
