import React from "react";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import CollapseCardSession from "../../components/Card/CollapseCardSession";

describe("CollapseCardSession", () => {
  it("renders correctly", () => {
    const { getByText } = render(<CollapseCardSession username="testUser" />);

    expect(getByText("Hướng dẫn Chat")).toBeInTheDocument();
    expect(getByText("Google Meet")).toBeInTheDocument();
  });

  it('toggles chat instructions when "Hướng dẫn Chat" button is clicked', async () => {
    const { getByText, findByText, queryByText } = render(
      <CollapseCardSession username="testUser" />
    );

    fireEvent.click(getByText("Hướng dẫn Chat"));
    expect(
      await findByText(
        /Chọn nút "\+" bên cạnh My Chats, nhập tiêu đề đoạn chat rồi nhấn Enter/i
      )
    ).toBeInTheDocument();

    fireEvent.click(getByText("Hướng dẫn Chat"));
    expect(
      queryByText(
        /Chọn nút "\+" bên cạnh My Chats, nhập tiêu đề đoạn chat rồi nhấn Enter/i
      )
    ).not.toBeInTheDocument();
  });

  it('toggles meet instructions when "Google Meet" button is clicked', async () => {
    const { getByText, findByText, queryByText } = render(
      <CollapseCardSession username="testUser" />
    );

    fireEvent.click(getByText("Google Meet"));
    expect(
      await findByText(
        /Chọn nút "START GOOGLE MEET" bên dưới để truy cập vào Google Meet./i
      )
    ).toBeInTheDocument();

    fireEvent.click(getByText("Google Meet"));
    expect(
      queryByText(
        /Chọn nút "START GOOGLE MEET" bên dưới để truy cập vào Google Meet./i
      )
    ).not.toBeInTheDocument();
  });

  it("renders correctly when username is null", () => {
    const { getByText } = render(<CollapseCardSession username={null} />);

    expect(getByText("Hướng dẫn Chat")).toBeInTheDocument();
    expect(getByText("Google Meet")).toBeInTheDocument();
  });

  // Abnormal case: The component renders correctly when username is an empty string
  it("renders correctly when username is an empty string", () => {
    const { getByText } = render(<CollapseCardSession username="" />);

    expect(getByText("Hướng dẫn Chat")).toBeInTheDocument();
    expect(getByText("Google Meet")).toBeInTheDocument();
  });

  // Boundary case: The component renders correctly when username is a string of maximum length
  it("renders correctly when username is a string of maximum length", () => {
    const longUsername = "a".repeat(255); // adjust this to the actual maximum length
    const { getByText } = render(
      <CollapseCardSession username={longUsername} />
    );

    expect(getByText("Hướng dẫn Chat")).toBeInTheDocument();
    expect(getByText("Google Meet")).toBeInTheDocument();
  });
});
