import React from "react";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import { BrowserRouter as Router } from "react-router-dom";
import MentorRegistrationCard from "../../components/Card/MentorRegistrationCard";

describe("MentorRegistrationCard", () => {
  it("renders correctly", () => {
    const mentorRegis = {
      userCreatedMentorRequestDTO: {
        userId: 1,
        userName: "Test User",
        experience: "5 years",
        costPerHour: 1000,
        avatar: "avatar.jpg",
      },
      mentorRequestId: 1,
    };

    const { getByText } = render(
      <Router>
        <MentorRegistrationCard mentorRegis={mentorRegis} />
      </Router>
    );

    expect(
      getByText(mentorRegis.userCreatedMentorRequestDTO.userName)
    ).toBeInTheDocument();
    expect(
      getByText(mentorRegis.userCreatedMentorRequestDTO.experience)
    ).toBeInTheDocument();
    expect(
      getByText((content, node) =>
        node.textContent.startsWith(
          mentorRegis.userCreatedMentorRequestDTO.costPerHour.toLocaleString(
            "en-US",
            { maximumFractionDigits: 0 }
          )
        )
      )
    ).toBeInTheDocument();
  });

  it("navigates to mentor registration details page on button click", () => {
    const mentorRegis = {
      userCreatedMentorRequestDTO: {
        userId: 1,
        userName: "Test User",
        experience: "5 years",
        costPerHour: 1000,
        avatar: "avatar.jpg",
      },
      mentorRequestId: 1,
    };

    const { getByText } = render(
      <Router>
        <MentorRegistrationCard mentorRegis={mentorRegis} />
      </Router>
    );

    fireEvent.click(getByText("Chi tiết"));
    expect(window.location.pathname).toBe(
      `/mentorRegistration/${mentorRegis.userCreatedMentorRequestDTO.userId}/${mentorRegis.mentorRequestId}`
    );
  });

  // Boundary case: The component renders correctly when mentorRegis has maximum length properties
  it("renders correctly when mentorRegis has maximum length properties", () => {
    const longStringUsername = "a".repeat(255);
    const longStringExperience = "b".repeat(255);

    const mentorRegis = {
      userCreatedMentorRequestDTO: {
        userId: 1,
        userName: longStringUsername,
        experience: longStringExperience,
        costPerHour: 1000,
        avatar: "avatar.jpg",
      },
      mentorRequestId: 1,
    };

    const { getByText } = render(
      <Router>
        <MentorRegistrationCard mentorRegis={mentorRegis} />
      </Router>
    );

    expect(
      getByText(mentorRegis.userCreatedMentorRequestDTO.userName)
    ).toBeInTheDocument();
    expect(
      getByText(mentorRegis.userCreatedMentorRequestDTO.experience)
    ).toBeInTheDocument();
  });
});
