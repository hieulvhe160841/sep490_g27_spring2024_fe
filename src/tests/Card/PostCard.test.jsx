import React from "react";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import { BrowserRouter as Router } from "react-router-dom";
import PostCard from "../../components/Card/PostCard";

const mockNavigate = jest.fn();

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useNavigate: () => mockNavigate,
}));

describe("PostCard", () => {
  it("renders correctly", () => {
    const post = {
      postId: 1,
      postTitle: "Test Title",
      postCreatedUser: "Test User",
      postPrice: "1000",
    };

    const { getByText } = render(
      <Router>
        <PostCard post={post} />
      </Router>
    );

    expect(getByText(`ID Bài đăng: ${post.postId}`)).toBeInTheDocument();
    expect(getByText(`Tiêu đề: ${post.postTitle}`)).toBeInTheDocument();
    expect(getByText(`Học viên: ${post.postCreatedUser}`)).toBeInTheDocument();
    expect(getByText(`Price: ${post.postPrice}`)).toBeInTheDocument();
  });

  it("calls useNavigate with correct path on button click", () => {
    const post = {
      postId: 1,
      postTitle: "Test Title",
      postCreatedUser: "Test User",
      postPrice: "1000",
    };

    const { getByText } = render(
      <Router>
        <PostCard post={post} />
      </Router>
    );

    fireEvent.click(getByText("Học viên"));
    expect(mockNavigate).toHaveBeenCalledWith(
      `/admin/menteeDetails/${post.postCreatedUser}`
    );
  });
});
