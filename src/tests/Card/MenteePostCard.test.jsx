import React from "react";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom";
import MenteePostCard from "../../components/Card/MenteePostCard";

describe("MenteePostCard", () => {
  it("renders correctly", () => {
    const post = {
      postId: 1,
      postTitle: "Test Title",
      postStatus: true,
      postPrice: "1000",
    };

    const { getByText } = render(<MenteePostCard post={post} />);

    expect(
      getByText(
        (content, node) => node.textContent === `Bài đăng số: ${post.postId}`
      )
    ).toBeInTheDocument();
    expect(
      getByText((content, node) =>
        node.textContent.startsWith(`Tiêu đề: ${post.postTitle}`)
      )
    ).toBeInTheDocument();
    expect(
      getByText((content, node) => node.textContent === "Trạng thái: Waiting")
    ).toBeInTheDocument();
    expect(
      getByText((content, node) => node.textContent.startsWith("Giá tiền:"))
    ).toBeInTheDocument();
  });

  it("renders correctly when postStatus is false", () => {
    const post = {
      postId: 1,
      postTitle: "Test Title",
      postStatus: false,
      postPrice: "1000",
    };

    const { getByText } = render(<MenteePostCard post={post} />);

    expect(
      getByText((content, node) => node.textContent === "Trạng thái: Accepted")
    ).toBeInTheDocument();
  });

  it("renders correctly when post has maximum length properties", () => {
    const longString = "a".repeat(255); // adjust this to the actual maximum length
    const post = {
      postId: 1,
      postTitle: longString,
      postStatus: true,
      postPrice: longString,
    };

    const { getByText } = render(<MenteePostCard post={post} />);

    expect(
      getByText((content, node) =>
        node.textContent.startsWith(`Tiêu đề: ${post.postTitle}`)
      )
    ).toBeInTheDocument();
    expect(
      getByText((content, node) => node.textContent.startsWith("Giá tiền:"))
    ).toBeInTheDocument();
  });
});
