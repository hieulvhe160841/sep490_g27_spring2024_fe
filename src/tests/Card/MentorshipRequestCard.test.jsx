import React from "react";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import { BrowserRouter as Router } from "react-router-dom";
import MentorshipRequestCard from "../../components/Card/MentorshipRequestCard";

describe("MentorshipRequestCard", () => {
  it("renders correctly", () => {
    const request = {
      mentorName: "Test Mentor",
      menteeName: "Test Mentee",
      supportStatus: "Pending",
      supportCreatedDate: "2022-01-01",
      supportId: 1,
    };

    const { getByText } = render(
      <Router>
        <MentorshipRequestCard request={request} />
      </Router>
    );

    expect(getByText(`Gia sư: ${request.mentorName}`)).toBeInTheDocument();
    expect(getByText(`Học viên: ${request.menteeName}`)).toBeInTheDocument();
    expect(getByText(`Status: ${request.supportStatus}`)).toBeInTheDocument();
    expect(
      getByText(`Ngày tạo: ${request.supportCreatedDate}`)
    ).toBeInTheDocument();
  });

  it("navigates to request details page on button click", () => {
    const request = {
      mentorName: "Test Mentor",
      menteeName: "Test Mentee",
      supportStatus: "Pending",
      supportCreatedDate: "2022-01-01",
      supportId: 1,
    };

    const { getByText } = render(
      <Router>
        <MentorshipRequestCard request={request} />
      </Router>
    );

    fireEvent.click(getByText("Chi tiết"));
    expect(window.location.pathname).toBe(
      `/admin/requestDetail/${request.supportId}`
    );
  });
});
