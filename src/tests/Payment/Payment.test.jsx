import React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import Payment from "../../components/Payment/Payment";
import menteeStore from "../../store/menteeStore";
import userStore from "../../store/userStore";
import "@testing-library/jest-dom";

jest.mock("../../store/menteeStore");
jest.mock("../../store/userStore");

describe("Payment", () => {
  beforeEach(() => {
    menteeStore.mockImplementation(() => ({
      fetchListDenomination: jest
        .fn()
        .mockResolvedValue([{ denominationId: "1", value: 100, amount: 100 }]),
      listDenomination: [{ denominationId: "1", value: 100, amount: 100 }],
    }));

    userStore.mockImplementation(() => ({
      fetchUserWallet: jest.fn(),
      userBalance: { data: { balance: 1000 } },
      createPaymentApi: jest.fn().mockResolvedValue({
        statusCode: 201,
        data: { links: [{}, { href: "http://localhost:3000" }] },
      }),
      isLoading: false,
      isLoadingPayment: false,
    }));
  });

  it("renders without crashing", () => {
    const { container } = render(<Payment />);
    expect(container).toBeInTheDocument();
  });

  it('disables the "Nạp" button while a payment is being created', () => {
    // Mock the userStore to return isLoadingPayment as true
    userStore.mockImplementation(() => ({
      fetchUserWallet: jest.fn(),
      userBalance: { data: { balance: 1000 } },
      createPaymentApi: jest.fn(),
      isLoading: false,
      isLoadingPayment: true,
    }));

    const { getByText } = render(<Payment />);

    // Check if the "Nạp" button is disabled
    expect(getByText("Nạp").closest("button")).toBeDisabled();
  });
});
