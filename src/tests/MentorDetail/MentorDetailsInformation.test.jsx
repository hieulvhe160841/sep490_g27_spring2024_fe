import React from "react";
import { render } from "@testing-library/react";
import { BrowserRouter as Router } from "react-router-dom";
import DetailsInformation from "../../components/MentorDetails/DetailsInformation";
import "@testing-library/jest-dom";

describe("DetailsInformation", () => {
  it("renders without crashing", () => {
    const mentor = {
      mentorDetail: {
        userDetail: {
          experience: "Test Experience",
        },
      },
      certificates: [
        {
          certificateId: "1",
          certificateName: "Test Certificate",
        },
      ],
    };

    const { container } = render(
      <Router>
        <DetailsInformation mentor={mentor} />
      </Router>
    );

    expect(container).toBeInTheDocument();
  });

  it("displays the mentor's experience and certificates", () => {
    const mentor = {
      mentorDetail: {
        userDetail: {
          experience: "Test Experience",
        },
      },
      certificates: [
        {
          certificateId: "1",
          certificateName: "Test Certificate",
        },
      ],
    };

    const { getByText } = render(
      <Router>
        <DetailsInformation mentor={mentor} />
      </Router>
    );

    expect(getByText("Test Experience")).toBeInTheDocument();
    expect(getByText("Test Certificate")).toBeInTheDocument();
  });
});
