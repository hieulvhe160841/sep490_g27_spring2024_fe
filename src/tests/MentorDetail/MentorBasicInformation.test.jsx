import React from "react";
import "@testing-library/jest-dom";
import { render, fireEvent } from "@testing-library/react";
import { MemoryRouter, useNavigate } from "react-router-dom";
import userStore from "../../store/userStore";
import menteeStore from "../../store/menteeStore";
import BasicInformation from "../../components/MentorDetails/BasicInformation";

jest.mock("../../store/userStore");
jest.mock("../../store/menteeStore");
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useNavigate: jest.fn(),
}));

describe("BasicInformation", () => {
  let navigate;
  let baseUrl;

  beforeEach(() => {
    navigate = jest.fn();
    baseUrl = "http://localhost:3000";
    useNavigate.mockReturnValue(navigate);
    userStore.mockReturnValue({ baseUrl });
    menteeStore.mockReturnValue({ requestMentorDirectly: jest.fn() });
  });

  it("renders without crashing", () => {
    const mentor = {
      mentorDetail: {
        userDetail: {
          avatar: "avatar.jpg",
          name: "Test User",
          email: "test@example.com",
          educationLevel: "Test Level",
        },
        rating: 4.5,
        costPerHour: 100,
      },
      skills: [],
    };
    const username = "testuser";

    const { container } = render(
      <MemoryRouter>
        <BasicInformation mentor={mentor} username={username} />
      </MemoryRouter>
    );

    expect(container).toBeInTheDocument();
  });

  it("calls navigate when Liên hệ button is clicked", () => {
    const mentor = {
      mentorDetail: {
        userDetail: {
          avatar: "avatar.jpg",
          name: "Test User",
          email: "test@example.com",
          educationLevel: "Test Level",
        },
        rating: 4.5,
        costPerHour: 100,
      },
      skills: [],
    };
    const username = "testuser";

    const { getByText } = render(
      <MemoryRouter>
        <BasicInformation mentor={mentor} username={username} />
      </MemoryRouter>
    );

    fireEvent.click(getByText("Liên hệ"));

    expect(navigate).toHaveBeenCalled();
  });

  it("renders correctly when username has maximum length", () => {
    const longString = "a".repeat(255);
    const mentor = {
      mentorDetail: {
        userDetail: {
          avatar: "avatar.jpg",
          name: "Test User",
          email: "test@example.com",
          educationLevel: "Test Level",
        },
        rating: 4.5,
        costPerHour: 100,
      },
      skills: [],
    };

    const { getByText } = render(
      <MemoryRouter>
        <BasicInformation mentor={mentor} username={longString} />
      </MemoryRouter>
    );

    fireEvent.click(getByText("Liên hệ"));

    expect(navigate).toHaveBeenCalled();
  });
});
