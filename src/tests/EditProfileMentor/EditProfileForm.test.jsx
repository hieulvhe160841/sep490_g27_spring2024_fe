import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import EditProfileForm from "../../components/EditProfileMentor/EditProfileForm";

describe("EditProfileForm", () => {
  it("renders correctly when mentor is provided", () => {
    const mentor = {
      name: "Test Mentor",
      image: "test-image.jpg",
      description: "Test Description",
      skill: "Test Skill",
    };

    render(<EditProfileForm mentor={mentor} />);

    expect(screen.getByDisplayValue(mentor.name)).toBeInTheDocument();
    expect(screen.getByDisplayValue(mentor.description)).toBeInTheDocument();
    expect(screen.getByDisplayValue(mentor.skill)).toBeInTheDocument();
    expect(screen.getByDisplayValue("100")).toBeInTheDocument();
    expect(screen.getByDisplayValue("phD")).toBeInTheDocument();
    expect(screen.getByRole("img")).toHaveAttribute("src", mentor.image);
    expect(screen.getByRole("button", { name: /Lưu/i })).toBeInTheDocument();
  });
});
