import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import UserDetailInputForm from "../../components/InputForm/UserDetailInputForm";

describe("UserDetailInputForm", () => {
  const mockUser = {
    userName: "testUser",
    name: "Test User",
    phone: "1234567890",
    address: "123 Test St",
    status: true,
    gender: 1,
    dayOfBirth: "1990-01-01",
    avatar: "avatar.jpg",
    email: "test@example.com",
    roleDTO: {
      roleName: "ROLE_MENTOR",
    },
    educationLevel: "Bachelor",
    rating: "5",
    experience: "5 years",
    goal: "Learn React",
    interest: "Programming",
    dateCreated: "2022-01-01",
    dateModified: "2022-01-02",
  };

  it("renders without crashing", () => {
    render(<UserDetailInputForm user={mockUser} />);
    expect(screen.getByRole("button", { name: /Lưu/i })).toBeInTheDocument();
  });

  it("renders correctly when user is an empty object", () => {
    render(<UserDetailInputForm user={{}} />);
    expect(screen.getByRole("button", { name: /Lưu/i })).toBeInTheDocument();
  });

  it("renders correctly when userName is at maximum length", () => {
    const longString = "a".repeat(255);
    const user = { ...mockUser, userName: longString };

    render(<UserDetailInputForm user={user} />);
    expect(screen.getByRole("button", { name: /Lưu/i })).toBeInTheDocument();
  });
});
