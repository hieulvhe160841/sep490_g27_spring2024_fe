import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import SkillDetailInputForm from "../../components/InputForm/SkillDetailInputForm";

describe("SkillDetailInputForm", () => {
  const skill = {
    skillId: "1",
    skillName: "Test Skill",
    skillDescription: "Test Description",
    skillStatus: true,
  };

  it("renders correctly", () => {
    render(<SkillDetailInputForm skill={skill} />);

    expect(screen.getByDisplayValue(skill.skillId)).toBeInTheDocument();
    expect(screen.getByDisplayValue(skill.skillName)).toBeInTheDocument();
    expect(
      screen.getByDisplayValue(skill.skillDescription)
    ).toBeInTheDocument();
    expect(screen.getByRole("combobox").value).toBe(String(skill.skillStatus));
    expect(screen.getByRole("button", { name: /Lưu/i })).toBeInTheDocument();
  });

  it("updates on input change", () => {
    render(<SkillDetailInputForm skill={skill} />);

    const nameInput = screen.getByDisplayValue(skill.skillName);
    fireEvent.change(nameInput, { target: { value: "New Skill Name" } });
    expect(nameInput.value).toBe("New Skill Name");

    const descriptionInput = screen.getByDisplayValue(skill.skillDescription);
    fireEvent.change(descriptionInput, {
      target: { value: "New Skill Description" },
    });
    expect(descriptionInput.value).toBe("New Skill Description");

    const statusSelect = screen.getByRole("combobox");
    fireEvent.change(statusSelect, { target: { value: "false" } });
    expect(statusSelect.value).toBe("false");
  });

  it("updates on input change when skillName is at maximum length", () => {
    const longString = "a".repeat(255);
    const skill = {
      skillId: "1",
      skillName: longString,
      skillDescription: "Test Description",
      skillStatus: true,
    };

    render(<SkillDetailInputForm skill={skill} />);

    const nameInput = screen.getByDisplayValue(longString);
    fireEvent.change(nameInput, { target: { value: "New Skill Name" } });
    expect(nameInput.value).toBe("New Skill Name");
  });
});
