import React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom";
import { MemoryRouter } from "react-router-dom";
import userStore from "../../store/userStore";
import FormLogin from "../../components/Login/FormLogin";

jest.mock("../../store/userStore");

describe("FormLogin", () => {
  let loginContext;

  beforeEach(() => {
    loginContext = jest.fn();

    userStore.mockReturnValue({
      loginContext,
      isLoading: false,
    });
  });

  it("renders without crashing", () => {
    const { container } = render(
      <MemoryRouter>
        <FormLogin />
      </MemoryRouter>
    );
    expect(container).toBeInTheDocument();
  });

  it("calls loginContext when login button is clicked", async () => {
    const { getByText, getByPlaceholderText } = render(
      <MemoryRouter>
        <FormLogin />
      </MemoryRouter>
    );

    fireEvent.change(getByPlaceholderText("Nhập tài khoản..."), {
      target: { value: "testuser" },
    });
    fireEvent.change(getByPlaceholderText("Nhập mật khẩu..."), {
      target: { value: "testpassword" },
    });

    fireEvent.click(getByText("Đăng nhập"));

    await waitFor(() => {});

    expect(loginContext).toHaveBeenCalled();
  });

  it("calls onHandleEnterSubmit when Enter key is pressed in password field", async () => {
    const { getByPlaceholderText } = render(
      <MemoryRouter>
        <FormLogin />
      </MemoryRouter>
    );

    fireEvent.change(getByPlaceholderText("Nhập tài khoản..."), {
      target: { value: "testuser" },
    });
    fireEvent.change(getByPlaceholderText("Nhập mật khẩu..."), {
      target: { value: "testpassword" },
    });

    fireEvent.keyDown(getByPlaceholderText("Nhập mật khẩu..."), {
      keyCode: 13,
    });

    await waitFor(() => {});

    expect(loginContext).toHaveBeenCalled();
  });

  it("does not call loginContext when username or password is missing", async () => {
    const { getByText, getByPlaceholderText } = render(
      <MemoryRouter>
        <FormLogin />
      </MemoryRouter>
    );

    // Missing username
    fireEvent.change(getByPlaceholderText("Nhập mật khẩu..."), {
      target: { value: "testpassword" },
    });
    fireEvent.click(getByText("Đăng nhập"));
    await waitFor(() => {});
    expect(loginContext).not.toHaveBeenCalled();

    // Missing password
    fireEvent.change(getByPlaceholderText("Nhập tài khoản..."), {
      target: { value: "testuser" },
    });
    fireEvent.change(getByPlaceholderText("Nhập mật khẩu..."), {
      target: { value: "" },
    });
    fireEvent.click(getByText("Đăng nhập"));
    await waitFor(() => {});
    expect(loginContext).not.toHaveBeenCalled();
  });

  it("calls loginContext when username and password do not match any account", async () => {
    const { getByText, getByPlaceholderText } = render(
      <MemoryRouter>
        <FormLogin />
      </MemoryRouter>
    );

    fireEvent.change(getByPlaceholderText("Nhập tài khoản..."), {
      target: { value: "nonexistentuser" },
    });
    fireEvent.change(getByPlaceholderText("Nhập mật khẩu..."), {
      target: { value: "wrongpassword" },
    });
    fireEvent.click(getByText("Đăng nhập"));
    await waitFor(() => {});
    expect(loginContext).toHaveBeenCalled();
  });

  it("calls loginContext when password is '1234Abc#'", async () => {
    const { getByText, getByPlaceholderText } = render(
      <MemoryRouter>
        <FormLogin />
      </MemoryRouter>
    );

    fireEvent.change(getByPlaceholderText("Nhập tài khoản..."), {
      target: { value: "testuser" },
    });
    fireEvent.change(getByPlaceholderText("Nhập mật khẩu..."), {
      target: { value: "1234Abc#" },
    });
    fireEvent.click(getByText("Đăng nhập"));
    await waitFor(() => {});
    expect(loginContext).toHaveBeenCalled();
  });
});
