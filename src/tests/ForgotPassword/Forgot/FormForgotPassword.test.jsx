import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import { MemoryRouter } from "react-router-dom";
import FormForgotPassword from "../../../components/ForgotPassword/Forgot/FormForgotPassword";

describe("FormForgotPassword", () => {
  it("renders correctly", () => {
    render(
      <MemoryRouter>
        <FormForgotPassword />
      </MemoryRouter>
    );

    expect(screen.getByPlaceholderText("Nhập email...")).toBeInTheDocument();
    expect(screen.getByRole("button", { name: /Gửi mã/i })).toBeInTheDocument();
    expect(
      screen.getByRole("link", { name: /Quay lại trang Đăng nhập/i })
    ).toBeInTheDocument();
  });

  it("updates on input change", () => {
    render(
      <MemoryRouter>
        <FormForgotPassword />
      </MemoryRouter>
    );

    const input = screen.getByPlaceholderText("Nhập email...");
    fireEvent.change(input, { target: { value: "test@example.com" } });
    expect(input.value).toBe("test@example.com");
  });
});
