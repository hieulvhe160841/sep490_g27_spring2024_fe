import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import { MemoryRouter } from "react-router-dom";
import FormResetPassword from "../../../components/ForgotPassword/Reset/FormResetPassword";

describe("FormResetPassword", () => {
  it("renders correctly", () => {
    render(
      <MemoryRouter
        initialEntries={[
          { pathname: "/", state: { email: "test@example.com" } },
        ]}
      >
        <FormResetPassword />
      </MemoryRouter>
    );

    expect(
      screen.getByPlaceholderText("Nhập mật khẩu mới...")
    ).toBeInTheDocument();
    expect(
      screen.getByPlaceholderText("Xác nhận mật khẩu mới...")
    ).toBeInTheDocument();
    expect(
      screen.getByRole("button", { name: /Cài lại/i })
    ).toBeInTheDocument();
    expect(
      screen.getByRole("link", { name: /Quay lại trang Đăng nhập/i })
    ).toBeInTheDocument();
  });

  it("updates on input change", () => {
    render(
      <MemoryRouter
        initialEntries={[
          { pathname: "/", state: { email: "test@example.com" } },
        ]}
      >
        <FormResetPassword />
      </MemoryRouter>
    );

    const newPasswordInput = screen.getByPlaceholderText(
      "Nhập mật khẩu mới..."
    );
    fireEvent.change(newPasswordInput, { target: { value: "newPassword" } });
    expect(newPasswordInput.value).toBe("newPassword");

    const rePasswordInput = screen.getByPlaceholderText(
      "Xác nhận mật khẩu mới..."
    );
    fireEvent.change(rePasswordInput, { target: { value: "newPassword" } });
    expect(rePasswordInput.value).toBe("newPassword");
  });
});
