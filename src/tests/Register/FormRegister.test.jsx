import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import FormRegister from "../../components/Register/FormRegister";
import "@testing-library/jest-dom";
import { MemoryRouter } from "react-router-dom";

describe("FormRegister", () => {
  test("renders FormRegister component", () => {
    render(
      <MemoryRouter>
        <FormRegister />
      </MemoryRouter>
    );

    expect(
      screen.getByPlaceholderText("Nhập Tài khoản...")
    ).toBeInTheDocument();
    expect(screen.getByPlaceholderText("Nhập Tên...")).toBeInTheDocument();
    expect(screen.getByPlaceholderText("Nhập Email...")).toBeInTheDocument();
    expect(
      screen.getByPlaceholderText("Nhập Số điện thoại...")
    ).toBeInTheDocument();
    expect(screen.getByPlaceholderText("Nhập Mật khẩu...")).toBeInTheDocument();
    expect(screen.getByPlaceholderText("Nhập Địa chỉ...")).toBeInTheDocument();
    expect(screen.getByText("Giới tính:")).toBeInTheDocument();
    expect(screen.getByText("Đồng ý với")).toBeInTheDocument();
    expect(screen.getByText("Chính sách")).toBeInTheDocument();

    expect(screen.getByRole("button", { name: /Đăng ký/i })).toBeDisabled();
  });

  test('enables the "Đăng ký" button when all the form fields are filled and the checkbox is checked', () => {
    render(
      <MemoryRouter>
        <FormRegister />
      </MemoryRouter>
    );

    fireEvent.change(screen.getByPlaceholderText("Nhập Tài khoản..."), {
      target: { value: "testuser" },
    });
    fireEvent.change(screen.getByPlaceholderText("Nhập Tên..."), {
      target: { value: "Test User" },
    });
    fireEvent.change(screen.getByPlaceholderText("Nhập Email..."), {
      target: { value: "testuser@example.com" },
    });
    fireEvent.change(screen.getByPlaceholderText("Nhập Số điện thoại..."), {
      target: { value: "1234567890" },
    });
    fireEvent.change(screen.getByPlaceholderText("Nhập Mật khẩu..."), {
      target: { value: "password" },
    });
    fireEvent.change(screen.getByPlaceholderText("Nhập Địa chỉ..."), {
      target: { value: "123 Test Street" },
    });
    fireEvent.change(screen.getByLabelText("dob"), {
      target: { value: "1990-01-01" },
    });
    fireEvent.click(screen.getByLabelText("Nam"));

    fireEvent.click(screen.getByRole("checkbox"));

    expect(screen.getByRole("button", { name: /Đăng ký/i })).toBeEnabled();
  });
});
