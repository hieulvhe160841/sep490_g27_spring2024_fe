import React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import RatingForSupport from "../../components/Rating/RatingForSupport";
import menteeStore from "../../store/menteeStore";
import "@testing-library/jest-dom";

jest.mock("../../store/menteeStore");

describe("RatingForSupport", () => {
  beforeEach(() => {
    menteeStore.mockImplementation(() => ({
      menteeRatingSupportOfMentor: jest
        .fn()
        .mockResolvedValue({ statusCode: 201 }),
    }));
  });

  it('opens the modal when the "Đánh giá" button is clicked', () => {
    const { getByText } = render(
      <RatingForSupport request={{ supportId: "1" }} />
    );

    act(() => {
      fireEvent.click(getByText("Đánh giá"));
    });

    expect(getByText("Đánh giá gia sư")).toBeInTheDocument();
  });

  it('closes the modal when the "Đóng" button is clicked', () => {
    const { getByText, queryByText, getAllByRole } = render(
      <RatingForSupport request={{ supportId: "1" }} />
    );

    act(() => {
      fireEvent.click(getByText("Đánh giá"));
    });

    expect(getByText("Đánh giá gia sư")).toBeInTheDocument();

    // Click the "Đóng" button in the modal
    act(() => {
      fireEvent.click(getAllByRole("button")[7]);
    });

    expect(queryByText("Đánh giá gia sư")).not.toBeInTheDocument();
  });
});
