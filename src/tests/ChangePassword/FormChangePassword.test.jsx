import React from "react";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import { BrowserRouter as Router } from "react-router-dom";
import FormChangePassword from "../../components/ChangePassword/FormChangePassword";

const mockChangePasswordApi = jest.fn();
jest.mock("../../store/userStore", () => ({
  __esModule: true,
  default: () => ({
    changePasswordApi: mockChangePasswordApi,
    isLoading: false,
  }),
}));

const mockNavigate = jest.fn();
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useNavigate: () => mockNavigate,
}));

describe("FormChangePassword", () => {
  it("calls changePasswordApi with correct parameters on button click", () => {
    const { getByPlaceholderText, getByText } = render(
      <Router>
        <FormChangePassword />
      </Router>
    );

    fireEvent.change(getByPlaceholderText("Nhập mật khẩu cũ..."), {
      target: { value: "oldpassword" },
    });
    fireEvent.change(getByPlaceholderText("Nhập mật khẩu mới..."), {
      target: { value: "newpassword" },
    });
    fireEvent.change(getByPlaceholderText("Xác nhận mật khẩu mới..."), {
      target: { value: "newpassword" },
    });

    fireEvent.click(getByText("Change"));
    expect(mockChangePasswordApi).toHaveBeenCalledWith(
      "oldpassword",
      "newpassword",
      "newpassword"
    );
  });
});
