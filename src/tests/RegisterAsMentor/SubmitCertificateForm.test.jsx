import React from "react";
import { render, screen } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import SubmitCertificateForm from "../../components/RegisterAsMentor/SubmitCertificateForm";
import "@testing-library/jest-dom";

describe("SubmitCertificateForm", () => {
  test('renders SubmitCertificateForm component and shows "Gửi" button', () => {
    render(
      <MemoryRouter>
        <SubmitCertificateForm />
      </MemoryRouter>
    );

    // Check if the "Gửi" button is present
    expect(screen.getByRole("button", { name: /Gửi/i })).toBeInTheDocument();
  });
});
