import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import FormRegisterMentor from "../../components/RegisterAsMentor/FormRegisterMentor";
import "@testing-library/jest-dom";

describe("FormRegisterMentor", () => {
  test("renders FormRegisterMentor component", () => {
    render(
      <MemoryRouter>
        <FormRegisterMentor />
      </MemoryRouter>
    );

    expect(
      screen.getByPlaceholderText("Thêm trình độ học vấn...")
    ).toBeInTheDocument();
    expect(screen.getByPlaceholderText("Chọn kỹ năng")).toBeInTheDocument();
    expect(
      screen.getByPlaceholderText("Mô tả kinh nghiệm bản thân...")
    ).toBeInTheDocument();
    expect(screen.getByPlaceholderText("Thêm giá tiền...")).toBeInTheDocument();

    expect(
      screen.getByRole("button", { name: /Gửi thông tin/i })
    ).not.toBeDisabled();
  });

  it('keeps the "Gửi thông tin" button not disabled when the "Thêm trình độ học vấn..." field receives an invalid input', () => {
    render(
      <MemoryRouter>
        <FormRegisterMentor />
      </MemoryRouter>
    );

    fireEvent.change(screen.getByPlaceholderText("Thêm trình độ học vấn..."), {
      target: { value: "invalid input" }, // adjust this to an actual invalid input
    });

    expect(
      screen.getByRole("button", { name: /Gửi thông tin/i })
    ).not.toBeDisabled();
  });

  it('keeps the "Gửi thông tin" button not disabled when the "Mô tả kinh nghiệm bản thân..." field receives a too short input', () => {
    render(
      <MemoryRouter>
        <FormRegisterMentor />
      </MemoryRouter>
    );

    fireEvent.change(
      screen.getByPlaceholderText("Mô tả kinh nghiệm bản thân..."),
      {
        target: { value: "a" }, // adjust this to the actual minimum length - 1
      }
    );

    expect(
      screen.getByRole("button", { name: /Gửi thông tin/i })
    ).not.toBeDisabled();
  });
});
