import React from "react";
import { render, screen } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import ListRegistRequest from "../../components/RegisterAsMentor/ListRegistRequest";
import "@testing-library/jest-dom";

describe("ListRegistRequest", () => {
  test('renders ListRegistRequest component and shows "Đăng ký" button when there are no accepted requests', () => {
    const request = [
      { mentorRequestId: "1", mentorRequestStatus: "PENDING" },
      { mentorRequestId: "2", mentorRequestStatus: "PENDING" },
    ];

    render(
      <MemoryRouter>
        <ListRegistRequest request={request} />
      </MemoryRouter>
    );

    // Check if the "Đăng ký" button is present
    expect(
      screen.getByRole("button", { name: /Đăng ký/i })
    ).toBeInTheDocument();
  });

  it("renders correctly when request has maximum number of elements", () => {
    const request = Array(100).fill({
      mentorRequestId: "1",
      mentorRequestStatus: "PENDING",
    });

    render(
      <MemoryRouter>
        <ListRegistRequest request={request} />
      </MemoryRouter>
    );

    expect(
      screen.getByRole("button", { name: /Đăng ký/i })
    ).toBeInTheDocument();
  });
});
