import React from "react";
import "@testing-library/jest-dom";
import { render } from "@testing-library/react";
import MoreInformation from "../../components/MenteeDetail/MoreInformation";

describe("MoreInformation", () => {
  it("renders without crashing", () => {
    const mentee = {
      goal: "Test Goal",
      interest: "Test Interest",
    };

    const { container } = render(<MoreInformation mentee={mentee} />);

    expect(container).toBeInTheDocument();
  });

  it("displays the mentee's goal and interest", () => {
    const mentee = {
      goal: "Test Goal",
      interest: "Test Interest",
    };

    const { getByText } = render(<MoreInformation mentee={mentee} />);

    expect(getByText(mentee.goal)).toBeInTheDocument();
    expect(getByText(mentee.interest)).toBeInTheDocument();
  });

  it("renders correctly when mentee is an empty object", () => {
    const { queryByText } = render(<MoreInformation mentee={{}} />);

    expect(queryByText(/Test Goal/i)).not.toBeInTheDocument();
    expect(queryByText(/Test Interest/i)).not.toBeInTheDocument();
  });

  it("renders correctly when mentee has maximum length properties", () => {
    const longStringGoal = "a".repeat(255);
    const longString = "b".repeat(255);
    const mentee = {
      goal: longStringGoal,
      interest: longString,
    };

    const { getByText } = render(<MoreInformation mentee={mentee} />);

    expect(getByText(mentee.goal)).toBeInTheDocument();
    expect(getByText(mentee.interest)).toBeInTheDocument();
  });
});
