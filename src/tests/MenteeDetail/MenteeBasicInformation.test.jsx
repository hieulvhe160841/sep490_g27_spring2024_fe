import React from "react";
import { render, fireEvent } from "@testing-library/react";
import { MemoryRouter, useNavigate } from "react-router-dom";
import "@testing-library/jest-dom";
import userStore from "../../store/userStore";
import BasicInformation from "../../components/MenteeDetail/BasicInformation";

jest.mock("../../store/userStore");
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useNavigate: jest.fn(),
}));

describe("BasicInformation", () => {
  let navigate;
  let baseUrl;

  beforeEach(() => {
    navigate = jest.fn();
    baseUrl = "http://localhost:3000";
    useNavigate.mockReturnValue(navigate);
    userStore.mockReturnValue({ baseUrl });
  });

  it("renders without crashing", () => {
    const mentee = {
      avatar: "avatar.jpg",
      name: "Test User",
      phone: "1234567890",
      email: "test@example.com",
      address: "123 Test St",
    };
    const username = "testuser";

    const { container } = render(
      <MemoryRouter>
        <BasicInformation mentee={mentee} username={username} />
      </MemoryRouter>
    );

    expect(container).toBeInTheDocument();
  });

  it("calls navigate when Nhắn tin button is clicked", () => {
    const mentee = {
      avatar: "avatar.jpg",
      name: "Test User",
      phone: "1234567890",
      email: "test@example.com",
      address: "123 Test St",
    };
    const username = "testuser";

    const { getByText } = render(
      <MemoryRouter>
        <BasicInformation mentee={mentee} username={username} />
      </MemoryRouter>
    );

    fireEvent.click(getByText("Nhắn tin"));

    expect(navigate).toHaveBeenCalledWith("/mentee/chat", {
      state: { username },
    });
  });

  it("renders correctly when mentee is an empty object", () => {
    const username = "testuser";

    const { queryByText } = render(
      <MemoryRouter>
        <BasicInformation mentee={{}} username={username} />
      </MemoryRouter>
    );

    expect(queryByText(/Test User/i)).not.toBeInTheDocument();
    expect(queryByText(/1234567890/i)).not.toBeInTheDocument();
    expect(queryByText(/test@example.com/i)).not.toBeInTheDocument();
    expect(queryByText(/123 Test St/i)).not.toBeInTheDocument();
  });

  it("renders correctly when username has maximum length", () => {
    const longString = "a".repeat(255); // adjust this to the actual maximum length
    const mentee = {
      avatar: "avatar.jpg",
      name: "Test User",
      phone: "1234567890",
      email: "test@example.com",
      address: "123 Test St",
    };

    const { getByText } = render(
      <MemoryRouter>
        <BasicInformation mentee={mentee} username={longString} />
      </MemoryRouter>
    );

    fireEvent.click(getByText("Nhắn tin"));

    expect(navigate).toHaveBeenCalledWith("/mentee/chat", {
      state: { username: longString },
    });
  });
});
