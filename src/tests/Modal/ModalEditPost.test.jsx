import React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import ModalEditPost from "../../components/Modal/ModalEditPost";
import userStore from "../../store/userStore";
import "@testing-library/jest-dom";

jest.mock("../../store/userStore");

describe("ModalEditPost", () => {
  beforeEach(() => {
    userStore.mockImplementation(() => ({
      fetchPostById: jest.fn().mockResolvedValue({ data: {} }),
      updatePostStatus: jest.fn(),
    }));
  });

  it("renders without crashing", () => {
    const { container } = render(<ModalEditPost postId="1" />);
    expect(container).toBeInTheDocument();
  });

  it('opens the modal when the "Chi tiết" button is clicked', async () => {
    const { getByText } = render(<ModalEditPost postId="1" />);

    act(() => {
      fireEvent.click(getByText("Chi tiết"));
    });

    await waitFor(() =>
      expect(getByText("Chỉnh sửa bài đăng")).toBeInTheDocument()
    );
  });
});
