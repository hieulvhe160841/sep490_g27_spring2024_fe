import React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import ModalEditPostMentee from "../../components/Modal/ModalEditPostMentee";
import menteeStore from "../../store/menteeStore";
import "@testing-library/jest-dom";

jest.mock("../../store/menteeStore");

describe("ModalEditPostMentee", () => {
  beforeEach(() => {
    menteeStore.mockImplementation(() => ({
      updatePostById: jest.fn(),
      deletePostById: jest.fn(),
      fetchAllMenteePost: jest.fn(),
    }));
  });

  it("renders without crashing", () => {
    const { container } = render(<ModalEditPostMentee post={{}} />);
    expect(container).toBeInTheDocument();
  });

  it('opens the modal when the "Chi tiết" button is clicked', async () => {
    const { getByText } = render(<ModalEditPostMentee post={{}} />);

    act(() => {
      fireEvent.click(getByText("Chi tiết"));
    });

    await waitFor(() =>
      expect(getByText("Chỉnh sửa bài đăng")).toBeInTheDocument()
    );
  });
});
