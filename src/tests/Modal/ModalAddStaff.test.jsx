import React from "react";
import { render, fireEvent } from "@testing-library/react";
import ModalAddStaff from "../../components/Modal/ModalAddStaff";
import "@testing-library/jest-dom";

jest.mock("../../store/userStore", () => ({
  __esModule: true,
  default: () => ({
    addStaff: jest.fn(),
    fetchUsers: jest.fn(),
  }),
}));

describe("ModalAddStaff", () => {
  it("renders without crashing", () => {
    const { container } = render(<ModalAddStaff />);
    expect(container).toBeInTheDocument();
  });

  it("handles state and form submission", () => {
    const { getByText, getByLabelText } = render(<ModalAddStaff />);

    fireEvent.click(getByText("Thêm Nhân viên"));

    fireEvent.change(getByLabelText("Tài khoản"), {
      target: { value: "Test User" },
    });
    fireEvent.change(getByLabelText("Mật khẩu"), {
      target: { value: "Test Password" },
    });
    fireEvent.change(getByLabelText("Tên"), {
      target: { value: "Test Name" },
    });
    fireEvent.change(getByLabelText("Điện thoại"), {
      target: { value: "Test Phone" },
    });
    fireEvent.change(getByLabelText("Địa chỉ"), {
      target: { value: "Test Address" },
    });
    fireEvent.change(getByLabelText("Giới tính"), {
      target: { value: "male" },
    });
    fireEvent.change(getByLabelText("Ngày sinh"), {
      target: { value: "2022-01-01" },
    });
    fireEvent.change(getByLabelText("Role"), {
      target: { value: "transaction-staff" },
    });

    fireEvent.click(getByText("Thêm"));
  });
});
