import React from "react";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import ModalAddSkill from "../../components/Modal/ModalAddSkill";

jest.mock("../../store/userStore", () => ({
  __esModule: true,
  default: () => ({
    addSkill: jest.fn(),
    fetchSkills: jest.fn(),
  }),
}));

describe("ModalAddSkill", () => {
  it("renders without crashing", () => {
    const { container } = render(<ModalAddSkill />);
    expect(container).toBeInTheDocument();
  });

  it("handles state and form submission", () => {
    const { getByText, getByLabelText } = render(<ModalAddSkill />);

    fireEvent.click(getByText("Thêm mới"));

    fireEvent.change(getByLabelText("Tên kỹ năng"), {
      target: { value: "Test Skill" },
    });
    fireEvent.change(getByLabelText("Mô tả"), {
      target: { value: "Test Description" },
    });

    fireEvent.click(getByText("Thêm"));
  });
});
