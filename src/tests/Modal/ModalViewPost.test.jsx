import React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import ModalViewPost from "../../components/Modal/ModalViewPost";
import mentorStore from "../../store/mentorStore";
import "@testing-library/jest-dom";

jest.mock("../../store/mentorStore");

describe("ModalViewPost", () => {
  beforeEach(() => {
    mentorStore.mockImplementation(() => ({
      fetchPostById: jest.fn().mockResolvedValue({
        data: {
          postId: "1",
          postTitle: "Test",
          postContent: "Test content",
          postPrice: 100,
          postCreatedUser: "Test user",
        },
      }),
      acceptPost: jest.fn(),
      fetchSuggestPost: jest.fn(),
    }));
  });

  it("renders without crashing", () => {
    const { container } = render(<ModalViewPost postId="1" />);
    expect(container).toBeInTheDocument();
  });

  it('opens the modal when the "Chi tiết" button is clicked', async () => {
    const { getByText } = render(<ModalViewPost postId="1" />);

    act(() => {
      fireEvent.click(getByText("Chi tiết"));
    });

    await waitFor(() => expect(getByText("Xem bài đăng")).toBeInTheDocument());
  });
});
