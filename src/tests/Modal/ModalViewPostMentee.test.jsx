import React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import ModalViewPostMentee from "../../components/Modal/ModalViewPostMentee";
import menteeStore from "../../store/menteeStore";
import "@testing-library/jest-dom";

jest.mock("../../store/menteeStore");

describe("ModalViewPostMentee", () => {
  beforeEach(() => {
    menteeStore.mockImplementation(() => ({
      fetchPostByIdMentee: jest.fn().mockResolvedValue({
        postId: "1",
        postTitle: "Test",
        postContent: "Test content",
        postPrice: 100,
      }),
    }));
  });

  it("renders without crashing", () => {
    const { container } = render(<ModalViewPostMentee postId="1" />);
    expect(container).toBeInTheDocument();
  });

  it("displays the correct post details when showModal is true", async () => {
    const { getByText, getByDisplayValue } = render(
      <ModalViewPostMentee postId="1" showModal={true} />
    );

    await waitFor(() => {
      expect(getByText("Xem bài đăng")).toBeInTheDocument();
      expect(getByDisplayValue("1")).toBeInTheDocument();
      expect(getByDisplayValue("Test")).toBeInTheDocument();
      expect(getByDisplayValue("Test content")).toBeInTheDocument();
      expect(getByDisplayValue("100")).toBeInTheDocument();
    });
  });
});
