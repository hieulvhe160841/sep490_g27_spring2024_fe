import React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react";
import ModalAddPost from "../../components/Modal/ModalAddPost";
import "@testing-library/jest-dom";

jest.mock("../../store/menteeStore", () => ({
  __esModule: true,
  default: () => ({
    addPost: jest.fn(),
    fetchAllMenteePost: jest.fn(),
  }),
}));

describe("ModalAddPost", () => {
  it("renders without crashing", () => {
    const { container } = render(<ModalAddPost />);
    expect(container).toBeInTheDocument();
  });

  it("handles state and form submission", () => {
    const { getByText, getByLabelText } = render(<ModalAddPost />);

    fireEvent.click(getByText("Thêm mới"));

    fireEvent.change(getByLabelText("Tiêu đề"), {
      target: { value: "Test Title" },
    });
    fireEvent.change(getByLabelText("Nội dung"), {
      target: { value: "Test Content" },
    });
    fireEvent.change(getByLabelText("Giá tiền"), {
      target: { value: "1000" },
    });

    fireEvent.click(getByText("Thêm"));
  });

  it("handles addPost failure", async () => {
    const mockAddPost = jest.fn().mockRejectedValue(new Error("Network error"));
    jest.mock("../../store/menteeStore", () => ({
      __esModule: true,
      default: () => ({
        addPost: mockAddPost,
        fetchAllMenteePost: jest.fn(),
      }),
    }));

    const { getByText, getByLabelText } = render(<ModalAddPost />);

    fireEvent.click(getByText("Thêm mới"));

    fireEvent.change(getByLabelText("Tiêu đề"), {
      target: { value: "Test Title" },
    });
    fireEvent.change(getByLabelText("Nội dung"), {
      target: { value: "Test Content" },
    });
    fireEvent.change(getByLabelText("Giá tiền"), {
      target: { value: "1000" },
    });

    fireEvent.click(getByText("Thêm"));

    await waitFor(() => expect(mockAddPost).toHaveBeenCalled());
  });

  it("prevents form submission when fields are empty", () => {
    const { getByText, getByLabelText } = render(<ModalAddPost />);

    fireEvent.click(getByText("Thêm mới"));

    fireEvent.change(getByLabelText("Tiêu đề"), {
      target: { value: "" },
    });
    fireEvent.change(getByLabelText("Nội dung"), {
      target: { value: "" },
    });
    fireEvent.change(getByLabelText("Giá tiền"), {
      target: { value: "" },
    });

    fireEvent.click(getByText("Thêm"));
  });
});
