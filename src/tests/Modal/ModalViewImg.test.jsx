import React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import ModalViewImg from "../../components/Modal/ModalViewImg";
import userStore from "../../store/userStore";
import "@testing-library/jest-dom";

jest.mock("../../store/userStore");

describe("ModalViewImg", () => {
  beforeEach(() => {
    userStore.mockImplementation(() => ({
      baseUrl: "http://localhost:3000",
    }));
  });

  it("renders without crashing", () => {
    const { container } = render(
      <ModalViewImg cert={{ certificateUrl: "test.jpg" }} />
    );
    expect(container).toBeInTheDocument();
  });

  it('opens the modal when the "Xem" button is clicked', async () => {
    const { getByText } = render(
      <ModalViewImg cert={{ certificateUrl: "test.jpg" }} />
    );

    act(() => {
      fireEvent.click(getByText("Xem"));
    });

    await waitFor(() => expect(getByText("Đóng")).toBeInTheDocument());
  });
});
