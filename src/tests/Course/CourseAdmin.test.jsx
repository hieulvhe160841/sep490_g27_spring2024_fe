import React from "react";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import CourseAdmin from "../../components/Course/CourseAdmin";

const mockApproveCourseRequest = jest.fn();
jest.mock("../../store/userStore", () => ({
  __esModule: true,
  default: () => ({
    approveCourseRequest: mockApproveCourseRequest,
  }),
}));

describe("CourseAdmin", () => {
  it("renders correctly", () => {
    const courseRegis = {
      courseName: "Test Course",
      mentorUserName: "Test Mentor",
      description: "Test Description",
      courseRequestId: 1,
      price: 100,
      courseAvatar: "test.png",
    };

    const { getByText, getByAltText } = render(
      <CourseAdmin courseRegis={courseRegis} />
    );

    expect(getByText(courseRegis.courseName)).toBeInTheDocument();
    expect(
      getByText(`Gia sư: ${courseRegis.mentorUserName}`)
    ).toBeInTheDocument();
    expect(getByText(`Giá tiền: $${courseRegis.price}`)).toBeInTheDocument();
    expect(getByAltText("Course")).toHaveAttribute(
      "src",
      courseRegis.courseAvatar
    );
  });

  it("calls approveCourseRequest with correct parameters on approve button click", () => {
    const courseRegis = {
      courseName: "Test Course",
      mentorUserName: "Test Mentor",
      description: "Test Description",
      courseRequestId: 1,
      price: 100,
      courseAvatar: "test.png",
    };

    const { getByText, getByPlaceholderText } = render(
      <CourseAdmin courseRegis={courseRegis} />
    );

    fireEvent.change(getByPlaceholderText("Nhận xét..."), {
      target: { value: "Test feedback" },
    });

    fireEvent.click(getByText("Chấp thuận"));
    expect(mockApproveCourseRequest).toHaveBeenCalledWith(
      courseRegis.courseRequestId,
      "Test feedback",
      "accepted"
    );
  });

  it("calls approveCourseRequest with correct parameters on reject button click", () => {
    const courseRegis = {
      courseName: "Test Course",
      mentorUserName: "Test Mentor",
      description: "Test Description",
      courseRequestId: 1,
      price: 100,
      courseAvatar: "test.png",
    };

    const { getByText, getByPlaceholderText } = render(
      <CourseAdmin courseRegis={courseRegis} />
    );

    fireEvent.change(getByPlaceholderText("Nhận xét..."), {
      target: { value: "Test feedback" },
    });

    fireEvent.click(getByText("Từ chối"));
    expect(mockApproveCourseRequest).toHaveBeenCalledWith(
      courseRegis.courseRequestId,
      "Test feedback",
      "rejected"
    );
  });
});
