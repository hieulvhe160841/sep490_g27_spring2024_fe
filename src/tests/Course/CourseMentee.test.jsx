import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import CourseMentee from "../../components/Course/CourseMentee";

describe("CourseMentee", () => {
  it("renders correctly when course is provided", () => {
    const course = {
      name: "Test Course",
      mentor: "Test Mentor",
      price: 100,
      description: "Test Description",
    };

    const { getByText, getByAltText } = render(
      <CourseMentee course={course} />
    );

    expect(getByText(course.name)).toBeInTheDocument();
    expect(getByText(`Gia sư: ${course.mentor}`)).toBeInTheDocument();
    expect(getByText(`Giá tiền: $${course.price}`)).toBeInTheDocument();
    expect(getByText(course.description)).toBeInTheDocument();
    expect(getByAltText("Course")).toBeInTheDocument();
  });

  it("renders LoadingState when course is not provided", () => {
    render(<CourseMentee />);

    expect(screen.getByRole("img")).toBeInTheDocument();
  });
});
