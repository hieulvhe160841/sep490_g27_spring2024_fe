import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import userStore from "../../store/userStore";
import TransactionChart from "../../components/AdminDashboard/TransactionChart";

jest.mock("../../store/userStore");
jest.mock("../../components/LoadingState", () => () => <div>Loading...</div>);

describe("TransactionChart", () => {
  beforeEach(() => {
    userStore.mockClear();
  });

  test("renders LoadingState when isLoading is true", () => {
    userStore.mockReturnValue({
      isLoading: true,
      adminDashboard: { numberOfUserAndCoursePerMonthDTOList: [] },
      fetchAdminDashboard: jest.fn(),
    });

    render(<TransactionChart />);
    expect(screen.getByText("Loading...")).toBeInTheDocument();
  });
});
