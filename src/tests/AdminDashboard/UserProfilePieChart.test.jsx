import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import userStore from "../../store/userStore";
import UserProfilePieChart from "../../components/AdminDashboard/UserProfilePieChart";

jest.mock("../../store/userStore");
jest.mock("../../components/LoadingState", () => () => <div>Loading...</div>);

describe("UserProfilePieChart", () => {
  beforeEach(() => {
    userStore.mockClear();
  });

  test("renders LoadingState when isLoading is true", () => {
    userStore.mockReturnValue({
      isLoading: true,
      adminDashboard: {
        numberOfMentors: 0,
        numberOfMentees: 0,
        numberOfOthers: 0,
      },
      fetchAdminDashboard: jest.fn(),
    });

    render(<UserProfilePieChart />);
    expect(screen.getByText("Loading...")).toBeInTheDocument();
  });
});
