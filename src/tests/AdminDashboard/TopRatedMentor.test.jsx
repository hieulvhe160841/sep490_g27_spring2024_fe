import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import { BrowserRouter as Router } from "react-router-dom";
import userStore from "../../store/userStore";
import TopRatedMentor from "../../components/AdminDashboard/TopRatedMentor";

jest.mock("../../store/userStore");
jest.mock("../../components/LoadingState", () => () => <div>Loading...</div>);

describe("TopRatedMentor", () => {
  beforeEach(() => {
    userStore.mockClear();
  });

  test("renders without crashing", () => {
    userStore.mockReturnValue({
      isLoading: false,
      adminDashboard: { topRatedMentor: [] },
      fetchAdminDashboard: jest.fn(),
    });

    render(
      <Router>
        <TopRatedMentor />
      </Router>
    );
    expect(screen.getByText("Gia sư đánh giá cao")).toBeInTheDocument();
  });

  test("renders LoadingState when isLoading is true", () => {
    userStore.mockReturnValue({
      isLoading: true,
      adminDashboard: { topRatedMentor: [] },
      fetchAdminDashboard: jest.fn(),
    });

    render(
      <Router>
        <TopRatedMentor />
      </Router>
    );
    expect(screen.getByText("Loading...")).toBeInTheDocument();
  });

  test("renders mentor data when isLoading is false", () => {
    const mockData = {
      topRatedMentor: [
        {
          userDetail: {
            userId: 1,
            name: "Mentor 1",
            avatar: "avatar1.jpg",
            educationLevel: "PhD",
          },
          rating: 5,
        },
      ],
    };
    userStore.mockReturnValue({
      isLoading: false,
      adminDashboard: mockData,
      fetchAdminDashboard: jest.fn(),
      baseUrl: "http://localhost",
    });

    render(
      <Router>
        <TopRatedMentor />
      </Router>
    );
    expect(screen.getByText("Mentor 1")).toBeInTheDocument();
    expect(screen.getByText("PhD")).toBeInTheDocument();
    expect(screen.getByText("Đánh giá: 5")).toBeInTheDocument();
  });

  test("renders error message when topRatedMentor is null", () => {
    userStore.mockReturnValue({
      isLoading: false,
      adminDashboard: { topRatedMentor: null },
      fetchAdminDashboard: jest.fn(),
    });

    render(
      <Router>
        <TopRatedMentor />
      </Router>
    );
    expect(screen.getByText(/Error loading mentors/)).toBeInTheDocument();
  });
});
