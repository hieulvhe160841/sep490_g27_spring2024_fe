import React from "react";
import { render, screen, act } from "@testing-library/react";
import "@testing-library/jest-dom";
import DashboardStatsGrid from "../../components/AdminDashboard/DashboardStatsGrid";
import userStore from "../../store/userStore";
import LoadingState from "../../components/LoadingState";

jest.mock("../../store/userStore");
jest.mock("../../components/LoadingState");

describe("DashboardStatsGrid", () => {
  beforeEach(() => {
    userStore.mockClear();
    LoadingState.mockClear();
  });

  test("renders without crashing", () => {
    userStore.mockReturnValue({
      isLoading: false,
      adminDashboard: {},
      fetchAdminDashboard: jest.fn(),
    });

    render(<DashboardStatsGrid />);
    expect(screen.getByTestId("dashboard-stats-grid")).toBeInTheDocument();
  });

  test("calls fetchAdminDashboard on mount", () => {
    const mockFetchAdminDashboard = jest.fn();
    userStore.mockReturnValue({
      isLoading: false,
      adminDashboard: {},
      fetchAdminDashboard: mockFetchAdminDashboard,
    });

    render(<DashboardStatsGrid />);
    expect(mockFetchAdminDashboard).toHaveBeenCalledTimes(1);
  });

  test("renders LoadingState when isLoading is true", () => {
    userStore.mockReturnValue({
      isLoading: true,
      adminDashboard: {},
      fetchAdminDashboard: jest.fn(),
    });

    render(<DashboardStatsGrid />);
    expect(LoadingState).toHaveBeenCalledTimes(1);
  });

  test("renders correct data when isLoading is false", () => {
    const mockData = {
      totalProfit: 1000,
      totalRequest: 200,
      totalUsers: 300,
      totalPost: 400,
    };
    userStore.mockReturnValue({
      isLoading: false,
      adminDashboard: mockData,
      fetchAdminDashboard: jest.fn(),
    });

    render(<DashboardStatsGrid />);
    expect(screen.getByText(/200/)).toBeInTheDocument();
    expect(screen.getByText(/300/)).toBeInTheDocument();
    expect(screen.getByText(/400/)).toBeInTheDocument();
  });

  test("renders error message when adminDashboard is null", () => {
    userStore.mockReturnValue({
      isLoading: false,
      adminDashboard: null,
      fetchAdminDashboard: jest.fn(),
    });

    render(<DashboardStatsGrid />);
    const errorMessages = screen.getAllByText(/Error loading data/);
    expect(errorMessages.length).toBe(3);
  });
});
