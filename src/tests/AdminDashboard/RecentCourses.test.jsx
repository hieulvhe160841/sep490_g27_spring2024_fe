import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import { BrowserRouter as Router } from "react-router-dom";
import userStore from "../../store/userStore";
import RecentCourses from "../../components/AdminDashboard/RecentCourses";

jest.mock("../../store/userStore");
jest.mock("../../components/LoadingState", () => () => <div>Loading...</div>);

describe("RecentCourses", () => {
  beforeEach(() => {
    userStore.mockClear();
  });

  test("renders without crashing", () => {
    userStore.mockReturnValue({
      isLoading: false,
      adminDashboard: { recentCourseForAdminDashBoardList: [] },
      fetchAdminDashboard: jest.fn(),
    });

    render(
      <Router>
        <RecentCourses />
      </Router>
    );
    expect(screen.getByText("Recent Courses")).toBeInTheDocument();
  });

  test("renders LoadingState when isLoading is true", () => {
    userStore.mockReturnValue({
      isLoading: true,
      adminDashboard: { recentCourseForAdminDashBoardList: [] },
      fetchAdminDashboard: jest.fn(),
    });

    render(
      <Router>
        <RecentCourses />
      </Router>
    );
    expect(screen.getByText("Loading...")).toBeInTheDocument();
  });

  test("renders course data when isLoading is false", () => {
    const mockData = {
      recentCourseForAdminDashBoardList: [
        {
          courseId: 1,
          courseName: "Course 1",
          viewCourseOwnerDTO: { name: "Author 1" },
          courseRating: 5,
          price: 1000,
          numbersOfLesson: 10,
          courseCreatedDate: "2022-01-01",
        },
      ],
    };
    userStore.mockReturnValue({
      isLoading: false,
      adminDashboard: mockData,
      fetchAdminDashboard: jest.fn(),
    });

    render(
      <Router>
        <RecentCourses />
      </Router>
    );
    expect(screen.getByText("Course 1")).toBeInTheDocument();
    expect(screen.getByText("Author 1")).toBeInTheDocument();
    expect(screen.getByText("5")).toBeInTheDocument();
    expect(screen.getByText(/1,000/)).toBeInTheDocument();
    expect(screen.getByText("10")).toBeInTheDocument();
    expect(screen.getByText("2022-01-01")).toBeInTheDocument();
  });

  test("renders error message when recentCourseForAdminDashBoardList is null", () => {
    userStore.mockReturnValue({
      isLoading: false,
      adminDashboard: { recentCourseForAdminDashBoardList: null },
      fetchAdminDashboard: jest.fn(),
    });

    render(
      <Router>
        <RecentCourses />
      </Router>
    );
    expect(screen.getByText(/Error loading courses/)).toBeInTheDocument();
  });
});
