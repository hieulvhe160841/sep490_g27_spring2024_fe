import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import UpdateProfile from "../../../components/UserProfile/Mentee/UpdateProfile/UpdateProfile";
import menteeStore from "../../../store/menteeStore";
import "@testing-library/jest-dom";

jest.mock("../../../store/menteeStore");

describe("UpdateProfile", () => {
  // Normal case: The component renders correctly when there is a mentee
  it("renders correctly when there is a mentee", () => {
    menteeStore.mockReturnValue({
      isLoading: false,
      updateProfileMentee: jest.fn(),
    });

    render(
      <MemoryRouter
        initialEntries={[
          {
            pathname: "/",
            state: {
              avatar: "avatar.jpg",
              name: "Test Mentee",
              phone: "1234567890",
              email: "test@example.com",
              address: "Test Address",
              goal: "Test Goal",
              interest: "Test Interest",
            },
          },
        ]}
      >
        <UpdateProfile />
      </MemoryRouter>
    );

    expect(screen.getByText("Tải lên")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Test Mentee")).toBeInTheDocument();
    expect(screen.getByDisplayValue("1234567890")).toBeInTheDocument();
    expect(screen.getByDisplayValue("test@example.com")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Test Address")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Test Goal")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Test Interest")).toBeInTheDocument();
  });

  // Abnormal case: The component renders correctly when there is no mentee
  it("renders correctly when there is no mentee", () => {
    menteeStore.mockReturnValue({
      isLoading: false,
      updateProfileMentee: jest.fn(),
    });

    render(
      <MemoryRouter initialEntries={[{ pathname: "/", state: {} }]}>
        <UpdateProfile />
      </MemoryRouter>
    );

    expect(screen.getByText("Tải lên")).toBeInTheDocument();
    expect(screen.queryByDisplayValue("Test Mentee")).toBeNull();
    expect(screen.queryByDisplayValue("1234567890")).toBeNull();
    expect(screen.queryByDisplayValue("test@example.com")).toBeNull();
    expect(screen.queryByDisplayValue("Test Address")).toBeNull();
    expect(screen.queryByDisplayValue("Test Goal")).toBeNull();
    expect(screen.queryByDisplayValue("Test Interest")).toBeNull();
  });

  // Boundary case: The component renders correctly when it is loading
  it("renders correctly when it is loading", () => {
    menteeStore.mockReturnValue({
      isLoading: true,
      updateProfileMentee: jest.fn(),
    });

    render(
      <MemoryRouter
        initialEntries={[
          {
            pathname: "/",
            state: {
              avatar: "avatar.jpg",
              name: "Test Mentee",
              phone: "1234567890",
              email: "test@example.com",
              address: "Test Address",
              goal: "Test Goal",
              interest: "Test Interest",
            },
          },
        ]}
      >
        <UpdateProfile />
      </MemoryRouter>
    );

    expect(screen.getByText("Tải lên")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Test Mentee")).toBeInTheDocument();
    expect(screen.getByDisplayValue("1234567890")).toBeInTheDocument();
    expect(screen.getByDisplayValue("test@example.com")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Test Address")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Test Goal")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Test Interest")).toBeInTheDocument();
    expect(screen.getByText("Lưu")).toBeDisabled();
  });
});
