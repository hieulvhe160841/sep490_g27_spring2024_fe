import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import UpdateBasicInformation from "../../../components/UserProfile/Mentor/UpdateProfile/UpdateBasicInformation";
import mentorStore from "../../../store/mentorStore";
import "@testing-library/jest-dom";

jest.mock("../../../store/mentorStore");

describe("UpdateBasicInformation", () => {
  // Normal case: The component renders correctly when there is a mentor
  it("renders correctly when there is a mentor", () => {
    mentorStore.mockReturnValue({
      isLoading: false,
      updateProfileMentor: jest.fn(),
    });

    render(
      <MemoryRouter
        initialEntries={[
          {
            pathname: "/",
            state: {
              mentorDetail: {
                userDetail: {
                  name: "Test Mentor",
                  phone: "1234567890",
                  email: "test@example.com",
                  address: "Test Address",
                  educationLevel: "Test Education Level",
                  experience: "Test Experience",
                },
                costPerHour: 1000,
              },
              skills: [],
              certificates: [],
            },
          },
        ]}
      >
        <UpdateBasicInformation />
      </MemoryRouter>
    );

    expect(screen.getByText("Tải lên")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Test Mentor")).toBeInTheDocument();
    expect(screen.getByDisplayValue("1234567890")).toBeInTheDocument();
    expect(screen.getByText("test@example.com")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Test Address")).toBeInTheDocument();
    expect(
      screen.getByDisplayValue("Test Education Level")
    ).toBeInTheDocument();
    expect(screen.getByDisplayValue("Test Experience")).toBeInTheDocument();
    expect(screen.getByDisplayValue("1,000")).toBeInTheDocument();
  });

  // Boundary case: The component renders correctly when it is loading
  it("renders correctly when it is loading", () => {
    mentorStore.mockReturnValue({
      isLoading: true,
      updateProfileMentor: jest.fn(),
    });

    render(
      <MemoryRouter
        initialEntries={[
          {
            pathname: "/",
            state: {
              mentorDetail: {
                userDetail: {
                  name: "Test Mentor",
                  phone: "1234567890",
                  email: "test@example.com",
                  address: "Test Address",
                  educationLevel: "Test Education Level",
                  experience: "Test Experience",
                },
                costPerHour: 1000,
              },
              skills: [],
              certificates: [],
            },
          },
        ]}
      >
        <UpdateBasicInformation />
      </MemoryRouter>
    );

    expect(screen.getByText("Tải lên")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Test Mentor")).toBeInTheDocument();
    expect(screen.getByDisplayValue("1234567890")).toBeInTheDocument();
    expect(screen.getByText("test@example.com")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Test Address")).toBeInTheDocument();
    expect(
      screen.getByDisplayValue("Test Education Level")
    ).toBeInTheDocument();
    expect(screen.getByDisplayValue("Test Experience")).toBeInTheDocument();
    expect(screen.getByDisplayValue("1,000")).toBeInTheDocument();
    expect(screen.getByText("Lưu")).toBeDisabled();
  });
});
