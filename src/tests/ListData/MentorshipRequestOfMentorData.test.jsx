import React from "react";
import { render, waitFor, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import { MemoryRouter } from "react-router-dom";
import MentorshipRequestOfMentorData from "../../components/ListData/MentorshipRequestOfMentorData";
import mentorStore from "../../store/mentorStore";
import userStore from "../../store/userStore";

jest.mock("../../store/mentorStore");
jest.mock("../../store/userStore");

describe("MentorshipRequestOfMentorData", () => {
  let fetchMentorshipRequest;
  let mentorAcceptOrRejectRequest;

  beforeEach(() => {
    fetchMentorshipRequest = jest.fn();
    mentorAcceptOrRejectRequest = jest
      .fn()
      .mockResolvedValue({ statusCode: 200 });

    mentorStore.mockReturnValue({
      fetchMentorshipRequest,
      listMentorshipRequest: [
        {
          supportId: 1,
          supportStatus: "WAITING",
          postId: null,
          // Add other necessary properties here
        },
      ],
      mentorAcceptOrRejectRequest,
      isLoading: false,
    });

    userStore.mockReturnValue({
      baseUrl: "http://localhost",
    });
  });

  it("renders without crashing", () => {
    const { container } = render(
      <MemoryRouter>
        <MentorshipRequestOfMentorData />
      </MemoryRouter>
    );
    expect(container).toBeInTheDocument();
  });

  it("calls fetchMentorshipRequest when rendered", async () => {
    render(
      <MemoryRouter>
        <MentorshipRequestOfMentorData />
      </MemoryRouter>
    );
    await waitFor(() => expect(fetchMentorshipRequest).toHaveBeenCalled());
  });

  it('calls mentorAcceptOrRejectRequest when the "Chấp nhận" button is clicked', async () => {
    const { getByText } = render(
      <MemoryRouter>
        <MentorshipRequestOfMentorData />
      </MemoryRouter>
    );
    fireEvent.click(getByText("Chấp nhận"));
    await waitFor(() => expect(mentorAcceptOrRejectRequest).toHaveBeenCalled());
  });
});
