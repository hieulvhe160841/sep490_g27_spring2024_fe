import React from "react";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import MentorshipRequestOfMentee from "../../components/ListData/MentorshipRequestOfMentee";
import menteeStore from "../../store/menteeStore";
import userStore from "../../store/userStore";

jest.mock("../../store/menteeStore");
jest.mock("../../store/userStore");

describe("MentorshipRequestOfMentee", () => {
  beforeEach(() => {
    menteeStore.mockReturnValue({
      mentorshipRequestOfMentee: jest.fn(),
      listMentorshipRequest: [
        {
          supportId: 1,
          supportStatus: "WAITING",
          postId: 1,
          // Add other necessary properties here
        },
      ],
      menteeAcceptOrRejectRequest: jest.fn(),
      isLoading: false,
    });

    userStore.mockReturnValue({
      baseUrl: "http://localhost",
    });
  });

  it("renders without crashing", () => {
    const { container } = render(
      <MentorshipRequestOfMentee onOpenModal={jest.fn()} />
    );
    expect(container).toBeInTheDocument();
  });

  it("calls onOpenModal when the button is clicked", () => {
    const handleOpenModal = jest.fn();
    const { getByText } = render(
      <MentorshipRequestOfMentee onOpenModal={handleOpenModal} />
    );
    fireEvent.click(getByText("Chi tiết"));
    expect(handleOpenModal).toHaveBeenCalled();
  });
});
