import React from "react";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { BrowserRouter as Router } from "react-router-dom";
import BookedMentorData from "../../components/ListData/BookedMentorData";

const mockFetchBookedMentors = jest.fn();

jest.mock("../../store/menteeStore", () => ({
  __esModule: true,
  default: () => ({
    fetchBookedMentors: mockFetchBookedMentors,
    listBookedMentors: [],
    isLoading: false,
  }),
}));

jest.mock("../../store/userStore", () => ({
  __esModule: true,
  default: () => ({
    baseUrl: "http://localhost",
  }),
}));

describe("BookedMentorData", () => {
  it("renders without crashing", () => {
    render(
      <Router>
        <BookedMentorData />
      </Router>
    );
    expect(screen.getByText("Bạn chưa thuê gia sư nào :(")).toBeInTheDocument();
  });

  it("calls fetchBookedMentors on mount", () => {
    render(
      <Router>
        <BookedMentorData />
      </Router>
    );
    expect(mockFetchBookedMentors).toHaveBeenCalled();
  });
});
