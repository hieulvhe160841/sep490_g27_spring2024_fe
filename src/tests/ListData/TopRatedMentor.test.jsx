import React from "react";
import { render, screen } from "@testing-library/react";
import TopRatedMentor from "../../components/ListData/TopRatedMentor";
import menteeStore from "../../store/menteeStore";
import userStore from "../../store/userStore";
import "@testing-library/jest-dom";
import { MemoryRouter } from "react-router-dom";

jest.mock("../../store/menteeStore");
jest.mock("../../store/userStore");

describe("TopRatedMentor", () => {
  // Normal case: The component renders correctly when there are mentors
  it("renders correctly when there are mentors", () => {
    menteeStore.mockReturnValue({
      isLoading: false,
      fetchListTopMentor: jest.fn().mockResolvedValue([
        {
          userDetail: {
            userId: 1,
            name: "Test Mentor",
            educationLevel: "Test Education",
            numberOfDoneSupport: 5,
            experience: "Test Experience",
          },
          rating: 4.5,
        },
      ]),
    });

    userStore.mockReturnValue({
      baseUrl: "http://localhost",
    });

    render(
      <MemoryRouter>
        <TopRatedMentor />
      </MemoryRouter>
    );

    expect(screen.getByText("Tên gia sư:")).toBeInTheDocument();
    expect(screen.getByText("Trình độ học vấn:")).toBeInTheDocument();
    expect(screen.getByText("Yêu cầu/Bài đăng:")).toBeInTheDocument();
  });

  // Abnormal case: The component renders correctly when there are no mentors
  it("renders correctly when there are no mentors", () => {
    menteeStore.mockReturnValue({
      isLoading: false,
      fetchListTopMentor: jest.fn(),
    });

    userStore.mockReturnValue({
      baseUrl: "http://localhost",
    });

    render(
      <MemoryRouter>
        <TopRatedMentor />
      </MemoryRouter>
    );

    expect(screen.getByText("Hiện chưa có gia sư nào :(")).toBeInTheDocument();
  });

  // Boundary case: The component renders correctly when it is loading
  it("renders correctly when it is loading", () => {
    menteeStore.mockReturnValue({
      isLoading: true,
      fetchListTopMentor: jest.fn(),
    });

    userStore.mockReturnValue({
      baseUrl: "http://localhost",
    });

    render(
      <MemoryRouter>
        <TopRatedMentor />
      </MemoryRouter>
    );

    expect(screen.getByRole("img")).toBeInTheDocument();
  });
});
