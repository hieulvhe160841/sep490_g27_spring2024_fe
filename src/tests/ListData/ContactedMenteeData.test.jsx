import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import { BrowserRouter as Router } from "react-router-dom";
import ContactedMenteeData from "../../components/ListData/ContactedMenteeData";

const mockFetchContactedMenteeApi = jest.fn();

jest.mock("../../store/mentorStore", () => ({
  __esModule: true,
  default: () => ({
    fetchContactedMenteeApi: mockFetchContactedMenteeApi,
    listContactedMentee: null,
    isLoading: false,
  }),
}));

jest.mock("../../store/userStore", () => ({
  __esModule: true,
  default: () => ({
    baseUrl: "http://localhost",
  }),
}));

describe("ContactedMenteeData", () => {
  it("renders without crashing", () => {
    render(
      <Router>
        <ContactedMenteeData />
      </Router>
    );
    expect(
      screen.getByText("Bạn chưa giúp đỡ học viên nào trên hệ thống :(")
    ).toBeInTheDocument();
  });

  it("calls fetchContactedMenteeApi on mount", () => {
    render(
      <Router>
        <ContactedMenteeData />
      </Router>
    );
    expect(mockFetchContactedMenteeApi).toHaveBeenCalled();
  });
});
