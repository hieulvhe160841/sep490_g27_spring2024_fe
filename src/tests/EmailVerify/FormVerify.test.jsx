import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import { MemoryRouter } from "react-router-dom";
import FormVerify from "../../components/EmailVerify/FormVerify";

describe("FormVerify", () => {
  it("renders correctly", () => {
    render(
      <MemoryRouter initialEntries={[{ pathname: "/", state: { auth: true } }]}>
        <FormVerify />
      </MemoryRouter>
    );

    expect(screen.getByPlaceholderText("Nhập mã...")).toBeInTheDocument();
    expect(
      screen.getByRole("button", { name: /Nhập mã đăng ký/i })
    ).toBeInTheDocument();
    expect(
      screen.getByRole("button", { name: /Gửi lại/i })
    ).toBeInTheDocument();
    expect(
      screen.getByRole("link", { name: /Quay lại trang Đăng nhập/i })
    ).toBeInTheDocument();
  });

  it("updates on input change", () => {
    render(
      <MemoryRouter initialEntries={[{ pathname: "/", state: { auth: true } }]}>
        <FormVerify />
      </MemoryRouter>
    );

    const input = screen.getByPlaceholderText("Nhập mã...");
    fireEvent.change(input, { target: { value: "1234" } });
    expect(input.value).toBe("1234");
  });
});
