import React from "react";
import { Box, Typography, Button } from "@mui/material";
import { Link } from "react-router-dom";

const NotFound = () => {
  return (
    <Box
      display="flex"
      alignItems="center"
      justifyContent="center"
      height="calc(100vh - 64px)" // Adjust based on your layout
    >
      <Box textAlign="center">
        <Typography variant="h1" component="h1" gutterBottom>
          Not Found
        </Typography>
        <Typography variant="body1" component="p" gutterBottom>
          The page you're looking for doesn't exist.
        </Typography>
        <Button
          variant="contained"
          component={Link}
          to="/"
          sx={{ marginTop: "16px" }}
        >
          Go to Home
        </Button>
      </Box>
    </Box>
  );
};

export default NotFound;
