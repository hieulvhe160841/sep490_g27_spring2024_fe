import LoginComponent from "../pages/Login/Login";
import HomeComponent from "../pages/HomePage/HomePage";
import RegisterComponent from "../pages/Register/Register";
import NotFound from "./NotFound";
import { Outlet, Route, Router, Routes } from "react-router-dom";
import ChangePasswordComponent from "../pages/ChangePassword/ChangePassword";
import EmailVerifyComponent from "../pages/EmailVerify/EmailVerify";
import ForgotPasswordComponent from "../pages/ForgotPassword/ForgotPassword";
import ResetPasswordComponent from "../pages/ForgotPassword/ResetPassword";
import RegisterAsMentor from "../pages/Mentee/RegisterAsMentor/RegisterAsMentor";
import ListMentor from "../pages/ListMentor/ListMentor";
import BookedMentor from "../pages/Mentee/BookedMentor/BookedMentor";
import SuggestMentor from "../pages/Mentee/SuggestMentor/SuggestMentor";
import ContactedMentee from "../pages/Mentor/ContactedMentees/ContactedMentee";
import TopRateMentor from "../pages/TopRateMentor/TopRateMentor";
import Dashboard from "../pages/Admin/Dashboard";
import UserManagement from "../pages/Admin/UserManagement";
import SkillManagement from "../pages/Admin/SkillManagement";
import TransactionManagement from "../pages/Admin/TransactionManagement";
import MentorRegistration from "../pages/Admin/MentorRegistration";
import CourseRegistration from "../pages/Admin/CourseRegistration";
import MentorshipRequest from "../pages/Admin/MentorshipRequest";
import PostManagement from "../pages/Admin/PostManagement";
import UserDetail from "../pages/Admin/UserDetail";
import SkillDetail from "../pages/Admin/SkillDetail";
import MentorRegistrationDetail from "../pages/Admin/MentorRegistrationDetail";
import Layout from "../components/shared/Layout";
import CourseDetail from "../pages/Admin/CourseDetail";
import SuggestCourse from "../pages/SuggestCourse/SuggestCourse";
import MentorshipRequestMentor from "../pages/Mentor/MentorshipRequest/MentorshipRequest";
import MentorshipRequestMentee from "../pages/Mentee/MentorshipRequest/MentorshipRequest";
import PostManagementMentee from "./../pages/Mentee/PostManagement/PostManagement";
import EditProfile from "../pages/Mentor/EditProfile/EditProfile";
import MentorDetails from "../pages/Mentor/MentorDetails/MentorDetails";
import CourseDetailMentee from "../pages/Mentee/CourseDetailMentee/CourseDetailMentee";
import MentorshipRequestDetail from "../pages/Admin/MentorshipRequestDetail";
import MenteeLayout from "../components/shared/MenteeLayout";
import MentorLayout from "../components/shared/MentorLayout";
import SuggestedPost from "./../pages/Mentor/SuggestedPost/SuggestedPost";
import TransactionStaffLayout from "../components/shared/StaffLayout/TransactionStaffLayout";
import MenteeDetails from "../pages/MenteeDetail/MenteeDetail";
import SubmitCertificate from "../pages/Mentee/RegisterAsMentor/SubmitCertificate";
import MenteeProfile from "./../pages/Mentee/UserProfile/MenteeProfile";
import MentorProfile from "../pages/Mentor/UserProfile/MentorProfile";
import UpdateBasicInformation from "../components/UserProfile/Mentor/UpdateProfile/UpdateBasicInformation";
import UpdateProfile from "../components/UserProfile/Mentee/UpdateProfile/UpdateProfile";
import EducationStaffLayout from "./../components/shared/StaffLayout/EduStaffLayout";
import Chat from "../pages/Chat/Chat";
import RequestSession from "../pages/SupportSession/RequestSession";
import PostSession from "../pages/SupportSession/PostSession";
import MenteeTransaction from "../pages/Mentee/Transaction/MenteeTransaction";
import ChoosePrice from "../pages/Mentee/ChoosePrice/ChoosePrice";
import ListRegisterRequest from "../pages/Mentee/RegisterAsMentor/ListRegisterRequest";
import RegisterCourse from "../pages/Mentor/RegisterCourse/RegisterCourse";
import MyCourse from "../pages/Mentor/Course/MyCourse";
import CourseDetailMentor from "./../pages/Mentor/Course/CourseDetail";
import RegisterRequestDetail from "./../pages/Mentee/RegisterAsMentor/RegisterRequestDetail";
import CourseDetailCommon from "./../pages/SuggestCourse/CourseDetail";
import ApprovedCourseDetail from "../pages/Admin/ApprovedCourseDetail";
import ListCourseRequest from "../pages/Mentor/Course/ListCourseRequest";
import ListCourseOfMentee from "../pages/Mentee/Course/ListCourseOfMentee";
import BoughtCourseDetail from "../pages/Mentee/Course/BoughtCourseDetail";
import MentorWallet from "../pages/Mentor/Payment/MentorWallet";

const AppRoutes = () => {
  return (
    <>
      <Routes>
        <Route path="/" index element={<HomeComponent />}></Route>
        <Route path="login" element={<LoginComponent />}></Route>
        <Route path="register" element={<RegisterComponent />}></Route>
        <Route
          path="forgotPassword"
          element={<ForgotPasswordComponent />}
        ></Route>
        <Route
          path="resetPassword"
          element={<ResetPasswordComponent />}
        ></Route>
        <Route path="emailVerify" element={<EmailVerifyComponent />}></Route>
        <Route path="topRateMentor" element={<TopRateMentor />}></Route>

        <Route
          path="changePassword"
          element={<ChangePasswordComponent />}
        ></Route>
        <Route path="listMentor" element={<ListMentor />}></Route>
        <Route path="suggestCourse" element={<SuggestCourse />}></Route>
        <Route path="course/:id" element={<CourseDetailCommon />}></Route>
        <Route
          path="mentorDetails/:username"
          element={<MentorDetails />}
        ></Route>

        {/* Mentee Route */}
        <Route path="/mentee" element={<MenteeLayout />}>
          <Route index element={<SuggestMentor />}></Route>
          <Route path="profile" element={<MenteeProfile />}></Route>
          <Route path="profile/edit" element={<UpdateProfile />}></Route>
          <Route path="registerMentor" element={<RegisterAsMentor />}></Route>
          <Route path="addCertificate" element={<SubmitCertificate />}></Route>
          <Route
            path="listRegisterMentor"
            element={<ListRegisterRequest />}
          ></Route>
          <Route
            path="listRegisterMentor/registDetail/:requestId"
            element={<RegisterRequestDetail />}
          ></Route>
          <Route path="bookedMentor" element={<BookedMentor />}></Route>
          <Route
            path="courseDetail/:id"
            element={<CourseDetailMentee />}
          ></Route>
          <Route
            path="mentorshipRequestOfMentee"
            element={<MentorshipRequestMentee />}
          ></Route>
          <Route path="myPost" element={<PostManagementMentee />}></Route>
          <Route path="chat" element={<Chat />}></Route>

          <Route path="requestSession/:id" element={<RequestSession />}></Route>
          <Route path="postSession/:id" element={<PostSession />}></Route>
          <Route
            path="menteeDetails/:username"
            element={<MenteeDetails />}
          ></Route>
          <Route
            path="menteeTransaction"
            element={<MenteeTransaction />}
          ></Route>
          <Route path="choosePrice" element={<ChoosePrice />}></Route>
          <Route path="course" element={<ListCourseOfMentee />}></Route>
          <Route path="course/:id" element={<BoughtCourseDetail />}></Route>
        </Route>

        {/* Mentor Route */}
        <Route path="/mentor" element={<MentorLayout />}>
          <Route index element={<SuggestedPost />}></Route>
          <Route path="profile" element={<MentorProfile />}></Route>
          <Route
            path="profile/edit"
            element={<UpdateBasicInformation />}
          ></Route>
          <Route path="contactedMentee" element={<ContactedMentee />}></Route>
          <Route
            path="mentorshipRequest"
            element={<MentorshipRequestMentor />}
          ></Route>
          <Route path="editProfile/:id" element={<EditProfile />}></Route>
          <Route path="chat" element={<Chat />}></Route>
          <Route path="requestSession/:id" element={<RequestSession />}></Route>
          <Route path="postSession/:id" element={<PostSession />}></Route>
          <Route
            path="menteeDetails/:username"
            element={<MenteeDetails />}
          ></Route>
          <Route path="course" element={<MyCourse />}></Route>
          <Route path="course/:id" element={<CourseDetailMentor />}></Route>
          <Route path="addCourse" element={<RegisterCourse />}></Route>
          <Route path="courseRequest" element={<ListCourseRequest />}></Route>
          <Route path="wallet" element={<MentorWallet />}></Route>
        </Route>

        {/* Transaction staff Route */}
        <Route path="/staffTransaction" element={<TransactionStaffLayout />}>
          <Route index element={<TransactionManagement />}></Route>
          <Route path="chat" element={<Chat />}></Route>
          <Route
            path="menteeDetails/:username"
            element={<MenteeDetails />}
          ></Route>
          <Route
            path="mentorDetails/:username"
            element={<MentorDetails />}
          ></Route>
        </Route>

        {/* Education staff Route */}
        <Route path="/staffEducation" element={<EducationStaffLayout />}>
          <Route index element={<MentorshipRequest />} />
          <Route path="user" element={<UserManagement />} />
          <Route path="userInfo/:username" element={<UserDetail />} />
          <Route path="mentorRegist" element={<MentorRegistration />} />
          <Route path="course" element={<CourseRegistration />} />
          <Route path="course/courseInfo/:id" element={<CourseDetail />} />
          <Route
            path="requestDetail/:id"
            element={<MentorshipRequestDetail />}
          />
          <Route path="post" element={<PostManagement />} />
          <Route
            path="mentorRegist/mentorRegistration/:userId/:requestId"
            element={<MentorRegistrationDetail />}
          />
          <Route
            path="menteeDetails/:username"
            element={<MenteeDetails />}
          ></Route>
          <Route
            path="mentorDetails/:username"
            element={<MentorDetails />}
          ></Route>
          <Route path="chat" element={<Chat />}></Route>
        </Route>

        {/* Admin Route */}
        <Route path="/admin" element={<Layout />}>
          <Route index element={<Dashboard />} />
          <Route path="user" element={<UserManagement />} />
          <Route path="userInfo/:username" element={<UserDetail />} />
          <Route path="skill" element={<SkillManagement />} />
          <Route path="skillInfo/:id" element={<SkillDetail />} />

          <Route path="mentorRegist" element={<MentorRegistration />} />
          <Route path="course" element={<CourseRegistration />} />
          <Route path="course/courseInfo/:id" element={<CourseDetail />} />
          <Route path="request" element={<MentorshipRequest />} />
          <Route
            path="requestDetail/:id"
            element={<MentorshipRequestDetail />}
          />
          <Route path="post" element={<PostManagement />} />
          <Route
            path="mentorRegist/mentorRegistration/:userId/:requestId"
            element={<MentorRegistrationDetail />}
          />
          <Route path="transaction" element={<TransactionManagement />} />
          <Route
            path="menteeDetails/:username"
            element={<MenteeDetails />}
          ></Route>
          <Route
            path="mentorDetails/:username"
            element={<MentorDetails />}
          ></Route>
          <Route path="chat" element={<Chat />}></Route>
          <Route
            path="approvedCourse/:id"
            element={<ApprovedCourseDetail />}
          ></Route>
        </Route>
        <Route path="*" element={<NotFound />} />
      </Routes>
    </>
  );
};
export default AppRoutes;
