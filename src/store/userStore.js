import axios from "axios";
import { create } from "zustand";
import {
  addSkillService,
  addStaffService,
  approveCourseRequestService,
  approveMentorRegisService,
  fetchAdminDashboardService,
  fetchAllUserService,
  fetchCourseRegistDetailService,
  fetchCourseRegistService,
  fetchMentorRegisDetailService,
  fetchMentorRegisService,
  fetchMentorshipRequestByIdService,
  fetchMentorshipRequestService,
  fetchPostDetailService,
  fetchPostService,
  fetchSkillDetailService,
  fetchSkillService,
  fetchTransactionService,
  fetchUserDetailService,
  updatePostStatusService,
  updateSkillService,
  updateUserStatusService,
} from "../services/AdminService";
import {
  login,
  register,
  verifyEmail,
  regenerateOtp,
  forgotPassword,
  verifyEmailReset,
  resetPassword,
  changePassword,
  fetchAvailableSKill,
  fetchAllCert,
  fetchUserWallet,
  createPayment,
  regenerateOtpReset,
} from "../services/UserService";

const userStore = create((set) => ({
  user: {},

  baseUrl: "https://13.229.154.151",
  token: {},
  userRegister: {},
  setUserRegister: (res) => set({ userRegister: res.data }),
  listUsers: [],
  setListUsers: (res) => set({ listUsers: res.data }),

  listSkills: [],
  setListSkills: (res) => set({ listSkills: res.data }),

  listCerts: [],
  setListCert: (res) => set({ listCerts: res.data }),

  listMentorRegis: [],
  setListMentorRegis: (res) => set({ listMentorRegis: res.data }),

  adminDashboard: [],
  setAdminDashboard: (res) => set({ adminDashboard: res.data }),

  listCourseRegis: [],
  setListCourseRegis: (res) => set({ listCourseRegis: res.data }),

  listPost: [],
  setListPost: (res) => set({ listPost: res.data }),

  listMentorshipRequest: [],
  setListMentorshipRequest: (res) => set({ listMentorshipRequest: res.data }),

  listTransaction: [],
  setListTransaction: (res) => set({ listTransaction: res.data }),
  userBalance: {},
  setUserBalance: (res) => set({ userBalance: res }),

  isLoading: false,
  setIsLoading: (loading) => set({ isLoading: loading }),

  isOpen: true,
  setIsOpen: () => set((state) => ({ isOpen: !state.isOpen })),

  isLoadingPayment: false,
  setIsLoadingPayment: (loading) => set({ isLoadingPayment: loading }),

  fetchUsers: async () => {
    const store = userStore.getState();
    try {
      store.setIsLoading(true);
      const res = await fetchAllUserService();
      store.setListUsers(res);
    } catch (error) {
      console.error("Error fetching users:", error);
    } finally {
      store.setIsLoading(false);
    }
  },

  fetchUserByUsername: async (username) => {
    try {
      const user = await fetchUserDetailService(username);
      return user;
    } catch (error) {
      console.error("Error fetching user by username:", error);
      throw error;
    }
  },

  fetchSkills: async () => {
    const store = userStore.getState();
    try {
      store.setIsLoading(true);
      const res = await fetchSkillService();
      store.setListSkills(res);
    } catch (error) {
      console.error("Error fetching skills:", error);
    } finally {
      store.setIsLoading(false);
    }
  },

  fetchSkillById: async (id) => {
    try {
      const skill = await fetchSkillDetailService(id);
      return skill;
    } catch (error) {
      console.error("Error fetching skill by id:", error);
      throw error;
    }
  },

  fetchMentorRegis: async () => {
    const store = userStore.getState();
    try {
      store.setIsLoading(true);
      const res = await fetchMentorRegisService();
      store.setListMentorRegis(res);
    } catch (error) {
      console.error("Error fetching mentorRegis:", error);
    } finally {
      store.setIsLoading(false);
    }
  },

  fetchMentorRegisByUserIdAndRequestId: async (userId, requestId) => {
    try {
      const mentorRegis = await fetchMentorRegisDetailService(
        userId,
        requestId
      );
      return mentorRegis;
    } catch (error) {
      console.error(
        "Error fetching mentorRegis by userId and requestId:",
        error
      );
      throw error;
    }
  },

  fetchCourseById: async (courseId) => {
    try {
      const course = await fetchCourseDetailService(courseId);
      return course;
    } catch (error) {
      console.error("Error fetching course by courseId:", error);
      throw error;
    }
  },

  fetchAdminDashboard: async () => {
    const store = userStore.getState();
    try {
      store.setIsLoading(true);
      const res = await fetchAdminDashboardService();
      store.setAdminDashboard(res);
    } catch (error) {
      console.error("Error fetching admin dashboard:", error);
      throw error;
    } finally {
      store.setIsLoading(false);
    }
  },

  fetchCourseRegis: async () => {
    const store = userStore.getState();
    try {
      store.setIsLoading(true);
      const res = await fetchCourseRegistService();
      store.setListCourseRegis(res);
    } catch (error) {
      console.error("Error fetching course registration:", error);
    } finally {
      store.setIsLoading(false);
    }
  },

  fetchCourseRegisById: async (courseRegisId) => {
    try {
      const courseRegis = await fetchCourseRegistDetailService(courseRegisId);
      return courseRegis;
    } catch (error) {
      console.error("Error fetching course registration by Id:", error);
      throw error;
    }
  },

  fetchPosts: async () => {
    const store = userStore.getState();
    try {
      store.setIsLoading(true);
      const res = await fetchPostService();
      store.setListPost(res);
    } catch (error) {
      console.error("Error fetching posts:", error);
    } finally {
      store.setIsLoading(false);
    }
  },

  fetchPostById: async (postId) => {
    try {
      const post = await fetchPostDetailService(postId);
      return post;
    } catch (error) {
      console.error("Error fetching post by Id:", error);
      throw error;
    }
  },

  fetchMentorshipRequest: async (status, startDate, endDate) => {
    const store = userStore.getState();
    try {
      store.setIsLoading(true);
      const res = await fetchMentorshipRequestService(
        status,
        startDate,
        endDate
      );
      store.setListMentorshipRequest(res);
    } catch (error) {
      console.error("Error fetching mentorship request:", error);
      store.setListMentorshipRequest([]);
    } finally {
      store.setIsLoading(false);
    }
  },

  fetchMentorshipRequestById: async (supportId) => {
    try {
      const request = await fetchMentorshipRequestByIdService(supportId);
      return request;
    } catch (error) {
      console.error("Error fetching mentorship request by Id:", error);
      throw error;
    }
  },

  fetchTransactions: async () => {
    const store = userStore.getState();
    try {
      store.setIsLoading(true);
      const res = await fetchTransactionService();
      store.setListTransaction(res);
    } catch (error) {
      console.error("Error fetching transactions:", error);
    } finally {
      store.setIsLoading(false);
    }
  },

  addStaff: async (staffData) => {
    try {
      const response = await addStaffService(staffData);
      return response;
    } catch (error) {
      throw error;
    }
  },

  addSkill: async (skillData) => {
    try {
      const response = await addSkillService(skillData);
      return response;
    } catch (error) {
      throw error;
    }
  },

  updateUserStatus: async (username, status) => {
    try {
      const response = await updateUserStatusService(username, status);
      return response;
    } catch (error) {
      throw error;
    }
  },

  updateSkill: async (id, skillData) => {
    try {
      const response = await updateSkillService(id, skillData);
      return response;
    } catch (error) {
      throw error;
    }
  },

  updatePostStatus: async (postId, status) => {
    try {
      const response = await updatePostStatusService(postId, status);
      return response;
    } catch (error) {
      throw error;
    }
  },

  approveCourseRequest: async (courseId, feedback, status) => {
    try {
      const response = await approveCourseRequestService(
        courseId,
        feedback,
        status
      );
      return response;
    } catch (error) {
      throw error;
    }
  },

  approveMentorRegis: async (userId, feedback, status) => {
    try {
      const response = await approveMentorRegisService(
        userId,
        feedback,
        status
      );
      return response;
    } catch (error) {
      throw error;
    }
  },

  loginContext: async (userName, password) => {
    const timeExpired = new Date();
    const timeRefreshExpired = new Date();
    const store = userStore.getState();
    try {
      store.setIsLoading(true);
      const response = await login(userName, password);
      set({
        user: { userName, password, auth: true, role: response.data.data.role },
        token: true,
      });
      timeExpired.setMinutes(
        timeExpired.getMinutes() + response.data.data.expire / (60 * 1000)
      );
      timeRefreshExpired.setMinutes(
        timeRefreshExpired.getMinutes() +
          (response.data.data.expire / (60 * 1000)) * 2
      );
      const usernameLowerCase = userName.toLowerCase();
      localStorage.setItem("token", response.data.data.token);
      localStorage.setItem("refreshToken", response.data.data.refreshToken);
      localStorage.setItem("user", usernameLowerCase);
      localStorage.setItem("expire", timeExpired);
      localStorage.setItem("refreshExpire", timeRefreshExpired);
      localStorage.setItem("role", response.data.data.role);
      localStorage.setItem("secret", response.data.data.secret);

      return response;
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },
  registerApi: async (
    userName,
    password,
    name,
    phone,
    address,
    dayOfBirth,
    gender,
    avatar,
    email
  ) => {
    const store = userStore.getState();
    try {
      store.setIsLoading(true);
      const response = await register(
        userName,
        password,
        name,
        phone,
        address,
        dayOfBirth,
        gender,
        avatar,
        email
      );
      return response;
    } catch (error) {
      return error.response.data;
    } finally {
      store.setIsLoading(false);
    }
  },

  verifyForRegister: async (email, otpCode) => {
    const store = userStore.getState();
    try {
      store.setIsLoading(true);
      const response = await verifyEmail(email, otpCode);
      return response;
    } catch (error) {
      return error.response.data;
    } finally {
      store.setIsLoading(false);
    }
  },
  regenerateCode: async (email) => {
    try {
      const response = await regenerateOtp(email);
      return response;
    } catch (error) {
      return error.response;
    }
  },
  forgotPasswordApi: async (email) => {
    const store = userStore.getState();
    try {
      store.setIsLoading(true);
      const response = await forgotPassword(email);
      return response;
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },
  verifyForReset: async (email, otpCode) => {
    const store = userStore.getState();
    try {
      store.setIsLoading(true);
      const response = await verifyEmailReset(email, otpCode);
      return response;
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },
  resetPasswordAPi: async (email, newPassword) => {
    const store = userStore.getState();
    try {
      store.setIsLoading(true);
      const response = await resetPassword(email, newPassword);
      return response;
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },
  logout: async () => {
    set({ user: "" });
    localStorage.clear();
  },
  changePasswordApi: async (oldPassword, newPassword, confirmNewPassword) => {
    const store = userStore.getState();
    try {
      store.setIsLoading(true);
      const response = await changePassword(
        oldPassword,
        newPassword,
        confirmNewPassword
      );
      return response;
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },
  listAllAvailableSKill: async () => {
    const store = userStore.getState();
    try {
      store.setIsLoading(true);
      const response = await fetchAvailableSKill();
      store.setListSkills(response);
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },
  listAllCert: async () => {
    const store = userStore.getState();
    try {
      store.setIsLoading(true);
      const response = await fetchAllCert();
      store.setListCert(response);
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },
  fetchUserWallet: async () => {
    const store = userStore.getState();
    try {
      store.setIsLoading(true);
      const response = await fetchUserWallet();
      store.setUserBalance(response);
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },
  createPaymentApi: async (denominationId, value, amount) => {
    const store = userStore.getState();
    try {
      store.setIsLoadingPayment(true);
      const response = await createPayment(denominationId, value, amount);
      return response;
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoadingPayment(false);
    }
  },
  regenerateCodeReset: async (formData) => {
    try {
      const response = await regenerateOtpReset(formData);
      return response;
    } catch (error) {
      return error.response;
    }
  },
}));

export default userStore;
