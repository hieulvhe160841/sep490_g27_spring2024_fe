import { create } from "zustand";
import { fetchTopMentor, fetchAllMentor } from "../services/UserService";
import {
  fetchSuggestMentor,
  fetchBookedMentor,
  fetchMenteeDetail,
  fetchListMenteePost,
  updatePostService,
  deletePost,
  submitSkillRegisterAsMentor,
  submitCertRegisterAsMentor,
  fetchSuggestCourse,
  menteeProfile,
  updateProfile,
  requestMentorDirectlyService,
  confirmSupportStartService,
  confirmSupportEndService,
  fetchSupportByIdMenteeService,
  addPostService,
  acceptOrRejectRequest,
  ratingSupport,
  fetchMenteeTransactionService,
  fetchAllDenomination,
  fetchPostByIdMenteeService,
  fetchAllMentorRequest,
  confirmChangeRole,
  viewDetailMentorRequest,
  fetchMentorshipRequestMentee,
  fetchAllCoursePurchased,
  purchaseCourse,
} from "../services/MenteeService";
import { toast } from "react-toastify";

const menteeStore = create((set) => ({
  isLoading: false,
  setIsLoading: (loading) => set({ isLoading: loading }),

  isOpen: true,
  setIsOpen: () => set((state) => ({ isOpen: !state.isOpen })),

  listTopMentor: [],
  setListTopMentor: (res) => set({ listTopMentor: res.data }),

  listSuggestMentor: [],
  setListSuggestMentor: (res) => set({ listSuggestMentor: res.data }),

  listSuggestCourse: [],
  setListSuggestCourse: (res) => set({ listSuggestCourse: res.data }),

  listMentors: [],
  setListMentor: (res) => set({ listMentors: res.data }),

  listBookedMentors: [],
  setListBookedMentor: (res) => set({ listBookedMentors: res }),

  listMenteePost: [],
  setListMenteePost: (res) => set({ listMenteePost: res.data }),

  listMentorshipRequest: [],
  setListMentorshipRequest: (res) => set({ listMentorshipRequest: res.data }),

  listMenteeTransaction: [],
  setListMenteeTransaction: (res) => set({ listMenteeTransaction: res.data }),

  listDenomination: [],
  setListDenomination: (res) => set({ listDenomination: res.data }),

  listMentorRequest: [],
  setListMentorRequest: (res) => set({ listMentorRequest: res.data }),

  mentorRequest: {},
  setMentorRequest: (res) => set({ mentorRequest: res.data }),
  courses: [],
  setCourses: (res) => set({ courses: res.data }),

  fetchListTopMentor: async () => {
    const store = menteeStore.getState();
    try {
      store.setIsLoading(true);
      const response = await fetchTopMentor();
      return response.data;
    } catch (error) {
      return error.response.data;
    } finally {
      store.setIsLoading(false);
    }
  },

  fetchListSuggestCourse: async () => {
    const store = menteeStore.getState();
    try {
      store.setIsLoading(true);
      const response = await fetchSuggestCourse();
      store.setListSuggestCourse(response);
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },

  fetchListSuggestCourse: async (
    mentorName,
    courseName,
    fromPrice,
    toPrice
  ) => {
    const store = menteeStore.getState();
    try {
      store.setIsLoading(true);
      const response = await fetchSuggestCourse(
        mentorName,
        courseName,
        fromPrice,
        toPrice
      );
      store.setListSuggestCourse(response);
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },

  fetchListSuggestMentor: async () => {
    const store = menteeStore.getState();
    try {
      store.setIsLoading(true);
      const response = await fetchSuggestMentor();
      store.setListSuggestMentor(response);
    } catch (error) {
      return error.response.data;
    } finally {
      store.setIsLoading(false);
    }
  },

  fetchAllMentors: async () => {
    const store = menteeStore.getState();
    try {
      store.setIsLoading(true);
      const response = await fetchAllMentor();
      store.setListMentor(response);
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },
  fetchBookedMentors: async () => {
    const store = menteeStore.getState();
    try {
      store.setIsLoading(true);
      const response = await fetchBookedMentor();
      store.setListBookedMentor(response.data);
      return response;
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },
  fetchMenteeDetail: async (username) => {
    const store = menteeStore.getState();
    try {
      store.setIsLoading(true);
      const mentee = await fetchMenteeDetail(username);
      return mentee.data;
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },
  fetchAllMenteePost: async () => {
    const store = menteeStore.getState();
    try {
      store.setIsLoading(true);
      const response = await fetchListMenteePost();
      store.setListMenteePost(response);
    } catch (error) {
      console.log(error.response);
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },

  fetchPostByIdMentee: async (id) => {
    try {
      const res = await fetchPostByIdMenteeService(id);
      return res;
    } catch (error) {
      throw error;
    }
  },

  updatePostById: async (id, postData) => {
    try {
      const response = await updatePostService(id, postData);
      return response;
    } catch (error) {
      throw error;
    }
  },

  deletePostById: async (id) => {
    try {
      const response = await deletePost(id);
      return response;
    } catch (error) {
      return error.response;
    }
  },
  submitSkillForRegisterMentor: async (
    userName,
    experience,
    educationLevel,
    skills,
    cost
  ) => {
    const store = menteeStore.getState();
    try {
      store.setIsLoading(true);
      const res = await submitSkillRegisterAsMentor(
        userName,
        experience,
        educationLevel,
        skills,
        cost
      );
      return res.data;
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },
  submitCertForRegisterMentor: async (formData) => {
    const store = menteeStore.getState();
    try {
      store.setIsLoading(true);
      const res = await submitCertRegisterAsMentor(formData);
      return res;
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },

  menteeProfileApi: async () => {
    try {
      const res = await menteeProfile();
      return res.data;
    } catch (error) {
      return error.response;
    }
  },
  updateProfileMentee: async (formData) => {
    const store = menteeStore.getState();
    try {
      store.setIsLoading(true);
      const res = await updateProfile(formData);
      return res;
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },

  requestMentorDirectly: async (username, formData) => {
    try {
      const response = await requestMentorDirectlyService(username, formData);
      return response;
    } catch (error) {
      throw error.data;
    }
  },

  fetchSupportByIdMentee: async (id) => {
    const store = menteeStore.getState();
    try {
      store.setIsLoading(true);
      const res = await fetchSupportByIdMenteeService(id);
      return res;
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },

  confirmSupportStartMentee: async (supportId) => {
    try {
      const response = await confirmSupportStartService(supportId);
      return response;
    } catch (error) {
      return error;
    }
  },

  confirmSupportEnd: async (supportId) => {
    try {
      const response = await confirmSupportEndService(supportId);
      return response;
    } catch (error) {
      throw error;
    }
  },

  addPost: async (postData) => {
    try {
      const response = await addPostService(postData);
      return response;
    } catch (error) {
      throw error.data;
    }
  },

  mentorshipRequestOfMentee: async (status, startDate, endDate) => {
    const store = menteeStore.getState();
    try {
      store.setIsLoading(true);
      const response = await fetchMentorshipRequestMentee(
        status,
        startDate,
        endDate
      );
      store.setListMentorshipRequest(response);
    } catch (error) {
      console.error("Error fetching mentorship request:", error);
      store.setListMentorshipRequest([]);
    } finally {
      store.setIsLoading(false);
    }
  },

  menteeAcceptOrRejectRequest: async (supportId, status) => {
    try {
      const response = await acceptOrRejectRequest(supportId, status);
      return response;
    } catch (error) {
      return error.response;
    }
  },

  menteeRatingSupportOfMentor: async (supportId, rating, feedback) => {
    try {
      const response = await ratingSupport(supportId, rating, feedback);
      return response;
    } catch (error) {
      return error.response;
    }
  },

  fetchMenteeTransaction: async () => {
    const store = menteeStore.getState();
    try {
      store.setIsLoading(true);
      const response = await fetchMenteeTransactionService();
      store.setListMenteeTransaction(response);
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },

  fetchListDenomination: async () => {
    const store = menteeStore.getState();
    try {
      store.setIsLoading(true);
      const response = await fetchAllDenomination();
      store.setListDenomination(response);
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },

  fetchListMentorRequest: async () => {
    const store = menteeStore.getState();
    try {
      store.setIsLoading(true);
      const response = await fetchAllMentorRequest();
      store.setListMentorRequest(response);
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },

  updateMenteeToMentor: async () => {
    const store = menteeStore.getState();
    try {
      store.setIsLoading(true);
      const response = await confirmChangeRole();
      store.setListMentorRequest(response);
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },

  viewDetailMentorRequest: async (requestId) => {
    const store = menteeStore.getState();
    try {
      store.setIsLoading(true);
      const response = await viewDetailMentorRequest(requestId);
      return response;
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },

  fetchListCoursePurchased: async () => {
    const store = menteeStore.getState();
    try {
      store.setIsLoading(true);
      const response = await fetchAllCoursePurchased();
      store.setCourses(response);
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },

  purchaseCourseMentee: async (id) => {
    const store = menteeStore.getState();
    try {
      store.setIsLoading(true);
      const response = await purchaseCourse(id);
      return response;
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },
}));
export default menteeStore;
