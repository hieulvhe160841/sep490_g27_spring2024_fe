import { create } from "zustand";
import {
  confirmSupportStartService,
  fetchContactedMentee,
  fetchMentorDetail,
  fetchSuggestPost,
  acceptOrRejectRequest,
  mentorProfile,
  updateProfile,
  fetchSupportByIdMentorService,
  fetchPostDetailService,
  acceptPostService,
  registerCourse,
  fetchAllCourse,
  fetchCourseById,
  updateCourse,
  fetchAllRequestCourse,
  fetchMentorshipRequestMentor,
  updateCourseInfo,
} from "../services/MentorService";

const mentorStore = create((set) => ({
  isLoading: false,
  setIsLoading: (loading) => set({ isLoading: loading }),
  listContactedMentee: [],
  setListContactedMentee: (res) => set({ listContactedMentee: res.data }),
  setListContactedMentee: (res) => set({ listContactedMentee: res.data }),
  mentor: {},
  setMentor: (res) => set({ mentor: res.data }),
  listSuggestPost: [],
  setListSuggestPost: (res) => set({ listSuggestPost: res.data }),

  listMentorshipRequest: [],
  setListMentorshipRequest: (res) => set({ listMentorshipRequest: res.data }),

  listCourseOfMentor: [],
  setListCourseOfMentor: (res) => set({ listCourseOfMentor: res.data }),
  course: {},
  setCourse: (res) => set({ course: res.data }),

  fetchContactedMenteeApi: async () => {
    const store = mentorStore.getState();
    try {
      store.setIsLoading(true);
      const response = await fetchContactedMentee();
      store.setListContactedMentee(response);
    } catch (error) {
      return error.response.data;
    } finally {
      store.setIsLoading(false);
    }
  },
  fetchDetailMentor: async (username) => {
    const store = mentorStore.getState();
    try {
      store.setIsLoading(true);
      const mentor = await fetchMentorDetail(username);
      return mentor.data;
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },

  fetchSuggestPost: async () => {
    const store = mentorStore.getState();
    try {
      store.setIsLoading(true);
      const response = await fetchSuggestPost();
      store.setListSuggestPost(response);
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },

  fetchMentorshipRequestMentor: async (status, startDate, endDate) => {
    const store = mentorStore.getState();
    try {
      store.setIsLoading(true);
      const response = await fetchMentorshipRequestMentor(
        status,
        startDate,
        endDate
      );
      store.setListMentorshipRequest(response);
    } catch (error) {
      console.error("Error fetching mentorship request:", error);
      store.setListMentorshipRequest([]);
    } finally {
      store.setIsLoading(false);
    }
  },

  mentorAcceptOrRejectRequest: async (supportId, status) => {
    try {
      const response = await acceptOrRejectRequest(supportId, status);
      return response;
    } catch (error) {
      return error.response;
    }
  },
  mentorProfileApi: async () => {
    try {
      const res = await mentorProfile();
      return res.data;
    } catch (error) {
      return error.response;
    }
  },
  updateProfileMentor: async (formData) => {
    const store = mentorStore.getState();
    try {
      store.setIsLoading(true);
      const res = await updateProfile(formData);
      return res;
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },

  fetchSupportByIdMentor: async (id) => {
    const store = mentorStore.getState();
    try {
      store.setIsLoading(true);
      const res = await fetchSupportByIdMentorService(id);
      return res;
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },

  confirmSupportStartMentor: async (supportId) => {
    try {
      const response = await confirmSupportStartService(supportId);
      return response;
    } catch (error) {
      throw error;
    }
  },

  fetchPostById: async (postId) => {
    try {
      const post = await fetchPostDetailService(postId);
      return post;
    } catch (error) {
      console.error("Error fetching post by Id:", error);
      throw error;
    }
  },

  acceptPost: async (postId) => {
    try {
      const response = await acceptPostService(postId);
      return response;
    } catch (error) {
      throw error;
    }
  },
  addCourse: async (formData) => {
    const store = mentorStore.getState();
    try {
      store.setIsLoading(true);
      const res = await registerCourse(formData);
      return res;
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },
  listCourse: async (username) => {
    const store = mentorStore.getState();
    try {
      store.setIsLoading(true);
      const response = await fetchAllCourse(username);
      store.setListCourseOfMentor(response);
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },
  viewCourseDetail: async (courseId) => {
    const store = mentorStore.getState();
    try {
      store.setIsLoading(true);
      const response = await fetchCourseById(courseId);
      store.setCourse(response);
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },
  addCourseVideo: async (formData, id) => {
    const store = mentorStore.getState();
    try {
      store.setIsLoading(true);
      const response = await updateCourse(formData, id);
      return response;
    } catch (error) {
      return error.data;
    } finally {
      store.setIsLoading(false);
    }
  },
  listCourseRequest: async () => {
    const store = mentorStore.getState();
    try {
      store.setIsLoading(true);
      const response = await fetchAllRequestCourse();
      store.setListCourseOfMentor(response);
    } catch (error) {
      return error.response;
    } finally {
      store.setIsLoading(false);
    }
  },
  updateCourse: async (formData, id) => {
    const store = mentorStore.getState();
    try {
      store.setIsLoading(true);
      const response = await updateCourseInfo(formData, id);
      return response;
    } catch (error) {
      return error.data;
    } finally {
      store.setIsLoading(false);
    }
  },
}));
export default mentorStore;
