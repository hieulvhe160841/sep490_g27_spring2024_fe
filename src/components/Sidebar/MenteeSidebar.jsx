import React, { useState } from "react";
import SidebarItem from "./SidebarItem";
import { IoIosArrowBack } from "react-icons/io";
import {
  FaChalkboardTeacher,
  FaUserCheck,
  FaMoneyCheckAlt,
  FaBookOpen,
  FaHandshake,
  FaRegNewspaper,
  FaUserGraduate,
  FaUserPlus,
} from "react-icons/fa";
import { GiTeacher, GiBookmarklet } from "react-icons/gi";
import { FiEdit } from "react-icons/fi";
import userStore from "../../store/userStore";

const IconDefaultSize = "25";

const items = [
  {
    label: "Gợi ý gia sư",
    icon: <FaChalkboardTeacher size={IconDefaultSize} className="mr-2" />,
    to: "/mentee",
  },
  {
    label: "Gia sư đã thuê",
    icon: <FaUserCheck size={IconDefaultSize} className="mr-2" />,
    to: "/mentee/bookedMentor",
  },
  {
    label: "Lịch sử giao dịch",
    icon: <FaMoneyCheckAlt size={IconDefaultSize} className="mr-2" />,
    to: "/mentee/menteeTransaction",
  },
  {
    label: "Khóa học của tôi",
    icon: <FaBookOpen size={IconDefaultSize} className="mr-2" />,
    to: "/mentee/course",
  },
  {
    label: "Yêu cầu gia sư",
    icon: <FaHandshake size={IconDefaultSize} className="mr-2" />,
    to: "/mentee/mentorshipRequestOfMentee",
  },
  {
    label: "Bài đăng",
    icon: <FaRegNewspaper size={IconDefaultSize} className="mr-2" />,
    to: "/mentee/myPost",
  },
  {
    label: "Xác nhận làm gia sư",
    icon: <FaUserGraduate size={IconDefaultSize} className="mr-2" />,
    to: "/mentee/listRegisterMentor",
  },
  {
    label: "Đăng ký làm gia sư",
    icon: <FaUserPlus size={IconDefaultSize} className="mr-2" />,
    to: "/mentee/registerMentor",
  },
];
const SidebarItemsContainer = ({ open }) => (
  <>
    {items.map((item, index) => (
      <SidebarItem item={item} key={index} open={open} />
    ))}
  </>
);

const MenteeSidebar = () => {
  const [isSidebarMobileOpen, setIsSidebarMobileOpen] = useState(true);
  const { isOpen, setIsOpen } = userStore();

  const toggleSidebar = () => {
    setIsOpen();
  };

  return (
    <nav
      className={`${
        isOpen ? "md:w-58" : "md:w-11"
      } duration-300 bg-gradient-to-b from-sidebarTop to-sidebarBottom 
      text-white fixed top-20 h-screen z-30 text-sm rounded-lg w-11`}
    >
      <div className="flex justify-between items-center md:block mt-4 lg:mt-0">
        {isSidebarMobileOpen && (
          <IoIosArrowBack
            color="black"
            className={`bg-btn cursor-pointer absolute -right-4 top-1/2 w-7 h-7 rounded-full ${
              !isOpen && "rotate-180 -right-6"
            } hidden md:block`}
            onClick={toggleSidebar}
          />
        )}
      </div>
      <ul className={`block ${isSidebarMobileOpen ? "" : "hidden"}`}>
        <SidebarItemsContainer open={isOpen} />
      </ul>
    </nav>
  );
};

export default MenteeSidebar;
