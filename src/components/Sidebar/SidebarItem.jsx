import React from "react";
import { Link, useLocation } from "react-router-dom";

const SidebarItem = ({ item, open }) => {
  const { label, icon, to } = item;
  const location = useLocation();

  const isMobileScreen = window.innerWidth < 768;

  return (
    <Link to={to} className="block">
      <li
        className={`sideBarItem cursor-pointer${
          location.pathname === to ? " bg-active" : ""
        } ${
          location.pathname === "/admin" ||
          location.pathname === "/mentor" ||
          location.pathname === "/mentee" ||
          location.pathname === "/staffEducation" ||
          location.pathname === "/staffTransaction"
            ? "rounded-t-lg overflow-hidden"
            : ""
        }`}
      >
        <div className="flex items-center">
          {icon}
          {open && !isMobileScreen && <h3>{label}</h3>}
        </div>
      </li>
    </Link>
  );
};

export default SidebarItem;
