import React from "react";

function TitleHeader(props) {
  return (
    <>
      <div className="md:mx-auto md:w-full my-10">
        <h2 className="text-center text-3xl font-bold leading-9 tracking-tight text-textHeader">
          {props.title}
        </h2>
      </div>
    </>
  );
}

export default TitleHeader;
