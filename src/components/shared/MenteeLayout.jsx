import React, { useState } from "react";
import { Navigate, Outlet } from "react-router-dom";
import Header from "../Header/Header";
import { toast } from "react-toastify";
import userStore from "../../store/userStore";
import MenteeSidebar from "../Sidebar/MenteeSidebar";

const MenteeLayout = () => {
  const { isOpen, setIsOpen } = userStore();
  const role = localStorage.getItem("role");
  if (role === null) {
    toast.warn("Bạn phải đăng nhập trước!");
    return <Navigate to="/login" replace />;
  }
  if (role !== "ROLE_MENTEE") {
    toast.warn("Bạn không có quyền truy cập vào trang này!");
    return <Navigate to="/" replace />;
  }
  return (
    <div>
      <Header />
      <div className="h-auto flex flex-row ">
        <MenteeSidebar isOpen={isOpen} toggleSidebar={setIsOpen} />
        <div className={`flex flex-col flex-1 ${isOpen ? "md:ml-56" : ""} `}>
          <div className="flex-1">
            <Outlet />
          </div>
        </div>
      </div>
    </div>
  );
};

export default MenteeLayout;
