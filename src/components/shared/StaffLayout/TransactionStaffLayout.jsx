import React, { useState } from "react";
import { Navigate, Outlet } from "react-router-dom";
import Header from "../../Header/Header";
import userStore from "../../../store/userStore";
import { toast } from "react-toastify";
import TransactionStaffSidebar from "../../Sidebar/TransactionStaffSidebar";

const TransactionStaffLayout = () => {
  const { isOpen, setIsOpen } = userStore();
  const role = localStorage.getItem("role");
  if (role === null) {
    toast.warn("Bạn phải đăng nhập trước!");
    return <Navigate to="/login" replace />;
  }
  if (role !== "ROLE_TRANSACTION_STAFF") {
    toast.warn("Bạn không có quyền truy cập vào trang này!");
    return <Navigate to="/" replace />;
  }
  return (
    <div>
      <Header />
      <div className="h-screen w-screen flex flex-row ">
        <TransactionStaffSidebar isOpen={isOpen} toggleSidebar={setIsOpen} />
        <div
          className={`flex flex-col flex-1 ${isOpen ? "md:ml-56" : ""} ml-14`}
        >
          <div className="flex-1 p-4 min-h-0">
            <Outlet />
          </div>
        </div>
      </div>
    </div>
  );
};

export default TransactionStaffLayout;
