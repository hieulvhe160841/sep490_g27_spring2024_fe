import React, { useState } from "react";
import { Navigate, Outlet } from "react-router-dom";
import Header from "../Header/Header";
import Sidebar from "../Sidebar/AdminSidebar";
import userStore from "../../store/userStore";
import { toast } from "react-toastify";

const Layout = () => {
  const { isOpen, setIsOpen } = userStore();
  const role = localStorage.getItem("role");
  if (role === null) {
    toast.warn("Bạn phải đăng nhập trước!");
    return <Navigate to="/login" replace />;
  }
  if (role !== "ROLE_ADMIN") {
    toast.warn("Bạn không có quyền truy cập vào trang này!");
    return <Navigate to="/" replace />;
  }
  return (
    <div>
      <Header />
      <div className="h-screen w-screen flex flex-row ">
        <Sidebar isOpen={isOpen} toggleSidebar={setIsOpen} />
        <div
          className={`flex flex-col flex-1 ${
            isOpen ? "md:ml-56" : "md:ml-14"
          } ml-14`}
        >
          <div className="flex-1 p-4 min-h-0">
            <Outlet />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Layout;
