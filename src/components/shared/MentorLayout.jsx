import React, { useState } from "react";
import { Navigate, Outlet } from "react-router-dom";
import Header from "../Header/Header";
import { toast } from "react-toastify";
import userStore from "../../store/userStore";
import MentorSidebar from "./../Sidebar/MentorSidebar";

const MentorLayout = () => {
  const { isOpen, setIsOpen } = userStore();
  const role = localStorage.getItem("role");
  if (role === null) {
    toast.warn("Bạn phải đăng nhập trước!");
    return <Navigate to="/login" replace />;
  }
  if (role !== "ROLE_MENTOR") {
    toast.warn("Bạn không có quyền truy cập vào trang này!");
    return <Navigate to="/" replace />;
  }
  return (
    <div>
      <Header />
      <div className="h-screen flex flex-row ">
        <MentorSidebar isOpen={isOpen} toggleSidebar={setIsOpen} />
        <div className={`flex flex-col flex-1 ${isOpen ? "md:ml-56" : ""}`}>
          <div className="flex-1">
            <Outlet />
          </div>
        </div>
      </div>
    </div>
  );
};

export default MentorLayout;
