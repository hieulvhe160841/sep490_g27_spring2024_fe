import React from "react";
import { Link } from "react-router-dom";
import ModalViewImg from "../Modal/ModalViewImg";

function DetailsInformation({ mentor }) {
  return (
    <>
      <div className="bg-white shadow-2xl p-4 mr-5 border rounded-2xl overflow-hidden">
        <h2 className="text-xl font-bold mt-6 mb-4">Kinh nghiệm</h2>
        <div className="mb-6">
          <p className="mt-2">{mentor.mentorDetail.userDetail.experience}</p>
        </div>
        <h2 className="text-xl font-bold mt-6 mb-4">Chứng chỉ</h2>
        {mentor.certificates.map((cert) => {
          return (
            <div className="mb-6" key={cert.certificateId}>
              <div className=" w-full">
                <span className="text-gray-700 font-bold">
                  {cert.certificateName}
                </span>
                <div className=" text-blue-600 items-start">
                  <ModalViewImg cert={cert} />
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
}

export default DetailsInformation;
