import React, { useState } from "react";
import { Button, Modal } from "@mui/material";
import { FaStar } from "react-icons/fa";
import { useNavigate } from "react-router-dom";
import userStore from "../../store/userStore";
import menteeStore from "../../store/menteeStore";
import { toast } from "react-toastify";

function BasicInformation({ mentor, username }) {
  const navigate = useNavigate();
  const { baseUrl } = userStore();
  const { requestMentorDirectly } = menteeStore();
  const [showModal, setShowModal] = useState(false);
  const [hours, setHours] = useState("");
  const role = localStorage.getItem("role");
  const menteeRole = "ROLE_MENTEE";
  const mentorRole = "ROLE_MENTOR";
  const eduStaffRole = "ROLE_EDUCATION_STAFF";
  const transStaffRole = "ROLE_TRANSACTION_STAFF";
  const adminRole = "ROLE_ADMIN";

  const handleContactClick = () => {
    if (role === menteeRole) {
      navigate("/mentee/chat", { state: { username } });
    } else if (role === mentorRole) {
      navigate("/mentor/chat", { state: { username } });
    } else if (role === transStaffRole) {
      navigate("/staffTransaction/chat", { state: { username } });
    } else if (role === eduStaffRole) {
      navigate("/staffEducation/chat", { state: { username } });
    } else if (role === adminRole) {
      navigate("/admin/chat", { state: { username } });
    } else {
      toast.warning("Bạn phải đăng nhập trước");
      navigate("/login");
    }
  };

  const handleRequestClick = async () => {
    if (role === menteeRole) {
      setShowModal(true);
    } else if (role) {
      toast.warning("Tính năng này chỉ dành cho Học viên.");
    } else {
      toast.warning("Bạn phải đăng nhập trước");
      navigate("/login");
    }
  };

  const handleHoursChange = (e) => {
    const value = e.target.value;
    if (value === "" || parseInt(value) >= 0) {
      setHours(value);
    }
  };

  const handleSubmitRequest = async (e) => {
    e.preventDefault();
    try {
      await requestMentorDirectly(username, hours);
      setShowModal(false);
      toast.success("Gửi yêu cầu thành công");
    } catch (error) {
      toast.error(error);
    }
  };

  return (
    <>
      <div className="bg-white py-6 pl-1 border rounded-2xl shadow-2xl">
        <div className="flex flex-col">
          <div className="flex justify-center">
            <img
              src={`${baseUrl}/view/${mentor.mentorDetail.userDetail.avatar}`}
              className="w-32 h-32 bg-gray-300 rounded-full mb-4 shrink-0 shadow-2xl border-2 absolute -top-16"
            ></img>
          </div>
          <div className="flex flex-col mt-16 ml-4 overflow-auto">
            <p className="text-2xl font-semibold mb-2 text-sidebarTop">
              Tên:&nbsp;
              <span className="text-gray-700">
                {mentor.mentorDetail.userDetail.name}
              </span>
            </p>
            <div className="flex items-center pb-2">
              {mentor.mentorDetail.rating ? (
                <p className="text-xl font-semibold flex items-center text-sidebarTop gap-x-1">
                  Đánh giá:{" "}
                  <span className="text-gray-700">
                    {mentor.mentorDetail.rating.toLocaleString("en-US", {
                      maximumFractionDigits: 1,
                    })}
                  </span>
                  <span className="pb-3">
                    <FaStar color="orange" />
                  </span>
                </p>
              ) : (
                <p className="text-lg font-semibold mr-1 text-blue-400">
                  Chưa đánh giá
                </p>
              )}
            </div>
            <p className="text-lg font-semibold mb-2 text-sidebarTop">
              Email:&nbsp;
              <span className="text-gray-700">
                {mentor.mentorDetail.userDetail.email}
              </span>
            </p>
            <p className="text-lg font-semibold mb-2">
              {mentor.mentorDetail.userDetail.numberOfDoneSupport} Yêu cầu/Bài
              đăng
            </p>
            <p className="text-lg font-semibold mb-2">
              {mentor.mentorDetail.costPerHour
                ? mentor.mentorDetail.costPerHour.toLocaleString("en-US", {
                    maximumFractionDigits: 0,
                  })
                : 0}
              &nbsp;VNĐ/h
            </p>
          </div>
          <div className="mt-6 flex flex-wrap gap-4 justify-center">
            <Button
              variant="outlined"
              sx={{
                borderColor: "#0B4B88",
                color: "#0B4B88",
                backgroundColor: "#ffffff",
                borderRadius: "12px",
                borderWidth: "2px",
                boxSizing: "border-box",
                width: "120px",
                height: "40px",
                "&:hover": {
                  backgroundColor: "#ffffff",
                },
              }}
              onClick={handleRequestClick}
            >
              Yêu cầu
            </Button>
            <Button
              variant="contained"
              sx={{
                backgroundColor: "#0B4B88",
                borderRadius: "12px",
                "&:hover": {
                  backgroundColor: "#083B5F",
                },
                boxSizing: "border-box",
                width: "120px",
                height: "40px",
              }}
              onClick={handleContactClick}
            >
              Liên hệ
            </Button>
          </div>
        </div>
        {showModal ? (
          <>
            <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
              <div className="relative w-auto my-6 mx-auto max-w-3xl">
                {/*content*/}
                <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                  <form onSubmit={handleSubmitRequest}>
                    {/*header*/}
                    <div className="flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
                      <h3 className="text-3xl font-semibold">Thời gian thuê</h3>
                      <button
                        className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                        onClick={() => setShowModal(false)}
                      >
                        <span className="bg-transparent text-black h-6 w-6 text-2xl block outline-none focus:outline-none">
                          ×
                        </span>
                      </button>
                    </div>
                    {/*body*/}
                    <div className="relative p-6 flex-auto max-h-96 overflow-y-auto">
                      <div className="w-full max-w-sm">
                        <div className="md:flex md:items-center mb-6 justify-center">
                          Bạn muốn thuê gia sư này bao nhiêu giờ?
                        </div>
                        <div className="md:flex md:items-center mb-6 text-sm justify-center">
                          Hãy nhập chính xác số giờ bạn muốn thuê!
                        </div>
                        <div className="md:flex md:items-center mb-6 justify-center">
                          <input
                            className="inputField"
                            id="inline-description"
                            type="text"
                            value={hours}
                            onChange={handleHoursChange}
                          />
                        </div>
                      </div>
                    </div>
                    {/*footer*/}
                    <div className="btnInModal">
                      <Button
                        type="submit"
                        variant="outlined"
                        sx={{
                          border: "2px solid #2B90D9",
                          textTransform: "none",
                          borderRadius: "12px",
                          height: "35px",
                          width: "115px",
                          color: "#000000",
                        }}
                        disabled={!hours || hours <= 0}
                        onClick={handleSubmitRequest}
                      >
                        Gửi
                      </Button>
                      <Button
                        variant="contained"
                        color="error"
                        sx={{
                          backgroundColor: "#FF0A0A",
                          textTransform: "none",
                          borderRadius: "12px",
                          height: "35px",
                          width: "115px",
                          color: "#ffffff",
                        }}
                        onClick={() => setShowModal(false)}
                      >
                        Đóng
                      </Button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
          </>
        ) : null}
        <hr className="my-6 border-t border-gray-300" />
        <div className="flex flex-col ml-2">
          <span className="text-lg font-semibold mb-2 text-sidebarTop">
            Trình độ học vấn:{" "}
            <span className="text-gray-700">
              {mentor.mentorDetail.userDetail.educationLevel}
            </span>
          </span>
          <ul>
            <div className="text-lg font-semibold mb-2 text-sidebarTop">
              {" "}
              Kĩ năng:
            </div>

            {mentor.skills.map((s) => {
              return (
                <li className="mb-2" key={s.skillId}>
                  {s.skillName}
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    </>
  );
}

export default BasicInformation;
