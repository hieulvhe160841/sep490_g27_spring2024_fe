import React, { useState } from "react";
import { FaStar } from "react-icons/fa";
import Pagination from "@mui/material/Pagination";

function FeedBackComponent({ mentor }) {
  const feedback = mentor.supports;
  const [currentPage, setCurrentPage] = useState(1);
  const pageSize = 7;
  const handlePageChange = (event, page) => {
    setCurrentPage(page);
  };
  let paginatedFeedback = [];
  if (feedback && feedback.length > 0) {
    const startIndex = (currentPage - 1) * pageSize;
    const endIndex = Math.min(startIndex + pageSize, feedback.length);
    paginatedFeedback = feedback.slice(startIndex, endIndex);
  }
  return (
    <>
      <div className="text-3xl font-semibold mb-5 text-sidebarTop">
        Feedback
      </div>
      <div className="bg-gray-100 shadow-2xl p-6 border rounded-2xl overflow-hidden">
        <p className="text-gray-700"></p>

        <div className="mb-6 divide-y-2 divide-gray-300">
          {feedback ? (
            paginatedFeedback.map((m) => {
              return (
                m.rating && (
                  <div key={m.id}>
                    <div className="flex justify-between flex-wrap gap-2 w-full mt-3 p-2 ">
                      <span className="text-gray-700 font-bold">
                        {m.menteeName}
                      </span>
                    </div>
                    <div className="flex items-center mt-2 ml-3 mb-3">
                      <p className="text-xl font-semibold mr-1">{m.rating}</p>
                      <span>
                        <FaStar color="orange" />
                      </span>
                      <p className="ml-4">
                        {m.supportFeedback ? (
                          <div>{m.supportFeedback}</div>
                        ) : null}
                      </p>
                    </div>
                  </div>
                )
              );
            })
          ) : (
            <div>Gia sư này chưa có đánh giá</div>
          )}
        </div>
      </div>
      {feedback && feedback.length > 0 && (
        <Pagination
          count={Math.ceil(feedback.length / pageSize)}
          page={currentPage}
          onChange={handlePageChange}
          color="primary"
          className="flex flex-col items-center mt-10"
        />
      )}
    </>
  );
}

export default FeedBackComponent;
