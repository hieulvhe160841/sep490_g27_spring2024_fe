import React from "react";
import { IoKeyOutline } from "react-icons/io5";
import { FaRegUserCircle } from "react-icons/fa";
import { IconContext } from "react-icons";
import Title from "../../components/Title";
import { useEffect, useState } from "react";
import { Link, Navigate, useNavigate } from "react-router-dom";
import userStore from "../../store/userStore";
import InputComponent from "../../components/InputComponent";
import { Button } from "@mui/material";
import { CgSpinnerAlt } from "react-icons/cg";
import { toast } from "react-toastify";
import RedirectToHome from "../RedirectPage/RedirectToHome";
import { IoIosArrowBack } from "react-icons/io";
import { FaEye } from "react-icons/fa";
import { FaEyeSlash } from "react-icons/fa";

function FormLogin() {
  const navigate = useNavigate();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isShowPassword, setIsShowPassword] = useState(false);
  const { loginContext, isLoading } = userStore();

  const onHandleLogin = async () => {
    if (!username || !password) {
      toast.warn("Tài khoản/mật khẩu là bắt buộc!");
      return;
    }

    let res = await loginContext(username.trim(), password);

    if (res && res.status === 200) {
      toast.success("Đăng nhập thành công!");
      console.log(res);

      if (res.data.data.role === "ROLE_ADMIN") {
        navigate("/admin");
      } else if (res.data.data.role === "ROLE_EDUCATION_STAFF") {
        navigate("/staffEducation");
      } else if (res.data.data.role === "ROLE_TRANSACTION_STAFF") {
        navigate("/staffTransaction");
      } else if (res.data.data.role === "ROLE_MENTOR") {
        navigate("/mentor");
      } else if (res.data.data.role === "ROLE_MENTEE") {
        navigate("/mentee");
      }
    } else {
      //error
      if (res && res.status !== 200) {
        toast.error("Đăng nhập thất bại");
      }
    }
  };
  const onHandleEnterSubmit = (event) => {
    if (event.keyCode === 13) {
      onHandleLogin();
    }
  };
  return (
    <>
      <form className="space-y-6" action="login" method="POST">
        <div>
          <div className="inputForm">
            <IconContext.Provider
              value={{
                className: "ml-2 mt-3 iconForm",
              }}
            >
              <div>
                <FaRegUserCircle />
              </div>
            </IconContext.Provider>
            <InputComponent
              id="username"
              name="username"
              type="text"
              placeholder="Nhập tài khoản..."
              onChange={(event) => {
                setUsername(event.target.value);
              }}
            />
          </div>
        </div>

        <div>
          <div className="inputForm">
            <IconContext.Provider
              value={{
                className: "ml-2 mt-3 iconForm",
              }}
            >
              <div>
                <IoKeyOutline />
              </div>
            </IconContext.Provider>
            <InputComponent
              id="password"
              name="password"
              type={`${isShowPassword ? "text" : "password"}`}
              autoComplete="current-password"
              placeholder="Nhập mật khẩu..."
              required
              className="block w-full rounded-xl border-0 py-4 pl-14 bg-table font-normal shadow-sm relative"
              onChange={(event) => {
                setPassword(event.target.value);
              }}
              onKeyDown={(e) => {
                onHandleEnterSubmit(e);
              }}
            />
            <div
              className="absolute right-3 top-5"
              onClick={() => setIsShowPassword(!isShowPassword)}
            >
              <FaEye className={`${isShowPassword ? "hidden" : ""}`}></FaEye>
              <FaEyeSlash
                className={`${isShowPassword ? "" : "hidden"}`}
              ></FaEyeSlash>
            </div>
          </div>
        </div>
        {/*  */}

        <Button
          type="submit"
          variant="contained"
          size="large"
          disabled={isLoading ? true : false}
          className="mx-auto w-full"
          sx={{
            backgroundColor: "#0B4B88",
            color: "white",
          }}
          onClick={() => {
            onHandleLogin();
          }}
        >
          Đăng nhập&nbsp;
          {isLoading && (
            <svg
              className="animate-spin h-5 w-5 mr-5 text-white ..."
              viewBox="0 0 17 17"
            >
              <CgSpinnerAlt />
            </svg>
          )}
        </Button>
      </form>
      <div className="flex flex-col justify-center items-center mt-5">
        <div className="text-center">
          Bạn chưa có tài khoản? &nbsp;
          <Link
            to="/register"
            className={` text-gray-600 ${
              isLoading ? "pointer-events-none" : ""
            }`}
          >
            Tạo tài khoản mới
          </Link>
          <div className="mr-5">hoặc</div>
          <Link
            to="/forgotPassword"
            className={` text-gray-600 ${
              isLoading ? "pointer-events-none" : ""
            }`}
          >
            Quên mật khẩu
          </Link>
        </div>
        <div
          className={`mt-6 ${
            isLoading ? "pointer-events-none text-gray-400" : ""
          }`}
        >
          <Link to="/" className="underline">
            <IoIosArrowBack className="inline" />
            Back to Homepage
          </Link>
        </div>
      </div>
    </>
  );
}

export default FormLogin;
