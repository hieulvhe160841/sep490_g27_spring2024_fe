import * as React from "react";
import { DataGrid } from "@mui/x-data-grid";
import { IconButton, styled } from "@mui/material";
import { CiEdit } from "react-icons/ci";
import { RiDeleteBin5Line } from "react-icons/ri";
import { Link } from "react-router-dom";
import { useState } from "react";
import userStore from "../../store/userStore";
import { useEffect } from "react";
import LoadingState from "../LoadingState";

const columns = [
  {
    field: "username",
    headerName: "Tài khoản",
    flex: 1,
    align: "left",
    headerAlign: "left",
    headerClassName: "header-color",
    renderHeader: (params) => (
      <span className="font-bold">{params.colDef.headerName}</span>
    ),
  },
  {
    field: "name",
    headerName: "Họ Tên",
    flex: 1,
    align: "left",
    headerAlign: "left",
    headerClassName: "header-color",
    renderHeader: (params) => (
      <span className="font-bold">{params.colDef.headerName}</span>
    ),
  },
  {
    field: "role",
    headerName: "Role",
    flex: 1.25,
    align: "left",
    headerAlign: "left",
    headerClassName: "header-color",
    renderHeader: (params) => (
      <span className="font-bold">{params.colDef.headerName}</span>
    ),
  },
  {
    field: "status",
    headerName: "Trạng thái",
    flex: 0.5,
    align: "center",
    headerAlign: "center",
    headerClassName: "header-color",
    renderHeader: (params) => (
      <span className="font-bold">{params.colDef.headerName}</span>
    ),
    renderCell: (params) => (
      <span
        style={{
          backgroundColor:
            params.row.status === "Hoạt động" ? "limeGreen" : "red",
          color: "white",
          borderRadius: "5px",
          padding: "5px 10px",
          display: "inline-block",
        }}
      >
        {params.row.status}
      </span>
    ),
  },
  {
    field: "action",
    headerName: "Thao tác",
    flex: 0.5,
    align: "center",
    headerAlign: "center",
    headerClassName: "header-color",
    cellClassName: "last-column",
    renderHeader: (params) => (
      <span className="font-bold">{params.colDef.headerName}</span>
    ),
    renderCell: (params) => (
      <div className="space-x-1">
        <Link to={`/admin/userInfo/${params.row.username}`}>
          <IconButton variant="contained">
            <CiEdit />
          </IconButton>
        </Link>
      </div>
    ),
  },
];

const StyledDataGrid = styled(DataGrid)(({ theme }) => ({
  "& .MuiDataGrid-cell": {
    borderBottom: `1px solid darkGray`,
  },
  "& .MuiDataGrid-cell.last-column": {
    borderRight: `none`,
  },
}));

export default function UserDataTable() {
  const { isLoading, listUsers, fetchUsers } = userStore();

  useEffect(() => {
    fetchUsers();
  }, []);

  const getRoleText = (role) => {
    switch (role) {
      case "ROLE_TRANSACTION_STAFF":
        return "Nhân viên giao dịch";
      case "ROLE_EDUCATION_STAFF":
        return "Nhân viên quản lý";
      case "ROLE_MENTOR":
        return "Gia sư";
      case "ROLE_MENTEE":
        return "Học viên";
      case "ROLE_ADMIN":
        return "Quản trị viên";
      default:
        return role;
    }
  };

  const mappedRows = listUsers.map((user) => ({
    ...user,
    id: user.userId,
    username: user.userName,
    role: getRoleText(user.roleDTO.roleName).toUpperCase(),
    status: user.status ? "Hoạt động" : "Không hoạt động",
  }));

  return (
    <div className="bg-white rounded-md h-[400] w-[90%]">
      {isLoading ? (
        <LoadingState />
      ) : (
        <StyledDataGrid
          rows={mappedRows}
          columns={columns}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 7 },
            },
          }}
          sx={{
            "& .header-color": {
              color: "#0B4B88",
              fontWeight: "700",
              fontSize: "1.125rem",
              lineHeight: "1.75rem",
            },
          }}
          pageSizeOptions={[7, 10]}
          disableRowSelectionOnClick
        />
      )}
    </div>
  );
}
