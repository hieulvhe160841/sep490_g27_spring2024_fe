import React, { useEffect } from "react";
import { DataGrid } from "@mui/x-data-grid";
import { styled } from "@mui/material";
import menteeStore from "../../store/menteeStore";
import LoadingState from "../LoadingState";

const columns = [
  {
    field: "id",
    headerName: "STT",
    flex: 0.5,
    align: "center",
    headerAlign: "center",
    headerClassName: "header-color",
    renderHeader: (params) => (
      <span className="font-bold">{params.colDef.headerName}</span>
    ),
  },
  {
    field: "orderId",
    headerName: "Mã giao dịch",
    flex: 1,
    align: "center",
    headerAlign: "center",
    headerClassName: "header-color",
    renderHeader: (params) => (
      <span className="font-bold">{params.colDef.headerName}</span>
    ),
  },
  {
    field: "value",
    headerName: "Giá trị",
    flex: 1,
    align: "center",
    headerAlign: "center",
    headerClassName: "header-color",
    renderHeader: (params) => (
      <span className="font-bold">{params.colDef.headerName}&nbsp;(VNĐ)</span>
    ),
  },
  {
    field: "createdDate",
    headerName: "Ngày tạo",
    flex: 1,
    align: "center",
    headerAlign: "center",
    headerClassName: "header-color",
    renderHeader: (params) => (
      <span className="font-bold">{params.colDef.headerName}</span>
    ),
  },
];

const StyledDataGrid = styled(DataGrid)(({ theme }) => ({
  "& .MuiDataGrid-cell": {
    borderBottom: `1px solid darkGray`,
  },
  "& .MuiDataGrid-cell.last-column": {
    borderRight: `none`,
  },
}));

const MenteeTransactionDataTable = () => {
  const { isLoading, listMenteeTransaction, fetchMenteeTransaction } =
    menteeStore();

  useEffect(() => {
    fetchMenteeTransaction();
  }, []);

  const mappedRows = listMenteeTransaction.map((transaction, index) => ({
    id: index + 1,
    orderId: transaction.orderId,
    value: transaction.value.toLocaleString("en-US", {
      maximumFractionDigits: 0,
    }),
    createdDate: transaction.createdDate,
  }));

  return (
    <div className="bg-white rounded-md h-[400] w-[80%]">
      {isLoading ? (
        <LoadingState />
      ) : listMenteeTransaction.length > 0 ? (
        <StyledDataGrid
          rows={mappedRows}
          columns={columns}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 7 },
            },
          }}
          sx={{
            "& .header-color": {
              color: "#0B4B88",
              fontWeight: "700",
              fontSize: "1.125rem",
              lineHeight: "1.75rem",
            },
          }}
          pageSizeOptions={[7, 10]}
          disableRowSelectionOnClick
        />
      ) : (
        <div className="text-3xl text-center mt-3">
          Bạn chưa có giao dịch nào :(
        </div>
      )}
    </div>
  );
};

export default MenteeTransactionDataTable;
