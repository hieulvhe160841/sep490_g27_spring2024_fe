import React from "react";
import { DataGrid } from "@mui/x-data-grid";
import { IconButton, styled } from "@mui/material";
import { CiEdit } from "react-icons/ci";
import { Link } from "react-router-dom";
import userStore from "../../store/userStore";
import { useEffect } from "react";
import LoadingState from "../LoadingState";

const columns = [
  {
    field: "id",
    headerName: "ID",
    flex: 0.5,
    align: "left",
    headerAlign: "left",
    headerClassName: "header-color",
    renderHeader: (params) => (
      <span className="font-bold">{params.colDef.headerName}</span>
    ),
  },
  {
    field: "skillName",
    headerName: "Tên Kỹ năng",
    flex: 0.75,
    align: "left",
    headerAlign: "left",
    headerClassName: "header-color",
    renderHeader: (params) => (
      <span className="font-bold">{params.colDef.headerName}</span>
    ),
  },
  {
    field: "description",
    headerName: "Mô tả",
    flex: 1.5,
    align: "left",
    headerAlign: "left",
    headerClassName: "header-color",
    renderHeader: (params) => (
      <span className="font-bold">{params.colDef.headerName}</span>
    ),
  },
  {
    field: "status",
    headerName: "Trạng thái",
    flex: 0.5,
    align: "center",
    headerAlign: "center",
    headerClassName: "header-color",
    renderHeader: (params) => (
      <span className="font-bold">{params.colDef.headerName}</span>
    ),
    renderCell: (params) => (
      <span
        style={{
          backgroundColor:
            params.row.status === "Hoạt động" ? "limeGreen" : "red",
          color: "white",
          borderRadius: "5px",
          padding: "5px 10px",
          display: "inline-block",
        }}
      >
        {params.row.status}
      </span>
    ),
  },
  {
    field: "action",
    headerName: "Thao tác",
    flex: 0.5,
    align: "center",
    headerAlign: "center",
    headerClassName: "header-color",
    cellClassName: "last-column",
    renderHeader: (params) => (
      <span className="font-bold">{params.colDef.headerName}</span>
    ),
    renderCell: (params) => (
      <div className="space-x-1">
        <Link to={`/admin/skillInfo/${params.row.id}`}>
          <IconButton variant="contained" color="success">
            <CiEdit />
          </IconButton>
        </Link>
      </div>
    ),
  },
];

const StyledDataGrid = styled(DataGrid)(({ theme }) => ({
  "& .MuiDataGrid-cell": {
    borderBottom: `1px solid darkGray`,
  },
  "& .MuiDataGrid-cell.last-column": {
    borderRight: `none`,
  },
}));

const SkillDataTable = () => {
  const { isLoading, listSkills, fetchSkills } = userStore();

  useEffect(() => {
    fetchSkills();
  }, []);

  const mappedRows = listSkills.map((skill) => ({
    id: skill.skillId,
    skillName: skill.skillName,
    description: skill.skillDescription,
    status: skill.skillStatus ? "Hoạt động" : "Không hoạt động",
  }));

  return (
    <div className="bg-white rounded-md h-[400] w-[90%]">
      {isLoading ? (
        <LoadingState />
      ) : listSkills.length > 0 ? (
        <StyledDataGrid
          rows={mappedRows}
          columns={columns}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 7 },
            },
          }}
          sx={{
            "& .header-color": {
              color: "#0B4B88",
              fontWeight: "700",
              fontSize: "1.125rem",
              lineHeight: "1.75rem",
            },
          }}
          pageSizeOptions={[7, 10]}
          disableRowSelectionOnClick
        />
      ) : (
        <div className="text-3xl text-center mt-3">
          Trang web chưa có Skill nào :(
        </div>
      )}
    </div>
  );
};

export default SkillDataTable;
