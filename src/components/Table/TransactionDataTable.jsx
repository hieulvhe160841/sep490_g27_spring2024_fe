import React, { useEffect } from "react";
import { DataGrid } from "@mui/x-data-grid";
import { styled } from "@mui/material";
import userStore from "../../store/userStore";
import LoadingState from "../LoadingState";

const columns = [
  {
    field: "id",
    headerName: "ID",
    flex: 0.5,
    align: "center",
    headerAlign: "center",
    headerClassName: "header-color",
    renderHeader: (params) => (
      <span className="font-bold">{params.colDef.headerName}</span>
    ),
  },
  {
    field: "amount",
    headerName: "Số tiền (VNĐ)",
    flex: 1,
    align: "center",
    headerAlign: "center",
    headerClassName: "header-color",
    renderHeader: (params) => (
      <span className="font-bold">{params.colDef.headerName}</span>
    ),
  },
  {
    field: "type",
    headerName: "Loại giao dịch",
    flex: 1.25,
    align: "center",
    headerAlign: "center",
    headerClassName: "header-color",
    renderHeader: (params) => (
      <span className="font-bold">{params.colDef.headerName}</span>
    ),
    renderCell: (params) => (
      <div
        style={{
          color: "white",
          borderRadius: "5px",
          padding: "5px 10px",
          display: "inline-block",
          backgroundColor:
            params.value === "NẠP TIỀN"
              ? "#3f51b5"
              : params.value === "THUÊ GIA SƯ"
              ? "#4caf50"
              : params.value === "MUA KHÓA HỌC"
              ? "#ff5722"
              : "blue",
        }}
      >
        {params.value}
      </div>
    ),
  },
  {
    field: "time",
    headerName: "Mốc thời gian",
    flex: 1,
    align: "center",
    headerAlign: "center",
    headerClassName: "header-color",
    renderHeader: (params) => (
      <span className="font-bold">{params.colDef.headerName}</span>
    ),
  },
  {
    field: "status",
    headerName: "Trạng thái",
    flex: 1,
    align: "center",
    headerAlign: "center",
    headerClassName: "header-color",
    renderHeader: (params) => (
      <span className="font-bold">{params.colDef.headerName}</span>
    ),
  },
];

const StyledDataGrid = styled(DataGrid)(({ theme }) => ({
  "& .MuiDataGrid-cell": {
    borderBottom: `1px solid darkGray`,
  },
  "& .MuiDataGrid-cell.last-column": {
    borderRight: `none`,
  },
}));

export default function TransactionDataTable() {
  const { isLoading, listTransaction, fetchTransactions } = userStore();

  useEffect(() => {
    fetchTransactions();
  }, []);

  const transactionTypeToVietnamese = (type) => {
    switch (type) {
      case "DENOMINATION":
        return "NẠP TIỀN";
      case "SUPPORT":
        return "THUÊ GIA SƯ";
      case "COURSE_PURCHASE":
        return "MUA KHÓA HỌC";
      default:
        return type;
    }
  };

  const statusToVietnamese = (status) => {
    switch (status) {
      case "DONE":
        return "Hoàn thành";
      case "done":
        return "Hoàn thành";
      case "CANCELED":
        return "Đã hủy";
      case "PENDING":
        return "Chờ xử lý";
      default:
        return status;
    }
  };

  const mappedRows = listTransaction.map((transaction) => ({
    id: transaction.transactionId,
    amount: transaction.amount.toLocaleString("en-US", {
      maximumFractionDigits: 0,
    }),
    type: transactionTypeToVietnamese(transaction.transactionType),
    time: transaction.timeStamp,
    status: statusToVietnamese(transaction.status),
  }));

  return (
    <div className="bg-white rounded-md h-[400] w-[85%]">
      {isLoading ? (
        <LoadingState />
      ) : listTransaction.length > 0 ? (
        <StyledDataGrid
          rows={mappedRows}
          columns={columns}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 7 },
            },
          }}
          sx={{
            "& .header-color": {
              color: "#0B4B88",
              fontWeight: "700",
              fontSize: "1.125rem",
              lineHeight: "1.75rem",
            },
          }}
          pageSizeOptions={[7, 10]}
          disableRowSelectionOnClick
        />
      ) : (
        <div className="text-3xl text-center mt-3">
          Trang web chưa có giao dịch nào :(
        </div>
      )}
    </div>
  );
}
