import React from "react";
import { IoKeyOutline } from "react-icons/io5";
import { BiSolidLockAlt } from "react-icons/bi";
import { IconContext } from "react-icons";
import InputComponent from "../../components/InputComponent";
import { useState } from "react";
import { Button } from "@mui/material";
import { toast } from "react-toastify";
import userStore from "../../store/userStore";
import { CgSpinnerAlt } from "react-icons/cg";
import { useNavigate } from "react-router-dom";
import { FaEye } from "react-icons/fa";
import { FaEyeSlash } from "react-icons/fa";

function FormChangePassword() {
  const { changePasswordApi, isLoading } = userStore();
  const [oldpassword, setOldPassword] = useState("");
  const [newpassword, setNewPassword] = useState("");
  const [repassword, setRePassword] = useState("");
  const [isShowPassword, setIsShowPassword] = useState(false);
  const [isShowNewPassword, setIsShowNewPassword] = useState(false);
  const [isShowRePassword, setIsShowRePassword] = useState(false);

  const navigate = useNavigate();

  const onHandleConfirmPassword = () => {
    if (repassword !== newpassword) {
      toast.warn("Mật khẩu xác nhận phải trùng khớp với mật khẩu mới!");
      return false;
    }
    return true;
  };
  const onHandleChangePassword = async () => {
    if (onHandleConfirmPassword) {
      const res = await changePasswordApi(oldpassword, newpassword, repassword);
      if (res && res.status === 200) {
        toast.success(res.data.messages);
        navigate("/");
      } else {
        //error
        if (res && res.status !== 200) {
          toast.error(res.data.messages);
          console.log(res);
        }
      }
    }
  };
  return (
    <>
      <div className="space-y-6" action="changePassword" method="POST">
        <div>
          <div className="inputForm">
            <IconContext.Provider
              value={{
                className: "ml-2 mt-3 iconForm",
              }}
            >
              <div>
                <IoKeyOutline />
              </div>
            </IconContext.Provider>
            <InputComponent
              id="oldpassword"
              name="oldpassword"
              type={`${isShowPassword ? "text" : "password"}`}
              className="block w-full rounded-xl border-0 py-4 pl-14 bg-table font-normal shadow-sm relative"
              placeholder="Nhập mật khẩu cũ..."
              onChange={(event) => {
                setOldPassword(event.target.value);
              }}
            />
            <div
              className="absolute right-3 top-5"
              onClick={() => setIsShowPassword(!isShowPassword)}
            >
              <FaEye className={`${isShowPassword ? "hidden" : ""}`}></FaEye>
              <FaEyeSlash
                className={`${isShowPassword ? "" : "hidden"}`}
              ></FaEyeSlash>
            </div>
          </div>
        </div>
        <div>
          <div className="inputForm">
            <IconContext.Provider
              value={{
                className: "ml-2 mt-3 iconForm",
              }}
            >
              <div>
                <IoKeyOutline />
              </div>
            </IconContext.Provider>
            <InputComponent
              id="newpassword"
              name="newpassword"
              type={`${isShowNewPassword ? "text" : "password"}`}
              className="block w-full rounded-xl border-0 py-4 pl-14 bg-table font-normal shadow-sm relative"
              placeholder="Nhập mật khẩu mới..."
              onChange={(event) => {
                setNewPassword(event.target.value);
              }}
            />
            <div
              className="absolute right-3 top-5"
              onClick={() => setIsShowNewPassword(!isShowNewPassword)}
            >
              <FaEye className={`${isShowNewPassword ? "hidden" : ""}`}></FaEye>
              <FaEyeSlash
                className={`${isShowNewPassword ? "" : "hidden"}`}
              ></FaEyeSlash>
            </div>
          </div>
        </div>

        <div>
          <div className="inputForm">
            <IconContext.Provider
              value={{
                className: "ml-2 mt-3 iconForm",
              }}
            >
              <div>
                <BiSolidLockAlt />
              </div>
            </IconContext.Provider>
            <InputComponent
              id="repassword"
              name="repassword"
              type={`${isShowRePassword ? "text" : "password"}`}
              className="block w-full rounded-xl border-0 py-4 pl-14 bg-table font-normal shadow-sm relative"
              placeholder="Xác nhận mật khẩu mới..."
              onChange={(event) => {
                setRePassword(event.target.value);
              }}
            />
            <div
              className="absolute right-3 top-5"
              onClick={() => setIsShowRePassword(!isShowRePassword)}
            >
              <FaEye className={`${isShowRePassword ? "hidden" : ""}`}></FaEye>
              <FaEyeSlash
                className={`${isShowRePassword ? "" : "hidden"}`}
              ></FaEyeSlash>
            </div>
          </div>
        </div>

        <div>
          <Button
            type="submit"
            variant="contained"
            size="large"
            disabled={isLoading ? true : false}
            className="mx-auto w-full"
            sx={{
              backgroundColor: "#0B4B88",
              color: "white",
            }}
            onClick={() => {
              onHandleChangePassword();
            }}
          >
            Change&nbsp;
            {isLoading && (
              <svg
                className="animate-spin h-5 w-5 mr-5 text-white ..."
                viewBox="0 0 17 17"
              >
                <CgSpinnerAlt />
              </svg>
            )}
          </Button>
        </div>
      </div>
    </>
  );
}

export default FormChangePassword;
