import { Button } from "@mui/material";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import userStore from "../../store/userStore";

function BasicInformation({ mentee, username }) {
  const navigate = useNavigate();
  const { baseUrl } = userStore();

  const handleContactClick = () => {
    navigate("/mentor/chat", { state: { username } });
  };
  return (
    <>
      <div className="bg-white p-7 border  rounded-2xl overflow-hidden shadow-2xl">
        <div className="flex justify-center">
          <img
            src={`${baseUrl}/view/${mentee.avatar}`}
            className="w-32 h-32 bg-gray-300 rounded-full mb-4 shrink-0 border-2 border-white shadow-2xl absolute -top-16"
          ></img>
        </div>

        <div className="flex flex-col mt-12 ml-6">
          <p className="text-xl font-semibold mb-2">{mentee.name}</p>

          <p className="text-xl text-start font-semibold mr-2">
            {mentee.phone}
          </p>

          <p className="text-lg text-start font-semibold mb-2">
            {mentee.email}
          </p>
          <p className="text-lg text-start font-semibold mb-2">
            {mentee.address}
          </p>
        </div>
        <div className="mt-6 flex flex-wrap gap-4 justify-center">
          <Button
            variant="contained"
            sx={{
              backgroundColor: "#5584C6",
            }}
            type="button"
            onClick={handleContactClick}
          >
            Nhắn tin
          </Button>
        </div>
      </div>
    </>
  );
}

export default BasicInformation;
