import React from "react";
import { Link } from "react-router-dom";

function MoreInformation({ mentee }) {
  return (
    <>
      <div className="bg-white shadow-2xl p-6 border rounded-2xl overflow-hidden">
        <h2 className="text-2xl font-bold mt-6 mb-4">Mục tiêu</h2>
        <div className="mb-6">
          <p className="mt-2 text-base">{mentee.goal}</p>
        </div>
        <h2 className="text-2xl font-bold mt-6 mb-4">Sở thích</h2>
        <div className="mb-6">
          <p className="mt-2 text-base">{mentee.interest}</p>
        </div>
      </div>
    </>
  );
}

export default MoreInformation;
