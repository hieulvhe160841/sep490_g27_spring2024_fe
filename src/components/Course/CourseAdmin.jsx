import React, { useState } from "react";
import Button from "@mui/material/Button";
import { toast } from "react-toastify";
import userStore from "../../store/userStore";
import LoadingState from "../LoadingState";
import { useNavigate } from "react-router-dom";

const CourseAdmin = ({ courseRegis }) => {
  const { approveCourseRequest, baseUrl } = userStore();
  const [feedback, setFeedback] = useState("");
  const navigate = useNavigate();

  const handleApprove = async () => {
    try {
      await approveCourseRequest(
        courseRegis.courseRequestId,
        feedback,
        "accepted"
      );
      toast.success("Chấp thuận khóa học thành công");
    } catch (error) {
      console.error("Error approving course:", error);
      toast.error("Lỗi khi chấp thuận khóa học:", error);
    }
  };

  const handleReject = async () => {
    try {
      await approveCourseRequest(
        courseRegis.courseRequestId,
        feedback,
        "rejected"
      );
      toast.success("Từ chối khóa học thành công");
    } catch (error) {
      console.error("Error rejecting course:", error);
      toast.error("Lỗi khi từ chối khóa học:", error);
    }
    navigate(-1);
  };

  if (!courseRegis) {
    return <LoadingState />;
  }

  return (
    <div className="p-4">
      <div className="flex flex-col lg:flex-row justify-between mb-8">
        <div className="flex space-x-4">
          <img
            src={`${baseUrl}/view/${courseRegis.courseAvatar}`}
            alt="Course"
            className="w-48 h-28"
          />
          <div className="text-textHeader">
            <h2 className="text-2xl font-bold mr-4">
              {courseRegis.courseName}
            </h2>
            <p>Gia sư: {courseRegis.mentorUserName}</p>
          </div>
        </div>
        <div className="flex flex-col items-center space-y-4 my-10">
          <div className="flex space-x-6 items-center">
            <Button
              variant="contained"
              color="info"
              sx={{
                backgroundColor: "#5584C6",
                color: "#ffffff",
                width: "200px",
                height: "50px",
                fontSize: "20px",
                borderRadius: "10px",
                fontWeight: "600",
              }}
              onClick={handleApprove}
            >
              Chấp thuận
            </Button>
            <Button
              variant="contained"
              color="error"
              sx={{
                backgroundColor: "#FF0A0A",
                color: "#ffffff",
                width: "200px",
                height: "50px",
                fontSize: "20px",
                borderRadius: "10px",
                fontWeight: "600",
              }}
              onClick={handleReject}
            >
              Từ chối
            </Button>
          </div>
          <textarea
            className="inputField w-full"
            rows={2}
            type="text"
            placeholder="Nhận xét..."
            value={feedback}
            onChange={(e) => setFeedback(e.target.value)}
            maxLength={255}
          />
        </div>
      </div>

      <div className="mb-4">
        <h3 className="text-textHeader text-xl mb-2">Mô tả chi tiết</h3>
        <div className="bg-gray-100 border border-black p-4 rounded-lg">
          {courseRegis.description}
        </div>
      </div>

      <div className="mb-4">
        <h3 className="text-textHeader text-xl mb-2">
          Video giới thiệu về khóa học
        </h3>
        <div>
          <video
            controls
            width="800"
            height="600"
            src={`${baseUrl}/view/${courseRegis.demoSource}`}
            type="video/mp4"
          ></video>
        </div>
      </div>
    </div>
  );
};

export default CourseAdmin;
