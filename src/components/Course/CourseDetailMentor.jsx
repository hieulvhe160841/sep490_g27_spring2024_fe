import React, { useState } from "react";
import userStore from "../../store/userStore";
import { Button } from "@mui/material";
import mentorStore from "../../store/mentorStore";
import { toast } from "react-toastify";
import { CgSpinnerAlt } from "react-icons/cg";

function CourseDetailMentor({ course }) {
  const { baseUrl } = userStore();
  const [openedVideos, setOpenedVideos] = useState({});
  const [courseVideo, setCourseVideo] = useState();
  const [courseTitleVideo, setCourseTitleVideo] = useState();
  const [avatar, setAvatar] = useState(course.avatar);

  const { viewCourseDetail, addCourseVideo, updateCourse, isLoading } =
    mentorStore();

  const courseName = course.courseName;
  const category = "nghia";
  const price = course.price;
  const description = course.description;

  const toggleVideo = (index) => {
    // Toggle the state for the current video index
    setOpenedVideos((prevOpenedVideos) => ({
      ...prevOpenedVideos,
      [index]: !prevOpenedVideos[index],
    }));
  };
  const onHandleAddVideo = async (e) => {
    e.preventDefault();
    const formData = new FormData(e.currentTarget);

    formData.append(
      "registerCourse",
      JSON.stringify({ courseName, category, price, description })
    );

    console.log(Object.fromEntries(formData));
    const res = await addCourseVideo(formData, course.courseId);
    if (res.statusCode === 201) {
      toast.success("Thêm video thành công");
    } else {
      toast.error(res.data);
    }
    viewCourseDetail(course.courseId);
  };
  const formatNumberWithCommas = (number) => {
    const integerPart = Math.trunc(number);
    return integerPart.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };
  const onHandleUpdateCourse = async (e) => {
    e.preventDefault();
    const formData = new FormData(e.currentTarget);
    const price = formData.get("price");
    const description = formData.get("description");

    formData.append("course", JSON.stringify({ description, price }));
    console.log(Object.fromEntries(formData));
    const res = await updateCourse(formData, course.courseId);
    if (res.statusCode === 200) {
      toast.success("Cập nhật khóa học thành công");
    } else {
      toast.error(res.data);
    }
    console.log(res);
  };

  return (
    <>
      <div className="container mx-auto my-5 p-6">
        <div className="grid grid-cols-1 lg:grid-cols-12 gap-x-5 lg:divide-x-2">
          <form
            className="mb-4 col-span-4 lg:col-span-7"
            onSubmit={(e) => onHandleUpdateCourse(e)}
          >
            <div className="flex space-x-4">
              <img
                src={`${baseUrl}/view/${course.avatar}`}
                alt="Course"
                className="w-[300px] h-[200px] mr-2 rounded-xl border"
              />
              <div className="text-white">
                <h2 className="text-3xl font-semibold ">{courseName}</h2>
              </div>
            </div>

            <h3 className="text-textHeader text-2xl my-12">
              <div className="my-4">
                Giá tiền:&nbsp;
                <input
                  type="text"
                  name="price"
                  className="text-lg rounded-lg text-black bg-gray-200 w-1/4"
                  defaultValue={formatNumberWithCommas(course.price)}
                ></input>
                &nbsp;VNĐ
              </div>
              Mô tả:&nbsp;
              <br />
              <textarea
                rows={5}
                name="description"
                className="text-gray-900 bg-gray-200 rounded-lg w-full p-2"
                defaultValue={description}
                maxLength={255}
              ></textarea>
            </h3>
            <h2 className="text-lg font-semibold text-sidebarTop mt-10">
              Video của khóa học
            </h2>
            <ul className="mt-2">
              {course.videoDTOs && course.videoDTOs.length > 0 ? (
                course.videoDTOs.map((video, index) => (
                  <li key={index} className="items-center border-b py-2">
                    <div
                      onClick={() => toggleVideo(index)}
                      className="text-md font-semibold flex hover:cursor-pointer"
                    >{`${index + 1}. ${video.videoTitle}`}</div>
                    {openedVideos[index] && (
                      <div className="my-2">
                        <video
                          controls
                          width="600"
                          height="400"
                          src={`${baseUrl}/view/${video.videoUrl}`}
                          type="video/mp4"
                        ></video>
                      </div>
                    )}
                  </li>
                ))
              ) : (
                <div className="text-xl">Khóa học này chưa có video</div>
              )}
            </ul>
            <div className="flex justify-center space-x-2 mt-4">
              <Button variant="contained" type="submit">
                Cập nhật
              </Button>
            </div>
          </form>
          <div className="col-span-7 lg:col-span-5 lg:mt-56 mt-2">
            <form
              className="mt-5 bg-gray-100 rounded-lg border-2 p-7 shadow-xl ml-5"
              onSubmit={(e) => onHandleAddVideo(e)}
            >
              <div className="text-lg font-semibold">
                Thêm video cho khóa học
              </div>
              <div className="flex gap-x-3 mt-2 p-4">
                <div className="items-center">Tên</div>
                <input
                  type="text"
                  name="titles"
                  className="block p-3 w-full text-sm text-gray-900 rounded-lg border mb-4 bg-gray-200 shadow-lg border-black"
                  onChange={(e) => setCourseTitleVideo(e.target.value)}
                ></input>
              </div>
              <div className="flex gap-x-3 mt-2 p-1">
                <div className="items-center">Video</div>
                <input
                  type="file"
                  name="files"
                  accept=".mp4"
                  className="block p-3 w-full text-sm text-gray-900 rounded-lg border mb-4 bg-gray-200 shadow-lg border-black"
                  onChange={(e) => setCourseVideo(e.target.files)}
                ></input>
              </div>
              <input
                type="file"
                name="avatar"
                className="p-3 w-full text-sm text-gray-900 rounded-lg border mb-4 bg-gray-200 shadow-lg hidden"
                onChange={(e) => setAvatar(e.target.files)}
              ></input>
              <div className="flex justify-center">
                <Button
                  variant="contained"
                  disabled={isLoading ? true : false}
                  type="submit"
                >
                  Thêm video&nbsp;
                  {isLoading && (
                    <svg
                      className="animate-spin h-5 w-5 mr-5 text-white ..."
                      viewBox="0 0 17 17"
                    >
                      <CgSpinnerAlt />
                    </svg>
                  )}
                </Button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}

export default CourseDetailMentor;
