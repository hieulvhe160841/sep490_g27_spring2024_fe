import React from "react";
import LoadingState from "../LoadingState";
import userStore from "../../store/userStore";
import { Button } from "@mui/material";

const ApprovedCourseAdmin = ({ course }) => {
  const { baseUrl } = userStore();
  if (!course) {
    return <LoadingState />;
  }

  return (
    <div className="grid grid-cols-12 gap-x-28 mt-12">
      <div className="mb-4 col-span-4 sm:col-span-8">
        <div className="flex space-x-4">
          <img
            src={`${baseUrl}/view/${course.avatar}`}
            alt="Course"
            className="w-[300px] h-[200px] mr-2"
          />
          <div className="text-white items-end">
            <h2 className="text-3xl font-bold text-white">
              {course.courseName}
            </h2>
            <p className="text-xl">Gia sư: {course.mentorName}</p>
          </div>
        </div>

        <h3 className="text-textHeader text-2xl my-12 ">
          Mô tả:&nbsp;
          <div className="text-gray-900">{course.description}</div>
        </h3>
        <h3 className="text-textHeader text-2xl my-12 ">
          Video của khóa học
          <ul className="mt-2 text-lg">
            {course.videoDTOs && course.videoDTOs.length > 0 ? (
              course.videoDTOs.map((video, index) => (
                <li
                  key={index}
                  className="flex justify-between items-center border-b py-2"
                >
                  <div
                    onClick={() => toggleVideo(index)}
                    className="text-md font-semibold flex"
                  >{`${index + 1}. ${video.videoTitle}`}</div>
                </li>
              ))
            ) : (
              <div className="text-xl text-gray-900">
                Khóa học này chưa có video
              </div>
            )}
          </ul>
        </h3>
      </div>
      <div className="col-span-5 sm:col-span-4">
        <p className="text-lg text-white mt-6">
          Giá:{" "}
          {course.price
            ? course.price.toLocaleString("en-US", {
                maximumFractionDigits: 0,
              })
            : 0}
          &nbsp;VNĐ
        </p>
      </div>
    </div>
  );
};

export default ApprovedCourseAdmin;
