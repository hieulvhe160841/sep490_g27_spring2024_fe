import React, { useState } from "react";
import userStore from "../../store/userStore";
import { Button } from "@mui/material";
import mentorStore from "../../store/mentorStore";
import { toast } from "react-toastify";
import { CgSpinnerAlt } from "react-icons/cg";

function BoughtCourseDetailComponent({ course }) {
  const { baseUrl } = userStore();
  const [openedVideos, setOpenedVideos] = useState({});

  const toggleVideo = (index) => {
    // Toggle the state for the current video index
    setOpenedVideos((prevOpenedVideos) => ({
      ...prevOpenedVideos,
      [index]: !prevOpenedVideos[index],
    }));
  };

  return (
    <>
      <div className="container mx-auto my-8 p-6">
        <div className="grid grid-cols-1 lg:grid-cols-12">
          <div className="mb-4 col-span-4 lg:col-span-6">
            {console.log(course)}
            <div className="flex space-x-4">
              <img
                src={`${baseUrl}/view/${course.avatar}`}
                alt="Course"
                className="w-[300px] h-[200px] mr-2 rounded-xl border"
              />
              <div className="flex flex-col text-sidebarTop items-start gap-y-5">
                <h2 className="text-3xl font-semibold">{course.courseName}</h2>
                <p className="text-xl">Gia sư: {course.mentorName}</p>
              </div>
            </div>

            <h3 className="text-textHeader text-2xl my-12 ">
              Mô tả:&nbsp;
              <div className="text-gray-900">{course.description}</div>
            </h3>
          </div>
          <div className="col-span-6 lg:col-span-4 ml-28">
            <h2 className="text-lg font-semibold text-sidebarTop mt-10">
              Video của khóa học
            </h2>
            <ul className="mt-2">
              {course.videoDTOs && course.videoDTOs.length > 0 ? (
                course.videoDTOs.map((video, index) => (
                  <li key={index} className="items-center border-b py-2">
                    <div
                      onClick={() => toggleVideo(index)}
                      className="text-md font-semibold flex hover:cursor-pointer"
                    >{`${index + 1}. ${video.videoTitle}`}</div>
                    {openedVideos[index] && (
                      <div className="my-8">
                        <video
                          controls
                          width="600"
                          height="400"
                          src={`${baseUrl}/view/${video.videoUrl}`}
                          type="video/mp4"
                        ></video>
                      </div>
                    )}
                  </li>
                ))
              ) : (
                <div className="text-xl">Khóa học này chưa có video</div>
              )}
            </ul>
          </div>
        </div>
      </div>
    </>
  );
}

export default BoughtCourseDetailComponent;
