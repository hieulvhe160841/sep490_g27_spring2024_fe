import React from "react";
import courseImg from "../../assets/Rectangle.png";
import LoadingState from "../LoadingState";
import userStore from "../../store/userStore";
import { Button } from "@mui/material";
import bgImg from "../../assets/bg-img.png";
import menteeStore from "../../store/menteeStore";
import { toast } from "react-toastify";

const CourseMentee = ({ course }) => {
  const { baseUrl } = userStore();
  const { purchaseCourseMentee, isLoading } = menteeStore();
  if (!course) {
    return <LoadingState />;
  }
  const purchase = async (courseId) => {
    const res = await purchaseCourseMentee(courseId);
    if (res) {
      toast(res.messages);
    }
  };
  return (
    <div className="grid grid-cols-1 lg:grid-cols-12 gap-x-4 mt-12 absolute top-5 left-20 ml-4">
      <div className="mb-4 col-span-4 lg:col-span-9 ">
        <div className="flex space-x-4">
          <img
            src={`${baseUrl}/view/${course.avatar}`}
            alt="Course"
            className="lg:w-[300px] w-[150px] lg:h-[200px] h-[100px] mr-2"
          />
          <div className="flex flex-col text-white items-start gap-y-5">
            <h2 className="text-3xl font-semibold text-white">
              {course.courseName}
            </h2>
            <p className="text-xl">Đánh giá: {course.rating}</p>
            <p className="text-xl">Gia sư: {course.mentorName}</p>
          </div>
        </div>

        <h3 className="text-textHeader font-semibold text-2xl lg:mt-20 mt-40">
          Mô tả:&nbsp;
          <div className="text-gray-900 font-normal mt-3">
            {course.description}
          </div>
        </h3>
        <h3 className="text-textHeader font-semibold text-2xl my-12 ">
          Video của khóa học
          <ul className="mt-2 text-lg">
            {course.videoDTOs && course.videoDTOs.length > 0 ? (
              course.videoDTOs.map((video, index) => (
                <li
                  key={index}
                  className="flex justify-between items-center border-b py-2"
                >
                  <div
                    onClick={() => toggleVideo(index)}
                    className="text-md font-semibold flex"
                  >{`${index + 1}. ${video.videoTitle}`}</div>
                </li>
              ))
            ) : (
              <div className="text-2xl text-gray-900 font-normal">
                Khóa học này chưa có video
              </div>
            )}
          </ul>
        </h3>
      </div>
      <div className="col-span-5 lg:col-span-3 mr-10">
        <Button
          variant="contained"
          sx={{
            padding: "8px 12px",
            borderRadius: "12px",
            backgroundColor: "#0e579e",
            "&:hover": {
              backgroundColor: "#0d4275",
            },
            margin: "20px 0px 0px 0px",
            color: "ffffff",
            height: "auto",
            width: "100%",
            maxWidth: "250px",
            fontSize: "25px",
            fontWeight: "bold",
          }}
          type="button"
          onClick={() => purchase(course.courseId)}
        >
          Mua
        </Button>
        <p className="text-2xl lg:text-white text-sidebarTop mt-6">
          Giá:{" "}
          {course.price
            ? course.price.toLocaleString("en-US", {
                maximumFractionDigits: 0,
              })
            : 0}
          &nbsp;VNĐ
        </p>
      </div>
    </div>
  );
};

export default CourseMentee;
