import { Button } from "@mui/material";
import React from "react";
import { useLocation } from "react-router-dom";
import { Link, Navigate, useNavigate } from "react-router-dom";
import { useState } from "react";
import InputComponent from "../InputComponent";
import userStore from "../../store/userStore";
import { CgSpinnerAlt } from "react-icons/cg";
import { toast } from "react-toastify";
import { IoIosArrowBack } from "react-icons/io";

function FormVerify() {
  const navigate = useNavigate();
  const [code, setCode] = useState("");
  const {
    verifyForRegister,
    regenerateCode,
    verifyForReset,
    isLoading,
    regenerateCodeReset,
  } = userStore();

  const location = useLocation();

  const onHandleResetPasswordCode = async () => {
    const res = await verifyForReset(location.state.email, code);
    if (res.status === 200) {
      toast.success(res.data.messages);
      navigate("/resetPassword", {
        state: { email: location.state.email, code },
      });
    } else {
      if (res && res.status !== 200) {
        toast.error(res.data.messages);
      }
    }
  };
  const regenerateOTPCodeForReset = async () => {
    const formData = new FormData();
    formData.append("email", location.state.email);

    const res = await regenerateCodeReset(formData);
    console.log(res);
    toast.info(res.data.messages);
  };
  // -------------------------------
  // --------Register------------
  const onHandleRegisterCode = async () => {
    const res = await verifyForRegister(location.state.email, code);
    if (res.status === 200) {
      toast.success("Đăng ký thành công!");
      navigate("/login");
    } else {
      toast.error(res.messages);
    }
  };
  const regenerateOTPCode = async () => {
    const res = await regenerateCode(location.state.email);
    toast.info(res.data.messages);
  };
  const onHandleEnterRegister = (event) => {
    if (event.keyCode === 13) {
      onHandleRegisterCode();
    }
  };
  const onHandleEnterReset = (event) => {
    if (event.keyCode === 13) {
      onHandleResetPasswordCode();
    }
  };

  console.log(location);
  return (
    <>
      <div className="text-center mb-10">
        Mã xác nhận đã được gửi đến email của bạn.
        <br /> Hãy kiểm tra email và nhập mã xác nhận.
      </div>
      <div className="space-y-6" method="POST">
        <div>
          <div className="inputForm"></div>
        </div>
        <InputComponent
          id="code"
          name="code"
          type="text"
          placeholder="Nhập mã..."
          onChange={(event) => {
            setCode(event.target.value);
          }}
          onKeyDown={(e) => {
            location.state.auth
              ? onHandleEnterRegister(e)
              : onHandleEnterReset(e);
          }}
        />

        {location.state.auth ? (
          <div>
            <Button
              type="submit"
              variant="contained"
              size="large"
              className="mx-auto w-full"
              disabled={isLoading ? true : false}
              onClick={() => {
                onHandleRegisterCode();
              }}
            >
              Nhập mã đăng ký&nbsp;
              {isLoading && (
                <svg
                  className="animate-spin h-5 w-5 mr-5 text-white ..."
                  viewBox="0 0 17 17"
                >
                  <CgSpinnerAlt />
                </svg>
              )}
            </Button>
            <div className="text-sm text-center mt-3">
              Chúng tôi đã gửi cho bạn một mã xác nhận qua email để đăng ký
              <p>
                Bạn đã nhận được mã chưa?{" "}
                <button
                  className="text-blue-600"
                  onClick={() => {
                    regenerateOTPCode();
                  }}
                >
                  Gửi lại
                </button>
              </p>
            </div>
          </div>
        ) : (
          <div>
            <Button
              type="submit"
              variant="contained"
              size="large"
              className="mx-auto w-full"
              disabled={isLoading ? true : false}
              sx={{
                backgroundColor: "#0B4B88",
                color: "white",
              }}
              onClick={() => {
                onHandleResetPasswordCode();
              }}
            >
              Nhập mã&nbsp;
              {isLoading && (
                <svg
                  className="animate-spin h-5 w-5 mr-5 text-white ..."
                  viewBox="0 0 17 17"
                >
                  <CgSpinnerAlt />
                </svg>
              )}
            </Button>
            <div className="text-sm text-center mt-3">
              Chúng tôi đã gửi cho bạn một mã xác nhận qua email để cài lại mật
              khẩu.
              <p>
                Bạn đã nhận được mã chưa?{" "}
                <button
                  className="text-blue-600"
                  disabled={isLoading ? true : false}
                  onClick={() => {
                    regenerateOTPCodeForReset();
                  }}
                >
                  Gửi lại
                </button>
              </p>
            </div>
          </div>
        )}
      </div>
      <div className="flex flex-col justify-center items-center mt-5">
        <div>
          <Link
            to="/login"
            className={`underline ${
              isLoading ? "pointer-events-none text-gray-400" : ""
            }`}
          >
            <IoIosArrowBack className="inline" />
            Quay lại trang Đăng nhập
          </Link>
        </div>
      </div>
    </>
  );
}

export default FormVerify;
