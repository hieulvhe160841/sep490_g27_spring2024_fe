import React, { useState } from "react";
import { Button, InputLabel } from "@mui/material";
import mentorStore from "./../../store/mentorStore";
import { CgSpinnerAlt } from "react-icons/cg";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";

function FormRegisterCourse() {
  const navigate = useNavigate();
  const { addCourse, isLoading } = mentorStore();
  const [courseName, setCourseName] = useState();
  const [image, setImage] = useState();
  const [video, setVideo] = useState();
  const [price, setPrice] = useState();
  const [description, setDescription] = useState();
  const onHandleRegisterCourse = async (e) => {
    e.preventDefault();
    const formData = new FormData(e.currentTarget);
    formData.append(
      "courseRequest",
      JSON.stringify({
        courseName,
        description,
        price,
      })
    );

    const res = await addCourse(formData);
    if (res.statusCode === 201) {
      toast.success("Gửi yêu cầu tạo khóa học thành công");
    } else {
      toast.success("Gửi yêu cầu tạo khóa học thất bại");
    }
    navigate(-1);
  };
  const formatNumberWithCommas = (number) => {
    if (number !== undefined && number !== null) {
      const integerPart = Math.trunc(number);
      return integerPart.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    } else {
      return "";
    }
  };

  const handleInputChange = (e) => {
    const { value } = e.target;
    const numericValue = value.replace(/,/g, "");
    if (numericValue === "" || parseInt(numericValue) >= 0) {
      setPrice(numericValue);
    }
  };

  return (
    <>
      <div className="sm:mx-auto sm:w-full sm:max-w-xl">
        <form
          className="space-y-6"
          action="registerCourse"
          method="POST"
          onSubmit={(e) => onHandleRegisterCourse(e)}
        >
          <div className="title">
            <div className="inputForm">
              <div className="my-3 font-bold text-lg text-sidebarTop">
                Tên khóa học
              </div>
              <input
                type="text"
                className="block p-3 w-full text-sm text-gray-900 rounded-lg border bg-gray-200 shadow-lg"
                onChange={(event) => setCourseName(event.target.value)}
                maxLength={255}
                required
              ></input>
            </div>
          </div>
          <div className="image">
            <div className="inputForm">
              <div className="my-3 font-bold text-lg text-sidebarTop">
                Ảnh về khóa học
              </div>
              <>
                <input
                  type="file"
                  name="imageFile"
                  className="block p-3 w-full text-sm text-gray-900 rounded-lg border bg-gray-200 shadow-lg"
                  onChange={(e) => setImage(e.target.files)}
                  required
                ></input>
              </>
            </div>
          </div>

          <div className="video">
            <div className="inputForm">
              <div className="my-3 font-bold text-lg text-sidebarTop">
                Video giới thiệu về khóa học (Tối đa 100mb)
              </div>
              <>
                <input
                  type="file"
                  name="videoFile"
                  accept=".mp4"
                  className="block p-3 w-full text-sm text-gray-900 rounded-lg border bg-gray-200 shadow-lg"
                  onChange={(e) => setVideo(e.target.files)}
                  required
                ></input>
              </>
            </div>
          </div>

          <div className="description">
            <div className="inputForm">
              <div className="my-3 font-bold text-lg text-sidebarTop">
                Mô tả khóa học
              </div>
              <textarea
                rows="8"
                className="block p-3 w-full text-sm text-gray-900 rounded-lg border bg-gray-200 shadow-lg"
                placeholder="Thêm mô tả về khóa học..."
                onChange={(event) => setDescription(event.target.value)}
                required
                maxLength={255}
              ></textarea>
            </div>
          </div>
          <div className="price">
            <div className="inputForm">
              <div className="my-3 font-bold text-lg text-sidebarTop">
                Giá (VNĐ)
              </div>
              <input
                className="block p-3 w-full text-sm text-gray-900 rounded-lg border bg-gray-200 shadow-lg"
                placeholder="Thêm giá tiền..."
                value={formatNumberWithCommas(price)}
                type="text"
                onChange={(e) => handleInputChange(e)}
                required
              ></input>
            </div>
          </div>
          <div className="flex gap-x-5 justify-center">
            <Button
              variant="contained"
              type="submit"
              disabled={isLoading ? true : false}
            >
              Lưu&nbsp;
              {isLoading && (
                <svg
                  className="animate-spin h-5 w-5 mr-5 text-white ..."
                  viewBox="0 0 17 17"
                >
                  <CgSpinnerAlt />
                </svg>
              )}
            </Button>
          </div>
        </form>
      </div>
    </>
  );
}

export default FormRegisterCourse;
