const Title = (props) => {
  return (
    <div className="md:mx-auto md:w-full mt-3">
      <h2 className="text-center text-2xl font-medium leading-9 tracking-tight uppercase text-gray-900">
        {props.title}
      </h2>
    </div>
  );
};
export default Title;
