import React from "react";
import ModalEditPostMentee from "../Modal/ModalEditPostMentee";

function MenteePostCard({ post }) {
  const price = parseInt(post.postPrice);
  const statusText = post.postStatus ? "Đang chờ" : "Đã nhận";

  return (
    <div className="my-4">
      <div className="flex flex-col lg:flex-row justify-between">
        <div className="flex items-center space-x-4 ml-1">
          <div>
            <p className="text-textHeader text-lg font-bold">
              Bài đăng số: {post.postId}
            </p>
            <p className="text-textHeader text-lg font-bold">
              Tiêu đề: <span className="text-gray-700">{post.postTitle}</span>
            </p>
            <p className="text-textHeader font-semibold">
              Trạng thái: <span className="text-gray-700">{statusText}</span>
            </p>
            <p className="text-textHeader font-semibold">
              Giá tiền:{" "}
              <span className="text-gray-700">
                {price
                  ? price.toLocaleString("en-US", {
                      maximumFractionDigits: 0,
                    })
                  : 0}
                &nbsp;VNĐ
              </span>
            </p>
          </div>
        </div>
        <div className="space-x-2 shrink-0 pt-3 lg:flex lg:items-center">
          <ModalEditPostMentee post={post} />
        </div>
      </div>
    </div>
  );
}

export default MenteePostCard;
