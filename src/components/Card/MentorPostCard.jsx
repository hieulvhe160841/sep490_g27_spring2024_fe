import React from "react";
import Button from "@mui/material/Button";
import { useNavigate } from "react-router-dom";
import ModalViewPost from "../Modal/ModalViewPost";

const MentorPostCard = ({ post }) => {
  const navigate = useNavigate();
  const onHandleMenteeDetail = (mentee) => {
    navigate(`/mentor/menteeDetails/${mentee}`);
  };
  return (
    <div className="m-4">
      <div className="flex flex-col lg:flex-row justify-between">
        <div className="flex items-center space-x-4 ml-1">
          <div>
            <p className="text-textHeader text-lg font-bold">
              ID Bài đăng: {post.postId}
            </p>
            <p className="text-textHeader text-lg font-bold">
              Tiêu đề: {post.postTitle}
            </p>
            <p className="text-textHeader font-semibold">
              Học viên: {post.postCreatedUser}
            </p>
            <p className="text-textHeader">
              Giá tiền:{" "}
              {post.price
                ? post.price.toLocaleString("en-US", {
                    maximumFractionDigits: 0,
                  })
                : 0}{" "}
              VNĐ
            </p>
          </div>
        </div>

        <div className="space-x-2 shrink-0 pt-3 lg:flex lg:items-center">
          <Button
            variant="contained"
            sx={{
              backgroundColor: "#e6e6e6",
              textTransform: "none",
              borderRadius: "12px",
              color: "black",
              height: "35px",
              width: "115px",
            }}
            type="button"
            onClick={() => onHandleMenteeDetail(post.postCreatedUser)}
          >
            Học viên
          </Button>
          <ModalViewPost postId={post.postId} />
        </div>
      </div>
    </div>
  );
};

export default MentorPostCard;
