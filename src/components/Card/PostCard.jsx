import React from "react";
import Button from "@mui/material/Button";
import ModalEditPost from "../Modal/ModalEditPost";
import { useNavigate } from "react-router-dom";

const PostCard = ({ post }) => {
  const navigate = useNavigate();
  const onHandleMenteeDetail = (mentee) => {
    navigate(`/admin/menteeDetails/${mentee}`);
  };
  return (
    <div className="my-4">
      <div className="flex flex-col lg:flex-row items-center justify-between">
        <div className="flex items-center space-x-4 ml-1">
          <div>
            <p className="text-textHeader text-lg font-bold">
              Bài Đăng số: {post.postId}
            </p>
            <p className="text-textHeader text-lg font-bold">
              Tiêu đề: {post.postTitle}
            </p>
            <p className="text-textHeader font-semibold">
              Học viên: {post.postCreatedUser}
            </p>
            <p className="text-textHeader">Giá tiền: {post.postPrice} VNĐ</p>
          </div>
        </div>

        <div className="space-x-2 shrink-0 pt-3 lg:flex lg:items-center">
          <Button
            variant="outlined"
            sx={{
              border: "2px solid #4B5563",
              padding: "8px 12px",
              borderRadius: "8px",
              boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.1)",
              margin: "12px 0px 0px 0px",
              "&:hover": {
                boxShadow: "none",
              },
              color: "#000000",
              textTransform: "none",
              height: "35px",
              width: "115px",
            }}
            type="button"
            onClick={() => onHandleMenteeDetail(post.postCreatedUser)}
          >
            Học viên
          </Button>
          <ModalEditPost postId={post.postId} />
        </div>
      </div>
    </div>
  );
};

export default PostCard;
