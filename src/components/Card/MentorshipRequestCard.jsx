import React from "react";
import Button from "@mui/material/Button";
import { Link } from "react-router-dom";

const MentorshipRequestCard = ({ request }) => {
  const getStatusText = (status) => {
    switch (status) {
      case "WAITING":
        return "Đang chờ";
      case "ACCEPTED":
        return "Đã nhận";
      case "DONE":
        return "Hoàn thành";
      case "REJECTED":
        return "Từ chối";
      case "IN_PROGRESS":
        return "Đang tiến hành";
      default:
        return status;
    }
  };

  const formatDate = (dateString) => {
    const options = { year: "numeric", month: "2-digit", day: "2-digit" };
    return new Date(dateString).toLocaleDateString(undefined, options);
  };

  return (
    <div className="my-4">
      <div className="flex flex-col lg:flex-row justify-between">
        <div className="flex items-center space-x-4 ml-1">
          <div>
            <p className="text-textHeader text-lg font-bold">
              Gia sư: {request.mentorName}
            </p>
            <p className="text-textHeader text-lg font-bold">
              Học viên: {request.menteeName}
            </p>
            <p className="text-sidebarTop font-semibold">
              Trạng thái: {getStatusText(request.supportStatus)}
            </p>
            <p className="text-sidebarTop font-semibold">
              Ngày tạo: {formatDate(request.supportCreatedDate)}
            </p>
          </div>
        </div>
        <div className="space-x-2 shrink-0 pt-3 lg:flex lg:items-center">
          <Link to={`/admin/requestDetail/${request.supportId}`}>
            <Button
              variant="contained"
              sx={{
                backgroundColor: "#5584C6",
                textTransform: "none",
                borderRadius: "12px",
                height: "35px",
                width: "115px",
              }}
            >
              Chi tiết
            </Button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default MentorshipRequestCard;
