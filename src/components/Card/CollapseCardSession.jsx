import React, { useState } from "react";
import { Button } from "@mui/material";

const CollapseCardSession = ({ username }) => {
  const [showMeet, setShowMeet] = useState(false);
  const [showChat, setShowChat] = useState(true);

  const toggleShowMeet = () => setShowMeet(!showMeet);
  const toggleShowChat = () => setShowChat(!showChat);

  return (
    <>
      <div className="mb-4">
        <button
          type="button"
          className="inline-block rounded bg-textHeader px-6 pb-2 pt-2.5 text-base font-medium uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] dark:shadow-[0_4px_9px_-4px_rgba(59,113,202,0.5)] dark:hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)] dark:focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)] dark:active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)]"
          onClick={toggleShowChat}
        >
          Hướng dẫn Chat
        </button>
        {showChat && (
          <div className="block rounded-lg bg-white p-3 shadow-lg dark:bg-white dark:text-black">
            <p>
              1. Chọn nút "+" bên cạnh My Chats, nhập tiêu đề đoạn chat rồi nhấn
              Enter
              <br />
              2. Copy tên tài khoản của gia sư hoặc học viên mà bạn muốn chat
              cùng ở bên dưới. <br />
              3. Chọn People ở bên phải khung chat rồi dán hoặc điền tên tài
              khoản
              <br />
              4. Hãy tận hưởng cuộc trò chuyện!
            </p>
            {username && (
              <div className="text-lg font-bold">Tài khoản: {username}</div>
            )}
          </div>
        )}
      </div>

      <div className="mb-3">
        {/* Add margin bottom to separate the buttons */}
        <button
          type="button"
          className="inline-block rounded bg-textHeader px-6 pb-2 pt-2.5 text-base font-medium uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] dark:shadow-[0_4px_9px_-4px_rgba(59,113,202,0.5)] dark:hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)] dark:focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)] dark:active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)]"
          onClick={toggleShowMeet}
        >
          Google Meet
        </button>
        {showMeet && (
          <div className="block rounded-lg bg-white p-3 shadow-lg dark:bg-white dark:text-black">
            <p className="mb-1">
              1. Chọn nút "START GOOGLE MEET" bên dưới để truy cập vào Google
              Meet.
              <br />
              2. Hãy copy thông tin truy cập và gửi cho gia sư/học viên của bạn
              thông qua chat.
              <br />
              3. Đợi cho người còn lại truy cập vào Google Meet. <br />
              4. Hay nhớ bấm nút "BẮT ĐẦU" để tính thời gian nhé!
            </p>
            <Button
              variant="contained"
              color="primary"
              href="https://meet.google.com/new"
              target="_blank"
              rel="noopener noreferrer"
              sx={{ marginBottom: "5px" }}
            >
              Start Google Meet
            </Button>
          </div>
        )}
      </div>
    </>
  );
};

export default CollapseCardSession;
