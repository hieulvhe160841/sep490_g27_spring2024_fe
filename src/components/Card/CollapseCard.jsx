import React, { useState } from "react";

export default function CollapseCard({ username }) {
  const [showChat, setShowChat] = useState(true);
  const [showContact, setShowContact] = useState(false);

  const toggleShowChat = () => setShowChat(!showChat);
  const toggleShowContact = () => setShowContact(!showContact);

  return (
    <>
      <div className="mb-4">
        <button
          type="button"
          className="inline-block rounded bg-textHeader px-6 pb-2 pt-2.5 text-base font-medium uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] dark:shadow-[0_4px_9px_-4px_rgba(59,113,202,0.5)] dark:hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)] dark:focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)] dark:active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)]"
          onClick={toggleShowChat}
        >
          Hướng dẫn Chat
        </button>
        {showChat && (
          <div className="block rounded-lg bg-white p-3 shadow-lg dark:bg-white dark:text-black">
            <p>
              1. Chọn nút "+" bên cạnh My Chats, nhập tiêu đề đoạn chat rồi nhấn
              Enter
              <br />
              2. Copy tên tài khoản của gia sư hoặc học viên mà bạn muốn chat
              cùng ở bên dưới. <br />
              3. Chọn People ở bên phải khung chat rồi dán hoặc điền tên tài
              khoản
              <br />
              4. Hãy tận hưởng cuộc trò chuyện!
            </p>
            {username && (
              <div className="text-lg font-bold">Tài khoản: {username}</div>
            )}
          </div>
        )}
      </div>

      <div className="mb-4">
        <button
          type="button"
          className="inline-block rounded bg-textHeader px-6 pb-2 pt-2.5 text-base font-medium uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] dark:shadow-[0_4px_9px_-4px_rgba(59,113,202,0.5)] dark:hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)] dark:focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)] dark:active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)]"
          onClick={toggleShowContact}
        >
          Liên hệ với chúng tôi
        </button>
        {showContact && (
          <div className="block rounded-lg bg-white p-3 shadow-lg dark:bg-white dark:text-black">
            <p>
              1. Nếu bạn có bất kỳ vấn đề gì liên quan đến các giao dịch, hãy
              chat với nhân viên của chúng tôi thông qua Tài khoản:
              "transactionStaff".
              <br />
              2. Nếu bạn có bất kỳ vấn đề gì trong khi sử dụng web, hãy chat với
              nhân viên của chúng tôi thông qua Tài khoản: "educationStaff".
              <br />
              3. Hãy liên lạc với chúng tôi!
              <br />
            </p>
          </div>
        )}
      </div>
    </>
  );
}
