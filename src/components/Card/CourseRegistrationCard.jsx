import React from "react";
import Button from "@mui/material/Button";
import { Link } from "react-router-dom";

const CourseRegistrationCard = ({ courseRegis }) => {
  const statusToVietnamese = (status) => {
    switch (status) {
      case "ACCEPTED":
        return "Đã duyệt";
      case "REJECTED":
        return "Đã từ chối";
      case "Pending":
        return "Chờ phê duyệt";
      default:
        return status;
    }
  };

  return (
    <div className="my-4">
      <div className="flex flex-col lg:flex-row items-center justify-between">
        <div className="flex items-center space-x-4 ml-1">
          <div>
            <p className="text-textHeader text-lg font-bold">
              Tên khóa học: {courseRegis.courseName}
            </p>
            <p className="text-textHeader font-semibold">
              Gia sư: {courseRegis.mentorUserName}
            </p>
            <p className="text-textHeader max-w-[600px] truncate w-96">
              Mô tả: {courseRegis.description}
            </p>
            <p className="text-textHeader font-semibold">
              Trạng thái: {statusToVietnamese(courseRegis.courseRequestStatus)}
            </p>
          </div>
        </div>
        <div className="space-x-2 shrink-0 pt-3 lg:flex lg:items-center">
          <Link to={`courseInfo/${courseRegis.courseRequestId}`}>
            <Button
              variant="contained"
              sx={{
                backgroundColor: "#5584C6",
                textTransform: "none",
                borderRadius: "12px",
                height: "35px",
                width: "115px",
              }}
            >
              Chi tiết
            </Button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default CourseRegistrationCard;
