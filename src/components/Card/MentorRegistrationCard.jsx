import React from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import { Link } from "react-router-dom";
import userStore from "../../store/userStore";

const MentorRegistrationCard = ({ mentorRegis }) => {
  const { baseUrl } = userStore();

  const statusToVietnamese = (status) => {
    switch (status) {
      case "ACCEPTED":
        return "Đã duyệt";
      case "REJECTED":
        return "Đã từ chối";
      case "WAITING":
        return "Chờ phê duyệt";
      default:
        return status;
    }
  };

  return (
    <div className="my-4">
      <div className="flex flex-col lg:flex-row justify-between">
        <div className="flex items-center space-x-4 ml-1">
          <img
            src={`${baseUrl}/view/${mentorRegis.userCreatedMentorRequestDTO.avatar}`}
            alt={mentorRegis.userCreatedMentorRequestDTO.userName}
            className="md:h-24 md:w-24 w-12 h-12 flex-none rounded-full object-cover object-center mr-5"
          />
          <div>
            <p className="text-textHeader text-lg font-bold">
              Tài khoản: {mentorRegis.userCreatedMentorRequestDTO.userName}
            </p>
            <p className="text-textHeader font-semibold truncate md:w-[40rem] w-40">
              Kinh nghiệm: {mentorRegis.userCreatedMentorRequestDTO.experience}
            </p>
            <p className="text-textHeader max-w-[600px] truncate">
              Giá thuê:{" "}
              {mentorRegis.userCreatedMentorRequestDTO.costPerHour
                ? mentorRegis.userCreatedMentorRequestDTO.costPerHour.toLocaleString(
                    "en-US",
                    {
                      maximumFractionDigits: 0,
                    }
                  )
                : 0}
              &nbsp;VNĐ/h
            </p>
            <p className="text-textHeader text-lg font-bold">
              Trạng thái: {statusToVietnamese(mentorRegis.mentorRequestStatus)}
            </p>
          </div>
        </div>
        <div className="space-x-2 shrink-0 pt-3 lg:flex lg:items-center">
          <Link
            to={`mentorRegistration/${mentorRegis.userCreatedMentorRequestDTO.userId}/${mentorRegis.mentorRequestId}`}
          >
            <Button
              variant="contained"
              sx={{
                textTransform: "none",
                borderRadius: "12px",
                height: "35px",
                width: "115px",
              }}
            >
              Chi tiết
            </Button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default MentorRegistrationCard;
