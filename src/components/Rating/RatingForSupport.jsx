import { Button } from "@mui/material";
import React, { useState } from "react";
import { FaStar } from "react-icons/fa";
import menteeStore from "../../store/menteeStore";
import { toast } from "react-toastify";

function RatingForSupport({ request }) {
  const [showModal, setShowModal] = useState(false);
  const [rating, setRating] = useState(0);
  const [feedback, setFeedBack] = useState();
  const { menteeRatingSupportOfMentor } = menteeStore();

  const handleRating = (rate) => {
    setRating(rate);
  };
  const onHandleRatingForSupport = async () => {
    const res = await menteeRatingSupportOfMentor(
      request.supportId,
      rating,
      feedback
    );
    if (res.statusCode === 201) {
      toast.success("Đánh giá của bạn đã được gửi");
    } else {
      toast.error(res.messages);
    }
    setShowModal(false);
    console.log(res);
  };

  return (
    <>
      <Button
        variant="contained"
        size="large"
        color="info"
        sx={"border-radius: 15px"}
        type="button"
        onClick={() => setShowModal(true)}
      >
        Đánh giá
      </Button>
      {showModal ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-[350px] my-6 mx-auto max-w-4xl">
              <div className="border-0 rounded-lg shadow-lg pt-5 relative w-full bg-white outline-none focus:outline-none">
                <div className="mb-5">
                  <div className="text-xl flex justify-center font-semibold text-sidebarTop p-5">
                    Đánh giá gia sư
                  </div>
                  <div className="flex justify-center">
                    {[1, 2, 3, 4, 5].map((rate) => (
                      <button
                        key={rate}
                        onClick={() => handleRating(rate)}
                        style={{
                          color: rate <= rating ? "#FACA15" : "#BFBFBF",
                        }}
                      >
                        <FaStar size={"55px"} />
                      </button>
                    ))}
                  </div>
                  <div className="mt-7 justify-center flex w-full">
                    <textarea
                      rows="3"
                      className="border-2 border-black w-full mx-5 rounded-lg shadow-md p-3"
                      placeholder="Hãy cho chúng tôi biết về cảm nhận của bạn..."
                      onChange={(e) => setFeedBack(e.target.value)}
                      maxLength={255}
                    ></textarea>
                  </div>
                </div>

                <div className="btnInModal">
                  <Button
                    type="submit"
                    variant="outlined"
                    sx={{
                      border: "2px solid #2B90D9",
                      textTransform: "none",
                      borderRadius: "12px",
                      height: "35px",
                      width: "115px",
                      color: "#000000",
                    }}
                    onClick={() => onHandleRatingForSupport()}
                  >
                    Đánh giá
                  </Button>
                  <Button
                    variant="contained"
                    color="error"
                    sx={{
                      backgroundColor: "#FF0A0A",
                      textTransform: "none",
                      borderRadius: "12px",
                      height: "35px",
                      width: "115px",
                      color: "#ffffff",
                    }}
                    onClick={() => setShowModal(false)}
                  >
                    Đóng
                  </Button>
                </div>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </>
  );
}

export default RatingForSupport;
