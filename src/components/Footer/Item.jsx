import React from "react";
import logo from "../../assets/Logo.png";
import OMS from "../../assets/OMS.png";

const Item = ({ Links, title }) => {
  return (
    <ul>
      {title === "OMS" ? (
        <div className="flex rounded-md pb-1 gap-x-6">
          <img src={logo} alt="logo" className="w-[60px] h-[60px]" />
          <img src={OMS} alt="oms" className="w-[70px] h-[70px]" />
        </div>
      ) : (
        <h1 className="mb-1 font-bold">{title}</h1>
      )}
      {Links.map((link) => (
        <li key={link.name}>
          <p className="text-white duration-300 text-sm leading-6">
            {link.name}
          </p>
        </li>
      ))}
    </ul>
  );
};

export default Item;
