import Item from "./Item";
import { PRODUCTS, ABOUTUS, SERVICES, CONTACT } from "./Menus";
const ItemsContainer = () => {
  return (
    <div className="grid grid-cols-1 sm:grid-cols-3 lg:grid-cols-4 lg:ml-16 gap-16 sm:px-8 px-5 py-16">
      <Item Links={PRODUCTS} title="OMS" />
      <Item Links={ABOUTUS} title="Về chúng tôi" />
      <Item Links={SERVICES} title="Dịch vụ" />
      <Item Links={CONTACT} title="Thông tin liên lạc" />
    </div>
  );
};

export default ItemsContainer;
