export const PRODUCTS = [
  { name: "Bắt đầu hành trình học tập cá nhân hóa của bạn ngay hôm nay và đạt được mục tiêu một cách hiệu quả. Hãy cùng nhau bắt đầu một hành trình phát triển kiến thức và kỹ năng!", link: "#" },
];
export const ABOUTUS = [
  { name: "Industries and tools", link: "#" },
  { name: "Use cases", link: "#" },
  { name: "Blog", link: "#" },
];
export const SERVICES = [
  { name: "Diversity & inclusion", link: "#" },
  { name: "About us", link: "#" },
  { name: "Press", link: "#" },
];
export const CONTACT = [
  { name: "Documentation", link: "#" },
  { name: "Tutorials & guides", link: "#" },
  { name: "Webinars", link: "#" },
];

export const Icons = [
  { name: "logo-facebook", link: "#" },
  { name: "logo-twitter", link: "#" },
  { name: "logo-gitlab", link: "#" },
  { name: "logo-linkedin", link: "#" },
  { name: "logo-instagram", link: "#" },
];