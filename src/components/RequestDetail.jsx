import React from "react";
import userStore from "../store/userStore";
import LoadingState from "./LoadingState";

const RequestDetail = ({ mentorshipRequest }) => {
  const { baseUrl } = userStore();

  if (!mentorshipRequest) {
    return <LoadingState />;
  }

  return (
    <div className="container mx-3 py-4">
      <div className="grid grid-cols-1 md:grid-cols-12 gap-12 px-4">
        <div className="col-span-4 md:col-span-4">
          <div className="bg-gray-200 shadow rounded-lg p-6">
            <div className="flex flex-col items-center">
              <img
                src={`${baseUrl}/view/${mentorshipRequest.mentorAvatar}`}
                className="w-32 h-32 bg-gray-300 rounded-full mb-4 shrink-0"
              ></img>
              <h1 className="text-xl font-bold text-textHeader">
                Tài khoản gia sư: {mentorshipRequest.mentorUserName}
              </h1>
              <p className="text-textHeader">
                {mentorshipRequest.costPerHour
                  ? mentorshipRequest.costPerHour.toLocaleString("en-US", {
                      maximumFractionDigits: 0,
                    })
                  : 0}
                &nbsp;VNĐ/h
              </p>
            </div>

            <h2 className="text-xl font-bold mt-6 mb-4 text-textHeader">
              Đánh giá:
              <span className="text-lg font-semibold text-gray-800">
                {" "}
                {mentorshipRequest.rating}
              </span>
            </h2>

            <h2 className="text-xl font-bold mt-6 mb-4 text-textHeader">
              Trình độ học vấn:
              <span className="text-lg font-semibold text-gray-800">
                {" "}
                {mentorshipRequest.educationLevel}
              </span>
            </h2>

            <h2 className="text-xl font-bold mt-6 mb-4 text-textHeader">
              Điện thoại:
              <span className="text-lg font-semibold text-gray-800">
                {" "}
                {mentorshipRequest.mentorPhone}
              </span>
            </h2>
          </div>
        </div>
        <div className="col-span-4 md:col-span-4">
          <div className="bg-gray-200 shadow rounded-lg p-6">
            <div className="flex flex-col items-center">
              <h1 className="text-xl font-bold text-textHeader">Details</h1>
              <p className="text-textHeader">
                Trạng thái: {mentorshipRequest.supportStatus}
              </p>
            </div>
            <h2 className="text-xl font-bold mt-4 text-textHeader">About:</h2>
            <p className="text-gray-700">
              - Ngày tạo: {mentorshipRequest.supportCreatedDate}
            </p>
            <p className="text-gray-700">
              - Thời gian bắt đầu: {mentorshipRequest.timeStart}
            </p>
            <p className="text-gray-700">
              - Thời gian kết thúc: {mentorshipRequest.timeEnd}
            </p>
            {mentorshipRequest.postContent ? (
              <p className="text-gray-700">
                - Nội dung bài đăng: {mentorshipRequest.postContent}
              </p>
            ) : (
              ""
            )}
            <p className="text-gray-700">
              - Giá tiền bài đăng:{" "}
              {mentorshipRequest.postPrice
                ? mentorshipRequest.postPrice.toLocaleString("en-US", {
                    maximumFractionDigits: 0,
                  })
                : 0}
              &nbsp;VNĐ
            </p>
          </div>
        </div>
        <div className="col-span-4 md:col-span-4">
          <div className="bg-gray-200 shadow rounded-lg p-6">
            <div className="flex flex-col items-center">
              <img
                src={`${baseUrl}/view/${mentorshipRequest.menteeAvatar}`}
                className="w-32 h-32 bg-gray-300 rounded-full mb-4 shrink-0"
              ></img>
              <h1 className="text-xl font-bold text-textHeader">
                Tài khoản học viên: {mentorshipRequest.menteeUserName}
              </h1>
            </div>

            <h2 className="text-xl font-bold mt-6 mb-4 text-textHeader">
              Mục tiêu:
              <span className="text-lg font-semibold text-gray-800">
                {" "}
                {mentorshipRequest.goal}
              </span>
            </h2>

            <h2 className="text-xl font-bold mt-6 mb-4 text-textHeader">
              Sở thích:
              <span className="text-lg font-semibold text-gray-800">
                {" "}
                {mentorshipRequest.interest}
              </span>
            </h2>

            <h2 className="text-xl font-bold mt-6 mb-4 text-textHeader">
              Điện thoại:
              <span className="text-lg font-semibold text-gray-800">
                {" "}
                {mentorshipRequest.menteePhone}
              </span>
            </h2>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RequestDetail;
