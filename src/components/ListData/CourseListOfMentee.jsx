import React, { useEffect, useState } from "react";
import userStore from "../../store/userStore";
import { Link } from "react-router-dom";
import { Button } from "@mui/material";
import LoadingState from "../LoadingState";
import Pagination from "@mui/material/Pagination";
import menteeStore from "../../store/menteeStore";

function CourseListOfMentee() {
  const { fetchListCoursePurchased, courses, isLoading } = menteeStore();
  const { baseUrl } = userStore();
  const [currentPage, setCurrentPage] = useState(1);
  const pageSize = 3;
  useEffect(() => {
    fetchListCoursePurchased();
  }, []);
  const handlePageChange = (event, page) => {
    setCurrentPage(page);
  };
  let paginatedCourses = [];
  if (courses && courses.length > 0) {
    const startIndex = (currentPage - 1) * pageSize;
    const endIndex = Math.min(startIndex + pageSize, courses.length);
    paginatedCourses = courses.slice(startIndex, endIndex);
  }
  return (
    <div className="w-full md:mx-auto md:px-10">
      {isLoading ? (
        <LoadingState />
      ) : (
        <ul role="list" className="divide-y divide-gray-500">
          {courses.length > 0 ? (
            paginatedCourses.map((list) => (
              <li
                key={list.courseId}
                className="flex flex-col lg:flex-row justify-between gap-x-6 py-5 gap-y-5"
              >
                <div className="flex min-w-0 gap-x-4">
                  <img
                    className="md:h-28 md:w-36 w-20 h-12 flex-none bg-gray-50 rounded-md"
                    src={`${baseUrl}/view/${list.avatar}`}
                    alt=""
                  />
                  <div className="min-w-0 flex-auto gap-y-3">
                    <p className="text-xl font-bold leading-6 text-sidebarTop">
                      Tên khóa học:{" "}
                      <span className="text-gray-700">{list.courseName}</span>
                    </p>
                    <p className="text-lg font-semibold leading-6 text-sidebarTop w-96 truncate">
                      Mô tả:{" "}
                      <span className="text-gray-700  ">
                        {list.description}
                      </span>
                    </p>
                    <p className="text-lg font-semibold leading-6 text-sidebarTop">
                      Giá:{" "}
                      <span className="text-gray-700">
                        {list.price
                          ? list.price.toLocaleString("en-US", {
                              maximumFractionDigits: 0,
                            })
                          : 0}
                        VNĐ
                      </span>
                    </p>
                  </div>
                </div>
                <div className="shrink-0 flex flex-row lg:items-center space-x-10">
                  <Link to={`${list.courseId}`}>
                    <Button
                      variant="contained"
                      size="large"
                      sx={"border-radius: 15px"}
                    >
                      Chi tiết
                    </Button>
                  </Link>
                </div>
              </li>
            ))
          ) : (
            <div className="text-3xl text-center mt-4">
              Bạn chưa có khóa học nào :(
            </div>
          )}
        </ul>
      )}
      {courses && courses.length > 0 && (
        <Pagination
          count={Math.ceil(courses.length / pageSize)}
          page={currentPage}
          onChange={handlePageChange}
          color="primary"
          className="flex flex-col items-center"
        />
      )}
    </div>
  );
}

export default CourseListOfMentee;
