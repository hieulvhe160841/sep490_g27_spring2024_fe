import React, { useEffect, useState } from "react";
import { Button } from "@mui/material";
import { BiSolidMessageDetail } from "react-icons/bi";
import mentorStore from "../../store/mentorStore";
import userStore from "../../store/userStore";
import { Link, useNavigate } from "react-router-dom";
import LoadingState from "../LoadingState";
import Pagination from "@mui/material/Pagination";

function ContactedMenteeData() {
  const { fetchContactedMenteeApi, listContactedMentee, isLoading } =
    mentorStore();
  const { baseUrl } = userStore();
  const navigate = useNavigate();
  const [currentPage, setCurrentPage] = useState(1);
  const pageSize = 4;

  useEffect(() => {
    fetchContactedMenteeApi();
  }, []);

  const handleContactClick = (username) => {
    navigate("/mentor/chat", { state: { username } });
  };

  const handlePageChange = (event, page) => {
    setCurrentPage(page);
  };

  let paginatedMentee = [];

  if (listContactedMentee && listContactedMentee.length > 0) {
    const startIndex = (currentPage - 1) * pageSize;
    const endIndex = Math.min(
      startIndex + pageSize,
      listContactedMentee.length
    );
    paginatedMentee = listContactedMentee.slice(startIndex, endIndex);
  }

  const formatDate = (dateString) => {
    const options = { year: "numeric", month: "2-digit", day: "2-digit" };
    return new Date(dateString).toLocaleDateString(undefined, options);
  };

  return (
    <>
      <div className="w-full md:mx-auto md:px-10">
        {isLoading ? (
          <LoadingState />
        ) : (
          <div className="mt-4">
            <ul role="list" className="divide-y divide-gray-500">
              {listContactedMentee ? (
                paginatedMentee.map((list) => (
                  <li
                    key={list.mentee.userId}
                    className="flex flex-col lg:flex-row justify-between gap-x-6 py-5 gap-y-5"
                  >
                    <div className="flex min-w-0 gap-x-4">
                      <img
                        className="md:h-20 md:w-20 w-10 h-10 flex-none rounded-full bg-gray-50"
                        src={`${baseUrl}/view/${list.mentee.avatar}`}
                        alt=""
                      />
                      <div className="min-w-0 flex-auto gap-y-3">
                        <p className="text-xl font-bold leading-6 text-sidebarTop">
                          Học viên:{" "}
                          <span className="text-gray-700">
                            {list.mentee.name}
                          </span>
                        </p>
                        <p className="text-lg font-semibold leading-6 text-sidebarTop">
                          Email:{" "}
                          <span className="text-gray-700">
                            {list.mentee.email}
                          </span>
                        </p>
                        <p className="text-lg font-semibold leading-6 text-sidebarTop">
                          Ngày hỗ trợ gần nhất:{" "}
                          <span className="text-gray-700">
                            {formatDate(list.latestSupportDay)}
                          </span>
                        </p>
                      </div>
                    </div>
                    <div className="shrink-0 flex flex-row lg:items-center space-x-10">
                      <Link
                        to={`/mentor/menteeDetails/${list.mentee.userName}`}
                      >
                        <Button
                          variant="contained"
                          size="large"
                          sx={"border-radius: 15px"}
                        >
                          Chi tiết
                        </Button>
                      </Link>

                      <div className="flex space-x-2">
                        <Button
                          type="button"
                          onClick={() =>
                            handleContactClick(list.mentee.userName)
                          }
                        >
                          <BiSolidMessageDetail className="w-12 h-12 p-1 rounded-md " />
                        </Button>
                      </div>
                    </div>
                  </li>
                ))
              ) : (
                <div className="text-3xl text-center mt-4">
                  Bạn chưa giúp đỡ học viên nào trên hệ thống :(
                </div>
              )}
            </ul>
            {listContactedMentee && listContactedMentee.length > 0 && (
              <Pagination
                count={Math.ceil(listContactedMentee.length / pageSize)}
                page={currentPage}
                onChange={handlePageChange}
                color="primary"
                className="flex flex-col items-center"
              />
            )}
          </div>
        )}
      </div>
    </>
  );
}

export default ContactedMenteeData;
