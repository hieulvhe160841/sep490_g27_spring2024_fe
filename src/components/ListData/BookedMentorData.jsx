import React, { useEffect, useState } from "react";
import { Button, Pagination } from "@mui/material";
import menteeStore from "../../store/menteeStore";
import userStore from "../../store/userStore";
import { useNavigate } from "react-router-dom";
import LoadingState from "../LoadingState";
import { BiSolidMessageDetail } from "react-icons/bi";

function BookedMentorData() {
  const { fetchBookedMentors, listBookedMentors, isLoading } = menteeStore();
  const { baseUrl } = userStore();
  const navigate = useNavigate();

  useEffect(() => {
    fetchBookedMentors();
  }, []);

  const [currentPage, setCurrentPage] = useState(1);
  const pageSize = 4;

  const handlePageChange = (event, page) => {
    setCurrentPage(page);
  };

  const startIndex = (currentPage - 1) * pageSize;
  const paginatedMentors = listBookedMentors
    ? listBookedMentors.slice(startIndex, startIndex + pageSize)
    : [];

  const handleClickDetail = (mentorUsername) => {
    navigate(`/mentorDetails/${mentorUsername}`);
  };

  const handleContactClick = (username) => {
    navigate("/mentee/chat", { state: { username } });
  };

  const formatDate = (dateString) => {
    const options = { year: "numeric", month: "2-digit", day: "2-digit" };
    return new Date(dateString).toLocaleDateString(undefined, options);
  };

  return (
    <>
      {isLoading ? (
        <LoadingState />
      ) : paginatedMentors.length > 0 ? (
        <div className="mx-5">
          <ul role="list" className="divide-y divide-gray-500">
            {paginatedMentors.map((mentor) => (
              <li key={mentor.mentorDetail.userDetail.userId}>
                <div className="flex flex-col lg:flex-row items-center justify-between my-5">
                  <div className="flex items-center space-x-4 ml-1">
                    <img
                      className="h-20 w-20 flex-none rounded-full bg-gray-50 border-2 border-blue-700"
                      src={`${baseUrl}/view/${mentor.mentorDetail.userDetail.avatar}`}
                      alt="#"
                    />
                    <div className="min-w-0 flex-auto">
                      <p className="text-lg font-semibold leading-6 text-sidebarTop">
                        {mentor.mentorDetail.userDetail.name}
                      </p>
                      <p className="text-sm gap-x-2 leading-6 text-sidebarTop">
                        <span className="font-semibold">
                          Ngày hỗ trợ mới nhất:{" "}
                        </span>
                        :{formatDate(mentor.latestSupportDay)}
                      </p>
                      <p className="text-sm gap-x-2 leading-6 text-sidebarTop">
                        <span className="font-semibold">Giá thuê: </span>
                        {mentor.mentorDetail.costPerHour
                          ? mentor.mentorDetail.costPerHour.toLocaleString(
                              "en-US",
                              {
                                maximumFractionDigits: 0,
                              }
                            )
                          : 0}{" "}
                        VNĐ/h
                      </p>
                    </div>
                  </div>
                  <div className="shrink-0 flex flex-row lg:items-center space-x-5">
                    <div className="flex space-x-2">
                      <Button
                        type="button"
                        onClick={() =>
                          handleContactClick(
                            mentor.mentorDetail.userDetail.userName
                          )
                        }
                      >
                        <BiSolidMessageDetail className="lg:w-12 lg:h-12 w-9 h-9 lg:p-1 rounded-md" />
                      </Button>
                    </div>
                    <Button
                      variant="contained"
                      size="large"
                      sx={"border-radius: 15px"}
                      onClick={() =>
                        handleClickDetail(
                          mentor.mentorDetail.userDetail.userName
                        )
                      }
                    >
                      Chi tiết
                    </Button>
                  </div>
                </div>
              </li>
            ))}
          </ul>

          <Pagination
            count={Math.ceil(listBookedMentors.length / pageSize)}
            page={currentPage}
            onChange={handlePageChange}
            color="primary"
            className="flex flex-col items-center"
          />
        </div>
      ) : (
        <div className="text-3xl text-center mt-3">
          Bạn chưa thuê gia sư nào :(
        </div>
      )}
    </>
  );
}

export default BookedMentorData;
