import React, { useEffect, useRef, useState } from "react";
import { FaStar } from "react-icons/fa";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { Button } from "@mui/material";
import ArrowForData from "../ArrowForData";
import menteeStore from "../../store/menteeStore";
import { Link } from "react-router-dom";
import userStore from "../../store/userStore";
import LoadingState from "../LoadingState";
import { toast } from "react-toastify";

function SuggestedMentorData() {
  const {
    fetchListSuggestMentor,
    listSuggestMentor,
    isLoading,
    requestMentorDirectly,
  } = menteeStore();
  const { baseUrl } = userStore();
  const [selectedUsername, setSelectedUsername] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [hours, setHours] = useState("");

  const slider = useRef(null);

  const slideToShow = () => {
    if (listSuggestMentor.length >= 3) {
      return 3;
    } else if (listSuggestMentor.length === 2) {
      return 2;
    } else if (listSuggestMentor.length === 1) {
      return 1;
    }
  };
  useEffect(() => {
    fetchListSuggestMentor();
  }, []);

  const Arrow = (props) => {
    const { className, style, onClick } = props;

    return (
      <div
        className={className}
        style={{
          ...style,
          display: "block",
          color: "green",
        }}
        onClick={onClick}
      />
    );
  };
  var settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: slideToShow(),
    slidesToScroll: slideToShow(),
    initialSlide: 0,
    nextArrow: <Arrow />,
    prevArrow: <Arrow />,
    responsive: [
      {
        breakpoint: 1280,
        settings: {
          slidesToShow: slideToShow(),
          slidesToScroll: slideToShow(),
        },
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  const handleRequestClick = async (username) => {
    setSelectedUsername(username);
    setShowModal(true);
  };

  const handleHoursChange = (e) => {
    const value = e.target.value;
    if (value === "" || parseInt(value) >= 0) {
      setHours(value);
    }
  };

  const handleSubmitRequest = async (e) => {
    e.preventDefault();
    try {
      await requestMentorDirectly(selectedUsername, hours);
      setShowModal(false);
      toast.success("Gửi yêu cầu thành công");
    } catch (error) {
      toast.error(error);
    }
  };

  return (
    <>
      {isLoading ? (
        <LoadingState />
      ) : listSuggestMentor.length > 0 ? (
        <div className="mt-14 relative h-full md:mx-0 mx-28">
          <ArrowForData slider={slider} />
          <Slider ref={slider} {...settings}>
            {listSuggestMentor.slice(0, 6).map((mentor) => (
              <div
                key={mentor.userDetail.userId}
                className="flex flex-col h-full min-h-[32rem] justify-between relative border-x-2"
              >
                <div className="border-4 border-blue-700 overflow-hidden rounded-full w-24 h-24 lg:h-32 lg:w-32 mx-auto">
                  <img
                    src={`${baseUrl}/view/${mentor.userDetail.avatar}`}
                    alt={mentor.userDetail.name}
                    className="h-full w-full object-cover object-center"
                  />
                </div>
                <div className="md:px-10 md:py-6 ">
                  <div className="my-2 flex justify-between items-center">
                    <h3 className="text-xl font-bold text-sidebarTop ">
                      {mentor.userDetail.name}
                    </h3>

                    {mentor.rating ? (
                      <p className="flex text-base font-medium text-sidebarTop gap-x-1">
                        {mentor.rating.toLocaleString("en-US", {
                          maximumFractionDigits: 1,
                        })}
                        <span>
                          <FaStar color="orange" />
                        </span>
                      </p>
                    ) : (
                      <p className="flex text-sm text-sidebarTop">
                        Chưa đánh giá
                      </p>
                    )}
                  </div>
                  <p className="text-lg my-1 text-sidebarTop font-semibold">
                    Trình độ học vấn:{" "}
                    <span className="text-gray-700">
                      {mentor.userDetail.educationLevel}
                    </span>
                  </p>
                  <p className="text-lg my-1 text-sidebarTop font-semibold">
                    Yêu cầu/Bài đăng:{" "}
                    <span className="text-gray-700">
                      {mentor.numberOfDoneSupport}
                    </span>
                  </p>

                  <p className="text-lg text-sidebarTop font-semibold">
                    Giá thuê:{" "}
                    <span className="text-gray-700">
                      {mentor.costPerHour
                        ? mentor.costPerHour.toLocaleString("en-US", {
                            maximumFractionDigits: 0,
                          })
                        : 0}
                      &nbsp;VNĐ/h
                    </span>
                  </p>
                  <p className="text-lg my-5 text-sidebarTop font-semibold line-clamp-4">
                    <span className="text-gray-700">
                      {mentor.userDetail.experience}
                    </span>
                  </p>
                </div>

                <div className="flex justify-center absolute bottom-0 left-1/2 transform -translate-x-1/2 space-x-2">
                  <Button
                    variant="outlined"
                    sx={{
                      borderColor: "#0B4B88",
                      color: "#0B4B88",
                      backgroundColor: "#ffffff",
                      borderRadius: "12px",
                      borderWidth: "2px",
                      boxSizing: "border-box",
                      width: "120px",
                      height: "40px",
                      "&:hover": {
                        backgroundColor: "#ffffff",
                      },
                    }}
                    onClick={() =>
                      handleRequestClick(mentor.userDetail.userName)
                    }
                  >
                    Yêu cầu
                  </Button>
                  <Link to={`/mentorDetails/${mentor.userDetail.userName}`}>
                    <Button
                      variant="contained"
                      sx={{
                        backgroundColor: "#0B4B88",
                        borderRadius: "12px",
                        "&:hover": {
                          backgroundColor: "#083B5F",
                        },
                        boxSizing: "border-box",
                        width: "120px",
                        height: "40px",
                      }}
                    >
                      Chi tiết
                    </Button>
                  </Link>
                </div>
              </div>
            ))}
          </Slider>
        </div>
      ) : (
        <div className="text-3xl text-center mt-3">
          Không có gia sư để hiển thị.
        </div>
      )}
      {showModal ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-auto my-6 mx-auto max-w-3xl">
              {/*content*/}
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                <form onSubmit={handleSubmitRequest}>
                  {/*header*/}
                  <div className="flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
                    <h3 className="text-3xl font-semibold">Thời gian thuê</h3>
                    <button
                      className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                      onClick={() => setShowModal(false)}
                    >
                      <span className="bg-transparent text-black h-6 w-6 text-2xl block outline-none focus:outline-none">
                        ×
                      </span>
                    </button>
                  </div>
                  {/*body*/}
                  <div className="relative p-6 flex-auto max-h-96 overflow-y-auto">
                    <div className="w-full max-w-sm">
                      <div className="md:flex md:items-center mb-6 justify-center">
                        Bạn muốn thuê gia sư này bao nhiêu giờ?
                      </div>
                      <div className="md:flex md:items-center mb-6 text-sm justify-center">
                        Hãy nhập chính xác số giờ bạn muốn thuê!
                      </div>
                      <div className="md:flex md:items-center mb-6 justify-center">
                        <input
                          className="inputField"
                          id="inline-description"
                          type="text"
                          value={hours}
                          onChange={handleHoursChange}
                        />
                      </div>
                    </div>
                  </div>
                  {/*footer*/}
                  <div className="btnInModal">
                    <Button
                      type="submit"
                      variant="outlined"
                      sx={{
                        border: "2px solid #2B90D9",
                        textTransform: "none",
                        borderRadius: "12px",
                        height: "35px",
                        width: "115px",
                        color: "#000000",
                      }}
                      disabled={!hours || hours <= 0}
                      onClick={handleSubmitRequest}
                    >
                      Gửi
                    </Button>
                    <Button
                      variant="contained"
                      color="error"
                      sx={{
                        backgroundColor: "#FF0A0A",
                        textTransform: "none",
                        borderRadius: "12px",
                        height: "35px",
                        width: "115px",
                        color: "#ffffff",
                      }}
                      onClick={() => setShowModal(false)}
                    >
                      Đóng
                    </Button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </>
  );
}

export default SuggestedMentorData;
