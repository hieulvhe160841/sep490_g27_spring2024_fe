import React, { useEffect, useState } from "react";
import {
  Pagination,
  Button,
  Select,
  MenuItem,
  TextField,
  FormControl,
  InputLabel,
  InputAdornment,
} from "@mui/material";
import userStore from "../../store/userStore";
import menteeStore from "../../store/menteeStore";
import { toast } from "react-toastify";
import { Link, useNavigate } from "react-router-dom";
import LoadingState from "../LoadingState";
import RatingForSupport from "../Rating/RatingForSupport";
import ModalViewPostMentee from "../Modal/ModalViewPostMentee";
import { BiSolidMessageDetail } from "react-icons/bi";

function MentorshipRequestOfMentee() {
  const {
    mentorshipRequestOfMentee,
    listMentorshipRequest,
    menteeAcceptOrRejectRequest,
    isLoading,
  } = menteeStore();
  const { baseUrl } = userStore();
  const navigate = useNavigate();
  const [status, setStatus] = useState([]);
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const getStatusText = (status) => {
    switch (status) {
      case "WAITING":
        return "Đang chờ";
      case "ACCEPTED":
        return "Đã nhận";
      case "DONE":
        return "Hoàn thành";
      case "REJECTED":
        return "Từ chối";
      case "IN_PROGRESS":
        return "Đang tiến hành";
      default:
        return status;
    }
  };

  useEffect(() => {
    const statusString = status.join(",");
    mentorshipRequestOfMentee(statusString, startDate, endDate);
  }, [menteeAcceptOrRejectRequest, status, startDate, endDate]);

  const onHandleAcceptRequest = async (supportId) => {
    const res = await menteeAcceptOrRejectRequest(supportId, "accept");
    if (res.statusCode === 201) {
      toast.success("Bạn đã chấp nhận yêu cầu của gia sư");
      await mentorshipRequestOfMentee();
    } else {
      toast.error("Lỗi khi chấp nhận yêu cầu của gia sư");
    }

    console.log(res);
  };
  const onHandleRejectRequest = async (supportId) => {
    const res = await menteeAcceptOrRejectRequest(supportId, "reject");
    if (res.statusCode === 201) {
      toast.success("Bạn đã từ chối yêu cầu của gia sư");
      await mentorshipRequestOfMentee();
    } else {
      toast.error("Lỗi khi từ chối yêu cầu của gia sư");
    }

    console.log(res);
  };

  const [currentPage, setCurrentPage] = useState(1);
  const pageSize = 4;
  const [sortBy, setSortBy] = useState("supportId");
  const [sortOrder, setSortOrder] = useState("desc");

  const handlePageChange = (event, page) => {
    setCurrentPage(page);
  };

  const toggleSortOrder = () => {
    setSortOrder(sortOrder === "asc" ? "desc" : "asc");
  };

  const sortedRequests = listMentorshipRequest
    ? [...listMentorshipRequest].sort((a, b) => {
        if (sortBy === "supportId") {
          return sortOrder === "asc"
            ? a.supportId - b.supportId
            : b.supportId - a.supportId;
        } else if (sortBy === "supportStatus") {
          return sortOrder === "asc"
            ? a.supportStatus.localeCompare(b.supportStatus)
            : b.supportStatus.localeCompare(a.supportStatus);
        }
        return 0;
      })
    : [];

  const startIndex = (currentPage - 1) * pageSize;
  const endIndex = Math.min(startIndex + pageSize, sortedRequests.length);
  const paginatedRequests = sortedRequests.slice(startIndex, endIndex);

  const handleSortBy = (criteria) => {
    if (sortBy === criteria) {
      toggleSortOrder();
    } else {
      setSortBy(criteria);
      setSortOrder("asc");
    }
  };

  const formatDate = (dateString) => {
    const options = { year: "numeric", month: "2-digit", day: "2-digit" };
    return new Date(dateString).toLocaleDateString(undefined, options);
  };

  const handleContactClick = (username) => {
    navigate("/mentee/chat", { state: { username } });
  };

  return (
    <>
      <div className="flex gap-x-3 justify-center my-10">
        <FormControl sx={{ minWidth: 150 }}>
          <InputLabel id="status-label">Trạng thái</InputLabel>
          <Select
            labelId="status-label"
            label="Trạng thái"
            multiple
            value={status}
            onChange={(event) => setStatus(event.target.value)}
          >
            <MenuItem value="WAITING">Đang chờ</MenuItem>
            <MenuItem value="ACCEPTED">Đã nhận</MenuItem>
            <MenuItem value="DONE">Hoàn thành</MenuItem>
            <MenuItem value="REJECTED">Từ chối</MenuItem>
            <MenuItem value="IN_PROGRESS">Đang tiến hành</MenuItem>
          </Select>
        </FormControl>
        <TextField
          type="date"
          value={startDate}
          onChange={(event) => setStartDate(event.target.value)}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">Từ</InputAdornment>
            ),
          }}
        />
        <TextField
          type="date"
          value={endDate}
          onChange={(event) => setEndDate(event.target.value)}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">Đến</InputAdornment>
            ),
          }}
        />
      </div>
      {isLoading ? (
        <LoadingState />
      ) : paginatedRequests.length > 0 ? (
        <div className="m-5">
          <div className="flex items-center justify-start mb-2 gap-x-1">
            <span className="text-lg font-bold">Sắp xếp theo:</span>
            <Button
              variant="contained"
              onClick={() => handleSortBy("supportId")}
            >
              Số hỗ trợ
            </Button>
            <Button
              variant="contained"
              onClick={() => handleSortBy("supportStatus")}
            >
              Trạng thái
            </Button>
          </div>
          <ul role="list" className="divide-y divide-gray-500">
            {paginatedRequests.map((request) => (
              <li key={request.supportId}>
                <div className="flex flex-col lg:flex-row justify-between gap-x-6 py-5">
                  <div className="flex min-w-0 gap-x-4 items-center">
                    <img
                      className="h-20 w-20 flex-none object-contain max-w-full rounded-full bg-gray-50 border-2 border-blue-700"
                      src={`${baseUrl}/view/${request.mentorAvatar}`}
                      alt={request.mentorUsername}
                    />

                    <div className="flex-col">
                      <p className="text-lg font-bold leading-6 text-sidebarTop">
                        Hỗ trợ số: {request.supportId}
                      </p>
                      <p className="text-lg leading-6 text-sidebarTop mt-1 font-semibold">
                        Trạng thái:&nbsp;
                        <span className="text-gray-700">
                          {getStatusText(request.supportStatus)}
                        </span>
                      </p>
                      <p className="text-lg leading-6 text-sidebarTop mt-1 font-semibold">
                        Ngày tạo:&nbsp;
                        <span className="text-gray-700">
                          {formatDate(request.supportCreatedDate)}
                        </span>
                      </p>
                      <p className="text-lg leading-6 text-sidebarTop mt-1 font-semibold">
                        Loại:&nbsp;
                        {!request.postId ? (
                          <span className="text-gray-700">Yêu cầu</span>
                        ) : (
                          <span className="text-gray-700">Bài đăng</span>
                        )}
                      </p>
                      <p className="text-lg leading-6 text-sidebarTop mt-1 font-semibold">
                        Gia sư:&nbsp;
                        <span className="text-gray-700">
                          {request.mentorUsername}
                        </span>
                      </p>
                    </div>
                  </div>
                  {request.supportStatus === "WAITING" && request.postId && (
                    <div className="shrink-0 flex flex-row lg:items-center space-x-3">
                      <div>
                        <ModalViewPostMentee postId={request.postId} />
                      </div>

                      <Button
                        variant="outlined"
                        size="large"
                        sx={"border-radius: 15px"}
                        onClick={() => onHandleAcceptRequest(request.supportId)}
                      >
                        Chấp nhận
                      </Button>

                      <Button
                        variant="contained"
                        size="large"
                        sx={"border-radius: 15px"}
                        color="error"
                        onClick={() => onHandleRejectRequest(request.supportId)}
                      >
                        Từ chối
                      </Button>

                      <div className="ml-3">
                        <Button
                          type="button"
                          onClick={() =>
                            handleContactClick(request.mentorUsername)
                          }
                        >
                          <BiSolidMessageDetail className="w-12 h-12 p-1 rounded-md" />
                        </Button>
                      </div>
                    </div>
                  )}
                  {request.supportStatus === "WAITING" && !request.postId && (
                    <div className="shrink-0 flex flex-row lg:items-center space-x-3">
                      <Link to={`/mentorDetails/${request.mentorUsername}`}>
                        <Button
                          variant="contained"
                          size="large"
                          sx={"border-radius: 15px"}
                        >
                          Chi tiết
                        </Button>
                      </Link>

                      <Button
                        type="button"
                        onClick={() =>
                          handleContactClick(request.mentorUsername)
                        }
                      >
                        <BiSolidMessageDetail className="w-12 h-12 p-1 rounded-md" />
                      </Button>
                    </div>
                  )}
                  {request.supportStatus === "DONE" && request.postId && (
                    <div className="shrink-0 flex flex-row lg:items-center">
                      {!request.rating ? (
                        <RatingForSupport request={request} />
                      ) : (
                        ""
                      )}
                      <div className="mx-3">
                        <ModalViewPostMentee postId={request.postId} />
                      </div>

                      <div>
                        <Button
                          type="button"
                          onClick={() =>
                            handleContactClick(request.mentorUsername)
                          }
                        >
                          <BiSolidMessageDetail className="w-12 h-12 p-1 rounded-md" />
                        </Button>
                      </div>
                    </div>
                  )}
                  {request.supportStatus === "DONE" && !request.postId && (
                    <div className="shrink-0 flex flex-row lg:items-center space-x-3">
                      <Link to={`/mentorDetails/${request.mentorUsername}`}>
                        <Button
                          variant="contained"
                          size="large"
                          sx={"border-radius: 15px"}
                        >
                          Chi tiết
                        </Button>
                      </Link>
                      {!request.rating ? (
                        <RatingForSupport request={request} />
                      ) : (
                        ""
                      )}
                      <Button
                        type="button"
                        onClick={() =>
                          handleContactClick(request.mentorUsername)
                        }
                      >
                        <BiSolidMessageDetail className="w-12 h-12 p-1 rounded-md" />
                      </Button>
                    </div>
                  )}
                  {request.supportStatus === "REJECTED" && request.postId && (
                    <div className="shrink-0 flex flex-row lg:items-center">
                      <div className="mr-3">
                        <ModalViewPostMentee postId={request.postId} />
                      </div>
                      <Button
                        type="button"
                        onClick={() =>
                          handleContactClick(request.mentorUsername)
                        }
                      >
                        <BiSolidMessageDetail className="w-12 h-12 p-1 rounded-md" />
                      </Button>
                    </div>
                  )}
                  {request.supportStatus === "REJECTED" && !request.postId && (
                    <div className="shrink-0 flex flex-row lg:items-center space-x-3">
                      <Link to={`/mentorDetails/${request.mentorUsername}`}>
                        <Button
                          variant="contained"
                          size="large"
                          sx={"border-radius: 15px"}
                        >
                          Chi tiết
                        </Button>
                      </Link>
                      <Button
                        type="button"
                        onClick={() =>
                          handleContactClick(request.mentorUsername)
                        }
                      >
                        <BiSolidMessageDetail className="w-12 h-12 p-1 rounded-md" />
                      </Button>
                    </div>
                  )}
                  {request.supportStatus === "ACCEPTED" && request.postId && (
                    <div className="shrink-0 flex flex-row lg:items-center">
                      <div className="mr-3">
                        <ModalViewPostMentee postId={request.postId} />
                      </div>

                      <Link to={`/mentee/postSession/${request.supportId}`}>
                        <Button
                          variant="contained"
                          size="large"
                          color="success"
                          sx={"border-radius: 15px"}
                        >
                          {/* Start Post Session */}
                          Bắt đầu
                        </Button>
                      </Link>
                      <div className="ml-3">
                        <Button
                          type="button"
                          onClick={() =>
                            handleContactClick(request.mentorUsername)
                          }
                        >
                          <BiSolidMessageDetail className="w-12 h-12 p-1 rounded-md" />
                        </Button>
                      </div>
                    </div>
                  )}
                  {request.supportStatus === "ACCEPTED" && !request.postId && (
                    <div className="shrink-0 flex flex-row lg:items-center space-x-3">
                      <Link to={`/mentorDetails/${request.mentorUsername}`}>
                        <Button
                          variant="contained"
                          size="large"
                          sx={"border-radius: 15px"}
                        >
                          Chi tiết
                        </Button>
                      </Link>
                      <Link to={`/mentee/requestSession/${request.supportId}`}>
                        <Button
                          variant="contained"
                          size="large"
                          color="success"
                          sx={"border-radius: 15px"}
                        >
                          {/* Start Request Session */}
                          Bắt đầu
                        </Button>
                      </Link>
                      <Button
                        type="button"
                        onClick={() =>
                          handleContactClick(request.mentorUsername)
                        }
                      >
                        <BiSolidMessageDetail className="w-12 h-12 p-1 rounded-md" />
                      </Button>
                    </div>
                  )}
                  {request.supportStatus === "IN_PROGRESS" &&
                    request.postId && (
                      <div className="shrink-0 flex flex-row lg:items-center">
                        <div className="mr-3">
                          <ModalViewPostMentee postId={request.postId} />
                        </div>

                        <Link to={`/mentee/postSession/${request.supportId}`}>
                          <Button
                            variant="contained"
                            size="large"
                            color="success"
                            sx={"border-radius: 15px"}
                          >
                            {/* Join Post Session */}
                            Tham gia
                          </Button>
                        </Link>
                        <div className="ml-3">
                          <Button
                            type="button"
                            onClick={() =>
                              handleContactClick(request.mentorUsername)
                            }
                          >
                            <BiSolidMessageDetail className="w-12 h-12 p-1 rounded-md" />
                          </Button>
                        </div>
                      </div>
                    )}
                  {request.supportStatus === "IN_PROGRESS" &&
                    !request.postId && (
                      <div className="shrink-0 flex flex-row lg:items-center space-x-3">
                        <Link to={`/mentorDetails/${request.mentorUsername}`}>
                          <Button
                            variant="contained"
                            size="large"
                            sx={"border-radius: 15px"}
                          >
                            Chi tiết
                          </Button>
                        </Link>
                        <Link
                          to={`/mentee/requestSession/${request.supportId}`}
                        >
                          <Button
                            variant="contained"
                            size="large"
                            color="success"
                            sx={"border-radius: 15px"}
                          >
                            {/* Join Request Session */}
                            Tham gia
                          </Button>
                        </Link>
                        <Button
                          type="button"
                          onClick={() =>
                            handleContactClick(request.mentorUsername)
                          }
                        >
                          <BiSolidMessageDetail className="w-12 h-12 p-1 rounded-md" />
                        </Button>
                      </div>
                    )}
                </div>
              </li>
            ))}
          </ul>
          <Pagination
            count={Math.ceil(listMentorshipRequest.length / pageSize)}
            page={currentPage}
            onChange={handlePageChange}
            color="primary"
            className="flex flex-col items-center"
          />
        </div>
      ) : (
        <div className="text-3xl text-center mt-4">
          Bạn chưa có yêu cầu gia sư nào :(
        </div>
      )}
    </>
  );
}

export default MentorshipRequestOfMentee;
