import React, { useEffect, useState } from "react";
import { Button } from "@mui/material";
import { FaStar } from "react-icons/fa";
import menteeStore from "../../store/menteeStore";
import userStore from "../../store/userStore";
import { Link, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import LoadingState from "../LoadingState";

function TopRatedMentor() {
  const { fetchListTopMentor, isLoading, requestMentorDirectly } =
    menteeStore();
  const { baseUrl } = userStore();
  const [showModal, setShowModal] = useState(false);
  const role = localStorage.getItem("role");
  const menteeRole = "ROLE_MENTEE";
  const navigate = useNavigate();
  const [hours, setHours] = useState("");
  const [topMentor, setTopMentor] = useState([]);
  const [selectedUsername, setSelectedUsername] = useState(null);

  useEffect(() => {
    const fetchTopMentor = async () => {
      try {
        const topMentor = await fetchListTopMentor();
        console.log("Top Mentor Data: ", topMentor);
        setTopMentor(topMentor.data);
      } catch (error) {
        console.error("Error fetching Top Mentor:", error);
      }
    };
    fetchTopMentor();
  }, []);

  const handleRequestClick = async (username) => {
    if (role === menteeRole) {
      setSelectedUsername(username);
      setShowModal(true);
    } else if (role) {
      toast.warning("Chức năng này chỉ dành cho Học viên.");
    } else {
      toast.warning("Bạn phải đăng nhập trước");
      navigate("/login");
    }
  };

  const handleHoursChange = (e) => {
    const value = e.target.value;
    if (value === "" || parseInt(value) >= 0) {
      setHours(value);
    }
  };

  const handleSubmitRequest = async (e) => {
    e.preventDefault();
    try {
      await requestMentorDirectly(selectedUsername, hours);
      setShowModal(false);
      toast.success("Gửi yêu cầu thành công");
    } catch (error) {
      toast.error(error);
    }
  };

  return (
    <>
      {isLoading ? (
        <LoadingState />
      ) : topMentor.length > 0 ? (
        <div className="md:col-span-6 md:col-start-3 w-full md:mx-auto md:px-20 pt-4">
          <ul role="list" className="divide-y divide-gray-500">
            {topMentor.map((mentor) => (
              <li
                key={mentor.userDetail.userId}
                className="flex flex-col lg:flex-row justify-between gap-x-2 py-5 gap-y-3"
              >
                <div className="flex min-w-0 gap-10 items-center">
                  <img
                    src={`${baseUrl}/view/${mentor.userDetail.avatar}`}
                    alt={mentor.userDetail.name}
                    className="md:h-24 md:w-24 w-12 h-12 flex-none rounded-full object-cover object-center border-2 border-blue-700"
                  />
                  <div className="mt-3">
                    <div className="flex justify-between gap-2">
                      <p className="text-xl font-bold items-center text-sidebarTop mr-5">
                        Tên gia sư: {mentor.userDetail.name}
                      </p>

                      {mentor.rating ? (
                        <p className="text-xl  text-sidebarTop flex justify-center items-center gap-x-1 font-semibold">
                          Đánh giá:{" "}
                          {mentor.rating.toLocaleString("en-US", {
                            maximumFractionDigits: 1,
                          })}
                          <span className="pb-3">
                            <FaStar color="orange" />
                          </span>
                        </p>
                      ) : (
                        <p className="text-xs text-gray-600 pl-1.5">
                          Chưa đánh giá
                        </p>
                      )}
                    </div>
                    <p className="text-xl mt-3 truncate text-gray-700 lg:w-80 w-40">
                      <span className="text-xl flex">
                        <p className="font-semibold text-sidebarTop">
                          Trình độ học vấn:
                        </p>
                        &nbsp;{mentor.userDetail.educationLevel}
                      </span>
                    </p>
                    <p className="text-xl mt-3 truncate text-gray-700 lg:w-80 w-40">
                      <span className="text-xl flex">
                        <p className="font-semibold text-sidebarTop">
                          Yêu cầu/Bài đăng:
                        </p>
                        &nbsp;{mentor.numberOfDoneSupport}
                      </span>
                    </p>
                    <p className="text-xl mt-2 truncate text-gray-700 md:w-[30rem] w-40">
                      <span className="text-xl ">
                        {mentor.userDetail.experience}
                      </span>
                    </p>
                  </div>
                </div>
                <div className="shrink-0 lg:flex lg:items-center">
                  <Button
                    variant="outlined"
                    sx={{
                      borderColor: "#0B4B88",
                      color: "#0B4B88",
                      backgroundColor: "#ffffff",
                      borderRadius: "12px",
                      borderWidth: "2px",
                      boxSizing: "border-box",
                      width: "120px",
                      height: "40px",
                      "&:hover": {
                        backgroundColor: "#ffffff",
                      },
                      marginRight: "16px",
                    }}
                    onClick={() =>
                      handleRequestClick(mentor.userDetail.userName)
                    }
                  >
                    Yêu cầu
                  </Button>
                  {showModal ? (
                    <>
                      <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
                        <div className="relative w-auto my-6 mx-auto max-w-3xl">
                          {/*content*/}
                          <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                            <form onSubmit={handleSubmitRequest}>
                              {/*header*/}
                              <div className="flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
                                <h3 className="text-3xl font-semibold">
                                  Thời gian thuê
                                </h3>
                                <button
                                  className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                                  onClick={() => setShowModal(false)}
                                >
                                  <span className="bg-transparent text-black h-6 w-6 text-2xl block outline-none focus:outline-none">
                                    ×
                                  </span>
                                </button>
                              </div>
                              {/*body*/}
                              <div className="relative p-6 flex-auto max-h-96 overflow-y-auto">
                                <div className="w-full max-w-sm">
                                  <div className="md:flex md:items-center mb-6 justify-center">
                                    Bạn muốn thuê gia sư này bao nhiêu giờ?
                                  </div>
                                  <div className="md:flex md:items-center mb-6 text-sm justify-center">
                                    Hãy nhập chính xác số giờ bạn muốn thuê!
                                  </div>
                                  <div className="md:flex md:items-center mb-6 justify-center">
                                    <input
                                      className="inputField"
                                      id="inline-description"
                                      type="number"
                                      value={hours}
                                      onChange={handleHoursChange}
                                    />
                                  </div>
                                </div>
                              </div>
                              {/*footer*/}
                              <div className="btnInModal">
                                <Button
                                  type="submit"
                                  variant="outlined"
                                  sx={{
                                    border: "2px solid #2B90D9",
                                    textTransform: "none",
                                    borderRadius: "12px",
                                    height: "35px",
                                    width: "115px",
                                    color: "#000000",
                                  }}
                                  disabled={!hours || hours <= 0}
                                  onClick={handleSubmitRequest}
                                >
                                  Gửi
                                </Button>
                                <Button
                                  variant="contained"
                                  color="error"
                                  sx={{
                                    backgroundColor: "#FF0A0A",
                                    textTransform: "none",
                                    borderRadius: "12px",
                                    height: "35px",
                                    width: "115px",
                                    color: "#ffffff",
                                  }}
                                  onClick={() => setShowModal(false)}
                                >
                                  Đóng
                                </Button>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                      <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                    </>
                  ) : null}
                  <Link to={`/mentorDetails/${mentor.userDetail.userName}`}>
                    <Button
                      variant="contained"
                      sx={{
                        backgroundColor: "#0B4B88",
                        borderRadius: "12px",
                        "&:hover": {
                          backgroundColor: "#083B5F",
                        },
                        boxSizing: "border-box",
                        width: "120px",
                        height: "40px",
                      }}
                    >
                      Chi tiết
                    </Button>
                  </Link>
                </div>
              </li>
            ))}
          </ul>
        </div>
      ) : (
        <div className="text-3xl text-center mt-3">
          Hiện chưa có gia sư nào :(
        </div>
      )}
    </>
  );
}

export default TopRatedMentor;
