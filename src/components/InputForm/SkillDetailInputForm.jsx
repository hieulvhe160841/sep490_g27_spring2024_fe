import React, { useEffect, useState } from "react";
import { Button } from "@mui/material";
import userStore from "../../store/userStore";
import { toast } from "react-toastify";

const SkillDetailInputForm = ({ skill }) => {
  const { updateSkill } = userStore();

  const statusOptions = [
    { label: "Không hoạt động", value: false },
    { label: "Hoạt động", value: true },
  ];

  const [name, setName] = useState();
  const [description, setDescription] = useState();
  const [selectedStatus, setSelectedStatus] = useState();

  useEffect(() => {
    setName(skill.skillName);
    setDescription(skill.skillDescription);
    setSelectedStatus(skill.skillStatus);
  }, [skill]);

  const handleNameChange = (event) => {
    setName(event.target.value);
  };
  const handleDescriptionChange = (event) => {
    setDescription(event.target.value);
  };
  const handleStatusChange = (event) => {
    setSelectedStatus(event.target.value);
  };

  const handleSave = async () => {
    try {
      await updateSkill(skill.skillId, {
        skillName: name,
        skillDescription: description,
        skillStatus: selectedStatus,
      });
      toast.success("Cập nhật kỹ năng thành công");
    } catch (error) {
      toast.error("Lỗi cập nhật kỹ năng:", error);
    }
  };

  return (
    <form className="w-full max-w-3xl">
      <div className="md:flex md:items-center mb-6">
        <div className="md:w-1/3">
          <label className="labelInput" htmlFor="inline-full-name">
            ID
          </label>
        </div>
        <div className="md:w-2/3">
          <input
            className="inputField"
            id="inline-full-name"
            type="text"
            defaultValue={skill.skillId}
            disabled
          />
        </div>
      </div>
      <div className="md:flex md:items-center mb-6">
        <div className="md:w-1/3">
          <label className="labelInput" htmlFor="inline-name">
            Tên Kỹ năng
          </label>
        </div>
        <div className="md:w-2/3">
          <input
            className="inputField"
            id="inline-name"
            type="text"
            value={name}
            onChange={handleNameChange}
          />
        </div>
      </div>
      <div className="md:flex md:items-center mb-6">
        <div className="md:w-1/3">
          <label className="labelInput" htmlFor="inline-description">
            Mô tả
          </label>
        </div>
        <div className="md:w-2/3">
          <textarea
            className="inputField"
            id="inline-description"
            value={description}
            onChange={handleDescriptionChange}
            rows={3}
            maxLength={255}
          />
        </div>
      </div>
      <div className="md:flex md:items-center mb-6">
        <div className="md:w-1/3">
          <label className="labelInput" htmlFor="inline-status">
            Trang thái
          </label>
        </div>
        <div className="md:w-2/3">
          <select
            className="inputField"
            id="inline-status"
            value={selectedStatus}
            onChange={handleStatusChange}
          >
            {statusOptions.map((option) => (
              <option key={option.value} value={option.value}>
                {option.label}
              </option>
            ))}
          </select>
        </div>
      </div>
      <div className="md:flex md:justify-end md:items-center rounded-b">
        <Button
          variant="outlined"
          sx={{
            border: "2px solid #2B90D9",
            textTransform: "none",
            borderRadius: "12px",
            height: "35px",
            width: "115px",
            color: "#000000",
          }}
          onClick={handleSave}
        >
          Lưu
        </Button>
      </div>
    </form>
  );
};

export default SkillDetailInputForm;
