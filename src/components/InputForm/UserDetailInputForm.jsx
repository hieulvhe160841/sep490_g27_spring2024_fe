import React, { useState } from "react";
import { Button } from "@mui/material";
import userStore from "../../store/userStore";
import { toast } from "react-toastify";

const UserDetailInputForm = ({ user }) => {
  const { updateUserStatus } = userStore();

  const roleOptions = [
    { label: "Nhân viên giao dịch", value: "ROLE_TRANSACTION_STAFF" },
    { label: "Nhân viên quản lý", value: "ROLE_EDUCATION_STAFF" },
    { label: "Gia sư", value: "ROLE_MENTOR" },
    { label: "Học viên", value: "ROLE_MENTEE" },
    { label: "Quản trị viên", value: "ROLE_ADMIN" },
  ];

  const statusOptions = [
    { label: "Không hoạt động", value: false },
    { label: "Hoạt động", value: true },
  ];

  const genderOptions = [
    { label: "Nam", value: 1 },
    { label: "Nữ", value: 0 },
  ];

  const [selectedStatus, setSelectedStatus] = useState(user.status);

  const handleStatusChange = (event) => {
    setSelectedStatus(event.target.value);
  };

  const handleSave = async () => {
    try {
      await updateUserStatus(user.userName, selectedStatus);
      toast.success("Cập nhật trạng thái tài khoản thành công");
    } catch (error) {
      toast.error("Lỗi cập nhật trạng thái tài khoản:", error);
    }
  };

  return (
    <form className="w-full max-w-3xl">
      <div className="md:flex md:items-center mb-6">
        <div className="md:w-1/3">
          <label className="labelInput" htmlFor="inline-full-name">
            Tài khoản
          </label>
        </div>
        <div className="md:w-2/3">
          <input
            className="inputField"
            id="inline-full-name"
            type="text"
            defaultValue={user.userName}
            disabled
          />
        </div>
      </div>
      <div className="md:flex md:items-center mb-6">
        <div className="md:w-1/3">
          <label className="labelInput" htmlFor="inline-name">
            Tên
          </label>
        </div>
        <div className="md:w-2/3">
          <input
            className="inputField"
            id="inline-name"
            type="text"
            defaultValue={user.name}
            disabled
          />
        </div>
      </div>
      <div className="md:flex md:items-center mb-6">
        <div className="md:w-1/3">
          <label className="labelInput" htmlFor="inline-phone">
            Số điện thoại
          </label>
        </div>
        <div className="md:w-2/3">
          <input
            className="inputField"
            id="inline-phone"
            type="text"
            defaultValue={user.phone}
            disabled
          />
        </div>
      </div>
      <div className="md:flex md:items-center mb-6">
        <div className="md:w-1/3">
          <label className="labelInput" htmlFor="inline-address">
            Địa chỉ
          </label>
        </div>
        <div className="md:w-2/3">
          <input
            className="inputField"
            id="inline-address"
            type="text"
            defaultValue={user.address}
            disabled
          />
        </div>
      </div>
      <div className="md:flex md:items-center mb-6">
        <div className="md:w-1/3">
          <label className="labelInput" htmlFor="inline-status">
            Trạng thái
          </label>
        </div>
        <div className="md:w-2/3">
          <select
            className="inputField bg-white border-[#2B90D9]"
            id="inline-status"
            value={selectedStatus}
            onChange={handleStatusChange}
          >
            {statusOptions.map((option) => (
              <option
                key={option.value}
                selected={option.value === user.status}
              >
                {option.label}
              </option>
            ))}
          </select>
        </div>
      </div>
      <div className="md:flex md:items-center mb-6">
        <div className="md:w-1/3">
          <label className="labelInput" htmlFor="inline-gender">
            Giới tính
          </label>
        </div>
        <div className="md:w-2/3">
          <select className="inputField" id="inline-gender" disabled>
            {genderOptions.map((option) => (
              <option
                key={option.value}
                selected={option.value === user.gender}
              >
                {option.label}
              </option>
            ))}
          </select>
        </div>
      </div>
      <div className="md:flex md:items-center mb-6">
        <div className="md:w-1/3">
          <label className="labelInput" htmlFor="inline-dob">
            Ngày sinh
          </label>
        </div>
        <div className="md:w-2/3">
          <input
            className="inputField"
            id="inline-dob"
            type="date"
            defaultValue={user.dayOfBirth}
            disabled
          />
        </div>
      </div>
      <div className="md:flex md:items-center mb-6">
        <div className="md:w-1/3">
          <label className="labelInput" htmlFor="inline-address">
            Ảnh đại diện
          </label>
        </div>
        <div className="md:w-2/3">
          <input
            className="inputField"
            id="inline-address"
            type="text"
            defaultValue={user.avatar}
            disabled
          />
        </div>
      </div>
      <div className="md:flex md:items-center mb-6">
        <div className="md:w-1/3">
          <label className="labelInput" htmlFor="inline-email">
            Email
          </label>
        </div>
        <div className="md:w-2/3">
          <input
            className="inputField"
            id="inline-email"
            type="email"
            defaultValue={user.email}
            disabled
          />
        </div>
      </div>
      {user.roleDTO?.roleName === "ROLE_MENTOR" && (
        <>
          <div className="md:flex md:items-center mb-6">
            <div className="md:w-1/3">
              <label className="labelInput" htmlFor="inline-education-level">
                Trình độ học vấn
              </label>
            </div>
            <div className="md:w-2/3">
              <textarea
                className="inputField"
                id="inline-education-level"
                rows="3"
                defaultValue={user.educationLevel}
                disabled
              />
            </div>
          </div>
          <div className="md:flex md:items-center mb-6">
            <div className="md:w-1/3">
              <label className="labelInput" htmlFor="inline-rating">
                Đánh giá
              </label>
            </div>
            <div className="md:w-2/3">
              <textarea
                className="inputField"
                id="inline-rating"
                rows="3"
                defaultValue={user.rating}
                disabled
              />
            </div>
          </div>
          <div className="md:flex md:items-center mb-6">
            <div className="md:w-1/3">
              <label className="labelInput" htmlFor="inline-experience">
                Kinh nghiệm
              </label>
            </div>
            <div className="md:w-2/3">
              <textarea
                className="inputField"
                id="inline-experience"
                rows="3"
                defaultValue={user.experience}
                disabled
              />
            </div>
          </div>
        </>
      )}

      {user.roleDTO?.roleName === "ROLE_MENTEE" && (
        <>
          <div className="md:flex md:items-center mb-6">
            <div className="md:w-1/3">
              <label className="labelInput" htmlFor="inline-goal">
                Mục tiêu
              </label>
            </div>
            <div className="md:w-2/3">
              <textarea
                className="inputField"
                id="inline-goal"
                rows="4"
                defaultValue={user.goal}
                disabled
              />
            </div>
          </div>
          <div className="md:flex md:items-center mb-6">
            <div className="md:w-1/3">
              <label className="labelInput" htmlFor="inline-interest">
                Sở thích
              </label>
            </div>
            <div className="md:w-2/3">
              <textarea
                className="inputField"
                id="inline-interest"
                rows="4"
                defaultValue={user.interest}
                disabled
              />
            </div>
          </div>
        </>
      )}
      <div className="md:flex md:items-center mb-6">
        <div className="md:w-1/3">
          <label className="labelInput" htmlFor="inline-dob">
            Ngày tạo
          </label>
        </div>
        <div className="md:w-2/3">
          <input
            className="inputField"
            id="inline-dob"
            type="date"
            disabled
            defaultValue={user.dateCreated}
          />
        </div>
      </div>
      <div className="md:flex md:items-center mb-6">
        <div className="md:w-1/3">
          <label className="labelInput" htmlFor="inline-dob">
            Ngày chỉnh sửa
          </label>
        </div>
        <div className="md:w-2/3">
          <input
            className="inputField"
            id="inline-dob"
            type="date"
            disabled
            defaultValue={user.dateModified}
          />
        </div>
      </div>
      <div className="md:flex md:items-center mb-6">
        <div className="md:w-1/3">
          <label className="labelInput" htmlFor="inline-role">
            Role
          </label>
        </div>
        <div className="md:w-2/3">
          <select className="inputField" id="inline-role" disabled>
            {roleOptions.map((option) => (
              <option
                key={option.value}
                selected={option.value === user.roleDTO?.roleName}
              >
                {option.label}
              </option>
            ))}
          </select>
        </div>
      </div>
      <div className="md:flex md:justify-end md:items-center rounded-b">
        {user.roleDTO?.roleName !== "ROLE_ADMIN" ? (
          <Button
            variant="outlined"
            sx={{
              border: "2px solid #2B90D9",
              textTransform: "none",
              borderRadius: "12px",
              height: "35px",
              width: "115px",
              color: "#000000",
            }}
            onClick={handleSave}
          >
            Lưu
          </Button>
        ) : (
          ""
        )}
      </div>
    </form>
  );
};

export default UserDetailInputForm;
