import React from "react";
import { Link } from "react-router-dom";
import { IoIosArrowBack } from "react-icons/io";

function RedirectToLogin() {
  return (
    <>
      <div className="flex flex-col justify-center items-center mt-5">
        <div>
          <Link to="/login" className="underline">
            <IoIosArrowBack className="inline" />
            Quay lại Đăng nhập
          </Link>
        </div>
      </div>
    </>
  );
}

export default RedirectToLogin;
