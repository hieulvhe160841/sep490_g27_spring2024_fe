import React from "react";
import { Link } from "react-router-dom";
import { IoIosArrowBack } from "react-icons/io";

function RedirectToHome() {
  return (
    <>
      <Link to="/" className="underline">
        <IoIosArrowBack className="inline" />
        Quay lại Trang chủ
      </Link>
    </>
  );
}

export default RedirectToHome;
