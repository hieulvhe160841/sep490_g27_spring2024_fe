import React, { useEffect, useState } from "react";
import menteeStore from "../../store/menteeStore";
import LoadingState from "../LoadingState";
import userStore from "../../store/userStore";
import { useNavigate } from "react-router-dom";
import {
  Button,
  Select,
  MenuItem,
  TextField,
  FormControl,
  InputLabel,
} from "@mui/material";

function ListData() {
  const { fetchListSuggestCourse, listSuggestCourse, isLoading } =
    menteeStore();
  const { baseUrl } = userStore();
  const navigate = useNavigate();
  const [mentorName, setMentorName] = useState("");
  const [courseName, setCourseName] = useState("");
  const [toPrice, setToPrice] = useState("");
  const [fromPrice, setFromPrice] = useState("");

  useEffect(() => {
    fetchListSuggestCourse(mentorName, courseName, fromPrice, toPrice);
  }, [mentorName, courseName, fromPrice, toPrice]);
  const onHandleViewCourseDetail = (id) => {
    navigate(`/course/${id}`);
  };
  return (
    <>
      <div className="flex gap-x-3 justify-center mt-10">
        <FormControl sx={{ minWidth: 150 }}>
          <InputLabel id="price-label">Giá Từ(VNĐ)</InputLabel>
          <Select
            labelId="price-label"
            label="from"
            value={fromPrice}
            onChange={(event) => setFromPrice(event.target.value)}
          >
            <MenuItem value="">
              <em>Không</em>
            </MenuItem>
            <MenuItem value={100000}>100,000</MenuItem>
            <MenuItem value={300000}>300,000</MenuItem>
            <MenuItem value={500000}>500,000</MenuItem>
            <MenuItem value={800000}>800,000</MenuItem>
          </Select>
        </FormControl>
        <FormControl sx={{ minWidth: 150 }}>
          <InputLabel id="price-label">Giá Đến(VNĐ)</InputLabel>
          <Select
            labelId="price-label"
            label="to"
            value={toPrice}
            onChange={(event) => setToPrice(event.target.value)}
          >
            <MenuItem value="">
              <em>Không</em>
            </MenuItem>
            <MenuItem value={100000}>100,000</MenuItem>
            <MenuItem value={300000}>300,000</MenuItem>
            <MenuItem value={500000}>500,000</MenuItem>
            <MenuItem value={800000}>800,000</MenuItem>
          </Select>
        </FormControl>
        <TextField
          type="text"
          value={mentorName}
          label="Gia sư"
          onChange={(event) => setMentorName(event.target.value)}
          inputProps={{ maxLength: 50 }}
        />
        <TextField
          type="text"
          label="Khóa học"
          value={courseName}
          onChange={(event) => setCourseName(event.target.value)}
          inputProps={{ maxLength: 50 }}
        />
      </div>
      {isLoading ? (
        <LoadingState />
      ) : (
        <div className="bg-white">
          <div className="mx-auto px-6 py-24 lg:px-8">
            <div className="grid grid-cols-1 gap-x-10 gap-y-10 md:grid-cols-2 lg:grid-cols-3">
              {listSuggestCourse ? (
                listSuggestCourse.map((course) => (
                  <div
                    key={course.courseId}
                    className="group shadow-xl shadow-gray-300 rounded-2xl hover:cursor-pointer"
                    onClick={() => onHandleViewCourseDetail(course.courseId)}
                  >
                    <img
                      src={`${baseUrl}/view/${course.avatar}`}
                      alt={course.courseName}
                      className="md:h-[250px] sm:h-[350px] w-full object-cover object-center rounded-t-2xl ob group-hover:opacity-75"
                    />
                    <div className="p-5">
                      <div className="mt-4 text-2xl text-gray-700 w-full">
                        <div className="text-xl font-medium">
                          {course.courseName}
                        </div>
                      </div>
                      <p className="mt-2 flex">
                        <div className="text-lg">
                          Gia sư:&nbsp;{course.mentorName}
                        </div>
                      </p>
                      <p className="mt-3 flex">
                        <div className="text-lg">
                          {course.price
                            ? course.price.toLocaleString("en-US", {
                                maximumFractionDigits: 0,
                              })
                            : 0}{" "}
                          VNĐ
                        </div>
                      </p>
                    </div>
                  </div>
                ))
              ) : (
                <div className="text-xl text-center mt-3">
                  Hệ thống không tìm thấy khóa học nào.
                </div>
              )}
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export default ListData;
