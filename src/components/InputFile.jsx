const InputFile = (props) => {
  return (
    <>
      <input
        type="file"
        className="border border-black p-3 w-full rounded-xl bg-emerald-50"
        onChange={props.onChange}
      ></input>
    </>
  );
};
export default InputFile;
