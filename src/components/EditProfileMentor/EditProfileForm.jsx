import React from "react";
import { Button } from "@mui/material";

const EditProfileForm = ({ mentor }) => {
  return (
    <div className="container mx-auto py-4">
      <div className="grid grid-cols-1 md:grid-cols-9 gap-6 px-4">
        <div className="col-span-4 md:col-span-3">
          <div className="bg-table shadow rounded-lg p-6">
            <div className="flex flex-col items-center">
              <img
                src={mentor.image}
                className="w-32 h-32 bg-gray-300 rounded-full mb-4 shrink-0"
              ></img>
              <input
                type="text"
                className="text-xl font-bold text-textHeader text-center rounded mb-5 p-2"
                defaultValue={mentor.name}
              ></input>
              <div className="flex">
                VNĐ/h
                <input
                  type="number"
                  className="text-gray-700 border mx-2 rounded"
                  defaultValue={"100"}
                ></input>
              </div>

              <div className="mt-6 flex flex-wrap gap-4 justify-center"></div>
            </div>
            <hr className="my-6 border-t border-gray-300" />
          </div>
        </div>
        <div className="col-span-4 md:col-span-6">
          <div className="bg-table shadow rounded-lg p-6">
            <h2 className="text-xl font-bold mb-4 text-textHeader">
              Giới thiệu về tôi
            </h2>
            <input
              className="text-gray-700 w-full p-2 rounded border-black border"
              defaultValue={mentor.description}
              required
            />

            <h2 className="text-xl font-bold mt-6 mb-4 text-textHeader">
              Chứng chỉ:
            </h2>
            <div className="mb-6">
              <div className="flex justify-between flex-wrap gap-2 w-full">
                <span className="text-gray-700 font-bold">Web Developer</span>
                <p>
                  <span className="text-gray-700 mr-2">at ABC Company</span>
                  <span className="text-gray-700">2017 - 2019</span>
                </p>
              </div>
              <p className="mt-2">Lorem</p>
            </div>
            <div className="mb-6">
              <div className="flex justify-between flex-wrap gap-2 w-full">
                <span className="text-gray-700 font-bold">Web Developer</span>
                <p>
                  <span className="text-gray-700 mr-2">at ABC Company</span>
                  <span className="text-gray-700">2017 - 2019</span>
                </p>
              </div>
              <p className="mt-2">Lorem</p>
            </div>
            <div className="mb-6">
              <div className="flex justify-between flex-wrap gap-2 w-full">
                <span className="text-gray-700 font-bold">Web Developer</span>
                <p>
                  <span className="text-gray-700 mr-2">at ABC Company</span>
                  <span className="text-gray-700">2017 - 2019</span>
                </p>
              </div>
              <p className="mt-2">Lorem</p>
            </div>

            <h2 className="text-xl font-bold mt-6 mb-4 text-textHeader">
              Trình độ học vấn:
              <input
                className="text-lg font-semibold text-gray-800 w-full p-2 rounded border-black border"
                defaultValue={"phD"}
                required
              />
            </h2>

            <h2 className="text-xl font-bold mt-6 mb-5 text-textHeader">
              Kỹ năng ngôn ngữ:
              <input
                className="text-gray-700 w-full p-2 rounded border-black border"
                defaultValue={mentor.skill}
                required
              />
            </h2>
            <Button
              variant="contained"
              sx={{
                backgroundColor: "#5584C6",
                textTransform: "none",
                borderRadius: "12px",
                height: "35px",
                width: "115px",
              }}
              type="submit"
            >
              Lưu
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};
export default EditProfileForm;
