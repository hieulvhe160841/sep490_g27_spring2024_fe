const TextareaInput = (props) => {
  return (
    <>
      <textarea
        id="message"
        rows="10"
        className="block p-2.5 w-full text-sm bg-emerald-50 border border-black rounded-xl"
      ></textarea>
    </>
  );
};
export default TextareaInput;
