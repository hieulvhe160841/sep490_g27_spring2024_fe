import { Button } from "@mui/material";
import React from "react";
import { FaRegUserCircle } from "react-icons/fa";
import { IoKeyOutline } from "react-icons/io5";
import { TfiEmail } from "react-icons/tfi";
import { FaPhone } from "react-icons/fa6";
import { IconContext } from "react-icons";
import userStore from "../../store/userStore";
import { useState } from "react";
import InputComponent from "../../components/InputComponent";
import { Link, useNavigate } from "react-router-dom";
import { CgSpinnerAlt } from "react-icons/cg";
import { PiAddressBook } from "react-icons/pi";
import { CiCalendarDate } from "react-icons/ci";
import { toast } from "react-toastify";
import { IoIosArrowBack } from "react-icons/io";
import { FaEye } from "react-icons/fa";
import { FaEyeSlash } from "react-icons/fa";

function FormRegister() {
  const navigate = useNavigate();
  const [username, setUsername] = useState("");
  const [fullname, setFullname] = useState("");
  const [email, setEmail] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [password, setPassword] = useState("");
  const [address, setAddress] = useState("");
  const [dob, setDob] = useState("");
  const [avatar, setAvatar] = useState("");
  const [gender, setGender] = useState(1);
  const [isAccept, setIsAccept] = useState(false);
  const { registerApi, isLoading } = userStore();
  const [isShowPassword, setIsShowPassword] = useState(false);

  const currentDate = new Date();
  const inputYear = new Date(dob).getFullYear();
  const onHandleValidInput = () => {
    if (!username) {
      toast.warn("Bạn chưa nhập Tài khoản!");
      return false;
    }
    if (!fullname) {
      toast.warn("Bạn chưa nhập Tên!");
      return false;
    }
    if (!email) {
      toast.warn("Bạn chưa nhập Email!");
      return false;
    }
    let regex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    /^[A-Za-z0-9]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/;
    if (regex.test(email) === false) {
      toast.warn("Bạn nhập sai định dạng Email!");
      return false;
    }
    if (!phoneNumber) {
      toast.warn("Bạn chưa nhập Điện thoại!");
      return false;
    }
    if (!password) {
      toast.warn("Bạn chưa nhập Mật khẩu!");
      return false;
    }

    if (!address) {
      toast.warn("Bạn chưa nhập Địa chỉ!");
      return false;
    }
    if (!dob) {
      toast.warn("Bạn chưa nhập Ngày sinh!");
      return false;
    }

    if (inputYear > currentDate.getFullYear() === true) {
      toast.warn("Ngày sinh không thể là tương lai!");
      return false;
    }
    if (!gender) {
      toast.warn("Bạn chưa chọn Giới tính!");
      return false;
    }
    return true;
  };

  const onHandleRegister = async () => {
    if (onHandleValidInput()) {
      const res = await registerApi(
        username,
        password,
        fullname,
        phoneNumber,
        address,
        dob,
        gender,
        "avatar1.jpg",
        email
      );
      if (res && res.status === 201) {
        toast.success(res.data.messages);
        navigate("/emailVerify", { state: { email, auth: true } });
      } else {
        //error
        if (res && res.status !== 201) {
          toast.error(res.data[0]);
          console.log(res.data);
        }
      }
    }
  };
  return (
    <>
      <form className="space-y-2" action="register" method="POST">
        <div>
          <div className="inputForm">
            <IconContext.Provider
              value={{
                className: "ml-2 mt-3 iconForm",
              }}
            >
              <div>
                <FaRegUserCircle />
              </div>
            </IconContext.Provider>

            <InputComponent
              id="username"
              name="username"
              type="username"
              placeholder="Nhập Tài khoản..."
              onChange={(event) => {
                setUsername(event.target.value);
              }}
            />
          </div>
        </div>

        <div>
          <div className="inputForm">
            <IconContext.Provider
              value={{
                className: "ml-2 mt-3 iconForm",
              }}
            >
              <div>
                <FaRegUserCircle />
              </div>
            </IconContext.Provider>
            <InputComponent
              id="fullname"
              name="fullname"
              type="text"
              placeholder="Nhập Tên..."
              onChange={(event) => {
                setFullname(event.target.value);
              }}
            />
          </div>
        </div>
        <div>
          <div className="inputForm">
            <IconContext.Provider
              value={{
                className: "ml-2 mt-3 iconForm",
              }}
            >
              <div>
                <TfiEmail />
              </div>
            </IconContext.Provider>
            <InputComponent
              id="email"
              name="email"
              type="email"
              placeholder="Nhập Email..."
              onChange={(event) => {
                setEmail(event.target.value);
              }}
            />
          </div>
        </div>
        <div>
          <div className="inputForm">
            <IconContext.Provider
              value={{
                className: "ml-2 mt-3 iconForm",
              }}
            >
              <div>
                <FaPhone />
              </div>
            </IconContext.Provider>
            <InputComponent
              id="phonenumber"
              name="phonenumber"
              type="tel"
              placeholder="Nhập Số điện thoại..."
              onChange={(event) => {
                setPhoneNumber(event.target.value);
              }}
            />
          </div>
        </div>
        <div>
          <div className="inputForm">
            <IconContext.Provider
              value={{
                className: "ml-2 mt-3 iconForm",
              }}
            >
              <div>
                <IoKeyOutline />
              </div>
            </IconContext.Provider>

            <InputComponent
              id="password"
              name="password"
              type={`${isShowPassword ? "text" : "password"}`}
              autoComplete="current-password"
              placeholder="Nhập mật khẩu..."
              required
              className="block w-full rounded-xl border-0 py-4 pl-14 bg-table font-normal shadow-sm relative"
              onChange={(event) => {
                setPassword(event.target.value);
              }}
            />
            <div
              className="absolute right-3 top-5"
              onClick={() => setIsShowPassword(!isShowPassword)}
            >
              <FaEye className={`${isShowPassword ? "hidden" : ""}`}></FaEye>
              <FaEyeSlash
                className={`${isShowPassword ? "" : "hidden"}`}
              ></FaEyeSlash>
            </div>
          </div>
        </div>
        <div>
          <div className="inputForm">
            <IconContext.Provider
              value={{
                className: "ml-2 mt-3 iconForm",
              }}
            >
              <div>
                <PiAddressBook />
              </div>
            </IconContext.Provider>
            <InputComponent
              id="address"
              name="address"
              type="text"
              placeholder="Nhập Địa chỉ..."
              onChange={(event) => {
                setAddress(event.target.value);
              }}
            />
          </div>
        </div>
        <div>
          <div className="inputForm">
            <IconContext.Provider
              value={{
                className: "ml-2 mt-3 iconForm",
              }}
            >
              <div>
                <CiCalendarDate />
              </div>
            </IconContext.Provider>
            <input
              id="dob"
              name="dob"
              type="date"
              aria-label="dob"
              className="block w-full rounded-xl border-0 py-4 pr-5 pl-14 bg-gray-300 font-normal shadow-sm"
              onChange={(event) => {
                setDob(event.target.value);
              }}
              max="9999-12-31"
              required
            />
          </div>
        </div>
        <div>
          <div className="inputForm flex gap-x-10">
            <p>
              Giới tính:
              <label>
                <input
                  type="radio"
                  name="gender"
                  value="1"
                  className="m-2"
                  onChange={(event) => {
                    setGender(event.target.value);
                  }}
                  checked
                />
                Nam
              </label>
              <label>
                <input
                  type="radio"
                  name="gender"
                  value="0"
                  className="m-2"
                  onChange={(event) => {
                    setGender(event.target.value);
                  }}
                />
                Nữ
              </label>
            </p>
          </div>
        </div>
        <div>
          <input
            type="checkbox"
            className="mr-1 mb-0"
            onChange={(event) => {
              setIsAccept(event.target.checked);
            }}
          />
          <b className="font-medium">Đồng ý với</b> Chính sách
        </div>

        <div>
          <Button
            type="submit"
            variant="contained"
            size="large"
            disabled={isLoading || isAccept === false ? true : false}
            className="mx-auto w-full"
            sx={{
              backgroundColor: "#0B4B88",
              color: "white",
            }}
            onClick={() => onHandleRegister()}
          >
            Đăng ký&nbsp;
            {isLoading && (
              <svg
                className="animate-spin h-5 w-5 mr-5 text-white ..."
                viewBox="0 0 17 17"
              >
                <CgSpinnerAlt />
              </svg>
            )}
          </Button>
        </div>
      </form>
      <div className="flex flex-col justify-center items-center mt-5">
        <div>
          <Link
            to="/login"
            className={`underline ${
              isLoading ? "pointer-events-none text-gray-400" : ""
            }`}
          >
            <IoIosArrowBack className="inline" />
            Quay lại Đăng nhập
          </Link>
        </div>
      </div>
    </>
  );
}

export default FormRegister;
