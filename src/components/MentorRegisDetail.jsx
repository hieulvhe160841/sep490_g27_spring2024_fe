import React, { useState } from "react";
import Button from "@mui/material/Button";
import userStore from "../store/userStore";
import { toast } from "react-toastify";
import { Link, useNavigate } from "react-router-dom";
import ModalViewImg from "./Modal/ModalViewImg";
import LoadingState from "./LoadingState";

const MentorRegisDetail = ({ mentorRegis, username }) => {
  const { approveMentorRegis, baseUrl } = userStore();
  const [feedback, setFeedback] = useState("");
  const navigate = useNavigate();
  const eduStaffRole = "ROLE_EDUCATION_STAFF";
  const adminRole = "ROLE_ADMIN";
  const role = localStorage.getItem("role");

  const handleContactClick = () => {
    if (role === eduStaffRole) {
      navigate("/staffEducation/chat", { state: { username } });
    } else if (role === adminRole) {
      navigate("/admin/chat", { state: { username } });
    }
  };

  const handleApprove = async () => {
    try {
      await approveMentorRegis(
        mentorRegis.mentorRequestDetail.mentorRequestId,
        feedback,
        "accepted"
      );
      toast.success("Chấp thuận đăng ký thành công");
    } catch (error) {
      console.error("Error approving Mentor Registration:", error);
      toast.error("Lỗi chấp thuận đăng ký thành công:", error);
    }
    navigate(-1);
  };

  const handleReject = async () => {
    try {
      await approveMentorRegis(
        mentorRegis.mentorRequestDetail.mentorRequestId,
        feedback,
        "rejected"
      );
      toast.success("Từ chối đăng ký thành công");
    } catch (error) {
      console.error(mentorRegis.mentorRequestDetail.mentorRequestId);
      toast.error("Lỗi từ chối đăng ký thành công:", error);
    }
    navigate(-1);
  };

  if (!mentorRegis || !mentorRegis.mentorRequestDetail) {
    return (
      <div>
        <LoadingState />
      </div>
    );
  }

  const statusToVietnamese = (status) => {
    switch (status) {
      case "ACCEPTED":
        return "Đã duyệt";
      case "REJECTED":
        return "Đã từ chối";
      case "PENDING":
        return "Chờ phê duyệt";
      default:
        return status;
    }
  };

  return (
    <div className="container mx-auto py-4">
      <div className="grid grid-cols-1 lg:grid-cols-9 gap-3 px-4">
        <div className="col-span-4 lg:col-span-3">
          <div className="bg-gray-200 shadow rounded-lg p-6">
            <div className="flex flex-col items-center">
              <img
                src={`${baseUrl}/view/${mentorRegis.mentorRequestDetail.userCreatedMentorRequestDTO.avatar}`}
                className="w-32 h-32 bg-gray-300 rounded-full mb-4 shrink-0"
              ></img>
              <h1 className="text-xl font-bold text-textHeader">
                {
                  mentorRegis.mentorRequestDetail.userCreatedMentorRequestDTO
                    .userName
                }
              </h1>
              <p className="text-textHeader">
                {mentorRegis.mentorRequestDetail.userCreatedMentorRequestDTO
                  .costPerHour ? (
                  mentorRegis.mentorRequestDetail.userCreatedMentorRequestDTO.costPerHour.toLocaleString(
                    "en-US",
                    {
                      maximumFractionDigits: 0,
                    }
                  )
                ) : (
                  <div className="text-center">0</div>
                )}
                VNĐ/h
              </p>
              <div className="mt-6 flex flex-wrap gap-4 justify-center">
                <Button variant="contained" onClick={handleContactClick}>
                  Liên hệ
                </Button>
              </div>
            </div>
            <hr className="my-6 border-t border-gray-300" />
          </div>
          {mentorRegis.mentorRequestDetail.mentorRequestStatus === "WAITING" ? (
            <div className="flex flex-col  items-center space-y-4 mt-4">
              <div className="flex space-x-3 items-center">
                <Button
                  variant="contained"
                  color="info"
                  sx={{
                    backgroundColor: "#5584C6",
                    color: "#ffffff",
                    width: "175px",
                    height: "50px",
                    fontSize: "20px",
                    borderRadius: "10px",
                    fontWeight: "600",
                  }}
                  onClick={handleApprove}
                >
                  Chấp thuận
                </Button>
                <Button
                  variant="contained"
                  color="error"
                  sx={{
                    backgroundColor: "#FF0A0A",
                    color: "#ffffff",
                    width: "175px",
                    height: "50px",
                    fontSize: "20px",
                    borderRadius: "10px",
                    fontWeight: "600",
                  }}
                  onClick={handleReject}
                >
                  Từ chối
                </Button>
              </div>
              <input
                className="inputField"
                type="text"
                placeholder="Feedback"
                value={feedback}
                onChange={(e) => setFeedback(e.target.value)}
              />
            </div>
          ) : (
            <div className="font-semibold text-xl flex justify-center mt-4">
              Trạng thái:{" "}
              {statusToVietnamese(
                mentorRegis.mentorRequestDetail.mentorRequestStatus
              )}
            </div>
          )}
        </div>
        <div className="col-span-4 lg:col-span-6">
          <div className="bg-gray-200 shadow rounded-lg p-6">
            <h2 className="text-xl font-bold mb-4 text-textHeader">
              Chứng chỉ:
            </h2>
            {mentorRegis.userCertificateDetailDTOList.map((cert) => {
              return (
                <div className="mb-6" key={cert.certificateName}>
                  <div className="flex justify-between flex-wrap gap-2 w-full">
                    <div className="text-gray-700 font-bold items-start">
                      {cert.certificateName}
                    </div>
                  </div>
                  <div className=" text-blue-600 items-start">
                    <ModalViewImg cert={cert} />
                  </div>
                </div>
              );
            })}

            <h2 className="text-xl font-bold mt-6 mb-4 text-textHeader">
              Trình độ học vấn:{" "}
              <span className="text-lg font-semibold text-gray-700">
                {
                  mentorRegis.mentorRequestDetail.userCreatedMentorRequestDTO
                    .educationLevel
                }
              </span>
            </h2>

            <h2 className="text-xl font-bold mt-6 mb-4 text-textHeader">
              Kinh nghiệm:{" "}
              <span className="text-lg font-semibold text-gray-700">
                {
                  mentorRegis.mentorRequestDetail.userCreatedMentorRequestDTO
                    .experience
                }
              </span>
            </h2>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MentorRegisDetail;
