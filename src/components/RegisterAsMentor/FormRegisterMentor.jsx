import React, { useEffect } from "react";
import { Link, Navigate, useNavigate } from "react-router-dom";
import { IoIosArrowBack } from "react-icons/io";
import { useState } from "react";
import {
  Checkbox,
  FormControlLabel,
  FormGroup,
  InputLabel,
} from "@mui/material";
import { toast } from "react-toastify";

import ButtonComponent from "../ButtonComponent";
import menteeStore from "../../store/menteeStore";
import userStore from "./../../store/userStore";
import Multiselect from "multiselect-react-dropdown";
import { CgSpinnerAlt } from "react-icons/cg";

function FormRegisterMentor() {
  const navigate = useNavigate();
  const { submitSkillForRegisterMentor, isLoading } = menteeStore();
  const { listAllAvailableSKill, listSkills } = userStore();
  const username = localStorage.getItem("user");
  const [skill, setSkill] = useState();
  const [skillAdd, setSkillAdd] = useState();
  const [eduLevel, setEducationLevel] = useState();
  const [experience, setExperience] = useState();
  const [cost, setCost] = useState();
  const getSKill = [];
  useEffect(() => {
    listAllAvailableSKill();
    if (listSkills) {
      for (let i = 0; i < listSkills.length; i++) {
        getSKill.push(listSkills[i].skillName);
      }
      setSkill(getSKill);
    }
  }, [listSkills]);

  const onHandleValidInput = () => {
    if (!skillAdd) {
      toast.warn("Bạn phải điền kỹ năng");
      return false;
    }
    if (!eduLevel) {
      toast.warn("Bạn phải điền trình độ học vấn");
      return false;
    }
    if (!experience) {
      toast.warn("Bạn phải điền kinh nghiệm");
      return false;
    }
    if (!cost || cost < 0) {
      toast.warn("Giá tiền không hợp lệ, giá tiền phải lớn hơn 0");
      return false;
    }
    return true;
  };

  const formatNumberWithCommas = (number) => {
    if (number !== undefined && number !== null) {
      const integerPart = Math.trunc(number);
      return integerPart.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    } else {
      return "";
    }
  };

  const handleInputChange = (e) => {
    const { value } = e.target;
    const numericValue = value.replace(/,/g, "");
    if (numericValue === "" || parseInt(numericValue) >= 0) {
      setCost(numericValue);
    }
  };

  const onHandleSubmit = async () => {
    if (onHandleValidInput()) {
      const res = await submitSkillForRegisterMentor(
        username,
        experience,
        eduLevel,
        skillAdd,
        cost
      );
      console.log(res);
      if (res.statusCode === 201) {
        toast.success("Vui lòng thêm chứng chỉ");
        console.log(res);
        navigate("/mentee/addCertificate");
      } else {
        toast.error(res.messages);
      }
    }
  };
  return (
    <>
      <div className="md:mx-auto md:w-full md:max-w-sm">
        <div className="space-y-6" action="registerMentor" method="POST">
          <div className="academic-level">
            <div className="my-3 font-bold text-lg text-sidebarTop">
              Trình độ học vấn
            </div>
            <input
              className="block p-2.5 w-full text-sm text-gray-900 rounded-lg border border-gray-400 shadow-lg"
              placeholder="Thêm trình độ học vấn..."
              onChange={(event) => setEducationLevel(event.target.value)}
              maxLength={255}
              required
            ></input>
          </div>

          <div>
            <div className="skill inputForm">
              <div className="my-3 font-bold text-lg text-sidebarTop">
                Kỹ năng
              </div>

              <Multiselect
                className="block text-sm text-gray-900 rounded-md border border-gray-300 shadow-lg"
                placeholder="Chọn kỹ năng"
                options={skill}
                isObject={false}
                onRemove={(e) => setSkillAdd(e)}
                onSelect={(e) => setSkillAdd(e)}
              ></Multiselect>
            </div>

            <div className="inputForm">
              <div className="my-3 font-bold text-lg text-sidebarTop">
                Kinh nghiệm
              </div>
              <textarea
                rows="8"
                className="block p-2.5 w-full text-sm text-gray-900 rounded-lg border border-gray-400 shadow-lg"
                placeholder="Mô tả kinh nghiệm bản thân..."
                onChange={(event) => setExperience(event.target.value)}
                maxLength={255}
                required
              ></textarea>
            </div>
            <div className="inputForm">
              <div className="my-3 font-bold text-lg text-sidebarTop ">
                Giá theo giờ (VNĐ/h)
              </div>
              <input
                className="block p-2.5 w-full text-sm text-gray-900 rounded-lg border border-gray-400 shadow-lg"
                placeholder="Thêm giá tiền..."
                type="text"
                value={formatNumberWithCommas(cost)}
                onChange={(e) => handleInputChange(e)}
                required
              ></input>
            </div>
          </div>

          <div>
            <ButtonComponent
              onClick={() => {
                onHandleSubmit();
              }}
              disabled={isLoading ? true : false}
              out="Gửi thông tin"
            >
              &nbsp;
              {isLoading && (
                <svg
                  className="animate-spin h-5 w-5 mr-5 text-white ..."
                  viewBox="0 0 17 17"
                >
                  <CgSpinnerAlt />
                </svg>
              )}
            </ButtonComponent>
          </div>
        </div>
      </div>
    </>
  );
}

export default FormRegisterMentor;
