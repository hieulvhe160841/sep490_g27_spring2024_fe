import React, { useEffect, useState } from "react";
import { Button } from "@mui/material";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
import { MdDriveFolderUpload } from "react-icons/md";
import { CgSpinnerAlt } from "react-icons/cg";
import Multiselect from "multiselect-react-dropdown";
import userStore from "../../store/userStore";
import menteeStore from "../../store/menteeStore";

function SubmitCertificateForm() {
  const { listAllCert, listCerts } = userStore();
  const { submitCertForRegisterMentor, isLoading } = menteeStore();
  const [certificates, setCertificates] = useState([]);
  const [certAdd, setCertAdd] = useState([]);
  const [files, setFiles] = useState({});
  const navigate = useNavigate();

  useEffect(() => {
    listAllCert();
    if (listCerts) {
      const certNames = listCerts.map((cert) => cert.certificateName);
      setCertificates(certNames);
    }
  }, [listCerts]);

  const onHandleInputFile = (e, certName) => {
    const file = e.target.files[0];
    if (file) {
      setFiles((prevFiles) => ({ ...prevFiles, [certName]: file }));
    }
  };

  const onSubmitCertificate = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append("cert", certAdd);
    certAdd.forEach((certName) => {
      if (files[certName]) {
        formData.append("file", files[certName]);
      }
    });
    const res = await submitCertForRegisterMentor(formData);
    if (res.status === 201) {
      toast.success(
        "Gửi thông tin thành công, vui lòng chờ để được liên hệ phỏng vấn"
      );
      navigate("/mentee");
    } else {
      toast.error("Gửi thông tin thất bại, vui lòng điền đầy đủ thông tin");
    }
  };

  return (
    <>
      <div className="flex justify-center mx-auto italic w-full">
        Sau khi nộp chứng chỉ, vui lòng chờ để được liên hệ phỏng vấn
      </div>
      <form
        className="space-y-6 max-w-md mx-auto mt-5"
        onSubmit={(e) => onSubmitCertificate(e)}
      >
        <Multiselect
          className="block text-lg text-gray-900 rounded-md border border-blue-400 shadow-xl"
          placeholder="Chọn loại chứng chỉ"
          options={certificates}
          isObject={false}
          onSelect={(selectedList) => setCertAdd(selectedList)}
          onRemove={(selectedList) => setCertAdd(selectedList)}
        />
        {certAdd.map((certName, index) => (
          <div key={index} className="flex items-center justify-between my-2 ">
            <div className="flex justify-between w-1/3">
              <label className="text-lg font-medium text-gray-900">
                {certName}
              </label>
              <input
                type="file"
                id={certName}
                hidden
                onChange={(e) => onHandleInputFile(e, certName)}
              />
              <label
                htmlFor={certName}
                className="bg-sidebarTop text-white font-bold py-2 px-4 rounded cursor-pointer"
              >
                <MdDriveFolderUpload size="25px" />
              </label>
            </div>
            {files[certName] && (
              <span className="ml-2">{files[certName].name}</span>
            )}
          </div>
        ))}
        <div className="flex justify-center">
          <Button variant="contained" type="submit" disabled={isLoading}>
            Gửi{" "}
            {isLoading && (
              <CgSpinnerAlt className="animate-spin h-5 w-5 mr-3" />
            )}
          </Button>
        </div>
      </form>
    </>
  );
}

export default SubmitCertificateForm;
