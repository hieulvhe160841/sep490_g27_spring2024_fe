import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { Button, Pagination } from "@mui/material";
import menteeStore from "../../store/menteeStore";
import { toast } from "react-toastify";
import userStore from "../../store/userStore";

function ListRegistRequest({ request }) {
  const { updateMenteeToMentor } = menteeStore();
  const { logout } = userStore();
  const navigate = useNavigate();

  const [currentPage, setCurrentPage] = useState(1);
  const pageSize = 4;

  const handlePageChange = (event, page) => {
    setCurrentPage(page);
  };

  const startIndex = (currentPage - 1) * pageSize;
  const paginatedRequest = request
    ? request.slice(startIndex, startIndex + pageSize)
    : [];

  //   const handleClickDetail = (mentorUsername) => {
  //     navigate(`/mentorDetails/${mentorUsername}`);
  //   };

  const onHandleConfirmToBeMentor = async () => {
    await updateMenteeToMentor();
    toast.success("Bạn đã trở thành gia sư, vui lòng đăng nhập lại");
    navigate("/login");
    logout();
  };
  const statusToVietnamese = (status) => {
    switch (status) {
      case "ACCEPTED":
        return "Đã duyệt";
      case "REJECTED":
        return "Đã từ chối";
      case "WAITING":
        return "Chờ phê duyệt";
      default:
        return status;
    }
  };
  const listAcceptRequest =
    Array.isArray(request) &&
    request.some((req) => req.mentorRequestStatus === "ACCEPTED");

  return (
    <>
      {listAcceptRequest ? (
        <div>
          <div className="flex justify-center text-lg font-semibold mt-10">
            Đăng ký làm gia sư thành công, nhấn "Đồng ý" để trở thành gia sư
            &nbsp;
            <div className="italic">(bạn sẽ phải đăng nhập lại)</div>
          </div>
          <div className="flex justify-center mt-10 ">
            <Button
              variant="contained"
              sx={{
                padding: "10px 12px",
                borderRadius: "12px",
                color: "white",
                height: "45px",
                width: "140px",
              }}
              type="button"
              onClick={() => onHandleConfirmToBeMentor()}
            >
              ĐỒNG Ý
            </Button>
          </div>
        </div>
      ) : (
        <div className="m-5 p-5">
          <div className="flex md:justify-center justify-center">
            <Link to="/mentee/registerMentor">
              <Button
                variant="outlined"
                sx={{
                  border: "2px solid #4B5563",
                  padding: "8px 12px",
                  borderRadius: "12px",
                  boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.1)",
                  margin: "5px 0px 0px 0px",
                  "&:hover": {
                    boxShadow: "none",
                  },
                  color: "#000000",
                  textTransform: "none",
                  height: "35px",
                  width: "115px",
                }}
                type="button"
              >
                Đăng ký
              </Button>
            </Link>
          </div>
          <ul role="list" className="divide-y divide-gray-500">
            {Array.isArray(paginatedRequest) &&
              paginatedRequest.map((request) => (
                <li key={request.mentorRequestId}>
                  <div className="flex flex-col lg:flex-row items-center justify-between my-5">
                    <div className="flex items-center space-x-4 ml-1">
                      <div className="min-w-0 flex-auto">
                        <p className="text-lg font-semibold leading-6 text-sidebarTop">
                          Yêu cầu: {request.mentorRequestId}
                        </p>
                        <p className="text-lg font-semibold leading-6 text-sidebarTop">
                          Trạng thái:{" "}
                          {statusToVietnamese(request.mentorRequestStatus)}
                        </p>
                      </div>
                    </div>
                    <div className="shrink-0 flex flex-row lg:items-center space-x-10">
                      <Link to={`registDetail/${request.mentorRequestId}`}>
                        <Button
                          variant="contained"
                          size="large"
                          sx={"border-radius: 15px"}
                        >
                          Chi tiết
                        </Button>
                      </Link>
                    </div>
                  </div>
                </li>
              ))}
          </ul>

          <Pagination
            count={Math.ceil(request.length / pageSize)}
            page={currentPage}
            onChange={handlePageChange}
            color="primary"
            className="flex flex-col items-center"
          />
        </div>
      )}
    </>
  );
}

export default ListRegistRequest;
