import React from "react";
import { Link, Navigate, useNavigate } from "react-router-dom";
import { FaRegUserCircle } from "react-icons/fa";
import { IconContext } from "react-icons";
import { useState } from "react";
import InputComponent from "../../InputComponent";
import { Button } from "@mui/material";
import userStore from "../../../store/userStore";
import { toast } from "react-toastify";
import { CgSpinnerAlt } from "react-icons/cg";
import { IoIosArrowBack } from "react-icons/io";

function FormForgotPassword() {
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const { forgotPasswordApi, isLoading } = userStore();
  const onHandleForgotPassword = async () => {
    const res = await forgotPasswordApi(email);
    if (res && res.status === 200) {
      toast.success(res.data.messages);

      navigate("/emailVerify", { state: { email, auth: false } });
    } else {
      //error
      if (res && res.status !== 200) {
        toast.error(res.data.messages);
      }
    }
  };
  return (
    <>
      <div className="space-y-6" action="resetPassword" method="POST">
        <div>
          <div className="inputForm">
            <IconContext.Provider
              value={{
                className: "ml-2 mt-3 iconForm",
              }}
            >
              <div>
                <FaRegUserCircle />
              </div>
            </IconContext.Provider>
            <InputComponent
              id="email"
              name="email"
              type="email"
              placeholder="Nhập email..."
              onChange={(event) => {
                setEmail(event.target.value);
              }}
            />
          </div>
        </div>

        <div>
          <Button
            type="submit"
            variant="contained"
            size="large"
            className="mx-auto w-full"
            sx={{
              backgroundColor: "#0B4B88",
              color: "white",
            }}
            disabled={isLoading ? true : false}
            onClick={() => {
              onHandleForgotPassword();
            }}
          >
            Gửi mã&nbsp;
            {isLoading && (
              <svg
                className="animate-spin h-5 w-5 mr-5 text-white ..."
                viewBox="0 0 17 17"
              >
                <CgSpinnerAlt />
              </svg>
            )}
          </Button>
        </div>
      </div>
      <div className="flex flex-col justify-center items-center mt-5">
        <div>
          <Link
            to="/login"
            className={`underline ${
              isLoading ? "pointer-events-none text-gray-400" : ""
            }`}
          >
            <IoIosArrowBack className="inline" />
            Quay lại trang Đăng nhập
          </Link>
        </div>
      </div>
    </>
  );
}

export default FormForgotPassword;
