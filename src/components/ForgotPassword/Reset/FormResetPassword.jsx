import { Button } from "@mui/material";
import React, { useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";

import { IoKeyOutline } from "react-icons/io5";
import { BiSolidLockAlt } from "react-icons/bi";
import { IconContext } from "react-icons";
import InputComponent from "../../InputComponent";
import userStore from "../../../store/userStore";
import { toast } from "react-toastify";
import { CgSpinnerAlt } from "react-icons/cg";
import { IoIosArrowBack } from "react-icons/io";
import { FaEye } from "react-icons/fa";
import { FaEyeSlash } from "react-icons/fa";

function FormResetPassword() {
  const navigate = useNavigate();
  const location = useLocation();
  const [newpassword, setNewPassword] = useState("");
  const [repassword, setRePassword] = useState("");
  const { resetPasswordAPi, isLoading } = userStore();
  const [isShowPassword, setIsShowPassword] = useState(false);
  const [isShowRePassword, setIsShowRePassword] = useState(false);

  const onHandleValidPassword = () => {
    if (newpassword !== repassword) {
      return false;
    }
    return true;
  };
  const onHandleResetPassword = async () => {
    if (onHandleValidPassword()) {
      const res = await resetPasswordAPi(location.state.email, newpassword);
      if (res && res.status === 200) {
        toast.success(res.data.messages);
        navigate("/login");
      } else {
        //error
        if (res && res.status !== 200) {
          toast.error(res.data.messages);
        }
      }
    } else {
      toast.warn("Xác nhận mật khẩu không khớp, vui lòng nhập lại");
    }
  };
  return (
    <>
      <div className="space-y-6" action="resetPassword" method="POST">
        <div>
          <div className="inputForm">
            <IconContext.Provider
              value={{
                className: "ml-2 mt-3 iconForm",
              }}
            >
              <div>
                <IoKeyOutline />
              </div>
            </IconContext.Provider>
            <InputComponent
              id="newpassword"
              name="newPassword"
              type={`${isShowPassword ? "text" : "password"}`}
              placeholder="Nhập mật khẩu mới..."
              className="block w-full rounded-xl border-0 py-4 pl-14 bg-table font-normal shadow-sm relative"
              onChange={(event) => {
                setNewPassword(event.target.value);
              }}
            />
            <div
              className="absolute right-3 top-5"
              onClick={() => setIsShowPassword(!isShowPassword)}
            >
              <FaEye className={`${isShowPassword ? "hidden" : ""}`}></FaEye>
              <FaEyeSlash
                className={`${isShowPassword ? "" : "hidden"}`}
              ></FaEyeSlash>
            </div>
          </div>
        </div>

        <div>
          <div className="inputForm">
            <IconContext.Provider
              value={{
                className: "ml-2 mt-3 iconForm",
              }}
            >
              <div>
                <BiSolidLockAlt />
              </div>
            </IconContext.Provider>
            <InputComponent
              id="repassword"
              name="repassword"
              type={`${isShowRePassword ? "text" : "password"}`}
              autoComplete="current-password"
              placeholder="Xác nhận mật khẩu mới..."
              className="block w-full rounded-xl border-0 py-4 pl-14 bg-table font-normal shadow-sm relative"
              onChange={(event) => {
                setRePassword(event.target.value);
              }}
            />
            <div
              className="absolute right-3 top-5"
              onClick={() => setIsShowRePassword(!isShowRePassword)}
            >
              <FaEye className={`${isShowRePassword ? "hidden" : ""}`}></FaEye>
              <FaEyeSlash
                className={`${isShowRePassword ? "" : "hidden"}`}
              ></FaEyeSlash>
            </div>
          </div>
        </div>

        <div>
          <Button
            type="submit"
            variant="contained"
            size="large"
            className="mx-auto w-full"
            sx={{
              backgroundColor: "#0B4B88",
              color: "white",
            }}
            disabled={isLoading ? true : false}
            onClick={() => {
              onHandleResetPassword();
            }}
          >
            Cài lại&nbsp;
            {isLoading && (
              <svg
                className="animate-spin h-5 w-5 mr-5 text-white ..."
                viewBox="0 0 17 17"
              >
                <CgSpinnerAlt />
              </svg>
            )}
          </Button>
        </div>
      </div>
      <div className="flex flex-col justify-center items-center mt-5">
        <div>
          <Link
            to="/login"
            className={`underline ${
              isLoading ? "pointer-events-none text-gray-400" : ""
            }`}
          >
            <IoIosArrowBack className="inline" />
            Quay lại trang Đăng nhập
          </Link>
        </div>
      </div>
    </>
  );
}

export default FormResetPassword;
