import { NavLink } from "react-router-dom";

const NavPage = () => {
  return (
    <div className="nav-page flex-grow flex md:items-center md:w-auto md:px-5 px-2">
      <div className="text-md">
        <NavLink
          to="/topRateMentor"
          className="block mt-4 font-medium text-lg md:inline-block md:mt-0 hover:text-sidebarTop px-4 py-2 rounded mr-2 lg:mr-6"
        >
          Gia sư
        </NavLink>
        <NavLink
          to="/suggestCourse"
          className="block mt-4 font-medium text-lg md:inline-block md:mt-0 hover:text-sidebarTop px-4 py-2 rounded mr-2 lg:mr-6"
        >
          Khóa học
        </NavLink>
      </div>
    </div>
  );
};
export default NavPage;
