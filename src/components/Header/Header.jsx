import logo1 from "../../assets/Logo.png";
import UserStore from "../../store/userStore";
import NavPage from "./NavPage";
import NavWithoutLogin from "./NavWithoutLogin";
import OMS from "../../assets/OMS.png";
import NavWithLogin from "./NavWithLogin";
import { useState } from "react";
import { NavLink } from "react-router-dom";

const Header = () => {
  const [menuOpen, setMenuOpen] = useState(false);
  // const { user } = UserStore();
  const user = localStorage.getItem("user");
  const role = localStorage.getItem("role");

  let dynamicPath = "/";
  if (role === "ROLE_ADMIN") dynamicPath = "/admin";
  else if (role === "ROLE_MENTOR") dynamicPath = "/mentor";
  else if (role === "ROLE_MENTEE") dynamicPath = "/mentee";
  else if (role === "ROLE_EDUCATION_STAFF") dynamicPath = "/staffEducation";
  else if (role === "ROLE_TRANSACTION_STAFF") dynamicPath = "/staffTransaction";

  return (
    <>
      <div className="sticky top-0 z-50 flex items-center lg:flex-nowrap flex-wrap bg-footer pl-0 lg:pr-5 w-screen">
        <div className="flex lg:w-auto w-full justify-between lg:border-b-0 pr-2 border-solid border-b-2 border-gray-300 pb-5 lg:pb-0">
          <NavLink
            to={dynamicPath}
            className="flex items-center flex-shrink-0 rounded-md p-2 mx-2 gap-x-6"
          >
            <img src={logo1} alt="logo" className="w-[70px] h-[70px]" />
            <img src={OMS} alt="oms" className="w-[70px] h-[70px]" />
          </NavLink>

          <div className="block lg:hidden">
            <button
              id="nav"
              className="flex items-center px-4 py-4"
              onClick={() => setMenuOpen(!menuOpen)}
            >
              <svg
                className="fill-current h-3 w-3"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <title>Menu</title>
                <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
              </svg>
            </button>
          </div>
        </div>

        <div
          className={`lg:flex w-full menu justify-between lg:px-3 px-8 ${
            menuOpen ? "block" : "hidden"
          }`}
        >
          <NavPage />

          {!user ? <NavWithoutLogin /> : <NavWithLogin />}
        </div>
      </div>
    </>
  );
};
export default Header;
