import { IoChatboxEllipsesOutline, IoKeyOutline } from "react-icons/io5";
import { HiUsers } from "react-icons/hi2";
import { IoMdNotificationsOutline } from "react-icons/io";
import { FaUserCircle } from "react-icons/fa";
import { MdOutlineVpnKey } from "react-icons/md";
import { MdLogout } from "react-icons/md";
import { IoWalletOutline } from "react-icons/io5";
import { Link, NavLink, useLocation, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import { Button } from "@mui/material";
import { toast } from "react-toastify";
import userStore from "../../store/userStore";
import menteeStore from "./../../store/menteeStore";
import { MdAttachMoney } from "react-icons/md";
import { FaCirclePlus } from "react-icons/fa6";

const NavWithLogin = () => {
  const location = useLocation();
  const username = localStorage.getItem("user");
  const [popUp, setPopUp] = useState(false);
  const [popWallet, setPopWallet] = useState(false);
  const { logout, baseUrl, fetchUserWallet, userBalance } = userStore();
  const { fetchMenteeDetail, isLoading } = menteeStore();
  const [userProfile, setUserProfile] = useState();
  const role = localStorage.getItem("role");
  const menteeRole = "ROLE_MENTEE";
  const mentorRole = "ROLE_MENTOR";
  const eduStaffRole = "ROLE_EDUCATION_STAFF";
  const transStaffRole = "ROLE_TRANSACTION_STAFF";
  const adminRole = "ROLE_ADMIN";

  useEffect(() => {
    const userDetail = async () => {
      const user = await fetchMenteeDetail(username);
      await fetchUserWallet();
      if (user) {
        setUserProfile(user.avatar);
      }
      console.log(user);
    };
    userDetail();
  }, [fetchUserWallet]);

  const onHandleLogout = () => {
    logout();
    if (location.pathname === "/") {
      window.location.reload();
    }
  };

  const getChatLink = () => {
    if (role === menteeRole) {
      return "/mentee/chat";
    } else if (role === mentorRole) {
      return "/mentor/chat";
    } else if (role === transStaffRole) {
      return "/staffTransaction/chat";
    } else if (role === eduStaffRole) {
      return "/staffEducation/chat";
    } else if (role === adminRole) {
      return "/admin/chat";
    }
  };

  const viewUserProfile = () => {
    if (role === menteeRole) {
      return "/mentee/profile";
    } else if (role === mentorRole) {
      return "/mentor/profile";
    } else if (role === eduStaffRole) {
      return "/staffTransaction/profile";
    } else if (role === transStaffRole) {
      return "/staffTransaction/profile";
    } else if (role === adminRole) {
      return "/admin/profile";
    }
  };
  const onMentorshipRequest = () => {
    if (role === menteeRole) {
      return "/mentee/mentorshipRequestOfMentee";
    } else if (role === mentorRole) {
      return "/mentor/mentorshipRequest";
    } else if (role === eduStaffRole) {
      return "/staffEducation";
    } else if (role === transStaffRole) {
      return "/staffTransaction";
    } else if (role === adminRole) {
      return "/admin";
    }
  };
  const balanceDisplay =
    userBalance && userBalance.data && userBalance.data.balance
      ? userBalance.data.balance.toLocaleString("en-US", {
          maximumFractionDigits: 0,
        })
      : 0;
  return (
    <div className="flex gap-2 justify-between">
      <div className="flex lg:gap-7 gap-3 relative">
        {role === menteeRole || role === mentorRole ? (
          <div
            className="block text-md text-3xl text-black my-auto md:mt-3 hover:cursor-pointer"
            onClick={() => setPopWallet(!popWallet)}
          >
            <IoWalletOutline />
          </div>
        ) : null}

        {popWallet && (
          <div className="rounded-xl border-gray-400 right-32 bg-sidebarTop absolute md:top-10 top-16 w-[250px] p-3">
            <div className="text-xl text-white px-2 mt-2 ml-2">Số dư</div>
            <div className="text-lg text-white py-7 px-2 flex items-center justify-between">
              <div className="bg-white text-black px-3 ml-2 py-1 rounded-md">
                {balanceDisplay} &nbsp;VNĐ{" "}
              </div>
              <div className="text-2xl">
                {role === mentorRole && (
                  <NavLink
                    to={`/mentor/wallet`}
                    className="text-3xl text-white"
                    onClick={() => setPopWallet(!popWallet)}
                  >
                    <FaCirclePlus />
                  </NavLink>
                )}
                {role === menteeRole && (
                  <NavLink
                    to={`/mentee/choosePrice`}
                    className="text-3xl text-white"
                    onClick={() => setPopWallet(!popWallet)}
                  >
                    <FaCirclePlus />
                  </NavLink>
                )}
              </div>
            </div>
          </div>
        )}

        <NavLink
          to={getChatLink()}
          className="block text-md text-3xl text-black my-auto lg:mt-3"
        >
          <IoChatboxEllipsesOutline />
        </NavLink>

        <NavLink
          to={onMentorshipRequest()}
          className="block text-md text-3xl text-black my-3 lg:mt-3"
        >
          <HiUsers />
        </NavLink>
      </div>

      <div className="relative flex justify-self-end">
        <div className="flex items-center font-semibold ml-10">{username}</div>
        <div className="block text-md px-1 py-1 text-3xl md:ml-5 rounded-full text-white shadow-md bg-sidebarTop my-auto lg:mt-1 cursor-pointer">
          {isLoading ? (
            <FaUserCircle onClick={() => setPopUp(!popUp)} />
          ) : (
            <img
              src={`${baseUrl}/view/${userProfile}`}
              className="w-10 h-10 rounded-full shrink-0"
              onClick={() => setPopUp(!popUp)}
            ></img>
          )}

          {popUp && (
            <div className="rounded-xl border-gray-400 right-4 bg-sidebarTop absolute md:top-12 top-16 w-[210px] p-3 divide-y-2 space-y-2">
              {role !== adminRole ? (
                <Link
                  to={viewUserProfile()}
                  className="flex text-sm items-center"
                >
                  <FaUserCircle size={"35px"} />
                  <p className="ml-3 my-2">Hồ sơ</p>
                </Link>
              ) : null}

              <Link to="/changePassword" className="flex text-sm items-center">
                <MdOutlineVpnKey size={"35px"} />
                <p className="ml-3 my-2">Đổi mật khẩu</p>
              </Link>
              <Link
                to="/"
                className="flex text-sm items-center w-full"
                onClick={() => {
                  onHandleLogout();
                }}
              >
                <MdLogout size={"35px"} />
                <p className="ml-3 my-2">Đăng xuất</p>
              </Link>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};
export default NavWithLogin;
