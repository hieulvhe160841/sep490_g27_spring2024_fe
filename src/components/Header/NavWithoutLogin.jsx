import { NavLink } from "react-router-dom";

const NavWithoutLogin = () => {
  return (
    <div className="flex mr-6 my-4 ml-3">
      <NavLink
        to="/login"
        className="block text-md underline px-6 py-2 rounded-lg text-md"
      >
        Đăng nhập
      </NavLink>

      <NavLink
        to="/register"
        className="block text-md px-4 py-2 text-white text-md rounded-lg shadow-md bg-gradient-to-r from-[#2B90D9] to-[#174C73]"
      >
        Đăng ký
      </NavLink>
    </div>
  );
};
export default NavWithoutLogin;
