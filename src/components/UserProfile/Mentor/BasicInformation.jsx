import { Button } from "@mui/material";
import React, { useState } from "react";
import { FaStar } from "react-icons/fa";
import { Link, useNavigate } from "react-router-dom";
import userStore from "../../../store/userStore";
import mentorStore from "../../../store/mentorStore";
import { toast } from "react-toastify";
import ModalViewImg from "../../Modal/ModalViewImg";

function BasicInformation({ mentor }) {
  const navigate = useNavigate();
  const { baseUrl } = userStore();

  const onHandleUpdatePage = () => {
    navigate("edit", { state: mentor });
  };
  return (
    <>
      <div className="grid grid-cols-4 lg:grid-cols-12 gap-6 px-4 mt-20 ml-14">
        <div className="col-span-4 relative">
          <div className="bg-white shadow-2xl py-6 pl-1 rounded-2xl overflow-auto">
            <div className="flex flex-col">
              <div className="flex justify-center">
                <img
                  src={`${baseUrl}/view/${mentor.mentorDetail.userDetail.avatar}`}
                  className="w-32 h-32 bg-gray-300 rounded-full mb-4 shrink-0 shadow-2xl border-2 border-white absolute -top-16 "
                ></img>
              </div>
              <div className="flex flex-col mt-12 ml-6">
                <p className="text-lg font-semibold mb-2 text-sidebarTop">
                  Tên:&nbsp;
                  <span className="text-gray-700">
                    {mentor.mentorDetail.userDetail.name}
                  </span>
                </p>

                <p className="text-lg font-semibold mb-2 text-sidebarTop">
                  Email:&nbsp;
                  <span className="text-gray-700">
                    {mentor.mentorDetail.userDetail.email}
                  </span>
                </p>
                <p className="text-lg font-semibold mb-2 text-sidebarTop">
                  SĐT:&nbsp;
                  <span className="text-gray-700">
                    {mentor.mentorDetail.userDetail.phone}
                  </span>
                </p>
                <p className="text-lg font-semibold mb-2 text-sidebarTop">
                  Địa chỉ:&nbsp;
                  <span className="text-gray-700">
                    {mentor.mentorDetail.userDetail.address}
                  </span>
                </p>

                <p className="text-lg font-semibold mb-2">
                  {mentor.mentorDetail.costPerHour
                    ? mentor.mentorDetail.costPerHour.toLocaleString("en-US", {
                        maximumFractionDigits: 0,
                      })
                    : 0}
                  &nbsp;VNĐ/h
                </p>
              </div>
            </div>
            <hr className="my-6 border-t border-gray-300" />
            <div className="flex flex-col ml-4">
              <span className="text-gray-700 font-bold tracking-wider mb-2">
                Trình độ học vấn:{" "}
                {mentor.mentorDetail.userDetail.educationLevel}
              </span>
              <div className="font-semibold mb-2">Kĩ năng:</div>
              <ul>
                {mentor.skills.map((s) => {
                  return (
                    <li className="mb-2" key={s.skillId}>
                      {s.skillName}
                    </li>
                  );
                })}
              </ul>
            </div>
            <div className="mt-6 flex gap-4 justify-center">
              <Button
                variant="contained"
                sx={{
                  backgroundColor: "#5584C6",
                }}
                type="button"
                onClick={() => onHandleUpdatePage()}
              >
                Cập nhật hồ sơ
              </Button>
            </div>
          </div>
        </div>
        <div className="col-span-4 lg:col-span-8">
          <div className="bg-white shadow-2xl p-4 rounded-2xl overflow-hidden">
            <h2 className="text-xl font-bold mt-6 mb-4">Kinh nghiệm</h2>
            <div className="mb-6">
              <p className="mt-2">
                {mentor.mentorDetail.userDetail.experience}
              </p>
            </div>
          </div>
          <div className="bg-white shadow-2xl p-4 rounded-2xl overflow-hidden mt-4">
            <h2 className="text-xl font-bold mt-6 mb-4">Chứng chỉ</h2>
            {mentor.certificates.map((cert) => {
              return (
                <div className="mb-6" key={cert.certificateId}>
                  <div className=" w-full">
                    <span className="text-gray-700 font-bold">
                      {cert.certificateName}
                    </span>
                    <div className=" text-blue-600 items-start">
                      <ModalViewImg cert={cert} />
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </>
  );
}

export default BasicInformation;
