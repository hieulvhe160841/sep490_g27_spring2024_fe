import React, { useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import userStore from "../../../../store/userStore";
import mentorStore from "../../../../store/mentorStore";
import { FaStar } from "react-icons/fa";
import { Button } from "@mui/material";
import { toast } from "react-toastify";
import { CgSpinnerAlt } from "react-icons/cg";
import ModalViewImg from "../../../Modal/ModalViewImg";
import TitleHeader from "../../../TitleHeader";

function UpdateBasicInformation() {
  const navigate = useNavigate();
  const location = useLocation();
  const { updateProfileMentor, isLoading } = mentorStore();
  const mentor = location.state;
  const [imagePreviewUrl, setImagePreviewUrl] = useState(
    mentor.mentorDetail.userDetail || ""
  );
  const [costPerHour, setCostPerHour] = useState(
    mentor.mentorDetail.costPerHour
  );

  const onHandleUpdateProfile = async (e) => {
    e.preventDefault();
    const formData = new FormData(e.currentTarget);
    const name = formData.get("name");
    const address = formData.get("address");
    const phone = formData.get("phone");
    const dayOfBirth = mentor.mentorDetail.userDetail.dayOfBirth;
    const experience = formData.get("experience");
    const educationLevel = formData.get("educationLevel");
    const costPerHours = JSON.stringify(parseInt(costPerHour));
    formData.append(
      "profile",
      JSON.stringify({
        name,
        address,
        dayOfBirth,
        phone,
        experience,
        educationLevel,
        costPerHours,
      })
    );
    formData.delete("name");
    formData.delete("phone");
    formData.delete("experience");
    formData.delete("address");
    formData.delete("educationLevel");
    formData.delete("costPerHours");
    const res = await updateProfileMentor(formData);

    const data = Object.fromEntries(formData);
    console.log("DATA", data);
    if (res.status === 200) {
      toast.success("Cập nhật thành công");

      navigate("/mentor/profile");
    } else {
      toast.error("Cập nhật thất bại");
      navigate("/mentor/profile");
    }
    console.log(res);
  };
  const handleImageChange = (e) => {
    const file = e.target.files[0];
    if (file) {
      setImagePreviewUrl(URL.createObjectURL(file));
    }
  };
  const formatNumberWithCommas = (number) => {
    if (number !== undefined && number !== null) {
      const integerPart = Math.trunc(number);
      return integerPart.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    } else {
      return "";
    }
  };
  const handleInputChange = (e) => {
    const { value } = e.target;
    const numericValue = value.replace(/,/g, "");
    setCostPerHour(numericValue);
  };
  return (
    <>
      <TitleHeader title="Cập nhật hồ sơ" />
      <form
        className="grid grid-cols-4 lg:grid-cols-12 gap-6 px-4 ml-14"
        onSubmit={(e) => onHandleUpdateProfile(e)}
      >
        <div className="col-span-4 lg:col-span-3">
          <div className="bg-gray-100 shadow-xl p-6  border-2 rounded-2xl overflow-auto">
            <div className="flex flex-col items-center">
              <div className="relative">
                {imagePreviewUrl && (
                  <img
                    src={imagePreviewUrl}
                    className="w-32 h-32 rounded-full mb-4 border-2"
                  />
                )}
                <input
                  type="file"
                  name="avatar"
                  id="avatar-upload"
                  style={{ display: "none" }}
                  onChange={handleImageChange}
                />
                <label
                  htmlFor="avatar-upload"
                  className="absolute inset-0 flex justify-center items-center"
                >
                  <Button
                    variant="contained"
                    component="span"
                    sx={{
                      mb: 2,
                      backgroundColor: "#5584C6",
                      opacity: 0.8,
                      "&:hover": {
                        backgroundColor: "#4074B2",
                        opacity: 1,
                      },
                    }}
                  >
                    Tải lên
                  </Button>
                </label>
              </div>
              <input
                type="text"
                name="name"
                className="text-2xl font-semibold mb-2 text-center bg-gray-100 border border-gray-400 rounded-md"
                defaultValue={mentor.mentorDetail.userDetail.name}
              ></input>
              <p className="text-xl font-semibold mb-2 ">
                {mentor.mentorDetail.userDetail.email}
              </p>
              <input
                type="text"
                name="phone"
                className="text-xl font-semibold mb-2 text-center bg-gray-100 border border-gray-400 rounded-md"
                defaultValue={mentor.mentorDetail.userDetail.phone}
              ></input>
              <input
                type="text"
                name="address"
                className="text-xl font-semibold mb-2 text-center bg-gray-100 border border-gray-400 rounded-md"
                defaultValue={mentor.mentorDetail.userDetail.address}
              ></input>
              <input
                type="text"
                name="costPerHours"
                className="text-lg font-semibold mb-2 text-center bg-gray-100 border border-gray-400 rounded-md"
                defaultValue={formatNumberWithCommas(
                  mentor.mentorDetail.costPerHour
                )}
                onChange={(e) => handleInputChange(e)}
              ></input>
            </div>
            <hr className="my-6 border-t border-gray-300" />
            <div className="flex flex-col">
              Trình độ học vấn:
              <input
                type="text"
                name="educationLevel"
                className="text-gray-700 font-bold tracking-wider mb-2 text-center bg-gray-100 border border-gray-400 rounded-md"
                defaultValue={mentor.mentorDetail.userDetail.educationLevel}
              ></input>
              <ul>
                {mentor.skills.map((s) => {
                  return (
                    <li className="mb-2" key={s.skillId}>
                      {s.skillName}
                    </li>
                  );
                })}
              </ul>
            </div>
          </div>
          <div className="mt-6 flex gap-4 justify-center">
            <Button
              variant="contained"
              disabled={isLoading ? true : false}
              sx={{
                backgroundColor: "#5584C6",
              }}
              type="submit"
            >
              Lưu&nbsp;
              {isLoading && (
                <svg
                  className="animate-spin h-5 w-5 mr-5 text-white ..."
                  viewBox="0 0 17 17"
                >
                  <CgSpinnerAlt />
                </svg>
              )}
            </Button>
          </div>
        </div>
        <div className="col-span-4 lg:col-span-9">
          <div className="bg-gray-100 shadow-xl p-6  border-2  rounded-2xl overflow-hidden">
            <h2 className="text-xl font-bold mt-6 mb-4">Kinh nghiệm</h2>

            <textarea
              name="experience"
              rows="4"
              className="mt-2 w-full bg-gray-100 border border-gray-400 rounded-md p-3"
              defaultValue={mentor.mentorDetail.userDetail.experience}
              maxLength={255}
            ></textarea>

            <h2 className="text-xl font-bold mt-6 mb-4">Chứng chỉ</h2>
            {mentor.certificates.map((cert) => {
              return (
                <div className="mb-6" key={cert.certificateId}>
                  <div className=" w-full">
                    <span className="text-gray-700 font-bold">
                      {cert.certificateName}
                    </span>
                    <div className=" text-blue-600 items-start">
                      <ModalViewImg cert={cert} />
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </form>
    </>
  );
}

export default UpdateBasicInformation;
