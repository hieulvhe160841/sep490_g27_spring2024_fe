import React, { useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import menteeStore from "../../../../store/menteeStore";
import { toast } from "react-toastify";
import { Button } from "@mui/material";
import { CgSpinnerAlt } from "react-icons/cg";
import TitleHeader from "./../../../TitleHeader";
function UpdateProfile() {
  const navigate = useNavigate();
  const location = useLocation();
  const { updateProfileMentee, isLoading } = menteeStore();
  const mentee = location.state;
  const [imagePreviewUrl, setImagePreviewUrl] = useState(mentee.avatar || "");

  const onHandleUpdateProfile = async (e) => {
    e.preventDefault();
    const formData = new FormData(e.currentTarget);
    const name = formData.get("name");
    const phone = formData.get("phone");
    const dayOfBirth = mentee.dayOfBirth;
    const email = formData.get("email");
    const address = formData.get("address");
    const goal = formData.get("goal");
    const interest = formData.get("interest");

    formData.append(
      "profile",
      JSON.stringify({
        name,
        phone,
        email,
        address,
        goal,
        interest,
        dayOfBirth,
      })
    );
    formData.delete("name");
    formData.delete("phone");
    formData.delete("email");
    formData.delete("address");
    formData.delete("goal");
    formData.delete("interest");

    const res = await updateProfileMentee(formData);

    const data = Object.fromEntries(formData);
    console.log(data);
    if (res.status === 200) {
      toast.success("Cập nhật thành công");

      navigate("/mentee/profile");
    } else {
      toast.error("Cập nhật thất bại");
    }
    console.log(res);
  };
  const handleImageChange = (e) => {
    const file = e.target.files[0];
    if (file) {
      setImagePreviewUrl(URL.createObjectURL(file));
    }
  };

  return (
    <>
      <TitleHeader title="Cập nhật hồ sơ" />
      <form
        className="grid grid-cols-4 lg:grid-cols-12 gap-6 px-4 mt-20 ml-14"
        onSubmit={(e) => onHandleUpdateProfile(e)}
      >
        <div className="col-span-4 lg:col-span-3">
          <div className="bg-gray-100 shadow-xl p-6 border-2 rounded-2xl overflow-auto">
            <div className="flex flex-col items-center ">
              <div className="relative">
                {imagePreviewUrl && (
                  <img
                    src={imagePreviewUrl}
                    className="w-32 h-32 rounded-full mb-4 border-2"
                  />
                )}
                {console.log(mentee)}
                <input
                  type="file"
                  name="avatar"
                  id="avatar-upload"
                  style={{ display: "none" }}
                  onChange={handleImageChange}
                />
                <label
                  htmlFor="avatar-upload"
                  className="absolute inset-0 flex justify-center items-center"
                >
                  <Button
                    variant="contained"
                    component="span"
                    sx={{
                      mb: 2,
                      backgroundColor: "#5584C6",
                      opacity: 0.8,
                      "&:hover": {
                        backgroundColor: "#4074B2",
                        opacity: 1,
                      },
                    }}
                  >
                    Tải lên
                  </Button>
                </label>
              </div>

              <input
                type="text"
                name="name"
                className="text-xl text-center bg-gray-100 border border-gray-400 rounded-md font-semibold mb-2"
                defaultValue={mentee.name}
              ></input>

              <input
                type="text"
                name="phone"
                className="text-xl text-center bg-gray-100 border border-gray-400 rounded-md font-semibold mb-2"
                defaultValue={mentee.phone}
              ></input>

              <input
                type="text"
                name="email"
                className="text-xl text-center font-semibold mb-2 w-full"
                defaultValue={mentee.email}
                readOnly
              ></input>
              <input
                type="text"
                name="address"
                className="text-xl text-center bg-gray-100 border border-gray-400 rounded-md font-semibold mb-2"
                defaultValue={mentee.address}
              ></input>
            </div>
          </div>
        </div>

        <div className="col-span-4 lg:col-span-9">
          <div className="bg-gray-100 shadow-xl p-6 border-2 rounded-2xl overflow-hidden">
            <h2 className="text-xl font-bold mt-6 mb-4">Mục tiêu</h2>
            <div className="mb-6">
              <textarea
                rows="4"
                name="goal"
                className="mt-2 w-full border border-gray-400 rounded-md p-3"
                defaultValue={mentee.goal}
                maxLength={255}
              ></textarea>
            </div>
            <h2 className="text-xl font-bold mt-6 mb-4">Sở thích</h2>
            <div className="mb-6">
              <textarea
                rows="4"
                name="interest"
                className="mt-2 w-full  border border-gray-400 rounded-md p-3"
                defaultValue={mentee.interest}
                maxLength={255}
              ></textarea>
            </div>
          </div>
        </div>
        <div className="col-span-4 lg:col-span-12">
          <div className="flex justify-center">
            <Button
              variant="contained"
              size="large"
              disabled={isLoading ? true : false}
              sx={{
                backgroundColor: "#5584C6",
              }}
              type="submit"
            >
              Lưu&nbsp;
              {isLoading && (
                <svg
                  className="animate-spin h-5 w-5 mr-5 text-white ..."
                  viewBox="0 0 17 17"
                >
                  <CgSpinnerAlt />
                </svg>
              )}
            </Button>
          </div>
        </div>
      </form>
    </>
  );
}

export default UpdateProfile;
