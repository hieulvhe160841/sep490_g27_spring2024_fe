import { Button } from "@mui/material";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import userStore from "../../../store/userStore";
import menteeStore from "../../../store/menteeStore";
import { toast } from "react-toastify";

function BasicInformation({ mentee }) {
  const navigate = useNavigate();
  const { baseUrl } = userStore();

  const onHandleUpdatePage = () => {
    navigate("edit", { state: mentee });
  };

  return (
    <>
      <div className="grid grid-cols-4 lg:grid-cols-12 gap-6 px-4 mt-20 ml-14">
        <div className="col-span-4 relative">
          <div className="bg-white p-7 rounded-2xl overflow-hidden shadow-2xl">
            <div className="flex justify-center">
              <img
                src={`${baseUrl}/view/${mentee.avatar}`}
                className="w-32 h-32 bg-gray-300 rounded-full mb-4 shrink-0 border-2 border-white shadow-2xl absolute -top-16"
              ></img>
            </div>

            <div className="flex flex-col mt-12 mx-6 overflow-auto">
              <p className="text-xl font-semibold mb-2 text-sidebarTop">
                Tên: <span className="text-gray-700">{mentee.name}</span>
              </p>

              <p className="text-xl text-start font-semibold mb-2 text-sidebarTop">
                SĐT: <span className="text-gray-700">{mentee.phone}</span>
              </p>

              <p className="text-lg text-start font-semibold mb-2 text-sidebarTop">
                Email: <span className="text-gray-700">{mentee.email}</span>
              </p>
              <p className="text-lg text-start font-semibold mb-2 text-sidebarTop">
                Địa chỉ: <span className="text-gray-700">{mentee.address}</span>
              </p>
            </div>
            <div className="mt-6 flex gap-4 justify-center">
              <Button
                variant="contained"
                sx={{
                  backgroundColor: "#5584C6",
                }}
                type="button"
                onClick={() => onHandleUpdatePage()}
              >
                Cập nhật
              </Button>
            </div>
          </div>
        </div>

        <div className="col-span-4 lg:col-span-8">
          <div className="bg-white shadow-2xl p-4 rounded-2xl overflow-hidden">
            <h2 className="text-2xl font-bold mt-6 mb-4">Mục tiêu</h2>
            <div className="mb-6">
              <p className="mt-2 text-base">{mentee.goal}</p>
            </div>
          </div>
          <div className="bg-white shadow-2xl p-4 rounded-2xl overflow-hidden mt-4">
            <h2 className="text-2xl font-bold mt-6 mb-4">Sở thích</h2>
            <div className="mb-6">
              <p className="mt-2 text-base">{mentee.interest}</p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default BasicInformation;
