import React, { useState } from "react";
import { Button } from "@mui/material";
import userStore from "../../store/userStore";
import { toast } from "react-toastify";

const ModalAddStaff = () => {
  const [showModal, setShowModal] = useState(false);
  const { addStaff, fetchUsers } = userStore();

  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [status, setStatus] = useState(true);
  const [phone, setPhone] = useState("");
  const [address, setAddress] = useState("");
  const [gender, setGender] = useState(1);
  const [genderName, setGenderName] = useState();
  const [dayOfBirth, setDayOfBirth] = useState("");
  const [roleId, setRoleId] = useState(2);
  const [roleName, setRoleName] = useState("");

  const handleRoleChange = (e) => {
    const selectedRoleName = e.target.value;

    if (selectedRoleName === "transaction-staff") {
      setRoleId(2);
      setRoleName(e.target.value);
    } else if (selectedRoleName === "education-staff") {
      setRoleId(3);
      setRoleName(e.target.value);
    }
  };

  const handleGenderChange = (e) => {
    const selectedGender = e.target.value;

    if (selectedGender === "male") {
      setGender(1);
      setGenderName(e.target.value);
    } else if (selectedGender === "female") {
      setGender(0);
      setGenderName(e.target.value);
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const staff = {
      userName,
      password,
      name,
      status,
      phone,
      address,
      gender,
      dayOfBirth,
      roleDTO: {
        roleId,
        roleName,
      },
    };
    try {
      await addStaff(staff);
      setShowModal(false);
      toast.success("Thêm tài khoản nhân viên thành công!");
      await fetchUsers();
    } catch (error) {
      toast.warning(error.data[0]);
    }
  };

  return (
    <>
      <Button
        variant="outlined"
        sx={{
          border: "2px solid #4B5563",
          padding: "8px 12px",
          borderRadius: "8px",
          boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.1)",
          margin: "12px 0px 0px 0px",
          "&:hover": {
            boxShadow: "none",
          },
          color: "#000000",
          textTransform: "none",
          height: "40px",
          width: "170px",
        }}
        type="button"
        onClick={() => setShowModal(true)}
      >
        Thêm Nhân viên
      </Button>
      {showModal ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-[30%] my-6 mx-auto max-w-4xl">
              {/*content*/}
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                <form onSubmit={handleSubmit}>
                  {/*header*/}
                  <div className="flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
                    <h3 className="text-3xl font-semibold">Thêm nhân viên</h3>
                    <button
                      className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                      onClick={() => setShowModal(false)}
                    >
                      <span className="bg-transparent text-black h-6 w-6 text-2xl block outline-none focus:outline-none">
                        ×
                      </span>
                    </button>
                  </div>
                  {/*body*/}
                  <div className="relative p-6 flex-auto max-h-96 overflow-y-auto">
                    <form className="w-full max-w-md">
                      <div className="md:flex md:items-center mb-6">
                        <div className="md:w-1/3">
                          <label
                            className="labelInput"
                            htmlFor="inline-full-name"
                          >
                            Tài khoản
                          </label>
                        </div>
                        <div className="md:w-2/3">
                          <input
                            className="inputField"
                            id="inline-full-name"
                            type="text"
                            value={userName}
                            onChange={(e) => setUserName(e.target.value)}
                            required
                          />
                        </div>
                      </div>
                      <div className="md:flex md:items-center mb-6">
                        <div className="md:w-1/3">
                          <label
                            className="labelInput"
                            htmlFor="inline-password"
                          >
                            Mật khẩu
                          </label>
                        </div>
                        <div className="md:w-2/3">
                          <input
                            className="inputField"
                            id="inline-password"
                            type="password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            required
                          />
                        </div>
                      </div>
                      <div className="md:flex md:items-center mb-6">
                        <div className="md:w-1/3">
                          <label className="labelInput" htmlFor="inline-name">
                            Tên
                          </label>
                        </div>
                        <div className="md:w-2/3">
                          <input
                            className="inputField"
                            id="inline-name"
                            type="text"
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                            required
                          />
                        </div>
                      </div>
                      <div className="md:flex md:items-center mb-6">
                        <div className="md:w-1/3">
                          <label className="labelInput" htmlFor="inline-phone">
                            Điện thoại
                          </label>
                        </div>
                        <div className="md:w-2/3">
                          <input
                            className="inputField"
                            id="inline-phone"
                            type="text"
                            value={phone}
                            onChange={(e) => setPhone(e.target.value)}
                            required
                          />
                        </div>
                      </div>
                      <div className="md:flex md:items-center mb-6">
                        <div className="md:w-1/3">
                          <label
                            className="labelInput"
                            htmlFor="inline-address"
                          >
                            Địa chỉ
                          </label>
                        </div>
                        <div className="md:w-2/3">
                          <input
                            className="inputField"
                            id="inline-address"
                            type="text"
                            value={address}
                            onChange={(e) => setAddress(e.target.value)}
                            required
                          />
                        </div>
                      </div>
                      <div className="md:flex md:items-center mb-6">
                        <div className="md:w-1/3">
                          <label className="labelInput" htmlFor="inline-gender">
                            Giới tính
                          </label>
                        </div>
                        <div className="md:w-2/3">
                          <select
                            className="inputField"
                            id="inline-gender"
                            value={genderName}
                            onChange={handleGenderChange}
                            required
                          >
                            <option value="male">Nam</option>
                            <option value="female">Nữ</option>
                          </select>
                        </div>
                      </div>
                      <div className="md:flex md:items-center mb-6">
                        <div className="md:w-1/3">
                          <label className="labelInput" htmlFor="inline-dob">
                            Ngày sinh
                          </label>
                        </div>
                        <div className="md:w-2/3">
                          <input
                            className="inputField"
                            id="inline-dob"
                            type="date"
                            value={dayOfBirth}
                            onChange={(e) => setDayOfBirth(e.target.value)}
                            max="9999-12-31"
                            required
                          />
                        </div>
                      </div>
                      <div className="md:flex md:items-center mb-6">
                        <div className="md:w-1/3">
                          <label className="labelInput" htmlFor="inline-role">
                            Role
                          </label>
                        </div>
                        <div className="md:w-2/3">
                          <select
                            className="inputField"
                            id="inline-role"
                            value={roleName}
                            onChange={handleRoleChange}
                          >
                            <option value="transaction-staff">
                              Nhân viên giao dịch
                            </option>
                            <option value="education-staff">
                              Nhân viên quản lý
                            </option>
                          </select>
                        </div>
                      </div>
                    </form>
                  </div>
                  {/*footer*/}
                  <div className="btnInModal">
                    <Button
                      type="submit"
                      variant="outlined"
                      sx={{
                        border: "2px solid #2B90D9",
                        textTransform: "none",
                        borderRadius: "12px",
                        height: "35px",
                        width: "115px",
                        color: "#000000",
                      }}
                    >
                      Thêm
                    </Button>
                    <Button
                      variant="contained"
                      color="error"
                      sx={{
                        backgroundColor: "#FF0A0A",
                        textTransform: "none",
                        borderRadius: "12px",
                        height: "35px",
                        width: "115px",
                        color: "#ffffff",
                      }}
                      onClick={() => setShowModal(false)}
                    >
                      Đóng
                    </Button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </>
      ) : null}
    </>
  );
};

export default ModalAddStaff;
