import React, { useEffect, useState } from "react";
import { Button } from "@mui/material";
import menteeStore from "../../store/menteeStore";
import { toast } from "react-toastify";

function ModalEditPostMentee({ post }) {
  const [showModal, setShowModal] = useState(false);
  const [postContent, setPostContent] = useState(post.postContent);
  const [postPrice, setPostPrice] = useState(post.postPrice);
  const { updatePostById, deletePostById, fetchAllMenteePost } = menteeStore();

  const [title, setTitle] = useState();
  const [content, setContent] = useState();
  const [price, setPrice] = useState();

  useEffect(() => {
    setTitle(post.postTitle);
    setContent(post.postContent);
    setPrice(post.postPrice);
  }, []);

  const handleTitleChange = (event) => {
    setTitle(event.target.value);
  };
  const handleContentChange = (event) => {
    setContent(event.target.value);
  };

  const handleSave = async () => {
    try {
      await updatePostById(post.postId, {
        postTitle: title,
        postContent: content,
        postPrice: price,
      });
      toast.success("Cập nhật bài đăng thành công");
      setShowModal(false);
      await fetchAllMenteePost();
    } catch (error) {
      toast.error("Lỗi cập nhật bài đăng thành công:", error);
    }
  };

  const onHandleDelete = async (postId) => {
    const res = await deletePostById(postId);
    toast.success(res.messages);
    await fetchAllMenteePost();
  };
  const formatNumberWithCommas = (number) => {
    const integerPart = Math.trunc(number);
    return integerPart.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

  const handleInputChange = (e) => {
    const { value } = e.target;
    const numericValue = value.replace(/,/g, "");
    setPrice(numericValue);
  };
  return (
    <>
      <Button
        variant="contained"
        sx={{
          textTransform: "none",
          borderRadius: "12px",
          height: "40px",
          width: "130px",
        }}
        type="button"
        onClick={() => setShowModal(true)}
      >
        Chi tiết
      </Button>
      {post.postStatus && (
        <Button
          variant="contained"
          color="error"
          sx={{
            backgroundColor: "red",
            textTransform: "none",
            borderRadius: "12px",
            height: "40px",
            width: "115px",
          }}
          type="button"
          onClick={() => onHandleDelete(post.postId)}
        >
          Xóa
        </Button>
      )}

      {showModal ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-[30%] my-6 mx-auto max-w-4xl">
              {/*content*/}
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                {/*header*/}
                <div className="flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
                  <h3 className="text-3xl font-semibold">Chỉnh sửa bài đăng</h3>
                  <button
                    className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                    onClick={() => setShowModal(false)}
                  >
                    <span className="bg-transparent text-black h-6 w-6 text-2xl block outline-none focus:outline-none">
                      ×
                    </span>
                  </button>
                </div>
                {/*body*/}
                <div className="relative p-6 flex-auto max-h-96 overflow-y-auto">
                  <form className="w-full max-w-md">
                    <div className="md:flex md:items-center mb-6">
                      <div className="md:w-1/3">
                        <label className="labelInput" htmlFor="inline-title">
                          Tiêu đề
                        </label>
                      </div>
                      <div className="md:w-2/3">
                        <input
                          className="inputField"
                          id="inline-title"
                          type="text"
                          defaultValue={title}
                          onChange={handleTitleChange}
                          required
                        />
                      </div>
                    </div>
                    <div className="md:flex md:items-center mb-6">
                      <div className="md:w-1/3">
                        <label className="labelInput" htmlFor="inline-content">
                          Nội dung
                        </label>
                      </div>
                      <div className="md:w-2/3">
                        <textarea
                          className="inputField"
                          id="inline-content"
                          value={content}
                          onChange={handleContentChange}
                          rows={5}
                          required
                          maxLength={255}
                        />
                      </div>
                    </div>

                    <div className="md:flex md:items-center mb-6">
                      <div className="md:w-1/3">
                        <label className="labelInput" htmlFor="inline-price">
                          Giá tiền (VNĐ)
                        </label>
                      </div>
                      <div className="md:w-2/3">
                        <input
                          className="inputField"
                          id="inline-price"
                          type="text"
                          value={formatNumberWithCommas(price)}
                          onChange={(e) => handleInputChange(e)}
                        />
                      </div>
                    </div>
                  </form>
                </div>
                {/*footer*/}
                <div className="btnInModal">
                  <Button
                    variant="outlined"
                    sx={{
                      border: "2px solid #2B90D9",
                      textTransform: "none",
                      borderRadius: "12px",
                      height: "35px",
                      width: "115px",
                      color: "#000000",
                    }}
                    onClick={handleSave}
                  >
                    Cập nhật
                  </Button>
                  <Button
                    variant="contained"
                    color="error"
                    sx={{
                      backgroundColor: "#FF0A0A",
                      textTransform: "none",
                      borderRadius: "12px",
                      height: "35px",
                      width: "115px",
                      color: "#ffffff",
                    }}
                    onClick={() => setShowModal(false)}
                  >
                    Đóng
                  </Button>
                </div>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </>
  );
}

export default ModalEditPostMentee;
