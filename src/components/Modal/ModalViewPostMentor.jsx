import React, { useEffect, useState } from "react";
import { Button } from "@mui/material";
import { toast } from "react-toastify";
import mentorStore from "../../store/mentorStore";

const ModalViewPostMentor = ({ postId }) => {
  const [post, setPost] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const { fetchPostById } = mentorStore();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const postDetails = await fetchPostById(postId);
        setPost(postDetails.data);
      } catch (error) {
        console.error("Error fetching post details:", error);
      }
    };

    if (postId) {
      fetchData();
    }
  }, [fetchPostById, postId]);

  const formatNumberWithCommas = (number) => {
    const integerPart = Math.trunc(number);
    return integerPart.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

  return (
    <>
      <Button
        variant="contained"
        size="large"
        sx={"border-radius: 15px"}
        onClick={() => setShowModal(true)}
      >
        Chi tiết
      </Button>
      {showModal && post ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-[30%] my-6 mx-auto max-w-4xl">
              {/*content*/}
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                {/*header*/}
                <div className="flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
                  <h3 className="text-3xl font-semibold">Xem bài đăng</h3>
                  <button
                    className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                    onClick={() => setShowModal(false)}
                  >
                    <span className="bg-transparent text-black h-6 w-6 text-2xl block outline-none focus:outline-none">
                      ×
                    </span>
                  </button>
                </div>
                {/*body*/}
                <div className="relative p-6 flex-auto max-h-96 overflow-y-auto">
                  <form className="w-full max-w-md">
                    <div className="md:flex md:items-center mb-6">
                      <div className="md:w-1/3">
                        <label className="labelInput" htmlFor="inline-id">
                          ID
                        </label>
                      </div>
                      <div className="md:w-2/3">
                        <input
                          className="inputField"
                          id="inline-id"
                          type="text"
                          defaultValue={post.postId}
                          disabled
                        />
                      </div>
                    </div>
                    <div className="md:flex md:items-center mb-6">
                      <div className="md:w-1/3">
                        <label className="labelInput" htmlFor="inline-title">
                          Tiêu đề
                        </label>
                      </div>
                      <div className="md:w-2/3">
                        <input
                          className="inputField"
                          id="inline-title"
                          type="text"
                          defaultValue={post.postTitle}
                          disabled
                        />
                      </div>
                    </div>
                    <div className="md:flex md:items-center mb-6">
                      <div className="md:w-1/3">
                        <label className="labelInput" htmlFor="inline-content">
                          Nội dung
                        </label>
                      </div>
                      <div className="md:w-2/3">
                        <textarea
                          className="inputField"
                          id="inline-content"
                          defaultValue={post.postContent}
                          disabled
                          rows={5}
                        />
                      </div>
                    </div>
                    <div className="md:flex md:items-center mb-6">
                      <div className="md:w-1/3">
                        <label className="labelInput" htmlFor="inline-price">
                          Giá tiền(VNĐ)
                        </label>
                      </div>
                      <div className="md:w-2/3">
                        <input
                          className="inputField"
                          id="inline-price"
                          type="text"
                          defaultValue={formatNumberWithCommas(post.postPrice)}
                          disabled
                        />
                      </div>
                    </div>
                    <div className="md:flex md:items-center mb-6">
                      <div className="md:w-1/3">
                        <label
                          className="labelInput"
                          htmlFor="inline-mentee-name"
                        >
                          Học viên
                        </label>
                      </div>
                      <div className="md:w-2/3">
                        <input
                          className="inputField"
                          id="inline-mentee-name"
                          type="text"
                          defaultValue={post.postCreatedUser}
                          disabled
                        />
                      </div>
                    </div>
                  </form>
                </div>
                {/*footer*/}
                <div className="btnInModal">
                  <Button
                    variant="contained"
                    color="error"
                    sx={{
                      backgroundColor: "#FF0A0A",
                      textTransform: "none",
                      borderRadius: "12px",
                      height: "35px",
                      width: "115px",
                      color: "#ffffff",
                    }}
                    onClick={() => setShowModal(false)}
                  >
                    Đóng
                  </Button>
                </div>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </>
  );
};

export default ModalViewPostMentor;
