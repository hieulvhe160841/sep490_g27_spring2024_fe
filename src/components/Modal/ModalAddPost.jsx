import React, { useState } from "react";
import { Button } from "@mui/material";
import menteeStore from "../../store/menteeStore";
import { toast } from "react-toastify";

const ModalAddPost = () => {
  const [showModal, setShowModal] = useState(false);
  const { addPost, fetchAllMenteePost } = menteeStore();

  const [postTitle, setPostTitle] = useState("");
  const [postContent, setPostContent] = useState("");
  const [postPrice, setPostPrice] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    const post = {
      postTitle,
      postContent,
      postPrice,
    };
    try {
      await addPost(post);
      setShowModal(false);
      toast.success("Thêm Bài đăng thành công!");
      await fetchAllMenteePost();
    } catch (error) {
      toast.error(error);
    }
  };
  const formatNumberWithCommas = (number) => {
    if (number !== undefined && number !== null) {
      const integerPart = Math.trunc(number);
      return integerPart.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    } else {
      return "";
    }
  };

  const handleInputChange = (e) => {
    const { value } = e.target;
    const numericValue = value.replace(/,/g, "");
    setPostPrice(numericValue);
  };

  return (
    <>
      <Button
        variant="outlined"
        sx={{
          border: "2px solid #4B5563",
          padding: "8px 12px",
          borderRadius: "12px",
          boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.1)",
          margin: "12px 0px 0px 0px",
          "&:hover": {
            boxShadow: "none",
          },
          color: "#000000",
          textTransform: "none",
          height: "35px",
          width: "115px",
        }}
        type="button"
        onClick={() => setShowModal(true)}
      >
        Thêm mới
      </Button>
      {showModal ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-[30%] my-6 mx-auto max-w-4xl">
              {/*content*/}
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                <form onSubmit={handleSubmit}>
                  {/*header*/}
                  <div className="flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
                    <h3 className="text-3xl font-semibold">Thêm bài đăng</h3>
                    <button
                      className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                      onClick={() => setShowModal(false)}
                    >
                      <span className="bg-transparent text-black h-6 w-6 text-2xl block outline-none focus:outline-none">
                        ×
                      </span>
                    </button>
                  </div>
                  {/*body*/}
                  <div className="relative p-6 flex-auto max-h-96 overflow-y-auto">
                    <div className="w-full max-w-md">
                      <div className="md:flex md:items-center mb-6">
                        <div className="md:w-1/3">
                          <label className="labelInput" htmlFor="inline-title">
                            Tiêu đề
                          </label>
                        </div>
                        <div className="md:w-2/3">
                          <input
                            className="inputField"
                            id="inline-title"
                            type="text"
                            value={postTitle}
                            onChange={(e) => setPostTitle(e.target.value)}
                            required
                          />
                        </div>
                      </div>
                      <div className="md:flex md:items-center mb-6">
                        <div className="md:w-1/3">
                          <label
                            className="labelInput"
                            htmlFor="inline-post-content"
                          >
                            Nội dung
                          </label>
                        </div>
                        <div className="md:w-2/3">
                          <textarea
                            className="inputField"
                            id="inline-post-content"
                            value={postContent}
                            onChange={(e) => setPostContent(e.target.value)}
                            rows={5}
                            required
                            maxLength={255}
                          />
                        </div>
                      </div>
                      <div className="md:flex md:items-center mb-6">
                        <div className="md:w-1/3">
                          <label className="labelInput" htmlFor="inline-price">
                            Giá tiền (VNĐ)
                          </label>
                        </div>
                        <div className="md:w-2/3">
                          <input
                            className="inputField"
                            id="inline-price"
                            type="text"
                            value={formatNumberWithCommas(postPrice)}
                            onChange={(e) => handleInputChange(e)}
                            required
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/*footer*/}
                  <div className="btnInModal">
                    <Button
                      type="submit"
                      variant="outlined"
                      sx={{
                        border: "2px solid #2B90D9",
                        textTransform: "none",
                        borderRadius: "12px",
                        height: "35px",
                        width: "115px",
                        color: "#000000",
                      }}
                    >
                      Thêm
                    </Button>
                    <Button
                      variant="contained"
                      color="error"
                      sx={{
                        backgroundColor: "#FF0A0A",
                        textTransform: "none",
                        borderRadius: "12px",
                        height: "35px",
                        width: "115px",
                        color: "#ffffff",
                      }}
                      onClick={() => setShowModal(false)}
                    >
                      Đóng
                    </Button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </>
  );
};

export default ModalAddPost;
