import React, { useState } from "react";
import { Button } from "@mui/material";
import userStore from "../../store/userStore";
import { toast } from "react-toastify";

const ModalAddSkill = () => {
  const [showModal, setShowModal] = useState(false);
  const { addSkill, fetchSkills } = userStore();

  const [skillName, setSkillName] = useState("");
  const [skillDescription, setSkillDescription] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    const skill = {
      skillName,
      skillDescription,
    };
    try {
      await addSkill(skill);
      setShowModal(false);
      toast.success("Thêm kỹ năng thành công!");
      await fetchSkills();
    } catch (error) {
      toast.warning(error.data[0]);
    }
  };

  return (
    <>
      <Button
        variant="outlined"
        sx={{
          border: "2px solid #4B5563",
          padding: "8px 12px",
          borderRadius: "8px",
          boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.1)",
          margin: "12px 0px 0px 0px",
          "&:hover": {
            boxShadow: "none",
          },
          color: "#000000",
          textTransform: "none",
          height: "29px",
          width: "120px",
        }}
        type="button"
        onClick={() => setShowModal(true)}
      >
        Thêm mới
      </Button>
      {showModal ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-[30%] my-6 mx-auto max-w-4xl">
              {/*content*/}
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                <form onSubmit={handleSubmit}>
                  {/*header*/}
                  <div className="flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
                    <h3 className="text-3xl font-semibold">Thêm kỹ năng</h3>
                    <button
                      className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                      onClick={() => setShowModal(false)}
                    >
                      <span className="bg-transparent text-black h-6 w-6 text-2xl block outline-none focus:outline-none">
                        ×
                      </span>
                    </button>
                  </div>
                  {/*body*/}
                  <div className="relative p-6 flex-auto max-h-96 overflow-y-auto">
                    <form className="w-full max-w-md">
                      <div className="md:flex md:items-center mb-6">
                        <div className="md:w-1/3">
                          <label
                            className="labelInput"
                            htmlFor="inline-skill-name"
                          >
                            Tên kỹ năng
                          </label>
                        </div>
                        <div className="md:w-2/3">
                          <input
                            className="inputField"
                            id="inline-skill-name"
                            type="text"
                            value={skillName}
                            onChange={(e) => setSkillName(e.target.value)}
                            required
                          />
                        </div>
                      </div>
                      <div className="md:flex md:items-center mb-6">
                        <div className="md:w-1/3">
                          <label
                            className="labelInput"
                            htmlFor="inline-description"
                          >
                            Mô tả
                          </label>
                        </div>
                        <div className="md:w-2/3">
                          <textarea
                            className="inputField"
                            id="inline-description"
                            value={skillDescription}
                            onChange={(e) =>
                              setSkillDescription(e.target.value)
                            }
                            rows={5}
                            required
                            maxLength={255}
                          />
                        </div>
                      </div>
                    </form>
                  </div>
                  {/*footer*/}
                  <div className="btnInModal">
                    <Button
                      type="submit"
                      variant="outlined"
                      sx={{
                        border: "2px solid #2B90D9",
                        textTransform: "none",
                        borderRadius: "12px",
                        height: "35px",
                        width: "115px",
                        color: "#000000",
                      }}
                    >
                      Thêm
                    </Button>
                    <Button
                      variant="contained"
                      color="error"
                      sx={{
                        backgroundColor: "#FF0A0A",
                        textTransform: "none",
                        borderRadius: "12px",
                        height: "35px",
                        width: "115px",
                        color: "#ffffff",
                      }}
                      onClick={() => setShowModal(false)}
                    >
                      Đóng
                    </Button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </>
  );
};

export default ModalAddSkill;
