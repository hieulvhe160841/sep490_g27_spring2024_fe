import React, { useState } from "react";
import { Button } from "@mui/material";
import userStore from "../../store/userStore";

const ModalViewImg = (cert) => {
  const [showModal, setShowModal] = useState(false);
  const { baseUrl } = userStore();

  return (
    <>
      <Button
        variant="outlined"
        sx={{
          border: "2px solid #4B5563",
          padding: "8px 12px",
          borderRadius: "12px",
          boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.1)",
          margin: "12px 0px 0px 0px",
          "&:hover": {
            boxShadow: "none",
          },
          color: "#000000",
          textTransform: "none",
          height: "35px",
          width: "115px",
        }}
        type="button"
        onClick={() => setShowModal(true)}
      >
        Xem
      </Button>
      {showModal ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative my-6 mx-auto max-w-6xl">
              {/*content*/}
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                <div>
                  {/*header*/}

                  {/*body*/}

                  <img
                    src={`${baseUrl}/view/${cert.cert.certificateUrl}`}
                    className="w-[600px] h-[650px]"
                  ></img>

                  <></>
                  {/*footer*/}
                  <div className="btnInModal">
                    <Button
                      variant="contained"
                      color="error"
                      sx={{
                        backgroundColor: "#FF0A0A",
                        textTransform: "none",
                        borderRadius: "12px",
                        height: "35px",
                        width: "115px",
                        color: "#ffffff",
                      }}
                      onClick={() => setShowModal(false)}
                    >
                      Đóng
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      ) : null}
    </>
  );
};

export default ModalViewImg;
