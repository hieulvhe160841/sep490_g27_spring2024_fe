import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import userStore from "../../store/userStore";
import LoadingState from "../LoadingState";

const TopRatedMentor = () => {
  const { isLoading, adminDashboard, fetchAdminDashboard, baseUrl } =
    userStore();

  useEffect(() => {
    fetchAdminDashboard();
  }, []);

  return (
    <div className="w-[16rem] bg-white p-4 rounded-sm border border-gray-200">
      <strong className="text-gray-700 font-medium">Gia sư đánh giá cao</strong>
      {isLoading ? (
        <LoadingState />
      ) : adminDashboard && adminDashboard.topRatedMentor ? (
        <div className="mt-4 flex flex-col gap-3">
          {adminDashboard.topRatedMentor.slice(0, 4).map((mentor) => (
            <Link
              key={mentor.userDetail.userId}
              to={`/mentorDetails/${mentor.userDetail.userName}`}
              className="flex items-start hover:no-underline"
            >
              <div className="w-10 h-10 min-w-[2.5rem] bg-gray-200 rounded-sm">
                <img
                  className="w-full h-full object-cover rounded-sm"
                  src={`${baseUrl}/view/${mentor.userDetail.avatar}`}
                  alt={mentor.userDetail.name}
                />
              </div>
              <div className="ml-4 flex-1">
                <p className="text-sm text-gray-800">
                  {mentor.userDetail.name}
                </p>
                <span className={`text-xs font-medium `}>
                  {mentor.userDetail.educationLevel}
                </span>
              </div>
              {mentor.rating ? (
                <div className="text-xs text-gray-600 pl-1.5">
                  Đánh giá:{" "}
                  {mentor.rating.toLocaleString("en-US", {
                    maximumFractionDigits: 1,
                  })}
                </div>
              ) : (
                <p className="text-xs text-gray-600 pl-1.5">Chưa đánh giá</p>
              )}
            </Link>
          ))}
        </div>
      ) : (
        "Lỗi hiển thị mentor"
      )}
    </div>
  );
};

export default TopRatedMentor;
