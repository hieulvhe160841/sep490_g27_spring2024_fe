import React, { useState, useEffect } from "react";
import { PieChart, Pie, Cell, ResponsiveContainer, Legend } from "recharts";
import userStore from "../../store/userStore";
import LoadingState from "../LoadingState";

const RADIAN = Math.PI / 180;
const COLORS = ["#00C49F", "#FFBB28", "#FF8042"];

const renderCustomizedLabel = ({
  cx,
  cy,
  midAngle,
  innerRadius,
  outerRadius,
  percent,
}) => {
  const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy + radius * Math.sin(-midAngle * RADIAN);

  return (
    <text
      x={x}
      y={y}
      fill="white"
      textAnchor={x > cx ? "start" : "end"}
      dominantBaseline="central"
    >
      {`${(percent * 100).toFixed(0)}%`}
    </text>
  );
};

const UserProfilePieChart = () => {
  const { isLoading, adminDashboard, fetchAdminDashboard } = userStore();
  const [chartData, setChartData] = useState([]);

  useEffect(() => {
    fetchAdminDashboard();
  }, []);

  useEffect(() => {
    if (adminDashboard) {
      const data = [
        { name: "Gia sư", value: adminDashboard.numberOfMentors },
        { name: "Học viên", value: adminDashboard.numberOfMentees },
        { name: "Khác", value: adminDashboard.numberOfOthers },
      ];
      setChartData(data);
    }
  }, [adminDashboard]);

  return (
    <div className="w-[16rem] h-[22rem] bg-white p-4 rounded-sm border border-gray-200 flex flex-col">
      <strong className="text-gray-700 font-medium">Người dùng</strong>
      {isLoading || !adminDashboard ? (
        <LoadingState />
      ) : (
        <div className="mt-3 w-full flex-1 text-xs">
          <ResponsiveContainer
            width="100%"
            height="100%"
            data-testid="chart-container"
          >
            <PieChart width={400} height={300}>
              <Pie
                data={chartData}
                cx="50%"
                cy="45%"
                labelLine={false}
                label={renderCustomizedLabel}
                outerRadius={105}
                fill="#8884d8"
                dataKey="value"
              >
                {chartData.map((_, index) => (
                  <Cell
                    key={`cell-${index}`}
                    fill={COLORS[index % COLORS.length]}
                  />
                ))}
              </Pie>
              <Legend />
            </PieChart>
          </ResponsiveContainer>
        </div>
      )}
    </div>
  );
};

export default UserProfilePieChart;
