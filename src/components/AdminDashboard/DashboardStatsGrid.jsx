import React, { useEffect } from "react";
import {
  FaMoneyBillWave,
  FaRegListAlt,
  FaUsers,
  FaRegNewspaper,
} from "react-icons/fa";
import userStore from "../../store/userStore";
import LoadingState from "../LoadingState";

const DashboardStatsGrid = () => {
  const { isLoading, adminDashboard, fetchAdminDashboard } = userStore();

  useEffect(() => {
    fetchAdminDashboard();
  }, []);

  return (
    <div data-testid="dashboard-stats-grid">
      {isLoading ? (
        <LoadingState />
      ) : (
        <div className="flex gap-4">
          <BoxWrapper>
            <div className="rounded-full h-12 w-12 flex items-center justify-center bg-sky-500">
              <FaMoneyBillWave className="text-2xl text-white" />
            </div>
            <div className="pl-4">
              <span className="text-sm text-gray-500">Lợi nhuận</span>
              <div className="flex items-center">
                <strong className="text-xl text-gray-700 font-semibold">
                  {adminDashboard && adminDashboard.totalProfit
                    ? adminDashboard.totalProfit.toLocaleString("en-US", {
                        maximumFractionDigits: 0,
                      })
                    : 0}{" "}
                  VNĐ
                </strong>
                {/* <span className="text-sm text-green-500 pl-2">+343</span> */}
              </div>
            </div>
          </BoxWrapper>
          <BoxWrapper>
            <div className="rounded-full h-12 w-12 flex items-center justify-center bg-orange-600">
              <FaRegListAlt className="text-2xl text-white" />
            </div>
            <div className="pl-4">
              <span className="text-sm text-gray-500">Số yêu cầu</span>
              <div className="flex items-center">
                <strong className="text-xl text-gray-700 font-semibold">
                  {adminDashboard && adminDashboard.totalRequest
                    ? adminDashboard.totalRequest
                    : "Lỗi hiển thị dữ liệu"}
                </strong>
                {/* <span className="text-sm text-green-500 pl-2">-343</span> */}
              </div>
            </div>
          </BoxWrapper>
          <BoxWrapper>
            <div className="rounded-full h-12 w-12 flex items-center justify-center bg-yellow-400">
              <FaUsers className="text-2xl text-white" />
            </div>
            <div className="pl-4">
              <span className="text-sm text-gray-500">Số người dùng</span>
              <div className="flex items-center">
                <strong className="text-xl text-gray-700 font-semibold">
                  <strong className="text-xl text-gray-700 font-semibold">
                    {adminDashboard && adminDashboard.totalUsers
                      ? adminDashboard.totalUsers
                      : "Lỗi hiển thị dữ liệu"}
                  </strong>
                </strong>
                {/* <span className="text-sm text-red-500 pl-2">-30</span> */}
              </div>
            </div>
          </BoxWrapper>
          <BoxWrapper>
            <div className="rounded-full h-12 w-12 flex items-center justify-center bg-green-600">
              <FaRegNewspaper className="text-2xl text-white" />
            </div>
            <div className="pl-4">
              <span className="text-sm text-gray-500">Số bài đăng</span>
              <div className="flex items-center">
                <strong className="text-xl text-gray-700 font-semibold">
                  {adminDashboard && adminDashboard.totalPost
                    ? adminDashboard.totalPost
                    : "Lỗi hiển thị dữ liệu"}
                </strong>
                {/* <span className="text-sm text-red-500 pl-2">-43</span> */}
              </div>
            </div>
          </BoxWrapper>
        </div>
      )}
    </div>
  );
};

function BoxWrapper({ children }) {
  return (
    <div className="bg-white rounded-sm p-4 flex-1 border border-gray-200 flex items-center">
      {children}
    </div>
  );
}
export default DashboardStatsGrid;
