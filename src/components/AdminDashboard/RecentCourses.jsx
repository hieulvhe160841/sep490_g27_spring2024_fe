import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import userStore from "../../store/userStore";
import LoadingState from "../LoadingState";

const RecentCourses = () => {
  const { isLoading, adminDashboard, fetchAdminDashboard } = userStore();
  const [tableData, setTableData] = useState([]);

  useEffect(() => {
    fetchAdminDashboard();
  }, []);

  useEffect(() => {
    if (adminDashboard && adminDashboard.recentCourseForAdminDashBoardList) {
      const recentCourse = adminDashboard.recentCourseForAdminDashBoardList.map(
        (data) => ({
          id: data.courseId,
          course_name: data.courseName,
          created_by: data.viewCourseOwnerDTO.name,
          rating: data.courseRating,
          price: data.price,
          number_of_lessons: data.numbersOfLesson,
          created_date: data.courseCreatedDate,
        })
      );
      setTableData(recentCourse);
    }
  }, [adminDashboard]);

  return (
    <div className="bg-white px-4 pt-3 pb-4 rounded-sm border border-gray-200 flex-1">
      <strong className="text-gray-700 font-medium">Recent Courses</strong>
      {isLoading ? (
        <LoadingState />
      ) : tableData.length > 0 ? (
        <div className="border-x border-gray-200 rounded-sm mt-3">
          <table className="w-full text-gray-700">
            <thead>
              <tr>
                <th>Tên khóa học</th>
                <th>Tên gia sư</th>
                <th>Đánh giá</th>
                <th>Giá tiền</th>
                <th>Số bài học</th>
                <th>Ngày tạo</th>
              </tr>
            </thead>
            <tbody className="text-left">
              {tableData.map((course) => (
                <tr key={course.id}>
                  <td>
                    <Link to={`/admin/approvedCourse/${course.id}`}>
                      {course.course_name}
                    </Link>
                  </td>
                  <td>{course.created_by}</td>
                  <td>{course.rating}</td>
                  <td>
                    {course.price
                      ? course.price.toLocaleString("en-US", {
                          maximumFractionDigits: 0,
                        })
                      : 0}
                    &nbsp;VNĐ
                  </td>
                  <td>{course.number_of_lessons}</td>
                  <td>{course.created_date}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      ) : (
        "Lỗi hiển thị khóa học"
      )}
    </div>
  );
};

export default RecentCourses;
