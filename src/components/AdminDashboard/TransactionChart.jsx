import React, { useEffect, useState } from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";
import userStore from "../../store/userStore";
import LoadingState from "../LoadingState";

const TransactionChart = () => {
  const { isLoading, adminDashboard, fetchAdminDashboard } = userStore();
  const [chartData, setChartData] = useState([]);

  useEffect(() => {
    fetchAdminDashboard();
  }, []);

  useEffect(() => {
    if (adminDashboard && adminDashboard.numberOfUserAndCoursePerMonthDTOList) {
      const monthlyData =
        adminDashboard.numberOfUserAndCoursePerMonthDTOList.map((data) => ({
          Tháng: data.month,
          "Người dùng mới": data.numberOfPeopleRegisteredInMonth,
          "Khóa học mới": data.numberOfCourseCreatedInMonth,
        }));
      setChartData(monthlyData);
    }
  }, [adminDashboard]);

  return (
    <div className="h-[22rem] bg-white p-4 rounded-sm border border-gray-200 flex flex-col flex-1">
      <strong className="text-gray-700 font-medium">Số liệu thống kê</strong>
      {isLoading || !adminDashboard ? (
        <LoadingState />
      ) : (
        <div className="h-[20rem] w-full text-xs">
          <ResponsiveContainer
            width="100%"
            height="100%"
            data-testid="chart-container"
          >
            <BarChart
              width={500}
              height={300}
              data={chartData}
              margin={{
                top: 20,
                right: 10,
                left: -10,
                bottom: 0,
              }}
            >
              <CartesianGrid strokeDasharray="3 3 0 0" vertical={false} />
              <XAxis dataKey="Tháng" />
              <YAxis />
              <Tooltip />
              <Legend />
              <Bar dataKey="Khóa học mới" fill="#0ea5e9" />
              <Bar dataKey="Người dùng mới" fill="#ea580c" />
            </BarChart>
          </ResponsiveContainer>
        </div>
      )}
    </div>
  );
};

export default TransactionChart;
