import React from "react";

const ButtonComponent = (props) => {
  return (
    <button
      type="submit"
      className="mx-auto w-full rounded-xl bg-sidebarTop px-3 py-3 text-sm font-semibold text-white"
      onClick={props.onClick}
    >
      {props.out}
    </button>
  );
};
export default ButtonComponent;
