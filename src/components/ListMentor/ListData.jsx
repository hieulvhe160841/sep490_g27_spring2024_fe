import React, { useEffect } from "react";
import { Button } from "@mui/material";
import menteeStore from "../../store/menteeStore";
import userStore from "../../store/userStore";
import { Link } from "react-router-dom";
import LoadingState from "../LoadingState";

function ListData() {
  const { fetchAllMentors, listMentors, isLoading } = menteeStore();
  const { baseUrl } = userStore();
  useEffect(() => {
    fetchAllMentors();
  }, []);

  return (
    <>
      {isLoading ? (
        <LoadingState />
      ) : (
        <div className="md:col-span-6 md:col-start-3 w-full md:mx-auto md:px-20 ">
          <ul role="list" className="divide-y divide-gray-500">
            {listMentors.map((mentor) => (
              <li
                key={mentor.mentorDetail.userDetail.userId}
                className="flex flex-col lg:flex-row justify-between gap-x-6 py-10 gap-y-5"
              >
                <div className="flex min-w-0 gap-x-4">
                  <img
                    className="md:h-20 md:w-20 w-10 h-10 flex-none rounded-full bg-gray-50"
                    src={`${baseUrl}/view/${mentor.mentorDetail.userDetail.avatar}`}
                    alt={mentor.mentorDetail.userDetail.name}
                  />
                  <div className="min-w-0 flex-auto">
                    <p className="text-lg font-semibold leading-6 text-gray-900">
                      {mentor.mentorDetail.userDetail.name}
                    </p>
                    <p className="text-sm mt-1 truncate text-gray-700">
                      <span className="text-sx font-semibold">Đánh giá:</span>{" "}
                      {mentor.rating}
                    </p>
                  </div>
                </div>
                <div className="shrink-0 lg:flex lg:items-center space-x-4">
                  <Button variant="outlined">Yêu cầu</Button>
                  <Link
                    to={`/mentorDetails/${mentor.mentorDetail.userDetail.userName}`}
                  >
                    <Button variant="contained">Chi tiết</Button>
                  </Link>
                </div>
              </li>
            ))}
          </ul>
        </div>
      )}
    </>
  );
}

export default ListData;
