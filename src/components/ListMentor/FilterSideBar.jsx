import React, { useState } from "react";
import { Button, Checkbox, FormControlLabel, FormGroup } from "@mui/material";

function FilterSideBar() {
  const [skill, setSkill] = useState([]);
  const onHandleChooseSKill = (event) => {
    const value = event.target.value;
    const check = event.target.checked;

    if (check) {
      setSkill((pre) => [...pre, value]);
    } else {
      setSkill((pre) => {
        return [...pre.filter((skill) => !skill === value)];
      });
    }
  };
  return (
    <>
      <div className="md:col-span-2 text-center">
        <div
          open
          className="w-full overflow-hidden text-gray-700 hidden md:block"
        >
          <div className="w-full border-t border-gray-200 md:border-t-0">
            <div className=" bg-white p-6 pb-10 text-gray-900">
              <p className="text-lg font-medium">Tìm kiếm theo:</p>
              <div className="mt-4 flex items-center rounded-lg">
                <label for="priceFrom" className="mr-2 font-medium">
                  Giá từ:
                </label>
                <input
                  type="number"
                  name="price"
                  placeholder="from"
                  className="p-1 border-2 border-gray-400 rounded-sm w-1/4"
                ></input>
                <label for="priceTo" className="mx-2 font-medium">
                  đến
                </label>
                <input
                  type="number"
                  name="priceTo"
                  placeholder="to"
                  className="p-1 border-2 border-gray-400 rounded-sm w-1/4"
                ></input>
              </div>
              <div className="mt-4 flex items-center rounded-lg">
                <label for="language" className="mr-2 ml-1 font-medium">
                  Ngôn ngữ:
                </label>
                <FormGroup row>
                  <FormControlLabel
                    control={<Checkbox size="small" />}
                    value="english"
                    label="English"
                    onChange={(event) => onHandleChooseSKill(event)}
                  />
                  <FormControlLabel
                    control={<Checkbox size="small" />}
                    value="korean"
                    label="Korean"
                    onChange={(event) => onHandleChooseSKill(event)}
                  />
                </FormGroup>
              </div>
              <div className="mt-4 mb-7 flex items-center rounded-lg">
                <label for="rating" className="mr-2 ml-1 font-medium">
                  Đánh giá:
                </label>
                <input
                  type="number"
                  name="rating"
                  min={1}
                  max={5}
                  className="p-1 border-2 border-gray-400 rounded-sm w-1/4"
                ></input>
              </div>

              <Button variant="contained" className="w-2/3 m-auto">
                Áp dụng
              </Button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default FilterSideBar;
