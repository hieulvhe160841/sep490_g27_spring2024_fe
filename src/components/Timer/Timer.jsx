import React, { useState, useEffect } from "react";
import Button from "@mui/material/Button";

const Timer = () => {
  const { id } = useParams();
  const [time, setTime] = useState(0);
  const [sessionStatus, setSessionStatus] = useState("Bắt đầu");
  const [isSessionActive, setIsSessionActive] = useState(false);
  const {
    confirmSupportStartMentee,
    confirmSupportEnd,
    fetchSupportByIdMentee,
  } = menteeStore();
  const { confirmSupportStartMentor, fetchSupportByIdMentor } = mentorStore();
  const userRole = localStorage.getItem("role");
  const [isSessionStarted, setIsSessionStarted] = useState(false);

  const saveTimerState = (currentTime) => {
    localStorage.setItem("timer", JSON.stringify(currentTime));
  };

  useEffect(() => {
    let timer;

    if (isSessionActive) {
      timer = setInterval(() => {
        setTime((prevTime) => prevTime + 1);
      }, 1000);
    } else {
      clearInterval(timer);
    }

    return () => clearInterval(timer);
  }, [isSessionActive]);

  const handleStart = () => {
    if (!isSessionActive && sessionStatus === "Bắt đầu") {
      setTime(0); // Reset the timer when starting a new session
      setIsSessionActive(true);
      setSessionStatus("Đang chạy");
      setBorderColor("border-blue-500");
    } else if (sessionStatus === "Dừng") {
      setIsSessionActive(false);
      setSessionStatus("Tiếp tục");
    } else {
      setIsSessionActive(true);
      setSessionStatus("Dừng");
    }
  };

  const formatTime = (timeInSeconds) => {
    const minutes = Math.floor(timeInSeconds / 60);
    const seconds = timeInSeconds % 60;
    return `${minutes.toString().padStart(2, "0")}:${seconds
      .toString()
      .padStart(2, "0")}`;
  };

  return (
    <div className={`flex flex-col items-center`}>
      <div className={`border rounded p-5 text-center`}>
        <div className="text-5xl mb-4">{formatTime(time)}</div>
        <div className="flex">
          <Button
            variant="contained"
            color="primary"
            onClick={handleStart}
            disabled={
              sessionStatus === "Waiting" || sessionStatus === "Đang chạy"
            }
            sx={{ mr: 2 }}
          >
            {sessionStatus}
          </Button>
          <Button
            variant="outlined"
            color="error"
            disabled={!isSessionActive}
            onClick={() => {
              setIsSessionActive(false);
              setTime(0); // Reset the timer when ending the session
              setSessionStatus("Bắt đầu");
              setBorderColor("border-gray-300"); // Reset border color
            }}
          >
            Kết thúc
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Timer;
