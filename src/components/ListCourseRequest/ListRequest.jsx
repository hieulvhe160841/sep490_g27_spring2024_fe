import React, { useEffect, useState } from "react";
import mentorStore from "../../store/mentorStore";
import userStore from "../../store/userStore";
import LoadingState from "../LoadingState";
import { Link } from "react-router-dom";
import { Button } from "@mui/material";
import Pagination from "@mui/material/Pagination";

function ListRequest() {
  const { listCourseRequest, listCourseOfMentor, isLoading } = mentorStore();
  const { baseUrl } = userStore();
  const [currentPage, setCurrentPage] = useState(1);
  const [showModal, setShowModal] = useState(false);
  const pageSize = 3;

  useEffect(() => {
    listCourseRequest();
  }, []);
  const handlePageChange = (event, page) => {
    setCurrentPage(page);
  };
  let paginatedRequests = [];

  if (listCourseOfMentor && listCourseOfMentor.length > 0) {
    const startIndex = (currentPage - 1) * pageSize;
    const endIndex = Math.min(startIndex + pageSize, listCourseOfMentor.length);
    paginatedRequests = listCourseOfMentor.slice(startIndex, endIndex);
  }

  const statusToVietnamese = (status) => {
    switch (status) {
      case "accepted":
        return "Đã duyệt";
      case "rejected":
        return "Đã từ chối";
      case "Pending":
        return "Chờ phê duyệt";
      default:
        return status;
    }
  };

  console.log(listCourseOfMentor);
  return (
    <div className="w-full md:mx-auto md:px-10 mt-10">
      {isLoading ? (
        <LoadingState />
      ) : (
        <div>
          <ul role="list" className="divide-y divide-gray-500">
            {console.log(listCourseOfMentor)}
            {listCourseOfMentor.length > 0 ? (
              paginatedRequests.map((list) => (
                <li
                  key={list.courseRequestId}
                  className="flex flex-col lg:flex-row justify-between gap-x-6 py-5 gap-y-5"
                >
                  <div className="flex gap-x-4 w-full items-center ">
                    <img
                      className="md:h-28 md:w-36 w-20 h-12 flex-none bg-gray-50 rounded-md"
                      src={`${baseUrl}/view/${list.courseAvatar}`}
                      alt=""
                    />
                    <div className="flex flex-col gap-y-6 w-full">
                      <div>
                        <p className="text-xl font-bold leading-6 text-sidebarTop">
                          Tên khóa học:{" "}
                          <span className="text-gray-700">
                            {list.courseName}
                          </span>
                        </p>
                      </div>

                      <p className="text-lg font-semibold leading-6 text-sidebarTop">
                        Trạng thái:{" "}
                        <span className="text-gray-700">
                          {statusToVietnamese(list.courseRequestStatus)}
                        </span>
                      </p>
                    </div>
                  </div>
                  <div className="shrink-0 flex flex-row lg:items-center space-x-10">
                    <Button
                      variant="contained"
                      size="large"
                      sx={"border-radius: 15px"}
                      onClick={() => setShowModal(true)}
                    >
                      Xem lí do
                    </Button>
                    {showModal && (
                      <>
                        <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
                          <div className="relative w-[30%] my-6 mx-auto max-w-4xl">
                            {/*content*/}
                            <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                              {/*header*/}
                              <div className="flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
                                <h3 className="text-3xl font-semibold">
                                  Xem lí do
                                </h3>
                                <button
                                  className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                                  onClick={() => setShowModal(false)}
                                >
                                  <span className="bg-transparent text-black h-6 w-6 text-2xl block outline-none focus:outline-none">
                                    ×
                                  </span>
                                </button>
                              </div>
                              {/*body*/}
                              <div className="relative p-6 flex-auto max-h-96 overflow-y-auto">
                                <div className="md:flex md:items-center mb-6">
                                  <div className="md:w-full">
                                    <textarea
                                      className="inputField"
                                      id="inline-content"
                                      defaultValue={list.courseRequestFeedback}
                                      disabled
                                      rows={5}
                                    />
                                  </div>
                                </div>
                              </div>
                              {/*footer*/}
                              <div className="btnInModal">
                                <Button
                                  variant="contained"
                                  color="error"
                                  sx={{
                                    backgroundColor: "#FF0A0A",
                                    textTransform: "none",
                                    borderRadius: "12px",
                                    height: "35px",
                                    width: "115px",
                                    color: "#ffffff",
                                  }}
                                  onClick={() => setShowModal(false)}
                                >
                                  Đóng
                                </Button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                      </>
                    )}
                  </div>
                </li>
              ))
            ) : (
              <div className="text-3xl text-center mt-4">
                Bạn chưa có khóa học nào :(
              </div>
            )}
          </ul>
          {listCourseOfMentor && listCourseOfMentor.length > 0 && (
            <Pagination
              count={Math.ceil(listCourseOfMentor.length / pageSize)}
              page={currentPage}
              onChange={handlePageChange}
              color="primary"
              className="flex flex-col items-center"
            />
          )}
        </div>
      )}
    </div>
  );
}

export default ListRequest;
