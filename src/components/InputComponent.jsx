import React from "react";

const InputComponent = (props) => {
  return (
    <input
      id={props.id}
      name={props.name}
      type={props.type}
      placeholder={props.placeholder}
      required
      className="block w-full rounded-xl border-0 py-4 pl-14 bg-gray-300 font-normal shadow-sm"
      onChange={props.onChange}
      onKeyDown={props.onKeyDown}
    />
  );
};
export default InputComponent;
