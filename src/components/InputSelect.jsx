const InputSelect = (props) => {
  const arcademy = props.arcademy;

  return (
    <>
      <select
        id="countries"
        className="border border-black w-full p-4 rounded-lg bg-emerald-50"
      >
        {arcademy.map((a) => (
          <option value={a.id}>{a.value}</option>
        ))}
      </select>
    </>
  );
};
export default InputSelect;
