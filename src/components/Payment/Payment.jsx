import React, { useEffect, useState } from "react";
import { Button } from "@mui/material";
import { BiMoney } from "react-icons/bi";
import { toast } from "react-toastify";
import { Link } from "react-router-dom";
import TitleHeader from "../TitleHeader";
import menteeStore from "../../store/menteeStore";
import userStore from "../../store/userStore";
import LoadingState from "../LoadingState";
import { CgSpinnerAlt } from "react-icons/cg";

function Payment() {
  const { fetchListDenomination, listDenomination } = menteeStore();
  const {
    fetchUserWallet,
    userBalance,
    createPaymentApi,
    isLoading,
    isLoadingPayment,
  } = userStore();
  const [price, setPrice] = useState();

  useEffect(() => {
    async function fetchData() {
      try {
        await fetchUserWallet();
        await fetchListDenomination();
      } catch (error) {
        console.error("An error occurred during fetching:", error);
      }
      console.log(listDenomination, userBalance);
    }

    fetchData();
  }, []);

  const onHandleCreatePayment = async () => {
    const res = await createPaymentApi(
      price.denominationId,
      price.value,
      price.amount
    );
    if (res.statusCode === 201) {
      toast.success("Tạo thanh toán thành công");
      window.location.href = `${res.data.links[1].href}`;
    } else {
      toast.error("Tạo thanh toán không thành công");
    }
    console.log(res);
  };

  return (
    <>
      {isLoading ? (
        <LoadingState />
      ) : listDenomination && listDenomination.length > 0 ? (
        <div className="w-full items-center">
          <div className="flex justify-between my-10 bg-gradient-to-r from-[#478fc3d2] to-[#174C73] p-10 mx-auto rounded-3xl shadow-xl w-4/5">
            <div className="text-2xl text-white font-medium">Số dư</div>
            <div className="flex text-3xl text-white">
              {userBalance.data.balance
                ? userBalance.data.balance.toLocaleString("en-US", {
                    maximumFractionDigits: 0,
                  })
                : 0}
              &nbsp;VNĐ
            </div>
          </div>
          <TitleHeader title="Chọn mệnh giá để nạp" />
          <div className="flex justify-center text-base italic mt-8">
            Vui lòng chọn một trong số các mệnh giá dưới đây để nạp tiền vào tài
            khoản
          </div>
          <div className="md:flex md:space-x-20 justify-center md:mt-10 mt-5 w-full">
            <div className="flex flex-col md:space-y-10">
              {listDenomination.slice(0, 3).map((option, index) => (
                <div key={index} className="flex items-center justify-between">
                  <div className="flex items-center">
                    <input
                      type="radio"
                      name="priceOption"
                      id={`priceOption${index}`}
                      className="h-5 w-5"
                      onChange={() => setPrice(option)}
                    />
                    <label htmlFor={`priceOption${index}`} className="ml-2">
                      <span className="font-semibold text-lg text-sidebarTop">
                        {option.amount
                          ? option.amount.toLocaleString("en-US", {
                              maximumFractionDigits: 0,
                            })
                          : 0}
                        &nbsp;VNĐ
                      </span>
                    </label>
                  </div>
                </div>
              ))}
            </div>
            <div className="flex flex-col md:space-y-10">
              {listDenomination.slice(3, 6).map((option, index) => (
                <div key={index} className="flex items-center justify-between">
                  <div className="flex items-center">
                    <input
                      type="radio"
                      name="priceOption"
                      id={`priceOption${index}`}
                      className="h-5 w-5"
                      onChange={() => setPrice(option)}
                    />
                    <label htmlFor={`priceOption${index}`} className="ml-2">
                      <span className="font-semibold text-lg text-sidebarTop">
                        {option.amount
                          ? option.amount.toLocaleString("en-US", {
                              maximumFractionDigits: 0,
                            })
                          : 0}
                        &nbsp;VNĐ
                      </span>
                    </label>
                  </div>
                </div>
              ))}
            </div>
          </div>
          <div className="flex justify-center mt-14">
            <Button
              variant="contained"
              disabled={isLoadingPayment ? true : false}
              onClick={() => onHandleCreatePayment()}
            >
              Nạp&nbsp;
              {isLoadingPayment && (
                <svg
                  className="animate-spin h-5 w-5 mr-5 text-white ..."
                  viewBox="0 0 17 17"
                >
                  <CgSpinnerAlt />
                </svg>
              )}
            </Button>
          </div>
        </div>
      ) : (
        <div>Bạn không thể giao dịch</div>
      )}
    </>
  );
}

export default Payment;
