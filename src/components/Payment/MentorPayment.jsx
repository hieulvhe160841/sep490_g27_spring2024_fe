import React, { useEffect, useState } from "react";
import userStore from "../../store/userStore";
import LoadingState from "../LoadingState";

function MentorPayment() {
  const { fetchUserWallet, userBalance, isLoading } = userStore();

  useEffect(() => {
    async function fetchData() {
      try {
        await fetchUserWallet();
      } catch (error) {
        console.error("An error occurred during fetching:", error);
      }
      console.log(userBalance);
    }

    fetchData();
  }, []);

  return (
    <>
      {isLoading ? (
        <LoadingState />
      ) : (
        <div className="w-full items-center">
          <div className="flex justify-between my-10 bg-gradient-to-r from-[#478fc3d2] to-[#174C73] p-10 mx-auto rounded-3xl shadow-xl w-4/5">
            <div className="text-2xl text-white font-medium">Số dư</div>
            <div className="flex text-3xl text-white">
              {userBalance.data
                ? userBalance.data.balance.toLocaleString("en-US", {
                    maximumFractionDigits: 0,
                  })
                : 0}
              &nbsp;VNĐ
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export default MentorPayment;
