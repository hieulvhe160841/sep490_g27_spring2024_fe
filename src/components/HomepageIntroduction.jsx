import Book from "../../src/assets/Book.png";
import Course from "../../src/assets/Course.png";
import Screenshot1 from "../../src/assets/Screenshot1.png";
import Screenshot2 from "../../src/assets/Screenshot2.png";
import { Link } from "react-router-dom";

const HomepageIntroduction = () => {
  return (
    <>
      <div className="container flex justify-center items-center w-full flex-col lg:flex-row">
        <div className="introduction justify-items-center basis-3/5 md:ml-30">
          <div className="flex">
            <img className="md:p-1 lg:ml-5 md:w-15 md:h-20" src={Book} />
            <div className="text-4xl font-bold text-sidebarTop items-center justify-center text-shadow-lg uppercase p-2 md:p-4 text-center md:text-left">
              Các khóa học chi tiết
            </div>
          </div>
          <div className="text-xl lg:h-1/2 p-2 md:p-5 mb-2 rounded-t-lg h-auto w-auto text-center md:text-left">
            Hãy đắm mình vào thế giới của sự thành thạo ngôn ngữ với các khóa
            học toàn diện của chúng tôi, được thiết kế để phục vụ cho người học
            ở mọi cấp độ, từ người mới bắt đầu đến những người đam mê nâng cao.
            Nâng cao hành trình của bạn với dịch vụ gia sư cá nhân, nơi các
            chuyên gia ngôn ngữ dày dạn kinh nghiệm của chúng tôi cung cấp sự
            hướng dẫn 1-1, các chiến lược được cá nhân hóa, và động lực bạn cần
            để đạt được sự trôi chảy và tự tin khi sử dụng ngoại ngữ. Cùng nhau,
            các khóa học và sự cố vấn của chúng tôi mở ra một con đường độc đáo,
            hiệu quả không chỉ để học một ngôn ngữ mới, mà thực sự sống trong
            nó.
          </div>
          <div className="flex justify-center md:justify-start md:p-5">
            <Link
              to="/suggestCourse"
              className="rounded-full bg-sidebarTop px-5 py-2 text-md text-white"
            >
              Tìm hiểu thêm
            </Link>
          </div>
        </div>
        <img
          className="mb-2 md:basis-2/5 mt-5 md:p-1 lg:ml-10 md:w-16 md:h-50"
          src={Screenshot1}
        />
      </div>

      <div className="container flex justify-center items-center w-full flex-col lg:flex-row mt-10">
        <img
          className="mb-2 md:basis-2/5 mt-5 md:p-1 lg:mr-10 md:w-16 md:h-50"
          src={Screenshot2}
        />
        <div className="introduction justify-items-center basis-3/5 pl-10 md:mr-30">
          <div className="flex">
            <img className="md:p-1 lg:ml-5 md:w-15 md:h-20" src={Course} />
            <div className="text-4xl font-bold text-sidebarTop text-shadow-lg uppercase p-2 md:p-4 text-center md:text-left">
              Gia sư
            </div>
          </div>
          <div className="text-xl lg:h-1/2 p-2 md:p-5 mb-2 rounded-t-lg h-auto w-auto text-center md:text-left">
            Trong thế giới học tập ngày nay, gia sư không chỉ là người truyền
            đạt kiến thức, mà còn là người bạn đồng hành, người hướng dẫn tận
            tâm giúp học viên không chỉ hiểu bài học mà còn áp dụng chúng vào
            thực tiễn. Gia sư là cầu nối giữa sách vở và cuộc sống, giữa lý
            thuyết và thực hành, mang lại cho học viên cái nhìn toàn diện và sâu
            sắc về thế giới xung quanh. Họ là ngọn đuốc soi đường, giúp học viên
            khám phá ra khả năng tiềm ẩn và đạt được những mục tiêu học tập cao
            cả.
          </div>
          <div className="flex justify-center md:justify-start md:p-5">
            <Link
              to="/topRateMentor"
              className="rounded-full bg-sidebarTop px-5 py-2 text-md text-white"
            >
              Tìm hiểu thêm
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};
export default HomepageIntroduction;
