import React from "react";
import { GrPrevious } from "react-icons/gr";
import { GrNext } from "react-icons/gr";

function ArrowForData(props) {
  const slider = props.slider;
  return (
    <>
      <button
        onClick={() => slider.current.slickPrev()}
        className="absolute top-40 left-[-50px]"
      >
        <GrPrevious size={"40px"} color="gray" />
      </button>
      <button
        onClick={() => slider.current.slickNext()}
        className="absolute top-40 right-[-50px]"
      >
        <GrNext size={"40px"} color="gray" />
      </button>
    </>
  );
}

export default ArrowForData;
