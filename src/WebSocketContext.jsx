import React, { createContext, useEffect, useContext, useState } from "react";
import SockJS from "sockjs-client/dist/sockjs";
import Stomp from "stompjs";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import menteeStore from "./store/menteeStore";
import mentorStore from "./store/mentorStore";

const WebSocketContext = createContext(null);

export const useWebSocket = () => {
  return useContext(WebSocketContext);
};

export const WebSocketProvider = ({ children }) => {
  const [stompClient, setStompClient] = useState(null);
  const currentUsername = localStorage.getItem("user");
  const [modalOpen, setModalOpen] = useState(false);
  const [supportId, setSupportId] = useState(null);
  const navigate = useNavigate();
  const role = localStorage.getItem("role");
  const { fetchSupportByIdMentee } = menteeStore();
  const { fetchSupportByIdMentor } = mentorStore();

  const handleNavigate = async () => {
    setModalOpen(false);
    const menteeSupport = await fetchSupportByIdMentee(supportId);
    const mentorSupport = await fetchSupportByIdMentor(supportId);

    if (role === "ROLE_MENTOR") {
      if (mentorSupport.data.postContent) {
        navigate(`/mentor/postSession/${supportId}`);
      } else {
        navigate(`/mentor/requestSession/${supportId}`);
      }
    } else {
      if (menteeSupport.data.postContent) {
        navigate(`/mentee/postSession/${supportId}`);
      } else {
        navigate(`/mentee/requestSession/${supportId}`);
      }
    }
  };

  const handleClose = () => {
    setModalOpen(false);
  };

  useEffect(() => {
    const socket = new SockJS("https://54.169.171.52/oms");
    const client = Stomp.over(socket);

    client.connect({}, () => {
      console.log("Connected to WebSocket server");
      client.subscribe("/topic/notifySessionStart", (message) => {
        const notification = JSON.parse(message.body);
        setSupportId(notification.supportId);
        if (notification.username === currentUsername) {
          setModalOpen(true);
        }
      });
      setStompClient(client);
    });

    return () => {
      if (client) {
        client.disconnect();
      }
    };
  }, []);

  return (
    <>
      <WebSocketContext.Provider value={stompClient}>
        {children}
      </WebSocketContext.Provider>

      <Dialog
        open={modalOpen}
        onClose={() => setModalOpen(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Thông báo"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Gia sư/Học viên đã bắt đầu phiên gia sư với bạn!
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleNavigate}>Vào ngay thôi</Button>
          <Button onClick={handleClose} autoFocus>
            Để sau
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};
