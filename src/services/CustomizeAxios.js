import axios from "axios";

const instance = axios.create({
  baseURL: "https://13.229.154.151",
});

// Add a request interceptor
instance.interceptors.request.use(
  function (config) {
    // Get the token from local storage or wherever you store it
    // const token = localStorage.getItem('token');
    const token = localStorage.getItem("token");
    // If token exists, add it to the request headers
    if (token !== null && token !== "null") {
      config.headers = { Authorization: `Bearer ${token}` };
    }
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

// Add a response interceptor
instance.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    const currdate = new Date();
    const enddate = new Date(localStorage.getItem("expire"));
    const endRefreshDate = new Date(localStorage.getItem("refreshExpire"));

    const onHandleDateExpired = () => {
      if (currdate > enddate) {
        return true;
      } else {
        return false;
      }
    };
    const onHandleRefreshDateExpired = () => {
      if (currdate > endRefreshDate) {
        return true;
      } else {
        return false;
      }
    };

    const refreshToken = localStorage.getItem("refreshToken");
    if (error.response.status === 403) {
      if (!onHandleRefreshDateExpired()) {
        if (onHandleDateExpired()) {
          return axios
            .post(`https://13.229.154.151/auth/refresh-token`, {
              refreshToken,
            })
            .then(
              (res) => {
                console.log(res);
                if (res.status === 200) {
                  localStorage.setItem("token", res.data.data.token);
                  localStorage.setItem(
                    "refreshToken",
                    res.data.data.refreshToken
                  );
                  localStorage.setItem("expire", res.data.data.expire);
                  console.log("token", res.data.data.token);
                  console.log("refreshToken", res.data.data.refreshToken);

                  config.headers = { Authorization: `Bearer ${token}` };
                  // Continue the promise chain
                  return instance(error.config);
                }
              },
              (error) => {
                // Handle the error here
                if (error.response) {
                  console.log("Response error:", error.response.status);
                }

                console.log("fail");
              }
            );
        }
      } else {
        console.log("refresh token expired");
        console.log(endRefreshDate);

        localStorage.clear();
        window.location.href = "/login";
        return Promise.reject(error);
      }
    }
    return Promise.reject(error);
  }
);

export default instance;
