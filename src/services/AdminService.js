import { idID } from "@mui/material/locale";
import axios from "./CustomizeAxios";

const fetchAllUserService = async () => {
    try {
        const response = await axios.get(`/admin/listUser`);
        return response.data;
    } catch (error) {
        console.error('Error fetching all users:', error);
        throw error;
    }
};

const fetchUserDetailService = async (username) => {
  try {
    const response = await axios.get(`/admin/userDetail/${username}`);
    return response.data;
  } catch (error) {
    console.error("Error fetching user by username:", error);
    throw error;
  }
};

const fetchSkillService = async () => {
    try {
        const response = await axios.get(`/skillList`);
        return response.data;
    } catch (error) {
        console.error('Error fetching all skills:', error);
        throw error;
    }
};

const fetchSkillDetailService = async (id) => {
  try {
    const response = await axios.get(`/admin/skillDetail/${id}`);
    return response.data;
  } catch (error) {
    console.error("Error fetching skill by id:", error);
    throw error;
  }
};

const fetchMentorRegisService = async () => {
    try {
        const response = await axios.get(`/educationStaff/mentorRequestList`);
        return response.data;
    } catch (error) {
        console.error('Error fetching all mentorRegis:', error);
        throw error;
    }
};

const fetchMentorRegisDetailService = async (userId, requestId) => {
  try {
    const response = await axios.get(`/educationStaff/mentorRequestDetail/userId/${userId}/requestId/${requestId}`);
    return response.data;
  } catch (error) {
    console.error("Error fetching MentorRegis by userId and requestId:", error);
    throw error;
  }
};

const fetchAdminDashboardService = async () => {
    try {
        const response = await axios.get(`/admin/dashBoard`);
        return response.data;
    } catch (error) {
        console.error('Error fetching admin dashboard service:', error);
        throw error;
    }
};

const fetchCourseRegistService = async () => {
    try {
        const response = await axios.get(`/educationStaff/courseRequestList`);
        return response.data;
    } catch (error) {
        console.error('Error fetching course registration:', error);
        throw error;
    }
};

const fetchCourseRegistDetailService = async (courseRegisId) => {
  try {
    const response = await axios.get(`/educationStaff/viewCourseRequest/${courseRegisId}`);
    return response.data;
  } catch (error) {
    console.error("Error fetching course registration detail by Id:", error);
    throw error;
  }
};

const fetchPostService = async () => {
    try {
        const response = await axios.get(`/admin/postList`);
        return response.data;
    } catch (error) {
        console.error('Error fetching all posts:', error);
        throw error;
    }
};

const fetchPostDetailService = async (postId) => {
  try {
    const response = await axios.get(`/admin/viewPostDetail/${postId}`);
    return response.data;
  } catch (error) {
    console.error("Error fetching post detail by Id:", error);
    throw error;
  }
};

const fetchMentorshipRequestService = async (status, startDate, endDate) => {
    try {
        const response = await axios.get(`/educationStaff/support-list`, {
            params: {
                status,
                startDate,
                endDate
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error fetching all mentorship request:', error);
        throw error;
    }
};

const fetchMentorshipRequestByIdService = async (supportId) => {
    try {
        const response = await axios.get(`/admin/supportDetail/${supportId}`);
        return response.data;
    } catch (error) {
        console.error('Error fetching mentorship request by Id:', error);
        throw error;
    }
};

const fetchTransactionService = async () => {
    try {
        const response = await axios.get(`/transaction-staff/transaction-list`);
        return response.data;
    } catch (error) {
        console.error('Error fetching transaction:', error);
        throw error;
    }
};

const addStaffService = async (staffData) => {
  try {
    const response = await axios.post(`admin/addStaff`, staffData);
    return response.data;
  } catch (error) {
    throw error.response.data;
  }
};

const addSkillService = async (skillData) => {
  try {
    const response = await axios.post(`admin/addSkill`, skillData);
    return response.data;
  } catch (error) {
    throw error.response.data;
  }
};

const updateUserStatusService = async (username, status) => {
  try {
    const response = await axios.put(`/admin/listUser/${username}`, status);
    return response.data;
  } catch (error) {
    throw error.response.data;
  }
};

const updateSkillService = async (id, skillData) => {
  try {
    const response = await axios.put(`/admin/updateSkill/${id}`, skillData);
    return response.data;
  } catch (error) {
    throw error.response.data;
  }
};

const updatePostStatusService = async (postId, status) => {
  try {
    const response = await axios.put(`/admin/enableOrDisablePost/postId/${postId}`, status);
    return response.data;
  } catch (error) {
    throw error.response.data;
  }
};

const approveCourseRequestService = async (courseId, feedback, status) => {
  try {
    const response = await axios.put(`/educationStaff/approveCourseRequest/${courseId}`, {
      feedback: feedback,
      status: status
    });
    return response.data;
  } catch (error) {
    throw error.response.messages;
  }
};

const approveMentorRegisService = async (userId, feedback, status) => {
  try {
    const response = await axios.put(`/educationStaff/approveMentorRequest/${userId}`, {
      feedback: feedback,
      status: status
    });
    return response.data;
  } catch (error) {
    throw error.response.messages;
  }
};

export { fetchAllUserService, fetchUserDetailService, fetchSkillService, fetchMentorRegisService,
  fetchMentorRegisDetailService, fetchAdminDashboardService, fetchCourseRegistService,fetchPostService, 
  fetchCourseRegistDetailService, fetchPostDetailService, fetchSkillDetailService, fetchMentorshipRequestService,
  fetchMentorshipRequestByIdService, fetchTransactionService, 
  addStaffService, addSkillService, 
  updateUserStatusService, updateSkillService, approveCourseRequestService, updatePostStatusService,
  approveMentorRegisService,};