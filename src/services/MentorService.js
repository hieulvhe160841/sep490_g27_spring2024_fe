import axios from "./CustomizeAxios";

const fetchContactedMentee = async () => {
  try {
    const response = await axios.get(`/mentor/list-contact-mentee`);
    return response.data;
  } catch (error) {
    console.error("Error fetching contacted mentee:", error);
    return error;
  }
};
const fetchMentorDetail = async (username) => {
  try {
    const response = await axios.get(`/list-mentor/${username}`);
    return response.data;
  } catch (error) {
    console.error("Error fetching mentor:", error);
    throw error;
  }
};
const fetchSuggestPost = async () => {
  try {
    const response = await axios.get(`/mentor/viewPost`);
    return response.data;
  } catch (error) {
    console.error("Error fetching list post:", error);
    return error;
  }
};
const fetchMentorshipRequestMentor = async (status, startDate, endDate) => {
  try {
    const response = await axios.get(`/mentor/filter-support`, {
      params: {
        status,
        startDate,
        endDate,
      },
    });
    return response.data;
  } catch (error) {
    console.error("Error fetching support mentor:", error);
    return error;
  }
};

const acceptOrRejectRequest = async (supportId, status) => {
  try {
    const response = await axios.put(`/mentor/listSupport/${supportId}`, {
      status,
    });
    return response.data;
  } catch (error) {
    console.error("Error accpet or reject Request:", error);
    return error;
  }
};
const mentorProfile = async () => {
  try {
    const response = await axios.get(`/mentor/profile`);
    return response.data;
  } catch (error) {
    console.error("Error fetching mentor profile:", error);
    return error;
  }
};
const updateProfile = async (formData) => {
  try {
    const response = await axios.put(`/mentor/profile/edit`, formData);
    return response;
  } catch (error) {
    console.error("Error update mentor profile:", error);
    return error;
  }
};
const confirmSupportStartService = async (supportId) => {
  try {
    const response = await axios.put(
      `/mentor/support/${supportId}/confirm-start`
    );
    return response.data;
  } catch (error) {
    throw error.response.data;
  }
};

const fetchSupportByIdMentorService = async (id) => {
  try {
    const response = await axios.get(`/mentor/supportDetail/${id}`);
    return response.data;
  } catch (error) {
    console.error("Error fetching support by Id:", error);
    throw error;
  }
};

const fetchPostDetailService = async (postId) => {
  try {
    const response = await axios.get(`/mentor/viewPostDetail/${postId}`);
    return response.data;
  } catch (error) {
    console.error("Error fetching post detail by Id:", error);
    throw error;
  }
};

const acceptPostService = async (postId) => {
  try {
    const response = await axios.post(
      `/mentor/createSupportByPostId/${postId}`
    );
    return response.data;
  } catch (error) {
    throw error.response.data;
  }
};
const registerCourse = async (formData) => {
  try {
    const response = await axios.post(`/mentor/createCourseRequest`, formData);
    return response.data;
  } catch (error) {
    throw error.response.data;
  }
};
const fetchAllCourse = async (username) => {
  try {
    const response = await axios.get(`/course/list/${username}`);
    return response.data;
  } catch (error) {
    throw error.response.data;
  }
};
const fetchCourseById = async (id) => {
  try {
    const response = await axios.get(`/course/${id}`);
    return response.data;
  } catch (error) {
    throw error.response.data;
  }
};
const updateCourse = async (formData, id) => {
  try {
    const response = await axios.put(`/mentor/course-regis/${id}`, formData);
    return response.data;
  } catch (error) {
    throw error.response;
  }
};
const fetchAllRequestCourse = async () => {
  try {
    const response = await axios.get(`/mentor/course-request/list`);
    return response.data;
  } catch (error) {
    throw error.response;
  }
};
const updateCourseInfo = async (formData, id) => {
  try {
    const response = await axios.put(`/mentor/course/${id}`, formData);
    return response.data;
  } catch (error) {
    throw error.response;
  }
};

export {
  fetchContactedMentee,
  fetchPostDetailService,
  fetchMentorDetail,
  fetchSuggestPost,
  fetchSupportByIdMentorService,
  confirmSupportStartService,
  fetchMentorshipRequestMentor,
  acceptOrRejectRequest,
  mentorProfile,
  updateProfile,
  acceptPostService,
  registerCourse,
  fetchAllCourse,
  fetchCourseById,
  updateCourse,
  fetchAllRequestCourse,
  updateCourseInfo,
};
