import axios from "./CustomizeAxios";

const login = async (userName, password) => {
  try {
    return axios.post(`/auth/login`, { userName, password });
    // Return the data property of the response
  } catch (error) {
    console.error("Error login user:", error);
    return error; // Re-throw the error to be caught by the caller
  }
};
const register = async (
  userName,
  password,
  name,
  phone,
  address,
  dayOfBirth,
  gender,
  avatar,
  email
) => {
  try {
    return axios.post(`/auth/signup`, {
      userName,
      password,
      name,
      phone,
      address,
      dayOfBirth,
      gender,
      avatar,
      email,
    });
    // Return the data property of the response
  } catch (error) {
    console.error("Error registers user:", error);
    return error; // Re-throw the error to be caught by the caller
  }
};
const verifyEmail = async (email, otp) => {
  try {
    return axios.put(`/auth/verify?email=${email}&otp=${otp}`);
    // Return the data property of the response
  } catch (error) {
    console.error("Wrong otp:", error);
    return error; // Re-throw the error to be caught by the caller
  }
};
const regenerateOtp = async (email) => {
  try {
    return axios.put(`/auth/regenerate-otp?email=${email}`);
    // Return the data property of the response
  } catch (error) {
    console.error("Wrong Email:", error);
    return error; // Re-throw the error to be caught by the caller
  }
};
const forgotPassword = async (email) => {
  try {
    return axios.put(`/auth/forgot-password`, { email });
    // Return the data property of the response
  } catch (error) {
    console.error(error);
    return error; // Re-throw the error to be caught by the caller
  }
};
const verifyEmailReset = async (email, enterOtp) => {
  try {
    return axios.post(`/auth/identify?email=${email}`, { enterOtp });
    // Return the data property of the response
  } catch (error) {
    console.error(error);
    return error; // Re-throw the error to be caught by the caller
  }
};
const resetPassword = async (email, newPassword) => {
  try {
    return axios.put(`/auth/reset-password?email=${email}`, { newPassword });
    // Return the data property of the response
  } catch (error) {
    console.error(error);
    return error; // Re-throw the error to be caught by the caller
  }
};
const changePassword = async (oldPassword, newPassword, confirmNewPassword) => {
  try {
    return axios.put(`/auth/change-password`, {
      oldPassword,
      newPassword,
      confirmNewPassword,
    });
    // Return the data property of the response
  } catch (error) {
    console.error(error);
    return error; // Re-throw the error to be caught by the caller
  }
};

const fetchTopMentor = async () => {
  try {
    const response = axios.get(`/top-rate-mentor`);
    return response;

  } catch (error) {
    console.error(error);
    return error;
  }
};
const fetchAllMentor = async () => {
  try {
    const response = await axios.get(`/list-mentor`);
    return response.data;
  } catch (error) {
    console.error("Error fetching all mentors:", error);
    return error;
  }
};
const fetchAvailableSKill = async () => {
  try {
    const response = await axios.get(`/available-skill`);
    return response.data;
  } catch (error) {
    console.error("Error fetching all mentors:", error);
    return error;
  }
};
const fetchAllCert = async () => {
  try {
    const response = await axios.get(`/certificate`);
    return response.data;
  } catch (error) {
    console.error("Error fetching all mentors:", error);
    return error;
  }
};
const fetchUserWallet = async () => {
  try {
    const response = await axios.get(`/user-wallet`);
    return response.data;
  } catch (error) {
    console.error("Error fetching user wallet:", error);
    return error;
  }
};
const createPayment = async (denominationId, value, amount) => {
  try {
    const response = await axios.post(`/payment/create_payment`, {
      denominationId,
      value,
      amount,
    });
    return response.data;
  } catch (error) {
    console.error("Error create payment:", error);
    return error;
  }
};
const regenerateOtpReset = async (formData) => {
  try {
    return axios.post(`/auth/regenerate-otp-reset-password`, formData);
  } catch (error) {
    console.error("Wrong Email:", error);
    return error;
  }
};
export {
  login,
  register,
  verifyEmail,
  regenerateOtp,
  forgotPassword,
  verifyEmailReset,
  resetPassword,
  changePassword,
  fetchTopMentor,
  fetchAllMentor,
  fetchAvailableSKill,
  fetchAllCert,
  fetchUserWallet,
  createPayment,
  regenerateOtpReset,
};
