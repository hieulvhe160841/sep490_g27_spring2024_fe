import axios from "./CustomizeAxios";

const fetchCourseDetailService = async (courseId) => {
  try {
    const response = await axios.get(`/mentee/course/${courseId}`);
    return response.data;
  } catch (error) {
    console.error("Error fetching user by username:", error);
    return error;
  }
};

const fetchSuggestMentor = async () => {
  try {
    const response = await axios.get(`/mentee/suggest-mentor`);
    return response.data;
  } catch (error) {
    console.error("Error fetching suggest mentor:", error);
    return error;
  }
};

const fetchSuggestCourse = async (
  mentorName,
  courseName,
  fromPrice,
  toPrice
) => {
  try {
    const response = await axios.get(`/filter-courses`, {
      params: {
        mentorName,
        courseName,
        fromPrice,
        toPrice,
      },
    });
    return response.data;
  } catch (error) {
    console.error("Error fetching list course:", error);
    return error;
  }
};

const fetchBookedMentor = async () => {
  try {
    const response = await axios.get(`/mentee/list-book-mentor`);
    return response.data;
  } catch (error) {
    console.error("Error fetching booked course:", error);
    return error;
  }
};

const fetchMenteeDetail = async (username) => {
  try {
    const response = await axios.get(`/profile/${username}`);
    return response.data;
  } catch (error) {
    console.error("Error fetching mentee detail:", error);
    return error;
  }
};

const submitSkillRegisterAsMentor = async (
  userName,
  experience,
  educationLevel,
  skills,
  costPerHour
) => {
  try {
    const response = await axios.post(`/mentee/register-as-mentor-skill`, {
      userName,
      experience,
      educationLevel,
      skills,
      costPerHour,
    });
    return response;
  } catch (error) {
    console.error("Error add skill:", error);
    return error.response;
  }
};

const submitCertRegisterAsMentor = async (formData) => {
  try {
    const response = await axios.post(
      `/mentee/register-as-mentor-certificate`,
      formData
    );
    return response;
  } catch (error) {
    console.error("Error add cert:", error);
    return error;
  }
};

const fetchListMenteePost = async () => {
  try {
    const response = await axios.get(`/mentee/postListByMentee`);
    return response.data;
  } catch (error) {
    console.error("Error fetching list post:", error);
    return error;
  }
};

const updatePostService = async (id, postData) => {
  try {
    const response = await axios.put(`/mentee/updatePost/${id}`, postData);
    return response.data;
  } catch (error) {
    throw error.response.data;
  }
};

const deletePost = async (id) => {
  try {
    const response = await axios.delete(`/mentee/deletePost/${id}`);
    return response.data;
  } catch (error) {
    console.error("Error delete post:", error);
    return error;
  }
};

const menteeProfile = async () => {
  try {
    const response = await axios.get(`/mentee/profile`);
    return response.data;
  } catch (error) {
    console.error("Error fetching mentee profile:", error);
    return error;
  }
};

const updateProfile = async (formData) => {
  try {
    const response = await axios.put(`/mentee/profile/edit`, formData);
    return response;
  } catch (error) {
    console.error("Error fetching mentee profile:", error);
    return error;
  }
};

const requestMentorDirectlyService = async (username, duration) => {
  try {
    const formData = new FormData();
    formData.append("duration", duration);

    const response = await axios.post(`/mentee/request/${username}`, formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
    return response.data;
  } catch (error) {
    throw error.response.data;
  }
};

const confirmSupportStartService = async (supportId) => {
  try {
    const response = await axios.put(
      `/mentee/support/${supportId}/confirm-start`
    );
    return response.data;
  } catch (error) {
    throw error.response.data;
  }
};

const confirmSupportEndService = async (supportId) => {
  try {
    const response = await axios.put(
      `/mentee/support/${supportId}/confirm-end`
    );
    return response.data;
  } catch (error) {
    throw error.response.data;
  }
};

const addPostService = async (postData) => {
  try {
    const response = await axios.post(`/mentee/createPost`, postData);
    return response.data;
  } catch (error) {
    throw error.response.data;
  }
};

const fetchMentorshipRequestMentee = async (status, startDate, endDate) => {
  try {
    const response = await axios.get(`/mentee/filter-support`, {
      params: {
        status,
        startDate,
        endDate,
      },
    });
    return response.data;
  } catch (error) {
    console.error("Error fetching support mentee:", error);
    return error;
  }
};

const acceptOrRejectRequest = async (supportId, status) => {
  try {
    const response = await axios.put(`/mentee/support/${supportId}`, {
      status,
    });
    return response.data;
  } catch (error) {
    console.error("Error accept or reject Request:", error);
    return error;
  }
};

const ratingSupport = async (supportId, rating, feedback) => {
  try {
    const response = await axios.put(`/mentee/rating?supportId=${supportId}`, {
      rating,
      feedback,
    });
    return response.data;
  } catch (error) {
    console.error("Error rating support:", error);
    return error;
  }
};

const fetchSupportByIdMenteeService = async (id) => {
  try {
    const response = await axios.get(`/mentee/support/${id}`);
    return response.data;
  } catch (error) {
    console.error("Error fetching support by Id:", error);
    throw error;
  }
};

const fetchPostByIdMenteeService = async (id) => {
  try {
    const response = await axios.get(`/mentee/viewPostDetail/${id}`);
    return response.data;
  } catch (error) {
    console.error("Error fetching post by Id:", error);
    throw error;
  }
};

const fetchMenteeTransactionService = async () => {
  try {
    const response = await axios.get(`/mentee/transaction-list`);
    return response.data;
  } catch (error) {
    console.error("Error fetching mentee transaction:", error);
    throw error;
  }
};

const fetchAllDenomination = async () => {
  try {
    const response = await axios.get(`/mentee/denominationTransaction`);
    return response.data;
  } catch (error) {
    console.error("Error fetching list denomination:", error);
    return error;
  }
};

const fetchAllMentorRequest = async () => {
  try {
    const response = await axios.get(`/mentee/regis-as-mentor/list`);
    return response.data;
  } catch (error) {
    console.error("Error fetching list mentor request:", error);
    return error;
  }
};

const confirmChangeRole = async () => {
  try {
    const response = await axios.put(`/mentee/regis-as-mentor/confirm`);
    return response.data;
  } catch (error) {
    console.error("Error confirm:", error);
    return error;
  }
};

const viewDetailMentorRequest = async (requestId) => {
  try {
    const response = await axios.get(`/mentee/viewMentorRequest/${requestId}`);
    return response.data;
  } catch (error) {
    console.error("Error view detail request:", error);
    return error;
  }
};

const fetchAllCoursePurchased = async () => {
  try {
    const response = await axios.get(`/mentee/list-course-purchased`);
    return response.data;
  } catch (error) {
    console.error("Error fetching list courses purchased:", error);
    return error;
  }
};

const purchaseCourse = async (courseId) => {
  try {
    const response = await axios.post(`/mentee/purchase-course/${courseId}`);
    return response.data;
  } catch (error) {
    throw error.response;
  }
};

export {
  fetchCourseDetailService,
  fetchMenteeTransactionService,
  fetchSuggestMentor,
  fetchSuggestCourse,
  fetchBookedMentor,
  fetchMenteeDetail,
  fetchListMenteePost,
  fetchPostByIdMenteeService,
  fetchSupportByIdMenteeService,
  updatePostService,
  deletePost,
  submitSkillRegisterAsMentor,
  submitCertRegisterAsMentor,
  menteeProfile,
  updateProfile,
  requestMentorDirectlyService,
  confirmSupportStartService,
  confirmSupportEndService,
  addPostService,
  fetchMentorshipRequestMentee,
  acceptOrRejectRequest,
  ratingSupport,
  fetchAllDenomination,
  fetchAllMentorRequest,
  confirmChangeRole,
  viewDetailMentorRequest,
  fetchAllCoursePurchased,
  purchaseCourse,
};
