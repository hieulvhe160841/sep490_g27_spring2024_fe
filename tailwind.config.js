/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        sidebarTop: '#0B4B88',
        sidebarBottom: '#0B4B88',
        active: '#003379',
        table: '#96b0c2',
        footer: '#F6F6F6',
        btnBorder: '#507E73',
        btn: '#9BAEC8',
        textHeader: '#003379',
        borderBot: '9F9F9F',
      },
      width:{
        '58': '14.656rem',
      },
      transitionProperty: {
        'width': 'width',
      },
      fontFamily: {
        saira: ['Saira', 'sans-sefir']
      }
    },
  },
  plugins: [],
};
